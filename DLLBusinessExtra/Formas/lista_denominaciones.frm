VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form lista_denominaciones 
   BackColor       =   &H00606060&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   8505
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7665
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   8505
   ScaleWidth      =   7665
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame FrameVentana 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   810
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   7635
      Begin VB.PictureBox PicStellar 
         Appearance      =   0  'Flat
         BackColor       =   &H00404040&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         ForeColor       =   &H80000008&
         Height          =   795
         Left            =   4995
         Picture         =   "lista_denominaciones.frx":0000
         ScaleHeight     =   795
         ScaleWidth      =   2415
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   0
         Width           =   2415
      End
   End
   Begin VB.VScrollBar ScrollGrid 
      Height          =   6495
      LargeChange     =   2
      Left            =   6480
      TabIndex        =   3
      Top             =   840
      Width           =   1155
   End
   Begin VB.CommandButton Salir 
      Appearance      =   0  'Flat
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1200
      Left            =   6480
      MousePointer    =   99  'Custom
      Picture         =   "lista_denominaciones.frx":128F
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   7275
      Width           =   1155
   End
   Begin VB.CommandButton cmdClick 
      Appearance      =   0  'Flat
      Caption         =   "Seleccionar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1140
      Left            =   0
      TabIndex        =   1
      Top             =   7335
      Width           =   6540
   End
   Begin MSFlexGridLib.MSFlexGrid grid_denominaciones 
      Height          =   6435
      Left            =   0
      TabIndex        =   0
      Top             =   840
      Width           =   7560
      _ExtentX        =   13335
      _ExtentY        =   11351
      _Version        =   393216
      Rows            =   1
      Cols            =   3
      FixedRows       =   0
      FixedCols       =   0
      RowHeightMin    =   1100
      BackColor       =   14737632
      BackColorSel    =   12632256
      BackColorBkg    =   4210752
      WordWrap        =   -1  'True
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "lista_denominaciones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public fSalir As Boolean

Private FormaCargada As Boolean
Private Band As Boolean
Private mRowCell As Long, mColCell As Long, Fila As Long

Private EventoProgramado As Boolean

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF12 Then
        Salir_Click
    End If
End Sub

Private Sub FrameVentana_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub cmdClick_Click()
    grid_denominaciones_KeyPress vbKeyReturn
End Sub

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        
        If grid_denominaciones.Rows <= 0 Then
            fSalir = True
            Unload Me
            Exit Sub
        End If
        
        FormaCargada = True
        
        Fila = -1
        grid_denominaciones_EnterCell
        
        AjustarPantalla Me
        
        'Me.Left = Me.Left * POSRatioLeftVentanaPagos '11820 ' Para no tapar los totales y poder ver los totales y el descuento
        'Me.Top = Me.Top * POSRatioTopVentanaPagos '3330
        
        If FormaDePagoQR Then
            
            Dim RsDenominaQR As New ADODB.Recordset
            
            Call Open_Rec(True, RsDenominaQR)
            
            RsDenominaQR.Open _
            "SELECT * FROM MA_DENOMINACIONES " & _
            "WHERE c_CodMoneda = '" & Moneda_Sel & "' " & _
            "AND c_CodDenomina = '" & DenominacionQR & "' " & _
            "AND c_POS = 1 ", _
            Conexion, adOpenStatic, adLockReadOnly, adCmdText
            
            'Set RsDenominaQR.ActiveConnection = Nothing
            
            If Not RsDenominaQR.EOF Then
                
                Denominacion_Sel = RsDenominaQR!c_CodDenomina
                Denominacion_Sel_Des = RsDenominaQR!c_Denominacion
                Denominacion_Real = RsDenominaQR!c_Real
                
            End If
            
            fSalir = False
            
            FormaDePagoQR = True
            
            Unload Me
            
        End If
        
    End If
    
End Sub

Private Sub Form_Load()
    
    On Error GoTo Falla_Lista
    
    FormaCargada = False
    
    Set Conexion = Ent.BDD
    
    cmdClick.Caption = Stellar_Mensaje(5, True)
    
Reintentar:
    
    lista_denominaciones.Top = 3270 '3270
    lista_denominaciones.Left = 4100 '4100
    
    With grid_denominaciones
        
        .ColAlignment(1) = flexAlignLeftCenter
        '.ColWidth(1) = .Width - 250  '4080 '2400
        
        .RowHeightMin = 1200
        
        .ColWidth(0) = 0
        .ColWidth(2) = 0
        
        Call Open_Rec(True, RsDenomina)
        'VE_DECRETO_2602.VE_DECRETO_2602_Activo And VE_DECRETO_2602.VE_DECRETO_2602_AplicaTransaccion
'        If VE_DECRETO_2602.VE_DECRETO_2602_Activo And VE_DECRETO_2602.VE_DECRETO_2602_AplicaTransaccion Then
'
'            VE_DECRETO_2602.VE_DECRETO_2602_TmpObj = "SELECT * FROM MA_DENOMINACIONES " & _
'            "WHERE c_CodMoneda = '" & Moneda_Sel & "' " & _
'            "AND ((c_POS = 1 AND c_Real = 0) OR (c_POS = 1 AND c_Real = 1 AND n_Valor = 0 AND c_Denominacion <> 'Efectivo'))" & _
'            IIf(VE_DECRETO_2602.VE_DECRETO_2602_DenominacionesElectronicas <> "*", _
'            " AND c_CodDenomina IN " & VE_DECRETO_2602.VE_DECRETO_2602_DenominacionesClausulaIN & " ", "") & _
'            " ORDER BY c_Denominacion"
'
'            RsDenomina.Open VE_DECRETO_2602.VE_DECRETO_2602_TmpObj, _
'            Conexion, adOpenStatic, adLockReadOnly, adCmdText
'
'            Set RsDenomina.ActiveConnection = Nothing
'
'            .Rows = 1
'
'            I = 0
'
'            ScrollGrid.Min = 0
'            ScrollGrid.Max = RsDenomina.RecordCount
'            ScrollGrid.Value = 0
'
'        Else
            
            Set RsDenomina = Conexion.Execute( _
            "SELECT * FROM MA_DENOMINACIONES " & _
            "WHERE c_CodMoneda = '" & Moneda_Sel & "' " & _
            "AND c_CodDenomina = 'Efectivo'")
            
            .Row = 0
            
            If RsDenomina.EOF Then
                .Col = 0
                .Text = "Efectivo"
                .Col = 1
                .Text = "Efectivo"
            Else
                .Col = 0
                .Text = "Efectivo"
                .Col = 1
                .Text = RsDenomina!c_Denominacion
            End If
            
            .Col = 2
            '.Text = "S"
            .Text = "1"
            .Rows = .Rows + 1
            
            I = 1
            
            Open_Rec True, RsDenomina
            
            RsDenomina.Open _
            "SELECT * FROM MA_DENOMINACIONES " & _
            "WHERE c_CodMoneda = '" & Moneda_Sel & "' " & _
            "AND ((c_POS = 1 AND c_Real = 0) " & _
            "OR (c_POS = 1 AND c_Real = 1 AND n_Valor = 0 AND c_Denominacion <> 'Efectivo')) " & _
            "ORDER BY c_Denominacion", _
            Conexion, adOpenStatic, adLockReadOnly, adCmdText
            
            Set RsDenomina.ActiveConnection = Nothing
            
            .ScrollTrack = True
            
            .Col = .Cols - 1
            .ColSel = .Col
            
            ScrollGrid.Min = 0
            ScrollGrid.Max = RsDenomina.RecordCount + 1
            ScrollGrid.Value = 0
            
'        End If
        
        If Not RsDenomina.EOF Then
            
            Do Until RsDenomina.EOF
                
                .Row = I
                
                .Col = 0
                .Text = RsDenomina!c_CodDenomina
                
                .Col = 1
                .Text = RsDenomina!c_Denominacion
                
                .Col = 2
                .Text = RsDenomina!c_Real 'IIf(RsDenomina!c_Real, 1, 0)
                
                RsDenomina.MoveNext
                
                I = I + 1
                
                .Rows = .Rows + 1
                
            Loop
            
        End If
        
        .Rows = .Rows - 1
        .Col = 0
        If .Rows > 0 Then .Row = 0
        
        If grid_denominaciones.Rows > 5 Then
            ScrollGrid.Visible = True
            grid_denominaciones.ScrollBars = flexScrollBarVertical
            ReduccionScrollBar = ScrollGrid.Width - 100
            grid_denominaciones.ColWidth(1) = (grid_denominaciones.Width - ReduccionScrollBar)
        Else
            grid_denominaciones.ScrollBars = flexScrollBarNone
            ScrollGrid.Visible = False
            ReduccionScrollBar = 0
            grid_denominaciones.ColWidth(1) = grid_denominaciones.Width
        End If
        
        'SendKeys "{right}"
        oTeclado.Key_Right
        
    End With
    
    If Not DebugVBApp Then
        Call WheelHook(Me.hWnd)
    End If
    
    On Error GoTo 0
    
    Exit Sub
    
Falla_Lista:
    
    Set Conexion = Ent.BDD
    'Call SetNet(False)
    GoTo Reintentar
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not DebugVBApp Then
        Call WheelUnHook(Me.hWnd)
    End If
End Sub

Private Sub grid_denominaciones_DblClick()
    grid_denominaciones_KeyPress vbKeyReturn
End Sub

Private Sub grid_denominaciones_EnterCell()
    
    On Error Resume Next
    
    If grid_denominaciones.Rows = 1 Or Not FormaCargada Then
        Exit Sub
    End If
    
    'If Not Band Then
        
        If Fila <> grid_denominaciones.Row Then
            
            If Fila > 0 Then
                grid_denominaciones.RowHeight(Fila) = grid_denominaciones.RowHeightMin
            End If
            
            grid_denominaciones.RowHeight(grid_denominaciones.Row) = 1600
            Fila = grid_denominaciones.Row
            
            EventoProgramado = True
            ScrollGrid.Value = Fila + 1
            EventoProgramado = False
            
        End If
        
        grid_denominaciones.RowSel = grid_denominaciones.Row
        
        If grid_denominaciones.Col <> 1 Then Band = True
        
        grid_denominaciones.Col = grid_denominaciones.Cols - 1
        
        grid_denominaciones.ColSel = 0
        
        If PuedeObtenerFoco(grid_denominaciones) Then grid_denominaciones.SetFocus
        
    'Else
        'Band = False
    'End If
    
End Sub

Private Sub grid_denominaciones_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    grid_denominaciones_EnterCell
End Sub

Private Sub grid_denominaciones_SelChange()
    grid_denominaciones_EnterCell
End Sub

Private Sub grid_denominaciones_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case vbKeyA To vbKeyZ
            
            mFilaActual = grid_denominaciones.Row
            
            For mFilaProx = 0 To grid_denominaciones.Rows - 1
                
                If UCase(Mid(grid_denominaciones.TextMatrix(mFilaProx, 1), 1, 1)) = UCase(Chr(KeyCode)) Then
                    
                    mFilaAnterior = mFilaActual
                    mFilaActual = mFilaProx
                    
                    grid_denominaciones.Row = mFilaAnterior
                    grid_denominaciones.Col = 1
                    
                    'grid_denominaciones.CellBackColor = grid_denominaciones.BackColor
                    
                    grid_denominaciones.Row = mFilaActual
                    grid_denominaciones.Col = 1
                    
                    'grid_denominaciones.CellBackColor = grid_denominaciones.BackColorSel
                    
                    grid_denominaciones.TopRow = grid_denominaciones.Row
                    grid_denominaciones.SetFocus
                    
                    Exit Sub
                    
                End If
                
            Next mFilaProx
            
            grid_denominaciones.Row = mFilaActual
            grid_denominaciones.SetFocus
            
        Case vbKeyF12
            
            Salir_Click
            
        Case Else
            
            Call grid_denominaciones_KeyPress(KeyCode)
                     
    End Select
    
End Sub

Private Sub grid_denominaciones_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        
        FormaCargada = False
        
        With grid_denominaciones
            
            If .Rows > 0 Then
                
                .Row = .RowSel
                
                .Col = 1
                Denominacion_Sel_Des = .Text
                
                .Col = 0
                Denominacion_Sel = .Text
                
                .Col = 2
                Denominacion_Real = .Text = "1"
                
            End If
            
            fSalir = False
            Unload Me
            
        End With
        
    End If
    
End Sub

Private Sub Salir_Click()
    fSalir = True
    Unload Me
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If EventoProgramado Then Exit Sub
    If ScrollGrid.Value <> grid_denominaciones.Row Then
        grid_denominaciones.TopRow = ScrollGrid.Value
        grid_denominaciones.Row = ScrollGrid.Value
        If PuedeObtenerFoco(grid_denominaciones) Then grid_denominaciones.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    'Debug.Print ScrollGrid.value
    ScrollGrid_Change
End Sub

Public Sub MouseWheel(ByVal MouseKeys As Long, ByVal Rotation As Long, ByVal Xpos As Long, ByVal Ypos As Long)
    
    Dim Ctl As Control
    Dim bHandled As Boolean
    Dim bOver As Boolean
    
    For Each Ctl In Controls
      ' Is the mouse over the control
      On Error Resume Next
      bOver = (Ctl.Visible And IsOver(Ctl.hWnd, Xpos, Ypos))
      On Error GoTo 0
      
      If bOver Then
        ' If so, respond accordingly
        bHandled = True
        Select Case True
        
          Case TypeOf Ctl Is MSFlexGrid
            FlexGridScroll Ctl, MouseKeys, Rotation, Xpos, Ypos
            
          Case TypeOf Ctl Is PictureBox
            PictureBoxZoom Ctl, MouseKeys, Rotation, Xpos, Ypos
            
          Case TypeOf Ctl Is ListBox, TypeOf Ctl Is TextBox, TypeOf Ctl Is ComboBox
            ' These controls already handle the mousewheel themselves, so allow them to:
            If Ctl.Enabled Then Ctl.SetFocus
            
          Case Else
            bHandled = False
    
        End Select
        If bHandled Then Exit Sub
      End If
      bOver = False
    Next Ctl
    
    ' Scroll was not handled by any controls, so treat as a general message send to the form
    'Me.Caption = "Form Scroll " & IIf(Rotation < 0, "Down", "Up")
    
End Sub
