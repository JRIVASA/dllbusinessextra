VERSION 5.00
Begin VB.Form f_Vuelto 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7605
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   12510
   ControlBox      =   0   'False
   ForeColor       =   &H00000000&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7605
   ScaleWidth      =   12510
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame FrameContinue 
      BackColor       =   &H00E7E8E8&
      Height          =   6495
      Left            =   7920
      TabIndex        =   22
      Top             =   960
      Width           =   4440
      Begin VB.Label lbl_ValorVuelto2 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H00E0E0E0&
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Impact"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00008000&
         Height          =   420
         Left            =   720
         TabIndex        =   25
         Top             =   1920
         Visible         =   0   'False
         Width           =   3312
      End
      Begin VB.Label lbl_ValorVuelto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "999.999.999,00"
         BeginProperty Font 
            Name            =   "Impact"
            Size            =   27.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   660
         Left            =   240
         TabIndex        =   24
         Top             =   1080
         Width           =   3792
      End
      Begin VB.Label lbl_Vuelto 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Monto Vuelto:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   492
         Left            =   1440
         TabIndex        =   23
         Top             =   360
         Width           =   2508
      End
   End
   Begin VB.Frame FrameSummary 
      BackColor       =   &H00E7E8E8&
      Height          =   6450
      Left            =   120
      TabIndex        =   2
      Top             =   960
      Width           =   7692
      Begin VB.TextBox txt_montoapagar 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   555
         Left            =   2040
         TabIndex        =   29
         Text            =   "0.00"
         Top             =   3840
         Width           =   4215
      End
      Begin VB.CommandButton Cancelar 
         Caption         =   "Ca&ncelar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1320
         Left            =   3840
         MaskColor       =   &H8000000F&
         Picture         =   "f_Vuelto.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   28
         Top             =   4920
         Width           =   1365
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   552
         Left            =   3480
         Picture         =   "f_Vuelto.frx":2EC2
         Style           =   1  'Graphical
         TabIndex        =   27
         Top             =   3120
         Visible         =   0   'False
         Width           =   552
      End
      Begin VB.CommandButton cmd_Aceptar 
         Appearance      =   0  'Flat
         Caption         =   "Aceptar"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1320
         Left            =   2160
         MouseIcon       =   "f_Vuelto.frx":4C44
         MousePointer    =   99  'Custom
         Picture         =   "f_Vuelto.frx":4F4E
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   4920
         Width           =   1470
      End
      Begin VB.TextBox txt_BancoCod 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   16.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   516
         Left            =   2160
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   20
         Top             =   2640
         Visible         =   0   'False
         Width           =   1020
      End
      Begin VB.TextBox txt_DenominacionDesc 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   16.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   516
         Left            =   4320
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   19
         TabStop         =   0   'False
         Top             =   2040
         Visible         =   0   'False
         Width           =   2700
      End
      Begin VB.TextBox txt_Numero 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   16.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   516
         Left            =   4320
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   17
         Top             =   3120
         Visible         =   0   'False
         Width           =   2700
      End
      Begin VB.TextBox txt_BancoDesc 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   16.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   516
         Left            =   600
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   16
         Top             =   3120
         Visible         =   0   'False
         Width           =   2700
      End
      Begin VB.TextBox txt_Monto 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   516
         Left            =   1320
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   13
         Top             =   4920
         Visible         =   0   'False
         Width           =   1020
      End
      Begin VB.TextBox txt_Cantidad 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   516
         Left            =   360
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   12
         Top             =   4920
         Visible         =   0   'False
         Width           =   900
      End
      Begin VB.TextBox txt_MonedaFactor 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   516
         Left            =   4440
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   10
         Text            =   "999.999.999,00"
         Top             =   240
         Visible         =   0   'False
         Width           =   2100
      End
      Begin VB.TextBox txt_MonedaDesc 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   16.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   516
         Left            =   4320
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   960
         Width           =   2700
      End
      Begin VB.CommandButton CmdBuscarDenominacion 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   552
         Left            =   3480
         Picture         =   "f_Vuelto.frx":7E10
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   2040
         Visible         =   0   'False
         Width           =   552
      End
      Begin VB.TextBox txt_DenominacionCod 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   16.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   516
         Left            =   600
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   2040
         Visible         =   0   'False
         Width           =   2700
      End
      Begin VB.TextBox txt_MonedaCodigo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   16.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   516
         Left            =   600
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   960
         Width           =   2700
      End
      Begin VB.CommandButton CmdBuscarMoneda 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   552
         Left            =   3504
         Picture         =   "f_Vuelto.frx":9B92
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   960
         Width           =   552
      End
      Begin VB.Timer TimerFlash 
         Enabled         =   0   'False
         Interval        =   1000
         Left            =   0
         Top             =   120
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Monto:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   435
         Left            =   720
         TabIndex        =   30
         Top             =   3855
         Width           =   1230
      End
      Begin VB.Image bteclado 
         Height          =   720
         Left            =   6360
         MouseIcon       =   "f_Vuelto.frx":B914
         MousePointer    =   99  'Custom
         Picture         =   "f_Vuelto.frx":BC1E
         Top             =   3840
         Width           =   720
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Banco:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   480
         Left            =   720
         TabIndex        =   21
         Top             =   2640
         Visible         =   0   'False
         Width           =   2268
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Numero:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   480
         Left            =   4560
         TabIndex        =   18
         Top             =   2640
         Visible         =   0   'False
         Width           =   2148
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Monto:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   288
         Left            =   1560
         TabIndex        =   15
         Top             =   4560
         Visible         =   0   'False
         Width           =   744
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Cantidad:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   288
         Left            =   240
         TabIndex        =   14
         Top             =   4560
         Visible         =   0   'False
         Width           =   1032
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Factor:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   13.5
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   336
         Left            =   3480
         TabIndex        =   11
         Top             =   240
         Visible         =   0   'False
         Width           =   852
      End
      Begin VB.Label lbl_Denominacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Denominacion:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   480
         Left            =   720
         TabIndex        =   8
         Top             =   1560
         Visible         =   0   'False
         Width           =   2865
      End
      Begin VB.Label lbl_Moneda 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Moneda:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   18
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   480
         Left            =   720
         TabIndex        =   3
         Top             =   480
         Width           =   2268
      End
   End
   Begin VB.Frame FrameTitulo 
      BackColor       =   &H009E5300&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   810
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   13032
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   11880
         Picture         =   "f_Vuelto.frx":11C10
         Top             =   120
         Width           =   480
      End
      Begin VB.Image Logo 
         Height          =   810
         Left            =   9000
         Picture         =   "f_Vuelto.frx":13992
         Stretch         =   -1  'True
         Top             =   0
         Width           =   2595
      End
      Begin VB.Label LbWebsite 
         BackColor       =   &H009E5300&
         Caption         =   "Detalle de Forma de Pago Vuelto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   252
         Left            =   360
         TabIndex        =   1
         Top             =   240
         Width           =   8772
      End
   End
End
Attribute VB_Name = "f_Vuelto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Salir As Boolean
Public TipoVuelto As String

Public InfoMoneda       As New cls_Monedas
Public InfoDenominacion As New cls_Denominaciones

Private TotalVuelto As Double

Private Denominacion_SoloEnLinea    As Boolean
Private ProcesarVE                  As Boolean

Private FormaCargada As Boolean

Private CodBancoVuelto  As String
Private DescBancoVuelto As String
Private ReferenciaVuelto As String

Private Sub Aceptar_Click()
    'Me.Hide
End Sub

Private Sub bteclado_Click()
    TecladoAvanzado CampoT
End Sub

Private Sub Cancelar_Click()
    
    If VerificacionElectronica_Maneja Then
        If gVerificacionElectro.VerificacionEnproceso Then
            Exit Sub
        End If
    End If
    
    Salir = True
    
    If Me.Visible Then Me.Hide
    
End Sub

Private Sub LimpiarVariables()
    
    CodBancoVuelto = Empty
    DescBancoVuelto = Empty
    ReferenciaVuelto = Empty
    
End Sub

Private Sub Cmd_Aceptar_Click()
    
    If Not cmd_Aceptar.Enabled Then
        Exit Sub
    End If
    
    If Not IsNumeric(txt_montoapagar) Then
        
        txt_montoapagar.Text = RoundUp(CDec(lbl_ValorVuelto2.Tag), InfoMoneda.DecMoneda)
        
        SafeFocus txt_montoapagar
        
        Exit Sub
        
    End If
    
    If VerificacionElectronica_Maneja Then 'esta es si esta activado la verificacion electronica
        If gVerificacionElectro.VerificacionEnproceso Then
            Exit Sub
        End If
    End If
    
    LimpiarVariables
    
    Dim CodigoIsoMoneda As String
    
    If Trim(txt_DenominacionCod.Text) <> Empty _
    And Trim(txt_DenominacionDesc.Text) <> Empty _
    And Trim(txt_MonedaCodigo.Text) <> Empty Then
        
        InfoMoneda.BuscarMonedas , txt_MonedaCodigo.Text
        InfoDenominacion.BuscarDenominacion , _
        Trim(txt_MonedaCodigo.Text), Trim(txt_DenominacionCod.Text), , False
        
        If InfoDenominacion.ManejaVerificacionElectronica Then
            
            If UCase(TipoVuelto) = UCase("Merchant") _
            Or UCase(TipoVuelto) = UCase("Cupon_BWLCS") _
            Or UCase(TipoVuelto) = UCase("Stellar_Wallet") Then
                ProcesarVE = True 'Mensaje(False, FrmAppLink.StellarMensaje(16028))
            Else
                ProcesarVE = Mensaje(False, StellarMensaje(16028)) 'Inserte la tarjeta en el terminal de pago... �Listo?
            End If
            
            If ProcesarVE Then
                
                If CDec(txt_montoapagar) = RoundUp(CDec(lbl_ValorVuelto2.Tag), InfoMoneda.DecMoneda) Then
                    ' Permitir realizar el pago completo
                Else
                    
                    If Not PermitirVueltoVerifElecMontoParcial Then
                        
                        Mensaje True, StellarMensaje(4137) '"Por medidas de seguridad, la transacci�n con validaci�n de pago electr�nica " & _
                        "debe realizarse por el monto total restante, no se admiten montos parciales."
                        
                        txt_montoapagar.Text = RoundUp(CDec(lbl_ValorVuelto2.Tag), InfoMoneda.DecMoneda)
                        
                        SafeFocus txt_montoapagar
                        
                        Exit Sub
                        
                    Else
                        
                        Dim mAutorizadorCod As String, mAutorizadorNom As String, mAutorizadorNivel As Integer
                        
                        Aceptado = FrmAppLink.GetNivelUsuario >= PermitirVueltoVerifElecMontoParcial_Nivel
                        
                        If Not Aceptado Then
                            
                            FrmAutorizacion.mNivel = PermitirVueltoVerifElecMontoParcial_Nivel
                            FrmAutorizacion.Titulo = StellarMensaje(3138) ' "Introduzca las credenciales " & _
                            "de un usuario que ..."
                            
                            Set FrmAutorizacion.mConexion = Ent.BDD
                            
                            FrmAutorizacion.Show vbModal
                            
                            If FrmAutorizacion.mAceptada Then
                                
                                mAutorizadorCod = FrmAutorizacion.mCodigoUsuario
                                mAutorizadorNom = FrmAutorizacion.mUsuario
                                mAutorizadorNivel = FrmAutorizacion.mNivelSupervisor
                                
                                Set FrmAutorizacion = Nothing
                                
                                ' Continuar
                                
                                Aceptado = True
                                
                            Else
                                
                                Set FrmAutorizacion = Nothing
                                Mensaje True, StellarMensaje(16050)
                                Exit Sub
                                
                            End If
                            
                            If Aceptado Then
                                
                                If mAutorizadorNivel >= PermitirVueltoVerifElecMontoParcial_Nivel Then
                                    
                                    Aceptado = True
                                    
                                    mTmpInfoAdicional = "Vuelto con Verificaci�n Electr�nica " & _
                                    "[" & txt_MonedaDesc & "][" & txt_DenominacionDesc & "] " & _
                                    "por Monto Parcial: " & IIf(lbl_ValorVuelto2.Visible, _
                                    FormatoDecimalesDinamicos(txt_montoapagar) & " " & InfoMoneda.SimMoneda & _
                                    " (" & FormatNumber(txt_Monto, POS.DecMonedaPref) & " " & gSimMonedaPred & ") ", _
                                    FormatNumber(txt_Monto, POS.DecMonedaPref) & " " & gSimMonedaPred) & _
                                    ", Restante Factura: " & FormatNumber(mRestanteRaw, gDecMonedaPred) & _
                                    " " & gSimMonedaPred
                                    
                                    'Call GrabarAutorizacion("0000000051", mTmpInfoAdicional)
                                    
                                    InsertarAuditoria 481, "Vuelto Parcial con Verificaci�n Electr�nica", _
                                    mTmpInfoAdicional, "f_Vuelto", "Vuelto", "N/A", Ent.BDD
                                    
                                Else
                                    
                                    Aceptado = False
                                    Mensaje True, StellarMensaje(11) '"No posee el Nivel Necesario para realizar esta acci�n."
                                    
                                End If
                                
                            Else
                                Aceptado = False
                            End If
                            
                        End If
                        
                        If Not Aceptado Then
                            
                            SafeFocus txt_montoapagar
                            
                            Exit Sub
                            
                        End If
                        
                    End If
                    
                End If
                
                SafePropAssign gVerificacionElectro, _
                "Prop_CodigoBancoTemp_DenominacionSinConsorcioPrincipal", vbNullString
                
                SafePropAssign gVerificacionElectro, "Transaccion_CodLocalidad_Prop", FrmAppLink.GetCodLocalidadSistema
                SafePropAssign gVerificacionElectro, "Transaccion_DesLocalidad_Prop", FrmAppLink.GetNomLocalidadSistema
                SafePropAssign gVerificacionElectro, "Transaccion_Documento_Prop", TmpNumeroFactura
                SafePropAssign gVerificacionElectro, "Transaccion_DocAfec_Prop", vbNullString
                SafePropAssign gVerificacionElectro, "Transaccion_CodCaja_Prop", vbNullString ' N/A
                SafePropAssign gVerificacionElectro, "Transaccion_CodCajero_Prop", FrmAppLink.GetCodUsuario
                SafePropAssign gVerificacionElectro, "Transaccion_DesCajero_Prop", FrmAppLink.GetNomUsuario
                
                SafePropAssign gVerificacionElectro, "Transaccion_CodMonPred_Prop", FrmAppLink.CodMonedaPref
                SafePropAssign gVerificacionElectro, "Transaccion_DesMonPred_Prop", FrmAppLink.DesMonedaPref
                SafePropAssign gVerificacionElectro, "Transaccion_DecMonPred_Prop", FrmAppLink.DecMonedaPref
                SafePropAssign gVerificacionElectro, "Transaccion_FacMonPred_Prop", FrmAppLink.SimMonedaPref
                SafePropAssign gVerificacionElectro, "Transaccion_SimMonPred_Prop", FrmAppLink.FacMonedaPref
                
                SafePropAssign gVerificacionElectro, "Transaccion_CodCliente_Prop", TmpCodigoCliente
                SafePropAssign gVerificacionElectro, "Transaccion_NombreCliente_Prop", TmpNombreCliente
                SafePropAssign gVerificacionElectro, "Transaccion_RifCliente_Prop", TmpRifCliente
                SafePropAssign gVerificacionElectro, "Transaccion_NumTelfCliente_Prop", TmpTelefonoCliente
                SafePropAssign gVerificacionElectro, "Transaccion_EmailCliente_Prop", vbNullString ' N/A
                SafePropAssign gVerificacionElectro, "Transaccion_DirCliente_Prop", TmpDireccionCliente
                
                If Not CodigoMonedaTipo Is Nothing Then
                    
                    If CodigoMonedaTipo.Exists(UCase(txt_MonedaCodigo.Text)) Then
                        CodigoIsoMoneda = CodigoMonedaTipo.Item(UCase(txt_MonedaCodigo.Text))
                    Else
                        CodigoIsoMoneda = "VES"
                    End If
                    
                End If
                
                If VerificacionElectronica_IDClienteAuto Then
                    
                    Dim TmpRegex As RegExp
                    Set TmpRegex = New RegExp
                    
                    MerchantSUMA_TmpVar = FrmAppLink.RifCliente
                    
                    If MerchantSUMA_IDClienteAlfaNumerico Then
                        
                        TmpRegex.Global = True
                        TmpRegex.Pattern = "[^0-9VEJGP]"
                        
                        MerchantSUMA_IDCliente = TmpRegex.Replace(UCase(MerchantSUMA_TmpVar), vbNullString)
                        
                    Else
                        
                        TmpRegex.Global = True
                        TmpRegex.Pattern = "[^0-9]"
                        
                        MerchantSUMA_IDCliente = TmpRegex.Replace(UCase(MerchantSUMA_TmpVar), vbNullString)
                        
                    End If
                    
                    SafePropAssign gVerificacionElectro, "IDCliente_Entrada_Prop", _
                    IIf(Trim(MerchantSUMA_IDCliente) <> Empty, MerchantSUMA_IDCliente, Empty)
                    
                End If
                
                'If UCase(TipoVuelto) = UCase("Cupon_BWLCS") Then
                
                Select Case UCase(TipoVuelto)
                    
                    Case UCase("Cupon_BWLCS")
                        
                        'Dim mAliasLocalidad As String
                        
                        'If CuponDevolucion_AliasLocalidad = "*" Then
                            'mAliasLocalidad = FrmAppLink.GetCodLocalidadSistema
                        'Else
                            'mAliasLocalidad = CuponDevolucion_AliasLocalidad
                        'End If
                        
                        'Dim RandomSerial As Long: RandomSerial = gRutinas.RandomInt(100000, 999999)
                        
                        'Dim SerialCupon As String: SerialCupon = Left(mAliasLocalidad & _
                        TmpNumeroFactura & RandomSerial, 20)
                        
                        'Dim mFechaValidezInicio As Date, mFechaValidezFin As Date, mInstante As Date
                        
                        'mInstante = Now
                        
                        'If SVal(FechaValidezCupon_Inicio_Fac) > 0 Then
                            'mFechaValidezInicio = DateSerial(Year(mInstante), Month(mInstante), Day(mInstante))
                            'mFechaValidezInicio = DateAdd("d", SVal(FechaValidezCupon_Inicio_Fac), mFechaValidezInicio)
                        'Else
                            'mFechaValidezInicio = DateSerial(Year(mInstante), Month(mInstante), Day(mInstante))
                        'End If
                        
                        'If SVal(FechaValidezCupon_Fin_Fac) > 0 Then
                            'mFechaValidezFin = DateSerial(Year(mInstante), Month(mInstante), Day(mInstante))
                            'mFechaValidezFin = DateAdd("d", SVal(FechaValidezCupon_Fin_Fac), mFechaValidezFin)
                        'Else
                            'mFechaValidezFin = DateSerial(9999, 1, 1)
                        'End If
                        
                        ''SafePropAssign gDllVerificacion, _
                        "Prop_CodigoBancoTemp_DenominacionSinConsorcioPrincipal", CodigoBancoCuponCompra
                        
                        'Resp = gDllVerificacion.BWL_CS_AgregarCupon.BWL_CS_AgregarCupon(CodigoCuponCompra, SerialCupon, _
                        IDCuenta_BWL_CS, FrmAppLink.GetCodLocalidadSistema, VbNullString, mFechaValidezInicio, mFechaValidezFin, _
                        txt_MonedaCodigo.Text, CodigoIsoMoneda, _
                        SVal(IIf(lbl_ValorVuelto2.Visible, lbl_ValorVuelto2.Tag, Vuelt)), _
                        SVal(txt_MonedaFactor), True, CuponVueltoMostrarRespuesta)
                        
                        'If Resp Then
                            
                            'If CuponVueltoImprimir = 1 Then
                                
                                'SafePropAssign gDllVerificacion, "Transaccion_Documento_Prop", TmpNumeroFactura
                                'SafePropAssign gDllVerificacion, "Transaccion_CodLocalidad_Prop", FrmAppLink.GetCodLocalidadSistema
                                'SafePropAssign gDllVerificacion, "Transaccion_DesLocalidad_Prop", FrmAppLink.GetNomLocalidadSistema
                                'SafePropAssign gDllVerificacion, "Transaccion_CodCajero_Prop", LcCodUsuario
                                'SafePropAssign gDllVerificacion, "Transaccion_DesCajero_Prop", LcDescrUser
                                'SafePropAssign gDllVerificacion, "TarjetaNombre", TmpNombreCliente
                                'SafePropAssign gDllVerificacion, "TarjetaCedula", TmpRifCliente
                                
                                'SafePropAssign gDllVerificacion, "MonedaTmp_Prop", InfoMoneda
                                
                                'mImpreso = gDllVerificacion.BWL_CS_ImprimirCuponCompra( _
                                "Vuelto", CodigoCuponCompra, SerialCupon, _
                                IDCuenta_BWL_CS, FrmAppLink.GetCodLocalidadSistema, vbNullString, mFechaValidezInicio, mFechaValidezFin, _
                                txt_MonedaCodigo.Text, CodigoIsoMoneda, _
                                SVal(IIf(lbl_ValorVuelto2.Visible, lbl_ValorVuelto2.Tag, Vuelt)), _
                                SVal(txt_MonedaFactor), "*VUELTO_POS*", mAliasLocalidad, _
                                InfoMoneda.DecMoneda, InfoMoneda.SimMoneda, _
                                gCodMonedaPred, gDecMonedaPred, gSimMonedaPred, True, True, False)
                                
                            'End If
                            
                        'End If
                        
                'ElseIf UCase(TipoVuelto) = UCase("Merchant") Then
                        'Vuelto_Tipeado = FormatNumber(CDbl(Vuelt), 2)
                        'Resp = gDllVerificacion.cambio(CDbl(Vuelto_Tipeado), CodigoIsoMoneda)
                        
                    Case UCase("Merchant")
                        
                        mMontoElec = RoundUp(txt_Monto, gDecMonedaPred)
                        mCantidadElec = CDbl(txt_montoapagar)
                        mFactorElec = (mMontoElec / mCantidadElec)
                        
                        'Vuelto_Tipeado = FormatNumber(CDbl(Vuelt), 2)
                        Vuelto_Tipeado = RoundUp(mMontoElec, 2)
                        Resp = gVerificacionElectro.Cambio(CDbl(Vuelto_Tipeado), CodigoIsoMoneda)
                        
                'ElseIf UCase(TipoVuelto) = UCase("Stellar_Wallet") Then
                    
                    Case UCase("Stellar_Wallet")
                        
                        mMontoElec = VueltRaw 'RoundUp(txt_Monto, Pos.DecMonedaPref)
                        mCantidadElec = CDec(txt_Cantidad)
                        mFactorElec = (mMontoElec / mCantidadElec)
                        
                        Resp = gVerificacionElectro.StellarWallet_Recarga _
                        (f_TipoVuelto.DocumentoIDSW, _
                         f_TipoVuelto.TipoMonederoSW, _
                         CDbl(mCantidadElec), CDbl(mFactorElec))
                         
                End Select
                
                If Resp Then
                    
'                    If VerificacionElectronica_AutoBanco Then
'
'                        Dim mBanco As String, mIdentificadorBancoConsorcio As String, mInfoBanco As Variant
'
'                        mIdentificadorBancoConsorcio = Trim(UCase(QuitarComillasSimples(SafeProp( _
'                        gVerificacionElectro, "CodigoBancoAutorizador", Empty))))
'
'                        'Select Case PosRetail.VerificacionElectronica_ConsorcioAprobacion
'
'                            'Case 7, 8
'
'                                ' Actualizado. No limitar por consorcio. Cualquier consorcio que pueda devolver _
'                                ' algo en el property anterior ser�a compatible con esta funcionalidad.
'
'                                If VerificacionElectronica_ListaAsociacionAutoBanco.Exists(mIdentificadorBancoConsorcio) Then
'                                    mBanco = VerificacionElectronica_ListaAsociacionAutoBanco(mIdentificadorBancoConsorcio)
'                                Else
'                                    mBanco = VerificacionElectronica_CodBancoFallback
'                                End If
'
'                                mInfoBanco = Split(BuscarValorBD("Datos", _
'                                "SELECT (c_Codigo + '|' + c_Descripcio) AS Datos FROM MA_BANCOS " & _
'                                "WHERE c_Codigo = '" & mBanco & "'", "|", Ent.BDD), "|")
'
'                        'End Select
'
'                        If Len(mInfoBanco(0)) > 0 Then
'                            CodBancoVuelto = mInfoBanco(0)
'                            DescBancoVuelto = mInfoBanco(1)
'                        Else
'
'                            mInfoBanco = Split(BuscarValorBD("Datos", _
'                            "SELECT (c_Codigo + '|' + c_Descripcio) AS Datos FROM MA_BANCOS " & _
'                            "WHERE c_Codigo = '" & VerificacionElectronica_CodBancoFallback & "'", _
'                            "|", Ent.BDD), "|")
'
'                            If Len(mInfoBanco(0)) > 0 Then
'                                CodBancoVuelto = mInfoBanco(0)
'                                DescBancoVuelto = mInfoBanco(1)
'                            End If
'
'                        End If
'
'                        ReferenciaVuelto = gVerificacionElectro.TarjetaNumeroReferencia
'
'                    End If
                    
                    Call GrabarPagoVueltoTemporal
                    
                    f_TipoVuelto.Salir = False
                    Salir = False
                    
                    Call f_TipoVuelto.LlenarDatosGridVueltos(f_TipoVuelto.GridVuelto)
                    
                    Me.Hide
                    
                Else
                    
                    txt_DenominacionCod.Text = Empty
                    txt_DenominacionDesc.Text = Empty
                    
                    f_TipoVuelto.Salir = True
                    
                    Salir = True
                    
                    Me.Hide
                    
                End If
                
            Else 'el else de procesar_VE
                
                Call GrabarPagoVueltoTemporal
                
                f_TipoVuelto.Salir = False
                Salir = False
                
                Call f_TipoVuelto.LlenarDatosGridVueltos(f_TipoVuelto.GridVuelto)
                
                Me.Hide
                
            End If
            
        Else 'no maneja verificacion electronica
            
            Call GrabarPagoVueltoTemporal
            
            f_TipoVuelto.Salir = False
            Salir = False
            
            Call f_TipoVuelto.LlenarDatosGridVueltos(f_TipoVuelto.GridVuelto)
            
            Me.Hide
            
        End If
        
    Else
        
        Mensaje True, StellarMensaje(4138) '"Debe seleccionar la Denominacion."
        
    End If
    
End Sub

Private Sub CmdBuscarDenominacion_Click()
    
    If Not CmdBuscarDenominacion.Enabled Then
        Exit Sub
    End If
    
    If VerificacionElectronica_Maneja Then
        If gVerificacionElectro.VerificacionEnproceso Then
            Exit Sub
        End If
    End If
    
    Moneda_Sel = txt_MonedaCodigo.Text
    
    lista_denominaciones.Show vbModal
    
    If lista_denominaciones.fSalir Then
        'Form_KeyDown vbKeyF12, 0
        Exit Sub
    End If
    
    Dim CodigoIsoMoneda As String
    
    If Denominacion_Sel <> Empty Then
        
        txt_DenominacionCod.Text = Denominacion_Sel
        
        txt_DenominacionDesc.Text = Denominacion_Sel_Des
        
        InfoDenominacion.BuscarDenominacion , Moneda_Sel, Denominacion_Sel, , False
        
    End If
    
End Sub

Private Sub CmdBuscarMoneda_Click()
    
    If Not CmdBuscarMoneda.Enabled Then
        Exit Sub
    End If
    
    'lista_monedas.fVieneFormaPago = True
    lista_monedas.Show vbModal
    
    If lista_monedas.fSalir Then
        Exit Sub
    End If
    
    If Moneda_Sel <> Empty Then
        
        txt_MonedaCodigo.Text = Moneda_Sel
        txt_MonedaDesc.Text = Moneda_Sel_Des
        
        InfoMoneda.BuscarMonedas , Moneda_Sel
        
        txt_MonedaFactor.Text = FormatNumber(InfoMoneda.FacMoneda, InfoMoneda.DecMoneda)
        
        Lc_Cambio = RoundDownFive(CDec(VueltRaw) - CDec(MontoTotalVueltoPagado), 8)
        
        'If gFactorMonedaAlterna1 > 0 And MontoTotalVueltoPagado = 0 _
        And UCase(gCodMonedaAlterna1) = UCase(Clasemonedas.CodMoneda) Then
            'lbl_ValorVuelto2.Tag = FrmPagos.lblVueltoDivisas.Tag
        'Else
            lbl_ValorVuelto2.Tag = FormatNumber( _
            CDbl(lbl_ValorVuelto.Caption) / InfoMoneda.FacMoneda, InfoMoneda.DecMoneda) ' Aqui guardamos el valor de vuelto en la _
            moneda del pago al cliente con sus respectiva cantidad de decimales.
        'End If
        
        lbl_ValorVuelto2.Caption = InfoMoneda.SimMoneda & " " & _
        FormatNumber(lbl_ValorVuelto2.Tag, InfoMoneda.DecMoneda) ' Esto es para mostrar, a nivel de presentaci�n.
        
        txt_Cantidad.Text = FormatNumber(lbl_ValorVuelto2.Tag, InfoMoneda.DecMoneda)
        txt_montoapagar.Text = txt_Cantidad.Text
        
        txt_Monto.Text = Lc_Cambio
        
        InfoDenominacion.BuscarDenominacion , InfoMoneda.CodMoneda, "Efectivo", , False
        
        txt_DenominacionCod.Text = InfoDenominacion.CodDenomina
        txt_DenominacionDesc.Text = InfoDenominacion.DesDenomina
        
        If UCase(Moneda_Sel) <> UCase(gCodMonedaPred) Then
            lbl_ValorVuelto2.Visible = True
        Else
            lbl_ValorVuelto2.Visible = False
        End If
        
    End If
    
End Sub

Private Sub Exit_Click()
    
    If VerificacionElectronica_Maneja Then
        If gVerificacionElectro.VerificacionEnproceso Then
            Exit Sub
        End If
    End If
    
    Salir = True
    
    If Me.Visible Then
        Me.Hide
    End If
    
End Sub

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        
        FormaCargada = True
        
        'CodBancoVuelto = Empty
        'DescBancoVuelto = Empty
        'ReferenciaVuelto = Empty
        
        Select Case UCase(TipoVuelto)
            Case UCase("Efectivo"), UCase("Merchant"), UCase("Cupon_BWLCS"), UCase("Stellar_Wallet")
                SafeFocus cmd_Aceptar
            Case UCase("Otros")
                SafeFocus CmdBuscarDenominacion
        End Select
        
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyEscape, vbKeyF12
            Exit_Click
        Case vbKeyF2
            CmdBuscarMoneda_Click
        Case vbKeyF3
            CmdBuscarDenominacion_Click
        Case vbKeyF4
            Cmd_Aceptar_Click
    End Select
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    Select Case VBKEYCODE
        Case vbKey1
            Form_KeyDown vbKeyF2, 0
        Case vbKey2
            Form_KeyDown vbKeyF3, 0
        Case vbKey3
            Form_KeyDown vbKeyF4, 0
        Case vbKey4
            Form_KeyDown vbKeyF12, 0
    End Select
End Sub

Private Sub Form_Load()
    
    FormaCargada = False
    
    FrmAppLink.SetFormaDLL = Me
    
    'CodBancoVuelto = Empty
    'DescBancoVuelto = Empty
    'ReferenciaVuelto = Empty
    
    LimpiarVariables
    
    InfoMoneda.InicializarConexiones Ent.BDD, Ent.POS
    InfoDenominacion.InicializarConexiones Ent.BDD, Ent.POS
    
    Select Case UCase(TipoVuelto)
        
        Case UCase("Merchant"), UCase("Cupon_BWLCS"), UCase("Stellar_Wallet")
            'Ningun ajuste en particular.
            
        Case UCase("EFECTIVO")
            
            txt_DenominacionCod.Visible = False
            txt_DenominacionDesc.Visible = False
            CmdBuscarDenominacion.Visible = False
            
        Case UCase("OTROS")
            
            txt_DenominacionCod.Visible = True
            txt_DenominacionDesc.Visible = True
            CmdBuscarDenominacion.Visible = True
            
    End Select
    
    If UCase(Trim(txt_MonedaCodigo.Text)) <> UCase(gCodMonedaPred) Then
        lbl_ValorVuelto2.Visible = True
    Else
        lbl_ValorVuelto2.Visible = False
    End If
    
End Sub

Private Function GrabarPagoVueltoTemporal() As Boolean
    
    On Error GoTo Error
    
    'Ent.BDD.Execute _
    "DELETE FROM TMP_VENTAS_VUELTO " & _
    "WHERE CodUsuario = '" & FixTSQL(LcCodUsuario) & "' " & _
    "AND LAN_DeviceName = '" & FixTSQL(PCName) & "' ", Rows
    
    ''Entorno.VAD20Local.Execute "TRUNCATE TABLE TMP_DETALLEPAGO_VUELTO" ' POR SI ACASO.
    
    ' Ya no eliminamos, ahora tiene capacidad de multivuelto.
    
    Dim mRsTmpPagosVuelto As Recordset
    Set mRsTmpPagosVuelto = New Recordset
    
    With mRsTmpPagosVuelto
        
        .Open "SELECT * FROM TMP_VENTAS_VUELTO WHERE 1 = 2", _
        Ent.BDD, adOpenDynamic, adLockOptimistic
        
        .AddNew
            
            ' Antes
            
            '!N_FACTOR = SDec(txt_MonedaFactor)
            '!n_Cantidad = SDec(txt_Cantidad)
            '!n_Monto = SDec(txt_monto)
            
            ' Ahora por Manejo de Microvuelto y asegurar integridad.
            !LAN_DeviceName = FixTSQL(PCName)
            !LAN_IP = LocalIP
            !CodUsuario = FixTSQL(LcCodUsuario)
            
            ' A diferencia del POS / FOOD, no usamos VueltRaw en vez de Vuelt, ya que
            ' El business si permite pagar una parte de la factura y lo demas es dejarlo a credito.
            ' Por lo cual el manejo es un poco distinto, ya que lo que el usuario paga en todo
            ' momento es un monto redondeado a los decimales que maneje la moneda de la factura,
            ' Por lo tanto nunca esta pagando el monto exactamente crudo.
            ' En resumen aqui usamos Vuelt en vez de VueltRaw. En los otros sistemas si se debe usar VueltRaw
            
            '!n_Monto = CDec(Vuelt) 'VueltRaw 'SDec(txt_monto)
            '!n_Cantidad = CDec(txt_Cantidad.Text)
            
            'TasaImplicitaVuelto = CDec(Vuelt) / CDec(txt_Cantidad.Text)
            
            ' Ahora por Manejo de Microvuelto y asegurar integridad.
            
            !n_Monto = CDec(txt_Monto.Text) 'Vuelt 'SDec(txt_monto)
            !n_Cantidad = SDec(txt_Cantidad)
            
            If UCase(txt_MonedaCodigo.Text) = UCase(gCodMonedaPred) Then
                ' Para que a la moneda predeterminada no se le genere tasa implicita, siempre deber�a ser 1.
                !n_Cantidad = !n_Monto
                TasaImplicitaVuelto = CDec(1)
            Else
                !n_Cantidad = CDec(txt_Cantidad)
                TasaImplicitaVuelto = CDec(txt_Monto.Text) / CDec(txt_Cantidad)
            End If
            
            !n_Factor = TasaImplicitaVuelto
            
            !c_Factura = "[STELLAR_REPLACE]"
            !CodigoCliente = TmpCodigoCliente 'f_Punto.Codigo.Caption
            !RifCliente = TmpRifCliente 'f_Punto.cliente.Tag
            !c_CodMoneda = txt_MonedaCodigo.Text
            !c_CodDenominacion = txt_DenominacionCod.Text
            
            !c_Numero = IIf(ProcesarVE, "1", "0")
            
            If Trim(ReferenciaVuelto) <> Empty Then
                !c_Numero_Grid = ReferenciaVuelto
            Else
                !c_Numero_Grid = txt_Numero.Text
            End If
            
            !d_FechaHora = FechaBD(Now, FBD_FULL, True) 'Date 'TmpPago("d_FechaHora")
            
            !c_CodBanco = txt_BancoCod.Text
            !c_Banco = txt_BancoDesc.Text
            
            If Trim(CodBancoVuelto) <> Empty Then
                !c_CodBanco = CodBancoVuelto
            End If
            
            If Trim(DescBancoVuelto) <> Empty Then
                !c_Banco = DescBancoVuelto
            End If
            
            ' Los siguientes campos no contemplarlos ni grabarles nada a menos que ya por fin se _
            les vaya a dar uso. Por defecto que se graben con su respectivo Default a nivel de _
            Tabla SQL
            
            'If ExisteCampoTabla("InfoXMLConsorcio", mRsTmpPagosVuelto) Then
                '!InfoXMLConsorcio = "" 'vbNullString
            'End If
            
            'If ExisteCampoTabla("c_ID_Lote_Automatico", mRsTmpPagosVuelto) Then
                '!c_ID_Lote_Automatico = "" 'vbNullString
            'End If
            
        .Update
        
        .Close
        
    End With
    
    GrabarPagoVueltoTemporal = True
    
    Exit Function
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    '"Error al grabar datos temporales de pago. Informaci�n Adicional:"
    'Mensaje True, StellarMensaje(354) & _
    vbNewLine & mErrorDesc
    
    'MostrarErrorComun mErrorNumber, mErrorDesc, mErrorSource, StellarMensaje(757) & GetLines(2)
    
End Function

Private Sub txt_montoapagar_Change()
    
    Dim mMontoIngresado As Double
    Dim mVariacion As Double
    
    If FormaCargada Then
        
        If CheckCad(txt_montoapagar, InfoMoneda.DecMoneda) Then '2) Then
            
            mMontoIngresado = CDec(txt_montoapagar.Text) * CDec(txt_MonedaFactor.Text)
            
            mRestanteRaw = (VueltRaw - CDec(MontoTotalVueltoPagado))
            
            'If (CDec(MontoTotalVueltoPagado) + CDec(mMontoIngresado)) > VueltRaw Then ' se estaba usando vueltraw
                'mVariacion = (CDec(MontoTotalVueltoPagado) + CDec(mMontoIngresado)) - CDec(Vuelt) 'VueltRaw
                'Mensaje True, "El monto ingresado es mayor al que intenta cancelar por: " & mVariacion & " " & BuscarSimboloMoneda(Entorno.VAD10Local, BuscarCodigoMonedaPredeterminada(Entorno.VAD10Local))
                'cmd_aceptar.Enabled = False
            'Else
                'cmd_aceptar.Enabled = True
                'txt_Cantidad.Text = txt_montoapagar.Text
                'txt_monto.Text = mMontoIngresado
            'End If
            
            ' Aqui se cambia a validar y mostrar mensaje en la moneda que se este pagando. No tiene sentido
            ' hacer todo eso en base a la moneda predeterminada. Ademas, cuando sea pago completo, pagar el
            ' restante Raw para que se genere la tasa implicita. Si es pago parcial, entonces se usa la tasa
            ' normal de la moneda del pago. Esto es necesario tanto para facilidad de uso como para calcular
            ' corrrectamente.
            
            If CDec(txt_montoapagar) > RoundUp(CDec(lbl_ValorVuelto2.Tag), InfoMoneda.DecMoneda) Then
                
                mVariacion = (CDec(txt_montoapagar) - _
                CDec(RoundUp(CDec(lbl_ValorVuelto2.Tag), InfoMoneda.DecMoneda)))
                
                'Mensaje True, "El monto ingresado excede el monto restante a cancelar, " & _
                "por: " & FormatoDecimalesDinamicos(mVariacion) & " " & InfoMoneda.SimMoneda
                Mensaje True, Replace(StellarMensaje(4139), "$(Param1)", _
                FormatoDecimalesDinamicos(mVariacion) & " " & InfoMoneda.SimMoneda)
                
                cmd_Aceptar.Enabled = False
                
            Else
                
                If CDec(txt_montoapagar) = RoundUp(CDec(lbl_ValorVuelto2.Tag), InfoMoneda.DecMoneda) Then
                    cmd_Aceptar.Enabled = True
                    txt_Cantidad.Text = txt_montoapagar.Text
                    txt_Monto = mRestanteRaw
                Else
                    cmd_Aceptar.Enabled = True
                    txt_Cantidad.Text = txt_montoapagar.Text
                    txt_Monto = mMontoIngresado
                End If
                
            End If
            
        End If
        
    End If
    
End Sub

Private Sub txt_montoapagar_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn And cmd_Aceptar.Enabled Then
        Cmd_Aceptar_Click
    End If
    
End Sub
