VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form frm_VerificarFactura 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7710
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   11820
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7710
   ScaleWidth      =   11820
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame2 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame2"
      Height          =   4815
      Left            =   120
      TabIndex        =   10
      Top             =   2760
      Width           =   11535
      Begin MSFlexGridLib.MSFlexGrid GridDetalle 
         Height          =   4575
         Left            =   120
         TabIndex        =   4
         Top             =   120
         Width           =   11295
         _ExtentX        =   19923
         _ExtentY        =   8070
         _Version        =   393216
         Cols            =   4
         FixedCols       =   0
         ForeColor       =   5790296
         BackColorFixed  =   11426560
         ForeColorFixed  =   16448250
         BackColorBkg    =   15198440
         HighLight       =   2
         GridLinesFixed  =   0
         SelectionMode   =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   2055
      Left            =   120
      TabIndex        =   8
      Top             =   600
      Width           =   11535
      Begin VB.CommandButton btn_Mostrar 
         Caption         =   "Mostrar"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Index           =   1
         Left            =   7550
         Picture         =   "frm_VerificarFactura.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   1
         ToolTipText     =   "Presenta el reporte en pantalla"
         Top             =   840
         Width           =   1200
      End
      Begin VB.CommandButton btn_aceptar 
         Caption         =   "Verificar"
         Height          =   1095
         Left            =   8880
         Picture         =   "frm_VerificarFactura.frx":1D82
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   840
         Width           =   1200
      End
      Begin VB.CommandButton btn_salir 
         Caption         =   "Salir"
         Height          =   1095
         Left            =   10200
         Picture         =   "frm_VerificarFactura.frx":3B04
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   840
         Width           =   1200
      End
      Begin VB.TextBox txt_Numero_Factura 
         Appearance      =   0  'Flat
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   24
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   720
         Left            =   240
         TabIndex        =   0
         Top             =   1080
         Width           =   5490
      End
      Begin VB.Label lbl_Numero_Factura 
         BackStyle       =   0  'Transparent
         Caption         =   "Ingrese un n�mero de Factura:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   735
         Left            =   240
         TabIndex        =   9
         Top             =   240
         Width           =   5775
      End
   End
   Begin VB.Frame Frame5 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   12120
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Verificaci�n de Factura"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   75
         Width           =   5295
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   9840
         TabIndex        =   6
         Top             =   75
         Width           =   1815
      End
   End
End
Attribute VB_Name = "frm_VerificarFactura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private mEtapa As String

Private Sub btn_aceptar_Click()
    
    If Trim(txt_Numero_Factura) <> Empty Then
        
        If InsertarChequeo(Trim(txt_Numero_Factura.Text)) Then
            Mensaje True, "Factura verificada."
            txt_Numero_Factura.Text = Empty
            GridDetalle.Rows = 1
            GridDetalle.Rows = 2
            btn_aceptar.Enabled = False
        Else
            Mensaje False, "Factura no se pudo verificar. " & _
            IIf(mEtapa <> Empty, vbNewLine & "Razon: " & mEtapa, "")
        End If
        
    Else
        
        Mensaje True, "Debe indicar un n�mero de factura."
        
    End If
    
    mEtapa = Empty
    
End Sub

Private Function BuscarFactura(pFactura As String) As Boolean
    
    Dim mSQL As String
    Dim RsBusqueda As New ADODB.Recordset
    
    BuscarFactura = False
    
    If ExisteFactura(pFactura) Then
        
        mSQL = "SELECT * FROM MA_DOCUMENTOS_VERIFICADOS " & _
        "WHERE cu_DocumentoStellar = '" & pFactura & "' " & _
        "AND cu_Localidad = '" & FrmAppLink.GetCodLocalidadSistema & "' " & _
        "AND cu_DocumentoTipo = 'VEN' "
        
        RsBusqueda.Open mSQL, Ent.POS, adOpenStatic, adLockReadOnly
        
        If RsBusqueda.RecordCount > 0 Then
            
            'trajo algo ya esta registrada la factura
            mEtapa = "La factura fue verificada previamente. "
            RsBusqueda.Close
            Exit Function
            
        Else
            
            'si no trae nada entonces la factura no esta chequeada y se muestra en la grilla
            'se hace metodo para la busqueda de los renglones
            mSQL = "SELECT * FROM ( " & _
            "SELECT TR.Codigo, P.c_Descri, SUM(TR.Cantidad) AS Cantidad, MIN(TR.ID) AS OrdenEntrada " & _
            "FROM MA_TRANSACCION AS TR, MA_PRODUCTOS P " & _
            "WHERE TR.Cod_Principal = P.c_Codigo " & _
            "AND TR.c_Numero  = '" & pFactura & "' " & _
            "AND TR.c_Concepto = 'VEN' " & _
            "GROUP BY TR.Codigo, P.c_Descri) " & _
            "TB WHERE Cantidad > 0 ORDER BY OrdenEntrada "
            
            If RsBusqueda.State = 1 Then
                RsBusqueda.Close
            End If
            
            RsBusqueda.Open mSQL, Ent.POS, adOpenStatic, adLockReadOnly
            'aca ya tenemos los datos del detalle y los introducimos a la grilla
            
            If RsBusqueda.RecordCount > 0 Then
                
                GridDetalle.Rows = 1 ' Limpiar
                GridDetalle.Rows = 2
                
                Linea = GridDetalle.Rows - 1
                
                While Not RsBusqueda.EOF
                    
                    GridDetalle.Rows = Linea + 1
                    GridDetalle.Row = Linea
                    
                    GridDetalle.TextMatrix(Linea, 0) = CStr(Linea)
                    GridDetalle.TextMatrix(Linea, 1) = RsBusqueda!Codigo
                    GridDetalle.TextMatrix(Linea, 2) = RsBusqueda!c_Descri
                    GridDetalle.TextMatrix(Linea, 3) = FormatoDecimalesDinamicos(RsBusqueda!Cantidad)
                    
                    Linea = Linea + 1
                    
                    RsBusqueda.MoveNext
                    
                Wend
                
                BuscarFactura = True
                
            Else
                mEtapa = "Error al buscar el detalle de la factura "
            End If
            
        End If
        
    Else
        BuscarFactura = False
    End If
    
End Function

Private Function ExisteFactura(pFactura As String) As Boolean
    
    Dim mSQL As String
    Dim RsBusqueda As New ADODB.Recordset
    
    mSQL = "SELECT * FROM MA_PAGOS " & _
    "WHERE c_Numero = '" & pFactura & "' " & _
    "AND c_Concepto = 'VEN' "
    
    RsBusqueda.Open mSQL, Ent.POS, adOpenDynamic, adLockBatchOptimistic
    
    If RsBusqueda.RecordCount > 0 Then
        'la factura existe
        ExisteFactura = True
    Else
        ExisteFactura = False
        mEtapa = "Factura no Existe en el Sistema."
    End If
    
End Function

Private Sub CargarGridDetalle()
    
    GridDetalle.Cols = 4
    GridDetalle.RowHeightMin = 720
    'GridDetalle.Rows = 2
    GridDetalle.AllowUserResizing = flexResizeColumns
    GridDetalle.ScrollTrack = True
    GridDetalle.WordWrap = True
    
    GridDetalle.ColWidth(0) = 960
    GridDetalle.FixedAlignment(0) = flexAlignCenterCenter
    GridDetalle.ColAlignment(0) = flexAlignCenterCenter
    GridDetalle.ColWidth(1) = 2055
    GridDetalle.FixedAlignment(1) = flexAlignCenterCenter
    GridDetalle.ColAlignment(1) = flexAlignCenterCenter
    GridDetalle.ColWidth(2) = 6420
    GridDetalle.FixedAlignment(2) = flexAlignCenterCenter
    GridDetalle.ColAlignment(2) = flexAlignLeftCenter
    GridDetalle.ColWidth(3) = 1500
    GridDetalle.FixedAlignment(3) = flexAlignCenterCenter
    GridDetalle.ColAlignment(3) = flexAlignCenterCenter
    
    GridDetalle.TextMatrix(0, 0) = "L�nea"
    GridDetalle.TextMatrix(0, 1) = "C�digo"
    GridDetalle.TextMatrix(0, 2) = "Descripci�n"
    GridDetalle.TextMatrix(0, 3) = "Cantidad"
    
End Sub

Private Function InsertarChequeo(pFactura As String) As Boolean
    
    Dim RsToInsert As New ADODB.Recordset
    
    Dim mSQL As String
    Dim ActiveTrans As Boolean
    
    InsertarChequeo = False
    
    mSQL = "SELECT * FROM MA_DOCUMENTOS_VERIFICADOS WHERE 1 = 2 "
    
    RsToInsert.Open mSQL, Ent.POS, adOpenDynamic, adLockBatchOptimistic
    
    RsToInsert.AddNew
    
    RsToInsert!cu_DocumentoStellar = pFactura
    RsToInsert!cu_DocumentoTipo = "VEN"
    RsToInsert!cu_Localidad = FrmAppLink.GetCodLocalidadSistema
    
    RsToInsert.UpdateBatch
    
    InsertarChequeo = True
    
    RsToInsert.Close
    
    Exit Function
    
    On Error GoTo Error
    
Error:
    
    InsertarChequeo = False
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & "). (InsertarChequeo)"
    
End Function

Private Sub btn_Mostrar_Click(Index As Integer)
    
    If Trim(txt_Numero_Factura) <> Empty Then
        
        If BuscarFactura(Trim(txt_Numero_Factura.Text)) Then
            btn_aceptar.Enabled = True
        Else
            
            Mensaje True, "Factura " & txt_Numero_Factura.Text & " no se pudo verificar. " & _
            IIf(mEtapa <> Empty, vbNewLine & "Razon: " & mEtapa, Empty)
            
            txt_Numero_Factura.Text = Empty
            
        End If
        
    Else
        
        Mensaje True, "Debe indicar un n�mero de factura."
        
    End If
    
    mEtapa = Empty
    
End Sub

Private Sub btn_salir_Click()
    Unload Me
End Sub

Private Sub Form_Load()
    
    mEtapa = Empty
    
    btn_aceptar.Enabled = False
    
    Call CargarGridDetalle
    
End Sub

Private Sub txt_Numero_Factura_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        btn_Mostrar_Click (1)
    End If
End Sub
