VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Object = "{50BF2256-701F-46F2-8ADB-2202CE6922BC}#1.0#0"; "KlexGrid.ocx"
Begin VB.Form Ficha_Gestion_NCF_RD 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   11850
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   Icon            =   "Ficha_Gestion_NCF_RD.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11850
   ScaleWidth      =   15330
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   600
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   15360
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   600
         Left            =   14640
         Picture         =   "Ficha_Gestion_NCF_RD.frx":628A
         Stretch         =   -1  'True
         Top             =   0
         Width           =   600
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Ficha de Gestion de Comprobantes Fiscales"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   240
         TabIndex        =   10
         Top             =   105
         Width           =   9255
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   11835
         TabIndex        =   9
         Top             =   105
         Width           =   2535
      End
   End
   Begin VB.Frame Frame_Detalle 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Formato de Comisiones"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   8775
      Left            =   240
      TabIndex        =   5
      Top             =   2760
      Width           =   14775
      Begin VB.Frame FrameEdit 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   735
         Left            =   12960
         TabIndex        =   21
         Top             =   7920
         Visible         =   0   'False
         Width           =   720
         Begin VB.Image CmdEdit 
            Height          =   645
            Left            =   0
            Picture         =   "Ficha_Gestion_NCF_RD.frx":800C
            Stretch         =   -1  'True
            Top             =   0
            Width           =   645
         End
      End
      Begin VB.Frame FrameView 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   735
         Left            =   13800
         TabIndex        =   20
         Top             =   7920
         Visible         =   0   'False
         Width           =   720
         Begin VB.Image CmdView 
            Height          =   645
            Left            =   0
            Picture         =   "Ficha_Gestion_NCF_RD.frx":84E3
            Stretch         =   -1  'True
            Top             =   0
            Width           =   645
         End
      End
      Begin VB.CommandButton CmdNuevaFila 
         BackColor       =   &H8000000D&
         Caption         =   "+"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   3720
         TabIndex        =   2
         Top             =   7995
         Width           =   615
      End
      Begin VB.ComboBox Cmb 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   465
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   6
         TabStop         =   0   'False
         Top             =   1080
         Visible         =   0   'False
         Width           =   1335
      End
      Begin Grid.KlexGrid KGrid 
         Height          =   7095
         Left            =   240
         TabIndex        =   1
         Top             =   600
         Width           =   14295
         _ExtentX        =   25215
         _ExtentY        =   12515
         Editable        =   -1  'True
         BackColorAlternate=   16448250
         GridLinesFixed  =   2
         AllowBigSelection=   0   'False
         AllowUserResizing=   1
         Appearance      =   0
         BackColor       =   16448250
         BackColorBkg    =   16448250
         BackColorFixed  =   5000268
         BackColorSel    =   15724527
         BorderStyle     =   0
         Cols            =   6
         FixedCols       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   4210752
         ForeColorFixed  =   16777215
         ForeColorSel    =   4210752
         GridColor       =   13421772
         MouseIcon       =   "Ficha_Gestion_NCF_RD.frx":8A30
         RowHeightMin    =   500
         ScrollBars      =   2
         ScrollTrack     =   -1  'True
         WordWrap        =   -1  'True
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   3720
         X2              =   14500
         Y1              =   240
         Y2              =   240
      End
      Begin VB.Label lblListaComprobantes 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Configuraci�n de Comprobantes"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   375
         Left            =   240
         TabIndex        =   11
         Top             =   120
         Width           =   3375
      End
      Begin VB.Label lblNuevaFila 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "A�adir Nueva Fila:    (Alt + N)"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   240
         TabIndex        =   7
         Top             =   8040
         Width           =   3240
      End
   End
   Begin VB.Frame frame_Datos 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Datos de Descripcion"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   1695
      Left            =   240
      TabIndex        =   3
      Top             =   840
      Width           =   14775
      Begin VB.CommandButton Cancelar 
         Caption         =   "&Cancelar"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   945
         Left            =   8400
         Picture         =   "Ficha_Gestion_NCF_RD.frx":8A4C
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   600
         Width           =   1095
      End
      Begin VB.CommandButton Aceptar 
         Appearance      =   0  'Flat
         Caption         =   "Guardar"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   945
         Left            =   7080
         Picture         =   "Ficha_Gestion_NCF_RD.frx":A7CE
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   600
         Width           =   1095
      End
      Begin VB.ComboBox CmbPC 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   405
         ItemData        =   "Ficha_Gestion_NCF_RD.frx":C550
         Left            =   1920
         List            =   "Ficha_Gestion_NCF_RD.frx":C557
         Style           =   2  'Dropdown List
         TabIndex        =   16
         Top             =   1080
         Width           =   4860
      End
      Begin VB.OptionButton OptADMBiz 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Facturaci�n Administrativa"
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   240
         TabIndex        =   15
         Top             =   480
         Width           =   2670
      End
      Begin VB.OptionButton OptPOSFood 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "POS Food"
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   5145
         TabIndex        =   14
         Top             =   480
         Width           =   1665
      End
      Begin VB.OptionButton OptPOSRetail 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "POS Retail"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3390
         TabIndex        =   13
         Top             =   480
         Value           =   -1  'True
         Width           =   1590
      End
      Begin VB.TextBox txtFormatoCorrelativo 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   435
         Left            =   10080
         Locked          =   -1  'True
         MaxLength       =   50
         TabIndex        =   0
         Text            =   "00000000"
         Top             =   1005
         Width           =   2040
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "ID Caja / PC : "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   240
         TabIndex        =   17
         Top             =   1080
         Width           =   1560
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00AE5B00&
         X1              =   1200
         X2              =   14500
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label lblPerfil 
         BackColor       =   &H00E7E8E8&
         Caption         =   "General"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   375
         Left            =   240
         TabIndex        =   12
         Top             =   0
         Width           =   1095
      End
      Begin VB.Label lblFormato 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Formato Num. :  "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   10080
         TabIndex        =   4
         Top             =   480
         Width           =   1980
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   12960
      Top             =   1440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Gestion_NCF_RD.frx":C562
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Gestion_NCF_RD.frx":D23C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Gestion_NCF_RD.frx":EFCE
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Gestion_NCF_RD.frx":10D60
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Gestion_NCF_RD.frx":12AF2
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Gestion_NCF_RD.frx":14884
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Gestion_NCF_RD.frx":16616
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Ficha_Gestion_NCF_RD.frx":183A8
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "Ficha_Gestion_NCF_RD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Const ColorODC As Long = &HC0C0C0
Private Const ColorDefault As Long = &HFAFAFA
Private Const ColorModif As Long = &HFFC0C0
Private Const ColorAlertaComprobantes As Long = &HC0C0FF

Private AplicarSecuenciaPorCaja     As Boolean

Private mTipoSistema As String

Dim CmbCol As Integer

Dim blnModificar As Boolean
Dim FormaCargada As Boolean

Dim mCajas As Dictionary
Dim mCaja As Dictionary

Private Enum GridCols
    
    ColLinea
    
    CodCaja
    TipoCF
    NombreCF
    
    ActSerie
    ActRangoIni
    ActRangoFin
    ActCorrelativo
    ActFEmision
    RawDateFE
    ActFVcto
    RawDateFV
    
    ModPendiente
    ModAceptada
    ModVisualizar
    ColModificar
    
    ModSerie
    ModRangoIni
    ModRangoFin
    ModCorrelativo
    ModFEmision
    ModFVcto
    
    TipoSys
    FmtCorrelativo
    
    DBRowID ' ID en la BD. Nos permitir� saber si la linea es nueva o ya ha sido grabada.
    
    [ColCount]
    
End Enum

'Private Sub Aceptar_Click() ' GRABAR VIEJO ADMITIA 1 SOLA SERIE CON PRIMARY KEY POR CAJA / TIPO CF
'
'    On Error GoTo Error
'
'    With KGrid
'
'    ' Iniciar Validaciones de informaci�n del Grid.
'
'    For I = 1 To .Rows - 1
'        .TextMatrix(.Row, ColLinea) = I
'    Next I
'
'    For I = 1 To .Rows - 1
'        If OptPOSRetail Then
'            If CmbPC.ListIndex >= 0 _
'            And Trim(.TextMatrix(I, TipoCF)) = Empty _
'            And Trim(.TextMatrix(I, NombreCF)) = Empty Then
'                KGrid.RemoveItem CLng(I)
'            Else
'
'                Dim mMsgFila: mMsgFila = "Fila " & I & ". "
'
'                If CmbPC.ListIndex < 0 Then
'                    Mensaje True, "Debe seleccionar la caja."
'                    SafeFocus Cmb
'                    Exit Sub
'                End If
'
'                If .TextMatrix(I, ActSerie) = Empty Then
'                    Mensaje True, mMsgFila & "Debe indicar la Serie"
'                    Exit Sub
'                End If
'
'                If Not IsNumeric(.TextMatrix(I, ActRangoIni)) Then
'                    Mensaje True, mMsgFila & "Debe indicar el Inicio de Rango de Secuencia. " & _
'                    "Asegurese de que sea un valor num�rico."
'                    Exit Sub
'                End If
'
'                If Not IsNumeric(.TextMatrix(I, ActRangoFin)) Then
'                    Mensaje True, mMsgFila & "Debe indicar el Fin de Rango de Secuencia. " & _
'                    "Asegurese de que sea un valor num�rico."
'                    Exit Sub
'                End If
'
'                If Not IsNumeric(.TextMatrix(I, ActCorrelativo)) Then
'                    Mensaje True, mMsgFila & "Debe indicar el Pr�ximo Valor de Secuencia. " & _
'                    "Asegurese de que sea un valor num�rico y que este dentro del rango."
'                    Exit Sub
'                End If
'
'                If Not IsNumeric(.TextMatrix(I, RawDateFE)) Then
'                    Mensaje True, mMsgFila & "Debe indicar la fecha de emisi�n del tipo " & _
'                    "de comprobante fiscal."
'                    Exit Sub
'                End If
'
'                If Not IsNumeric(.TextMatrix(I, RawDateFV)) Then
'                    Mensaje True, mMsgFila & "Debe indicar la fecha de vencimiento del tipo " & _
'                    "de comprobante fiscal."
'                    Exit Sub
'                End If
'
'                If CDbl(.TextMatrix(I, ActRangoIni)) <= 0 Then
'                    Mensaje True, mMsgFila & "Debe indicar un valor positivo para el " & _
'                    "Inicio de Rango de Secuencia. "
'                    Exit Sub
'                End If
'
'                If CDbl(.TextMatrix(I, ActRangoFin)) <= 0 Then
'                    Mensaje True, mMsgFila & "Debe indicar un valor positivo para el " & _
'                    "Fin de Rango de Secuencia. "
'                    Exit Sub
'                End If
'
'                If CDbl(.TextMatrix(I, ActCorrelativo)) <= 0 Then
'                    Mensaje True, mMsgFila & "Debe indicar un valor positivo para el " & _
'                    "Proximo Valor de Secuencia. "
'                    Exit Sub
'                End If
'
'                If CDbl(.TextMatrix(I, ActRangoFin)) < CDbl(.TextMatrix(I, ActRangoIni)) Then
'                    Mensaje True, mMsgFila & "El valor final del rango debe ser superior al inicial. "
'                    Exit Sub
'                End If
'
'                If CDbl(.TextMatrix(I, ActCorrelativo)) < CDbl(.TextMatrix(I, ActRangoIni)) _
'                Or CDbl(.TextMatrix(I, ActCorrelativo)) > CDbl(.TextMatrix(I, ActRangoFin)) Then
'                    Mensaje True, mMsgFila & "El valor de secuencia indicado est� fuera del rango. "
'                    Exit Sub
'                End If
'
'                If CDbl(.TextMatrix(I, RawDateFV)) < CDbl(.TextMatrix(I, RawDateFE)) Then
'                    Mensaje True, mMsgFila & "La Fecha de Vencimiento no puede ser menor a la de Emisi�n. "
'                    Exit Sub
'                End If
'
'            End If
'        End If
'    Next
'
'    ' Finalizar Validaciones de informaci�n del Grid.
'
'    Dim SQL As String, mWherePC As String
'
'    If mTipoSistema = "POSRT" Then
'        Set mCaja = mCajas.Item(CStr(CmbPC.ItemData(CmbPC.ListIndex)))
'        mWherePC = "AND ID_CAJA_PC = '" & mCaja("Cod") & "' "
'    End If
'
'    Dim ActiveTrans As Boolean
'
'    Ent.BDD.BeginTrans
'    ActiveTrans = True
'
'    SQL = "SELECT * FROM MA_GESTION_NCF " & _
'    "WHERE TipoSistema = '" & mTipoSistema & "' " & _
'    mWherePC & _
'    "ORDER BY ID_CAJA_PC, TipoComprobante, ID "
'
'    Dim mRs As New ADODB.Recordset: Set mRs = New ADODB.Recordset
'    mRs.CursorLocation = adUseClient
'
'    mRs.Open SQL, Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
'    mRs.ActiveConnection = Nothing
'
'    Dim PreDelete As Boolean, Grabar As Boolean
'
'    For I = 1 To .Rows - 1
'
'        If .RowData(I) = 1 Then ' Registros Modificaci�n.
'
'            mRs.Filter = "TipoComprobante = '" & .TextMatrix(I, TipoCF) & "' "
'
'            If Not mRs.EOF Then
'
'Modificar:
'
'                If AplicarSecuenciaPorCaja Then
'
'                    mRs!bModificacionPendiente = True
'                    mRs!bModificacionAceptada = False
'
'                    mRs!SerieModificacion = .TextMatrix(I, ActSerie)
'                    mRs!InicioRangoModificacion = CDbl(.TextMatrix(I, ActRangoIni))
'                    mRs!FinRangoModificacion = CDbl(.TextMatrix(I, ActRangoFin))
'                    mRs!CorrelativoModificacion = CDbl(.TextMatrix(I, ActCorrelativo))
'                    mRs!FechaEmisionModificacion = FechaBD(CDate(CDbl(.TextMatrix(I, RawDateFE))), FBD_FULL, True) 'FechaBD(.TextMatrix(I, ActFEmision), , True)
'                    mRs!FechaVencimientoModificacion = FechaBD(CDate(CDbl(.TextMatrix(I, RawDateFV))), FBD_FULL, True) 'FechaBD(.TextMatrix(I, ActFVcto), , True)
'                    mRs!Cod_Usuario_Upd = FrmAppLink.GetCodUsuario
'                    mRs!Fecha_Upd = FechaBD(Now, FBD_FULL, True)
'                    mRs!FormatoCorrelativo = txtFormatoCorrelativo.Text
'
'                Else
'
'                    mRs!bModificacionPendiente = False
'                    mRs!bModificacionAceptada = True
'
'                    mRs!Serie = .TextMatrix(I, ActSerie)
'                    mRs!InicioRangoActual = CDbl(.TextMatrix(I, ActRangoIni))
'                    mRs!FinRangoActual = CDbl(.TextMatrix(I, ActRangoFin))
'                    mRs!CorrelativoActual = CDbl(.TextMatrix(I, ActCorrelativo))
'                    mRs!FechaEmision = FechaBD(CDate(CDbl(.TextMatrix(I, RawDateFE))), FBD_FULL, True) 'FechaBD(.TextMatrix(I, ActFEmision), , True)
'                    mRs!FechaVencimiento = FechaBD(CDate(CDbl(.TextMatrix(I, RawDateFV))), FBD_FULL, True) 'FechaBD(.TextMatrix(I, ActFVcto), , True)
'                    mRs!Cod_Usuario_Upd = FrmAppLink.GetCodUsuario
'                    mRs!Fecha_Upd = FechaBD(Now, FBD_FULL, True)
'                    mRs!FormatoCorrelativo = txtFormatoCorrelativo.Text
'
'                End If
'
'                'PreDelete = True
'                Grabar = True
'
'            End If
'
'        ElseIf .RowData(I) = 0 Then ' Registros Nuevos.
'
'            mRs.Filter = "TipoComprobante = '" & .TextMatrix(I, TipoCF) & "' "
'
'            If Not mRs.EOF Then
'
'                ' Ya estaba en la base de datos. Alguien grabo primero que nosotros?. _
'                Bueno en todo caso esto es una modificaci�n.
'
'                GoTo Modificar
'
'            Else
'
'                ' Agregar
'
'                mRs.AddNew
'
'                If AplicarSecuenciaPorCaja Then
'
'                    mRs!bModificacionPendiente = True
'                    mRs!bModificacionAceptada = False
'
'                Else
'
'                    mRs!bModificacionPendiente = False
'                    mRs!bModificacionAceptada = True
'
'                End If
'
'                mRs!ID_Caja_PC = mCaja("Cod")
'                mRs!TipoComprobante = .TextMatrix(I, TipoCF)
'                mRs!NomenclaturaComprobante = .TextMatrix(I, NombreCF)
'
'                mRs!SerieModificacion = .TextMatrix(I, ActSerie)
'                mRs!InicioRangoModificacion = CDbl(.TextMatrix(I, ActRangoIni))
'                mRs!FinRangoModificacion = CDbl(.TextMatrix(I, ActRangoFin))
'                mRs!CorrelativoModificacion = CDbl(.TextMatrix(I, ActCorrelativo))
'                mRs!FechaEmisionModificacion = FechaBD(CDate(CDbl(.TextMatrix(I, RawDateFE))), FBD_FULL, True)
'                mRs!FechaVencimientoModificacion = FechaBD(CDate(CDbl(.TextMatrix(I, RawDateFV))), FBD_FULL, True)
'
'                mRs!Serie = mRs!SerieModificacion
'                mRs!InicioRangoActual = mRs!InicioRangoModificacion
'                mRs!FinRangoActual = mRs!FinRangoModificacion
'                mRs!CorrelativoActual = mRs!CorrelativoModificacion
'                mRs!FechaEmision = mRs!FechaEmisionModificacion
'                mRs!FechaVencimiento = mRs!FechaVencimientoModificacion
'
'                mRs!TipoSistema = mTipoSistema
'                mRs!Cod_Usuario_Upd = FrmAppLink.GetCodUsuario
'                mRs!Fecha_Upd = FechaBD(Now, FBD_FULL, True)
'                mRs!FormatoCorrelativo = txtFormatoCorrelativo.Text
'
'                'PreDelete = True
'                Grabar = True
'
'            End If
'
'        End If
'
'    Next
'
'    If Grabar Then
'        mRs.Filter = vbNullString
'        mRs.ActiveConnection = Ent.BDD
'        mRs.UpdateBatch
'    End If
'
'    End With
'
'    Ent.BDD.CommitTrans
'    ActiveTrans = False
'
'    CmbPC_Click
'
'    Exit Sub
'
'Error:
'
'    Resume ' Debug
'
'    If ActiveTrans Then
'        Ent.BDD.RollbackTrans
'        ActiveTrans = False
'    End If
'
'    mErrorNumber = Err.Number
'    mErrorDesc = Err.Description
'    mErrorSource = Err.Source
'
'    Resume SafeErrHandler
'
'SafeErrHandler:
'
'    On Error Resume Next
'
'    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
'
'End Sub

Private Sub Aceptar_Click()
    
    On Error GoTo Error
    
    With KGrid
    
    ' Iniciar Validaciones de informaci�n del Grid.
    
    For I = 1 To .Rows - 1
        .TextMatrix(.Row, ColLinea) = I
    Next I
    
    For I = 1 To .Rows - 1
        If OptPOSRetail Then
            If CmbPC.ListIndex >= 0 _
            And Trim(.TextMatrix(I, TipoCF)) = Empty _
            And Trim(.TextMatrix(I, NombreCF)) = Empty Then
                KGrid.RemoveItem CLng(I)
            Else
                
                Dim mMsgFila: mMsgFila = "Fila " & I & ". "
                
                If CmbPC.ListIndex < 0 Then
                    Mensaje True, "Debe seleccionar la caja."
                    SafeFocus Cmb
                    Exit Sub
                End If
                
                If .TextMatrix(I, ActSerie) = Empty Then
                    Mensaje True, mMsgFila & "Debe indicar la Serie"
                    Exit Sub
                End If
                
                If Not IsNumeric(.TextMatrix(I, ActRangoIni)) Then
                    Mensaje True, mMsgFila & "Debe indicar el Inicio de Rango de Secuencia. " & _
                    "Asegurese de que sea un valor num�rico."
                    Exit Sub
                End If
                
                If Not IsNumeric(.TextMatrix(I, ActRangoFin)) Then
                    Mensaje True, mMsgFila & "Debe indicar el Fin de Rango de Secuencia. " & _
                    "Asegurese de que sea un valor num�rico."
                    Exit Sub
                End If
                
                If Not IsNumeric(.TextMatrix(I, ActCorrelativo)) Then
                    Mensaje True, mMsgFila & "Debe indicar el Pr�ximo Valor de Secuencia. " & _
                    "Asegurese de que sea un valor num�rico y que este dentro del rango."
                    Exit Sub
                End If
                
                If Not IsNumeric(.TextMatrix(I, RawDateFE)) Then
                    Mensaje True, mMsgFila & "Debe indicar la fecha de emisi�n del tipo " & _
                    "de comprobante fiscal."
                    Exit Sub
                End If
                
                If Not IsNumeric(.TextMatrix(I, RawDateFV)) Then
                    Mensaje True, mMsgFila & "Debe indicar la fecha de vencimiento del tipo " & _
                    "de comprobante fiscal."
                    Exit Sub
                End If
                
                If CDbl(.TextMatrix(I, ActRangoIni)) <= 0 Then
                    Mensaje True, mMsgFila & "Debe indicar un valor positivo para el " & _
                    "Inicio de Rango de Secuencia. "
                    Exit Sub
                End If
                
                If CDbl(.TextMatrix(I, ActRangoFin)) <= 0 Then
                    Mensaje True, mMsgFila & "Debe indicar un valor positivo para el " & _
                    "Fin de Rango de Secuencia. "
                    Exit Sub
                End If
                
                If CDbl(.TextMatrix(I, ActCorrelativo)) <= 0 Then
                    Mensaje True, mMsgFila & "Debe indicar un valor positivo para el " & _
                    "Proximo Valor de Secuencia. "
                    Exit Sub
                End If
                
                If CDbl(.TextMatrix(I, ActRangoFin)) < CDbl(.TextMatrix(I, ActRangoIni)) Then
                    Mensaje True, mMsgFila & "El valor final del rango debe ser superior al inicial. "
                    Exit Sub
                End If
                
                If .RowData(I) <> (-999) And _
                (CDbl(.TextMatrix(I, ActCorrelativo)) < CDbl(.TextMatrix(I, ActRangoIni)) _
                Or CDbl(.TextMatrix(I, ActCorrelativo)) > CDbl(.TextMatrix(I, ActRangoFin))) Then
                    Mensaje True, mMsgFila & "El valor de secuencia indicado est� fuera del rango. "
                    Exit Sub
                End If
                
                If .RowData(I) <> (-999) And _
                (CDbl(.TextMatrix(I, RawDateFV)) < CDbl(.TextMatrix(I, RawDateFE))) Then
                    Mensaje True, mMsgFila & "La Fecha de Vencimiento no puede ser menor a la de Emisi�n. "
                    Exit Sub
                End If
                
            End If
        End If
    Next
    
    ' Finalizar Validaciones de informaci�n del Grid.
    
    Dim SQL As String, mWherePC As String
    
    If mTipoSistema = "POSRT" Then
        Set mCaja = mCajas.Item(CStr(CmbPC.ItemData(CmbPC.ListIndex)))
        mWherePC = "AND ID_CAJA_PC = '" & mCaja("Cod") & "' "
    End If
    
    Dim ActiveTrans As Boolean
    
    Ent.BDD.BeginTrans
    ActiveTrans = True
    
    SQL = "SELECT * FROM MA_GESTION_NCF " & _
    "WHERE TipoSistema = '" & mTipoSistema & "' " & _
    mWherePC & _
    "ORDER BY ID_CAJA_PC, TipoComprobante, ID "
    
    Dim mRs As New ADODB.Recordset: Set mRs = New ADODB.Recordset
    mRs.CursorLocation = adUseClient
    
    mRs.Open SQL, Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
    mRs.ActiveConnection = Nothing
    
    Dim PreDelete As Boolean, Grabar As Boolean
    Dim mUltID: mUltID = -1
    Dim mUltTCF: mUltTCF = Empty
    
    For I = 1 To .Rows - 1
        
        If .RowData(I) = 1 Or .RowData(I) = (-999) Then ' Registros Modificaci�n o Eliminaci�n.
            
            'If .TextMatrix(I, TipoCF) <> mUltTCF Then
                'mUltTCF = .TextMatrix(I, TipoCF)
                'mUltID = -1
            'End If
            
            'mRs.Filter = "TipoComprobante = '" & .TextMatrix(I, TipoCF) & "' AND ID > (" & mUltID & ")"
            
            mRs.Filter = "TipoComprobante = '" & .TextMatrix(I, TipoCF) & "' AND ID = (" & .TextMatrix(I, DBRowID) & ")"
            
            If Not mRs.EOF Then
                
Modificar:
                
                mUltID = mRs!ID
                
                If AplicarSecuenciaPorCaja Then
                    
                    mRs!bModificacionPendiente = True
                    mRs!bModificacionAceptada = False
                    
                    mRs!SerieModificacion = .TextMatrix(I, ActSerie)
                    mRs!InicioRangoModificacion = CDbl(.TextMatrix(I, ActRangoIni))
                    mRs!FinRangoModificacion = CDbl(.TextMatrix(I, ActRangoFin))
                    mRs!CorrelativoModificacion = CDbl(.TextMatrix(I, ActCorrelativo))
                    mRs!FechaEmisionModificacion = FechaBD(CDate(CDbl(.TextMatrix(I, RawDateFE))), FBD_FULL, True) 'FechaBD(.TextMatrix(I, ActFEmision), , True)
                    mRs!FechaVencimientoModificacion = FechaBD(CDate(CDbl(.TextMatrix(I, RawDateFV))), FBD_FULL, True) 'FechaBD(.TextMatrix(I, ActFVcto), , True)
                    mRs!Cod_Usuario_Upd = FrmAppLink.GetCodUsuario
                    mRs!Fecha_Upd = FechaBD(Now, FBD_FULL, True)
                    mRs!FormatoCorrelativo = txtFormatoCorrelativo.Text
                    
                Else
                    
                    mRs!bModificacionPendiente = False
                    mRs!bModificacionAceptada = True
                    
                    mRs!Serie = .TextMatrix(I, ActSerie)
                    mRs!InicioRangoActual = CDbl(.TextMatrix(I, ActRangoIni))
                    mRs!FinRangoActual = CDbl(.TextMatrix(I, ActRangoFin))
                    mRs!CorrelativoActual = CDbl(.TextMatrix(I, ActCorrelativo))
                    mRs!FechaEmision = FechaBD(CDate(CDbl(.TextMatrix(I, RawDateFE))), FBD_FULL, True) 'FechaBD(.TextMatrix(I, ActFEmision), , True)
                    mRs!FechaVencimiento = FechaBD(CDate(CDbl(.TextMatrix(I, RawDateFV))), FBD_FULL, True) 'FechaBD(.TextMatrix(I, ActFVcto), , True)
                    mRs!Cod_Usuario_Upd = FrmAppLink.GetCodUsuario
                    mRs!Fecha_Upd = FechaBD(Now, FBD_FULL, True)
                    mRs!FormatoCorrelativo = txtFormatoCorrelativo.Text
                    
                End If
                
                'PreDelete = True
                Grabar = True
                
            End If
            
        ElseIf .RowData(I) = 0 Then ' Registros Nuevos.
            
            'mRs.Filter = "TipoComprobante = '" & .TextMatrix(I, TipoCF) & "' "
            
            'If Not mRs.EOF Then
                
                '' Ya estaba en la base de datos. Alguien grabo primero que nosotros?. _
                Bueno en todo caso esto es una modificaci�n.
                
                'GoTo Modificar
                
            'Else
                
                ' Agregar
                
                mRs.AddNew
                
                If AplicarSecuenciaPorCaja Then
                    
                    mRs!bModificacionPendiente = True
                    mRs!bModificacionAceptada = False
                    
                Else
                    
                    mRs!bModificacionPendiente = False
                    mRs!bModificacionAceptada = True
                    
                End If
                
                mRs!ID_Caja_PC = mCaja("Cod")
                mRs!TipoComprobante = .TextMatrix(I, TipoCF)
                mRs!NomenclaturaComprobante = .TextMatrix(I, NombreCF)
                
                mRs!SerieModificacion = .TextMatrix(I, ActSerie)
                mRs!InicioRangoModificacion = CDbl(.TextMatrix(I, ActRangoIni))
                mRs!FinRangoModificacion = CDbl(.TextMatrix(I, ActRangoFin))
                mRs!CorrelativoModificacion = CDbl(.TextMatrix(I, ActCorrelativo))
                mRs!FechaEmisionModificacion = FechaBD(CDate(CDbl(.TextMatrix(I, RawDateFE))), FBD_FULL, True)
                mRs!FechaVencimientoModificacion = FechaBD(CDate(CDbl(.TextMatrix(I, RawDateFV))), FBD_FULL, True)
                
                mRs!Serie = mRs!SerieModificacion
                mRs!InicioRangoActual = mRs!InicioRangoModificacion
                mRs!FinRangoActual = mRs!FinRangoModificacion
                mRs!CorrelativoActual = mRs!CorrelativoModificacion
                mRs!FechaEmision = mRs!FechaEmisionModificacion
                mRs!FechaVencimiento = mRs!FechaVencimientoModificacion
                
                mRs!TipoSistema = mTipoSistema
                mRs!Cod_Usuario_Upd = FrmAppLink.GetCodUsuario
                mRs!Fecha_Upd = FechaBD(Now, FBD_FULL, True)
                mRs!FormatoCorrelativo = txtFormatoCorrelativo.Text
                
                'PreDelete = True
                Grabar = True
                
            'End If
            
        End If
        
    Next
    
    If Grabar Then
        mRs.Filter = vbNullString
        mRs.ActiveConnection = Ent.BDD
        mRs.UpdateBatch
    End If
    
    End With
    
    Ent.BDD.CommitTrans
    ActiveTrans = False
    
    CmbPC_Click
    
    Exit Sub
    
Error:
    
    'Resume ' Debug
    
    If ActiveTrans Then
        Ent.BDD.RollbackTrans
        ActiveTrans = False
    End If
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Private Sub Cancelar_Click()
    CmbPC_Click
End Sub

Private Sub CmB_Click()
    
    If (Cmb.ListIndex >= 0) Then
        
        If CmbCol = TipoCF Or CmbCol = NombreCF Then
            
            Dim TipoCom, NombreCom
            
            Select Case Cmb.ItemData(Cmb.ListIndex)
                
                Case 1
                    
                    TipoCom = "01"
                    NombreCom = "CR�DITO FISCAL"
                    
                Case 2
                    
                    TipoCom = "02"
                    NombreCom = "CONSUMIDOR FINAL"
                    
                Case 4
                    
                    TipoCom = "04"
                    NombreCom = "NOTA DE CR�DITO"
                    
                Case 14
                    
                    TipoCom = "14"
                    NombreCom = "R�GIMEN ESPECIAL"
                    
                Case 15
                    
                    TipoCom = "15"
                    NombreCom = "GUBERNAMENTAL"
                    
                Case 16
                    
                    TipoCom = "16"
                    NombreCom = "EXPORTACIONES"
                    
                Case 31
                    
                    TipoCom = "31"
                    NombreCom = "CR�DITO FISCAL ELECTR�NICO"
                    
                Case 32
                    
                    TipoCom = "32"
                    NombreCom = "CONSUMIDOR FINAL ELECTR�NICO"
                    
                Case 34
                    
                    TipoCom = "34"
                    NombreCom = "NOTA DE CR�DITO ELECTR�NICA"
                    
                Case 44
                    
                    TipoCom = "44"
                    NombreCom = "R�GIMEN ESPECIAL ELECTR�NICO"
                    
                Case 45
                    
                    TipoCom = "45"
                    NombreCom = "GUBERNAMENTAL ELECTR�NICO"
                    
                Case 46
                    
                    TipoCom = "46"
                    NombreCom = "EXPORTACIONES ELECTR�NICO"
                    
            End Select
            
            'Dim InUse As Boolean
            
            'For I = 1 To KGrid.Rows - 1
                'If UCase(KGrid.TextMatrix(I, TipoCF)) = UCase(TipoCom) Then
                    'InUse = True
                    'Exit For
                'End If
            'Next I
            
            'If InUse Then
                'Mensaje True, "El tipo de comprobante ya se encuentra en uso. Fila " & I & "."
                'KGrid.Row = I
                'KGrid.Col = TipoCF
                'KGrid.TopRow = KGrid.Row
            'Else
                KGrid.TextMatrix(KGrid.Row, TipoCF) = TipoCom
                KGrid.TextMatrix(KGrid.Row, NombreCF) = NombreCom
            'End If
            
            ' Antes no permitiamos mas de una l�nea, ahora si la vamos a permitir...
            
        End If
        
        Cmb.Visible = False
        
    End If
    
End Sub

Private Sub Cmb_LostFocus()
    Cmb.Visible = False
End Sub

Private Sub CmbPC_Click()
    
    InicializarGrid
    
    If CmbPC.ListCount > 0 And CmbPC.ListIndex <> -1 Then
        
        Dim SQL As String, mWherePC As String
        
        'If CmbPC.ItemData(CmbPC.ListIndex) > 0 Then
            'mWherePC = Empty
        'Else
            Set mCaja = mCajas.Item(CStr(CmbPC.ItemData(CmbPC.ListIndex)))
            mWherePC = "AND ID_CAJA_PC = '" & mCaja("Cod") & "' "
        'End If
        
        Select Case True
        
        Case OptADMBiz
        
            mTipoSistema = "ADMBIZ"
            
        Case OptPOSRetail
            
            mTipoSistema = "POSRT"
            
        Case OptPOSFood
            
            mTipoSistema = "POSFOOD"
            
        End Select
        
        SQL = "SELECT * FROM MA_GESTION_NCF " & _
        "WHERE TipoSistema = '" & mTipoSistema & "' " & _
        mWherePC & _
        "ORDER BY ID_CAJA_PC, TipoComprobante, ID "
        
        Dim mRs: Set mRs = Ent.BDD.Execute(SQL)
        
        If Not mRs.EOF Then
            
            Dim I: I = 1
            
            KGrid.Row = I
            
            While Not mRs.EOF
                
                With KGrid
                    
                    .TextMatrix(I, ColLinea) = I
                    .TextMatrix(I, CodCaja) = mRs!ID_Caja_PC
                    .TextMatrix(I, TipoCF) = mRs!TipoComprobante
                    .TextMatrix(I, NombreCF) = mRs!NomenclaturaComprobante
                    .TextMatrix(I, ActSerie) = mRs!Serie
                    .TextMatrix(I, ActRangoIni) = mRs!InicioRangoActual
                    .TextMatrix(I, ActRangoFin) = mRs!FinRangoActual
                    .TextMatrix(I, ActCorrelativo) = mRs!CorrelativoActual
                    .TextMatrix(I, ActFEmision) = mRs!FechaEmision
                    .TextMatrix(I, ActFVcto) = mRs!FechaVencimiento
                    .TextMatrix(I, RawDateFE) = CDbl(mRs!FechaEmision)
                    .TextMatrix(I, RawDateFV) = CDbl(mRs!FechaVencimiento)
                    .TextMatrix(I, ModPendiente) = IIf(mRs!bModificacionPendiente, 1, 0)
                    .TextMatrix(I, ModAceptada) = IIf(mRs!bModificacionAceptada, 1, 0)
                    
                    If mRs!bModificacionPendiente Then
                        .Col = ModVisualizar
                        Set .CellPicture = CmdView.Picture
                    End If
                    
                    .Col = ColModificar
                    .RowData(I) = -1
                    Set .CellPicture = CmdEdit.Picture
                    
                    .TextMatrix(I, ModSerie) = mRs!SerieModificacion
                    .TextMatrix(I, ModRangoIni) = mRs!InicioRangoModificacion
                    .TextMatrix(I, ModRangoFin) = mRs!FinRangoModificacion
                    .TextMatrix(I, ModCorrelativo) = mRs!CorrelativoModificacion
                    .TextMatrix(I, ModFEmision) = mRs!FechaEmisionModificacion
                    .TextMatrix(I, ModFVcto) = mRs!FechaVencimientoModificacion
                    .TextMatrix(I, TipoSys) = mRs!TipoSistema
                    .TextMatrix(I, FmtCorrelativo) = mRs!FormatoCorrelativo
                    
                    If Fix(mRs!FinRangoActual - mRs!CorrelativoActual) <= 0 _
                    Or (mRs!FechaEmision >= mRs!FechaVencimiento) Then
                        ' Fecha de Vencimiento alcanzada o fin de rango alcanzado.
                        CambiarColorFilaKGrid KGrid, .Row, ColorODC
                    ElseIf (Fix(mRs!FinRangoActual - mRs!CorrelativoActual) <= 100) _
                    Or ((CLng(mRs!FechaVencimiento) - CLng(mRs!FechaEmision)) <= 10) Then
                        ' Quedan 10 Dias a Vencerse o Menos de 100 Comprobantes
                        CambiarColorFilaKGrid KGrid, .Row, ColorAlertaComprobantes
                    End If
                    
                    .TextMatrix(I, DBRowID) = mRs!ID
                    
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    I = I + 1
                    
                End With
                
                mRs.MoveNext
                
            Wend
            
            KGrid.Col = CodCaja
            
        End If
        
    End If
    
End Sub

Private Sub CmbPC_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case vbKeyF2
            
            Select Case True
            
            Case OptADMBiz
                
                'NotImplementedYet
                
            Case OptPOSFood, OptPOSRetail
                
                Dim SQL As String
                
                SQL = "SELECT c_Codigo, c_Desc_Caja " & _
                "FROM " & FrmAppLink.Srv_Remote_BD_POS & ".DBO.MA_CAJA " & _
                "ORDER BY c_Desc_Caja ASC "
                
                Titulo = " P O S " '"Reimprimir documento"
                
                Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
                
                Frm_Super_Consultas.Inicializar SQL, Titulo, Ent.POS, Me
                
                Frm_Super_Consultas.Add_ItemLabels StellarMensaje(142), "c_Codigo", 2235, 0 ' Codigo
                Frm_Super_Consultas.Add_ItemLabels StellarMensaje(143), "c_Desc_Caja", 8865, 0 ' Desc
                
                Frm_Super_Consultas.Add_ItemSearching StellarMensaje(143), "c_Desc_Caja" ' Desc
                Frm_Super_Consultas.Add_ItemSearching StellarMensaje(142), "c_Codigo" ' Codigo
                
                Frm_Super_Consultas.txtDato.Text = "%"
                Frm_Super_Consultas.BusquedaInstantanea = True
                Frm_Super_Consultas.CustomFontSize = 18
                
                Frm_Super_Consultas.StrOrderBy = "c_Desc_Caja DESC"
                Frm_Super_Consultas.Show vbModal
                
                mSel = Frm_Super_Consultas.ArrResultado
                
                If Not IsEmpty(mSel) Then
                    If mSel(0) <> "" Then
                        ListSafeItemSelection CmbPC, mSel(0) & " - " & mSel(1)
                    End If
                End If
                
                Set Frm_Super_Consultas = Nothing
                
            End Select
            
    End Select
    
End Sub

Private Sub CmdNuevaFila_Click()
    KGrid.Rows = KGrid.Rows + 1
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    If Not FormaCargada Then
        FormaCargada = True
        OptPOSRetail_Click
    End If
End Sub

Private Sub Form_Load()
    
    FormaCargada = False
    
    lbl_Organizacion.Caption = "Ficha de Gestion de Comprobantes Fiscales" 'Ficha de Comisiones
    
    txtFormatoCorrelativo.Text = BuscarReglaNegocioStr("Tri_FormatoCorrelativoNCF", "00000000")
    AplicarSecuenciaPorCaja = Val(BuscarReglaNegocioStr("Tri_NCFSecuenciaXCaja", "1"))
    
End Sub

Private Sub InicializarGrid()
    
    With KGrid
        
        .Cols = ColCount
        .Rows = 1
        .Row = 0
        .RowHeightMin = 640
        .RowHeight(0) = 600
        
        .TextMatrix(0, ColLinea) = "LN"
        .TextMatrix(0, CodCaja) = "ID POS"
        .TextMatrix(0, TipoCF) = "Tipo CF"
        .TextMatrix(0, NombreCF) = "Nombre CF"
        .TextMatrix(0, ActSerie) = "Serie"
        .TextMatrix(0, ActRangoIni) = "Rango Ini."
        .TextMatrix(0, ActRangoFin) = "Rango Fin."
        .TextMatrix(0, ActCorrelativo) = "Secuencia"
        .TextMatrix(0, ActFEmision) = "F. Emision"
        .TextMatrix(0, ActFVcto) = "F. Vcto"
        .TextMatrix(0, RawDateFE) = Empty
        .TextMatrix(0, RawDateFV) = Empty
        .TextMatrix(0, ModPendiente) = Empty
        .TextMatrix(0, ModAceptada) = Empty
        .TextMatrix(0, ModVisualizar) = "Ver Mod"
        .TextMatrix(0, ColModificar) = "Edit"
        .TextMatrix(0, ModSerie) = Empty
        .TextMatrix(0, ModRangoIni) = Empty
        .TextMatrix(0, ModRangoFin) = Empty
        .TextMatrix(0, ModCorrelativo) = Empty
        .TextMatrix(0, ModFEmision) = Empty
        .TextMatrix(0, ModFVcto) = Empty
        .TextMatrix(0, TipoSys) = "Tipo Sys"
        .TextMatrix(0, FmtCorrelativo) = Empty
        
        .Col = ColLinea
        .ColWidth(.Col) = 500
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = CodCaja
        If mTipoSistema = "ADMBIZ" Then
            .ColWidth(.Col) = 1500
            .ColAlignment(.Col) = flexAlignLeftCenter
            .CellAlignment = flexAlignCenterCenter
        Else
            .ColWidth(.Col) = 0
            .ColAlignment(.Col) = flexAlignRightCenter
            .CellAlignment = flexAlignRightCenter
        End If
        .ColDisplayFormat(.Col) = String(50, "&")
        
        .Col = TipoCF
        .ColWidth(.Col) = 1080
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        .ColDisplayFormat(.Col) = String(50, "&")
        
        .Col = NombreCF
        .ColWidth(.Col) = 3000
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        .ColDisplayFormat(.Col) = String(50, "&")
        
        .Col = ActSerie
        .ColWidth(.Col) = 800
        .ColAlignment(.Col) = flexAlignLeftCenter
        .CellAlignment = flexAlignCenterCenter
        .ColDisplayFormat(.Col) = String(50, "&")
        
        .Col = ActRangoIni
        .ColWidth(.Col) = 1515
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        .ColDisplayFormat(.Col) = "###,##0"
        
        .Col = ActRangoFin
        .ColWidth(.Col) = 1440
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        .ColDisplayFormat(.Col) = "###,##0"
        
        .Col = ActCorrelativo
        .ColWidth(.Col) = 1365
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        .ColDisplayFormat(.Col) = "###,##0"
        
        .Col = ActFEmision
        .ColWidth(.Col) = 1500
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        .ColDisplayFormat(.Col) = String(50, "&")
        
        .Col = ActFVcto
        .ColWidth(.Col) = 1500
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        .ColDisplayFormat(.Col) = String(50, "&")
        
        .Col = RawDateFE
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        
        .Col = RawDateFV
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        
        .Col = ModPendiente
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        
        .Col = ModAceptada
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        
        .Col = ModVisualizar
        .ColWidth(.Col) = 640
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        .ColDisplayFormat(.Col) = String(50, "&")
        
        .Col = ColModificar
        .ColWidth(.Col) = 640
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignCenterCenter
        .ColDisplayFormat(.Col) = String(50, "&")
        
        .Col = ModSerie
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        .ColDisplayFormat(.Col) = String(50, "&")
        .Col = ModRangoIni
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        .Col = ModRangoFin
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        .Col = ModCorrelativo
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        .Col = ModFEmision
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        .ColDisplayFormat(.Col) = String(50, "&")
        .Col = ModFVcto
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        .ColDisplayFormat(.Col) = String(50, "&")
        
        .Col = TipoSys
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        .ColDisplayFormat(.Col) = String(50, "&")
        .Col = FmtCorrelativo
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        .ColDisplayFormat(.Col) = String(50, "&")
        
        .Col = DBRowID
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        
        .Rows = 2
        .Row = 1
        
    End With
    
End Sub

Private Sub KlexGridComisiones_Keydown(KeyCode As Integer, Shift As Integer)
'
'    Select Case Shift
'
'        Case vbAltMask
'
'            Select Case KeyCode
'                Case vbKeyN
'                    KlexGridComisiones.Rows = KlexGridComisiones.Rows + 1
'
'                Case vbKeyE
'                    If (KlexGridComisiones.Rows > 2) Then
'                        KlexGridComisiones.Rows = KlexGridComisiones.Rows - 1
'                    Else
'                        KlexGridComisiones.Rows = KlexGridComisiones.Rows - 1
'                        KlexGridComisiones.Rows = KlexGridComisiones.Rows + 1
'                    End If
'            End Select
'
'    End Select
'
'    If KlexGridComisiones.Col = KlexGridCols.ColMontoFijo And KeyCode = 46 Then
'        DeleteCurrentRow
'    End If
    
End Sub

Private Sub KGrid_Click()
    
    With KGrid
        
        Select Case .Col
            
            Case ColModificar
                
                If .RowData(.Row) = -1 Then
                    .RowData(.Row) = 1
                    Dim TmpObj As Object
                    Set TmpObj = KGrid
                    CambiarColorFilaKGrid KGrid, .Row, ColorModif
                End If
                
            Case ModVisualizar
                
                If Val(.TextMatrix(.Row, ModPendiente)) = 1 Then
                    
                    Dim FrmMostrarXML As Object
                    Set FrmMostrarXML = FrmAppLink.GetFrmMostrarXML
                    
                    FrmMostrarXML.Caption = "Detalles de Modificaci�n Pendiente por Sincronizar."
                    
                    Dim mTexto As String
                    
                    mTexto = vbNewLine
                    
                    mTexto = mTexto & RellenarCadenasSeparadas("Tipo de Comprobante: ", .TextMatrix(.Row, NombreCF), 40, , False) & vbNewLine
                    mTexto = mTexto & vbNewLine
                    mTexto = mTexto & RellenarCadena_ALaDer("Propiedad", 30) & RellenarCadena_ALaDer("Actual", 20) & RellenarCadena_ALaDer("Mod. Pendiente", 20) & vbNewLine
                    mTexto = mTexto & vbNewLine
                    mTexto = mTexto & RellenarCadena_ALaDer("Serie", 30) & RellenarCadena_ALaDer(.TextMatrix(.Row, ActSerie), 20) & RellenarCadena_ALaDer(.TextMatrix(.Row, ModSerie), 20) & vbNewLine
                    mTexto = mTexto & RellenarCadena_ALaDer("Inicio de Rango", 30) & RellenarCadena_ALaDer(.TextMatrix(.Row, ActRangoIni), 20) & RellenarCadena_ALaDer(.TextMatrix(.Row, ModRangoIni), 20) & vbNewLine
                    mTexto = mTexto & RellenarCadena_ALaDer("Secuencia Actual", 30) & RellenarCadena_ALaDer(.TextMatrix(.Row, ActCorrelativo), 20) & RellenarCadena_ALaDer(.TextMatrix(.Row, ModCorrelativo), 20) & vbNewLine
                    mTexto = mTexto & RellenarCadena_ALaDer("Fin de Rango", 30) & RellenarCadena_ALaDer(.TextMatrix(.Row, ActRangoFin), 20) & RellenarCadena_ALaDer(.TextMatrix(.Row, ModRangoFin), 20) & vbNewLine
                    mTexto = mTexto & RellenarCadena_ALaDer("Fecha de Emisi�n", 30) & RellenarCadena_ALaDer(.TextMatrix(.Row, ActFEmision), 20) & RellenarCadena_ALaDer(.TextMatrix(.Row, ModFEmision), 20) & vbNewLine
                    mTexto = mTexto & RellenarCadena_ALaDer("Fecha de Vencimiento", 30) & RellenarCadena_ALaDer(.TextMatrix(.Row, ActFVcto), 20) & RellenarCadena_ALaDer(.TextMatrix(.Row, ModFVcto), 20) & vbNewLine
                    mTexto = mTexto & vbNewLine
                    
                    FrmMostrarXML.TextArea.Text = mTexto
                    Set FrmMostrarXML.TmpAlternateFont = GetFont("Lucida Console", 12)
                    FrmMostrarXML.Show vbModal
                    
                    Set FrmMostrarXML = Nothing
                    
                End If
                
            Case TipoCF, NombreCF
                
                If .TextMatrix(.Row, TipoCF) <> Empty Then Exit Sub
                If .RowData(.Row) = -1 Then Exit Sub
                
                Cmb.Move .CellLeft + .Left, .CellTop + .Top, 4000
                Cmb.Visible = True
                
                Cmb.Clear
                
                Cmb.AddItem "01 - Cr�dito Fiscal"
                Cmb.ItemData(Cmb.NewIndex) = 1
                
                Cmb.AddItem "02 - Consumidor Final"
                Cmb.ItemData(Cmb.NewIndex) = 2
                
                Cmb.AddItem "04 - Nota de Cr�dito"
                Cmb.ItemData(Cmb.NewIndex) = 4
                
                Cmb.AddItem "14 - R�gimen Especial"
                Cmb.ItemData(Cmb.NewIndex) = 14
                
                Cmb.AddItem "15 - Gubernamental"
                Cmb.ItemData(Cmb.NewIndex) = 15
                
                Cmb.AddItem "16 - Exportaciones"
                Cmb.ItemData(Cmb.NewIndex) = 16
                
                Cmb.AddItem "31 - Cr�dito Fiscal Electr�nico"
                Cmb.ItemData(Cmb.NewIndex) = 31
                
                Cmb.AddItem "32 - Consumidor Final Electr�nico"
                Cmb.ItemData(Cmb.NewIndex) = 32
                
                Cmb.AddItem "34 - Nota de Cr�dito Electr�nica"
                Cmb.ItemData(Cmb.NewIndex) = 34
                
                Cmb.AddItem "44 - R�gimen Especial Electr�nico"
                Cmb.ItemData(Cmb.NewIndex) = 44
                
                Cmb.AddItem "45 - Gubernamental Electr�nico"
                Cmb.ItemData(Cmb.NewIndex) = 45
                
                Cmb.AddItem "46 - Exportaciones Electr�nico"
                Cmb.ItemData(Cmb.NewIndex) = 46
                
                CmbCol = .Col
                
                SafeFocus Cmb
                
                SafeSendKeys "{F4}"
                
            Case ActFEmision, ActFVcto
                
                If .TextMatrix(.Row, TipoCF) = Empty Then Exit Sub
                If .RowData(.Row) = -1 Then Exit Sub
                
                Dim mCls As Object 'clsFechaSeleccion
                
                Set mCls = FrmAppLink.GetClassFechaSeleccion 'New clsFechaSeleccion
                
                mCls.MostrarInterfazFechaHora 1
                
                If mCls.Selecciono Then
                    If KGrid.Col = ActFEmision Then
                        KGrid.TextMatrix(KGrid.Row, RawDateFE) = CDbl(mCls.Fecha)
                        KGrid.Text = SDate(mCls.Fecha)
                    ElseIf KGrid.Col = ActFVcto Then
                        KGrid.TextMatrix(KGrid.Row, RawDateFV) = CDbl(mCls.Fecha)
                        KGrid.Text = SDate(mCls.Fecha)
                    End If
                End If
                
            Case Else
            
        End Select
    
    End With
    
End Sub

Private Sub KGrid_EnterCell()
    
    Select Case KGrid.Col
        
        Case CodCaja, TipoCF, NombreCF, ActFEmision, ActFVcto, ModVisualizar, ColModificar
            KGrid.Editable = False
        Case Else
            If KGrid.RowData(KGrid.Row) = 1 Then
                KGrid.Editable = False
            Else
                KGrid.Editable = True
            End If
            
    End Select
    
End Sub

Private Sub KGrid_KeyDown(KeyCode As Integer, Shift As Integer)
    
    With KGrid
        
        Dim I
        
        I = .Row
        
        If I <= 0 Then Exit Sub
        
        Select Case KeyCode
            
            Case vbKeyDelete, vbKeyF6
                
                If Mensaje(False, "Est� seguro de que desea eliminar esta l�nea?") Then
                    
                    If .TextMatrix(I, DBRowID) = Empty Then
                        .RemoveItem CLng(I)
                    Else
                        
                        .Col = ColModificar
                        KGrid_Click
                        
                        .TextMatrix(I, ActCorrelativo) = CDbl(.TextMatrix(I, ActRangoFin)) + 1
                        .TextMatrix(I, RawDateFE) = CDbl(Date - 1)
                        .TextMatrix(I, RawDateFV) = .TextMatrix(I, RawDateFE)
                        .TextMatrix(I, ActFEmision) = CDate(Date - 1)
                        .TextMatrix(I, ActFVcto) = .TextMatrix(I, ActFEmision)
                        
                        .RowData(I) = -999 ' Eliminar
                        
                    End If
                    
                End If
                
        End Select
        
    End With
    
End Sub

Private Sub KGrid_KeyPress(KeyAscii As Integer)
    
    With KGrid
        
        If .RowData(.Row) = -1 Then Exit Sub
        
        Select Case .Col
            
            Case ActRangoIni, ActRangoFin, ActCorrelativo
                
                Select Case KeyAscii
                    
                    Case 48 To 57
                        
                        KGrid.Editable = True
                        
                    Case Else
                        
                        KGrid.Editable = False
                        
                End Select
                
            Case ActSerie
                
                KGrid.Editable = True
                
        End Select
        
    End With
    
End Sub

Private Sub KGrid_KeyPressEdit(ByVal Row As Long, ByVal Col As Long, KeyAscii As Integer)
    
    If KGrid.RowData(KGrid.Row) = -1 Then
        KeyAscii = 0
        Exit Sub
    End If
    
    Select Case Col
        
        Case ActRangoIni, ActRangoFin, ActCorrelativo
            
            Select Case KeyAscii
                
                Case 48 To 57, 13, 27, vbKeyBack, vbKeyDelete
                    
                Case Else
                    
                    KeyAscii = 0
                    
            End Select
            
        Case ActSerie
            
            'If Len(KGrid.ED) = 1 And Len(KGrid.TextMatrix(Row, Col)) = 1 And Not (KeyAscii = vbKeyBack Or KeyAscii = vbKeyDelete) Then
                'KeyAscii = 0
            'End If
            
    End Select
    
End Sub

Private Sub KGrid_ValidateEdit(Row As Long, Col As Long, Cancel As Boolean)
    If Col = ActSerie Then
        KGrid.EditText = Left(KGrid.TextMatrix(Row, Col), 1)
        KGrid.TextMatrix(Row, Col) = KGrid.EditText
    End If
End Sub

Private Sub OptPOSRetail_Click()
    
    mTipoSistema = "POSRT"
    
    Dim SQL As String
    
    If AplicarSecuenciaPorCaja Then
        
        SQL = "SELECT c_Codigo, c_Desc_Caja " & _
        "FROM VAD20.DBO.MA_CAJA " & _
        "ORDER BY c_Desc_Caja ASC "
        
        Dim mRs: Set mRs = Ent.BDD.Execute(SQL)
        
        Set mCajas = New Dictionary
        
        CmbPC.Clear
        
        If Not mRs.EOF Then
            Dim I: I = 0
            While Not mRs.EOF
                I = I + 1
                Set mCaja = New Dictionary
                mCaja("Num") = I
                mCaja("Cod") = mRs!c_Codigo
                mCaja("Des") = mRs!c_Desc_Caja
                mCajas.Add CStr(I), mCaja
                CmbPC.AddItem mCaja("Cod") & " - " & mCaja("Des")
                CmbPC.ItemData(CmbPC.NewIndex) = I
                mRs.MoveNext
            Wend
            CmbPC_Click
        Else
            InicializarGrid
        End If
        
        mRs.Close
        
    Else
        
        CmbPC.Clear
        
        Set mCaja = New Dictionary
        mCaja("Num") = 1
        mCaja("Cod") = "[ANY]"
        mCaja("Des") = "CUALQUIER POS"
        If Not mCajas.Exists(CStr(I)) Then
            mCajas.Add CStr(I), mCaja
        End If
        CmbPC.AddItem mCaja("Cod") & " - " & mCaja("Des")
        CmbPC.ItemData(CmbPC.NewIndex) = 1
        CmbPC.ListIndex = 0
        CmbPC_Click
        
    End If
    
End Sub

Private Sub EliminarLineaSobrante()
'
'    eliminarLinea = True
'
'    KlexGridComisiones.Row = KlexGridComisiones.Rows - 1
'
'    For k = 0 To KlexGridComisiones.Cols - 1
'
'        If KlexGridComisiones.TextMatrix(KlexGridComisiones.Row, k) <> "" Or Len(KlexGridComisiones.TextMatrix(KlexGridComisiones.Row, k)) > 0 Then
'
'            eliminarLinea = False
'
'            Exit For
'
'        End If
'
'    Next k
'
'    If eliminarLinea Then DeleteCurrentRow
    
End Sub

Private Function ValidarDatos() As Boolean
'
'    ValidarDatos = True
'
'    Dim DatosCorrectos As Boolean
'
'    DatosCorrectos = False
'
'    If Len(Me.txt_Codigo) <= 0 Then
'        Call Mensaje(True, Stellar_Mensaje(16127)) '"Debe ingresar un c�digo para el Perfil.")
'        ValidarDatos = False
'        Exit Function
'    End If
'
'    If Len(Me.txt_Descripcion) <= 0 Then
'        Call Mensaje(True, Stellar_Mensaje(16128)) '"Debe ingresar una descripci�n para el Perfil.")
'        ValidarDatos = False
'        Exit Function
'    End If
'
'    If Me.KlexGridComisiones.Rows <= 1 Then
'        Call Mensaje(True, Stellar_Mensaje(16129)) '"No se puede grabar un perfil vacio. Debe ingresar al menos una fila en la Tabla.")
'        ValidarDatos = False
'        Exit Function
'    End If
'
'    Call EliminarLineaSobrante
'
'    For I = 1 To KlexGridComisiones.Rows - 1
'
'        For k = 0 To KlexGridComisiones.Cols - 1
'
'            If KlexGridComisiones.TextMatrix(I, k) = "" Or Len(KlexGridComisiones.TextMatrix(I, k)) <= 0 Then
'
'                Call Mensaje(True, Stellar_Mensaje(16130)) '"Existen campos en la tabla sin valor. Asegurese de ingresar la informaci�n correctamente.")
'
'                ValidarDatos = False
'
'                Exit Function
'
'            End If
'
'        Next k
'
'    Next I
'
'    DatosCorrectos = ValidarDetalle
'
'    If Not DatosCorrectos Then
'        ValidarDatos = False
'    End If
    
End Function

Private Function ValidarDetalle() As Boolean
'
'    ValidarDetalle = True
'
'    For I = 1 To KlexGridComisiones.Rows - 1
'
'        If ComprobarIntervalos(CInt(I)) = False Then
'
'            ValidarDetalle = False
'
'            Call Mensaje(True, Stellar_Mensaje(16131)) '"Existen conflictos entre los Rangos. Por favor verifique la informaci�n.")
'
'            Exit Function
'
'        End If
'
'    Next I

End Function

Private Function ComprobarIntervalos(Fila As Integer) As Boolean
'
'    ComprobarIntervalos = True
'
'    RangoMinimoLocal = CDbl(KlexGridComisiones.TextMatrix(Fila, KlexGridCols.ColRangoMinimo))
'    RangoMaximoLocal = CDbl(KlexGridComisiones.TextMatrix(Fila, KlexGridCols.ColRangoMaximo))
'
'    For I = 1 To KlexGridComisiones.Rows - 1
'
'        If I <> Fila Then
'
'            RangoMinimoTemp = CDbl(KlexGridComisiones.TextMatrix(I, KlexGridCols.ColRangoMinimo))
'            RangoMaximoTemp = CDbl(KlexGridComisiones.TextMatrix(I, KlexGridCols.ColRangoMaximo))
'
'            If (RangoMinimoTemp >= RangoMinimoLocal And RangoMinimoTemp <= RangoMaximoLocal) Or (RangoMaximoTemp >= RangoMinimoLocal And RangoMaximoTemp <= RangoMaximoLocal) Then
'                ComprobarIntervalos = False
'                Exit Function
'            End If
'
'        End If
'
'    Next I
    
End Function

Private Sub KlexGridComisiones_LeaveCell()
'
'    KlexGridComisiones.AutoSize 0, 0
'    KlexGridComisiones.AutoSize 1, 1
'    KlexGridComisiones.AutoSize 2, 2
'    KlexGridComisiones.AutoSize 3, 3
'    KlexGridComisiones.AutoSize 4, 4
'    KlexGridComisiones.AutoSize 5, 5
    
End Sub

Private Sub KlexGridComisiones_ValidateEdit(Row As Long, Col As Long, Cancel As Boolean)
'
'    Select Case Col
'
'        Case Is = KlexGridCols.ColRangoMinimo, KlexGridCols.ColRangoMaximo, KlexGridCols.ColPorcentaje, KlexGridCols.ColMontoFijo
'
'            ValorEntrada = KlexGridComisiones.TextMatrix(Row, Col)
'
'            If IsNumeric(ValorEntrada) Then
'
'                KlexGridComisiones.TextMatrix(Row, Col) = FormatNumber(ValorEntrada, 2)
'                KlexGridComisiones.EditText = FormatNumber(ValorEntrada, 2)
'
'                If Col = KlexGridCols.ColPorcentaje Then
'                    If CDbl(ValorEntrada) >= 100 Then
'                        KlexGridComisiones.TextMatrix(Row, Col) = FormatNumber(100, 2)
'                        KlexGridComisiones.EditText = FormatNumber(100, 2)
'                    End If
'                End If
'
'            Else
'
'                KlexGridComisiones.TextMatrix(Row, Col) = ""
'                KlexGridComisiones.EditText = ""
'
'            End If
'
'            If Len(ValorEntrada) > 20 Then
'                KlexGridComisiones.TextMatrix(Row, Col) = ""
'                KlexGridComisiones.EditText = ""
'            End If
'
'    End Select

End Sub
