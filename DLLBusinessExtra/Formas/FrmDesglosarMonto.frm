VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form FrmDesglosarMonto 
   Appearance      =   0  'Flat
   BackColor       =   &H00E7E8E8&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   8430
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8430
   ScaleMode       =   0  'User
   ScaleWidth      =   15360
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton CmdAgregar 
      Caption         =   "&Agregar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   10920
      Picture         =   "FrmDesglosarMonto.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   30
      Top             =   2040
      Width           =   1215
   End
   Begin VB.CommandButton CmdDelete 
      Caption         =   "&Eliminar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   12240
      Picture         =   "FrmDesglosarMonto.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   29
      Top             =   2040
      Width           =   1215
   End
   Begin VB.CommandButton CmdFinalizar 
      Caption         =   "Finalizar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1095
      Left            =   13560
      Picture         =   "FrmDesglosarMonto.frx":2A4C
      Style           =   1  'Graphical
      TabIndex        =   28
      Top             =   2040
      Width           =   1215
   End
   Begin VB.TextBox txtMontoAplicar 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   495
      Left            =   2760
      TabIndex        =   27
      Top             =   2520
      Width           =   1815
   End
   Begin VB.TextBox txtRestante 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   495
      Left            =   600
      Locked          =   -1  'True
      TabIndex        =   25
      Top             =   2520
      Width           =   1815
   End
   Begin VB.CommandButton CmdLocalidad 
      Height          =   495
      Left            =   9840
      Picture         =   "FrmDesglosarMonto.frx":47CE
      Style           =   1  'Graphical
      TabIndex        =   22
      Top             =   2640
      Width           =   615
   End
   Begin VB.TextBox txtLocalidad 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   495
      Left            =   6240
      Locked          =   -1  'True
      TabIndex        =   21
      Top             =   2640
      Width           =   3495
   End
   Begin VB.CommandButton CmdUnidad 
      Height          =   495
      Left            =   9840
      Picture         =   "FrmDesglosarMonto.frx":4FD0
      Style           =   1  'Graphical
      TabIndex        =   19
      Top             =   2040
      Width           =   615
   End
   Begin VB.TextBox txtUnidad 
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   495
      Left            =   6240
      Locked          =   -1  'True
      TabIndex        =   18
      Top             =   2040
      Width           =   3495
   End
   Begin VB.Frame FrameGrid 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame7"
      ForeColor       =   &H80000008&
      Height          =   4575
      Left            =   120
      TabIndex        =   7
      Top             =   3480
      Width           =   15135
      Begin VB.VScrollBar ScrollGrid 
         Height          =   4530
         LargeChange     =   10
         Left            =   14400
         TabIndex        =   8
         Top             =   0
         Width           =   674
      End
      Begin MSFlexGridLib.MSFlexGrid Grid 
         Height          =   4545
         Left            =   0
         TabIndex        =   9
         Top             =   0
         Width           =   15075
         _ExtentX        =   26591
         _ExtentY        =   8017
         _Version        =   393216
         BackColor       =   16448250
         ForeColor       =   3355443
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   15658734
         ForeColorSel    =   0
         BackColorBkg    =   16448250
         GridColor       =   13421772
         WordWrap        =   -1  'True
         ScrollTrack     =   -1  'True
         Enabled         =   -1  'True
         FocusRect       =   0
         FillStyle       =   1
         GridLinesFixed  =   0
         SelectionMode   =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame FrameHeader 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Frame7"
      ForeColor       =   &H80000008&
      Height          =   1455
      Left            =   120
      TabIndex        =   6
      Top             =   480
      Width           =   15015
      Begin VB.Frame FrameTipoDesglose 
         BackColor       =   &H00E7E8E8&
         Caption         =   "------------  Tipo de Desglose     ---------"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   1215
         Left            =   9960
         TabIndex        =   14
         Top             =   120
         Width           =   4695
         Begin VB.CheckBox ChkValorFijo 
            BackColor       =   &H0000C000&
            Caption         =   "Monto"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FAFAFA&
            Height          =   600
            Left            =   240
            Style           =   1  'Graphical
            TabIndex        =   16
            Top             =   360
            Width           =   2000
         End
         Begin VB.CheckBox ChkPorcentaje 
            BackColor       =   &H00AE5B00&
            Caption         =   "Porcentaje"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FAFAFA&
            Height          =   600
            Left            =   2400
            Style           =   1  'Graphical
            TabIndex        =   15
            Top             =   360
            Width           =   2000
         End
      End
      Begin VB.Frame FrameModalidad 
         BackColor       =   &H00E7E8E8&
         Caption         =   "----------------------------     Modalidad de Desglose     -----------------------------"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   1215
         Left            =   360
         TabIndex        =   10
         Top             =   120
         Width           =   9375
         Begin VB.CheckBox ChkAmbos 
            BackColor       =   &H00AE5B00&
            Caption         =   "Ambos"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FAFAFA&
            Height          =   600
            Left            =   6240
            Style           =   1  'Graphical
            TabIndex        =   13
            Top             =   360
            Width           =   2775
         End
         Begin VB.CheckBox ChkLocalidades 
            BackColor       =   &H00AE5B00&
            Caption         =   "Por Localidad"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FAFAFA&
            Height          =   600
            Left            =   3240
            Style           =   1  'Graphical
            TabIndex        =   12
            Top             =   360
            Width           =   2775
         End
         Begin VB.CheckBox ChkUnidades 
            BackColor       =   &H0000C000&
            Caption         =   "Por Unidad de Negocio"
            BeginProperty Font 
               Name            =   "Verdana"
               Size            =   9
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FAFAFA&
            Height          =   600
            Left            =   240
            Style           =   1  'Graphical
            TabIndex        =   11
            Top             =   360
            Width           =   2775
         End
      End
   End
   Begin VB.PictureBox Picture1 
      Height          =   255
      Left            =   2520
      Picture         =   "FrmDesglosarMonto.frx":57D2
      ScaleHeight     =   195
      ScaleWidth      =   195
      TabIndex        =   5
      Top             =   10680
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.PictureBox Picture2 
      Height          =   255
      Left            =   2880
      Picture         =   "FrmDesglosarMonto.frx":6C1C
      ScaleHeight     =   195
      ScaleWidth      =   195
      TabIndex        =   4
      Top             =   10680
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   -240
      TabIndex        =   0
      Top             =   0
      Width           =   19320
      Begin VB.Label LabelTitulo 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Desglosar Monto de Factura - Documento: 000000005 - 1,355.23 $"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   480
         TabIndex        =   3
         Top             =   120
         Width           =   12375
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   12840
         TabIndex        =   1
         Top             =   120
         Width           =   1815
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   14880
         Picture         =   "FrmDesglosarMonto.frx":8066
         Top             =   0
         Width           =   480
      End
   End
   Begin MSComctlLib.ProgressBar bar 
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   8085
      Visible         =   0   'False
      Width           =   15015
      _ExtentX        =   26485
      _ExtentY        =   450
      _Version        =   393216
      BorderStyle     =   1
      Appearance      =   0
      Scrolling       =   1
   End
   Begin MSFlexGridLib.MSFlexGrid GridEvitarFoco 
      Height          =   30
      Left            =   7680
      TabIndex        =   17
      Top             =   240
      Width           =   255
      _ExtentX        =   450
      _ExtentY        =   53
      _Version        =   393216
   End
   Begin VB.Label lblCaptionValor 
      BackStyle       =   0  'Transparent
      Caption         =   "Valor Aplicar:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   375
      Left            =   3000
      TabIndex        =   26
      Top             =   2040
      Width           =   1575
   End
   Begin VB.Label lblCaptionRestante 
      BackStyle       =   0  'Transparent
      Caption         =   "Restante:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   375
      Left            =   1080
      TabIndex        =   24
      Top             =   2040
      Width           =   1095
   End
   Begin VB.Label lblCaptionLocalidad 
      BackStyle       =   0  'Transparent
      Caption         =   "Localidad:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   375
      Left            =   4920
      TabIndex        =   23
      Top             =   2760
      Width           =   1245
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblCaptionUnidad 
      BackStyle       =   0  'Transparent
      Caption         =   "Unidad:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   375
      Left            =   4920
      TabIndex        =   20
      Top             =   2160
      Width           =   1245
   End
   Begin VB.Line Line2 
      BorderColor     =   &H00AE5B00&
      X1              =   120.235
      X2              =   15100.49
      Y1              =   3360
      Y2              =   3360
   End
End
Attribute VB_Name = "FrmDesglosarMonto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private mDatosDevolver As Dictionary

Private mVarDocumento                           As String
Private mVarConcepto                            As String
Private mVarLocalidad                           As String
Private mVarCodConcepto                         As Long
Private mVarSimboloDoc                          As String
Private mVarMontoInicial                        As Variant

Private mTotalIngresado                         As Variant

Const mColorChecked = &HC000&
Const mColorUnchecked = &HAE5B00

Private Enum GridDM ' Crear Nuevo Lote
    ColNull
    ColRowID
    ColMontoAplicar
    ColCodUnidad
    ColUnidad
    ColCodLocalidad
    ColLocalidad
    ColCount
End Enum

Private NomCol() As String
Private VarColWidth As Long

Private FormaCargada As Boolean

Public Cancelar As Boolean

Private EventoProgramado As Boolean

Private mClsGrupos As Object 'New cls_grupos

Private CambiandoModoEjecucion As Boolean

Property Let Documento(ByVal pValor As String)
    mVarDocumento = pValor
End Property

Property Let TipoDoc(ByVal pValor As String)
    mVarConcepto = pValor
End Property

Property Let Localidad(ByVal pValor As String)
    mVarLocalidad = pValor
End Property

Property Let Concepto(ByVal pValor As String)
    mVarCodConcepto = pValor
End Property

Property Let MontoInicial(ByVal pValor As Variant)
    mVarMontoInicial = pValor
End Property

Property Let SimboloMonedaDoc(ByVal pValor As String)
    mVarSimboloDoc = pValor
End Property

Property Get DatosDesglose() As Dictionary
    Set DatosDesglose = mDatosDevolver
End Property

Private Sub CmdLocalidad_Click()
    txtLocalidad_Click
End Sub

Private Sub CmdUnidad_Click()
    txtUnidad_Click
End Sub

Private Sub Form_Load()
    
    FormaCargada = False
    
    Cancelar = False
    
    AjustarPantalla Me
    
End Sub

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        
        FormaCargada = True
        
        If Trim(mVarDocumento) = Empty _
        Or Trim(mVarConcepto) = Empty _
        Or Trim(mVarLocalidad) = Empty _
        Or mVarCodConcepto <= 0 _
        Or mVarMontoInicial <= 0 Then
            
            Cancelar = True
            
            Unload Me
            
            Exit Sub
            
        End If
        
        LabelTitulo = StellarMensaje(4119) & " - " & StellarMensaje(10016) & ": " & _
        mVarDocumento & " - " & FormatoDecimalesDinamicos( _
        mVarMontoInicial, 0, 2) & " " & mVarSimboloDoc
        
        ChkUnidades.Caption = StellarMensaje(4120)
        ChkLocalidades.Caption = StellarMensaje(4121)
        ChkAmbos.Caption = StellarMensaje(16578)
        
        ChkValorFijo.Caption = StellarMensaje(2517)
        ChkPorcentaje.Caption = StellarMensaje(306)
        
        FrameModalidad.Caption = StellarMensaje(4122)
        FrameTipoDesglose.Caption = StellarMensaje(4123)
        
        lblCaptionRestante.Caption = StellarMensaje(4124)
        lblCaptionValor.Caption = StellarMensaje(4125)
        
        lblCaptionUnidad.Caption = StellarMensaje(10068)
        lblCaptionLocalidad.Caption = StellarMensaje(4126)
        
        CmdAgregar.Caption = StellarMensaje(197)
        CmdDelete.Caption = StellarMensaje(208)
        CmdFinalizar.Caption = StellarMensaje(33)
        
        ChkLocalidades.Value = vbChecked: ChkUnidades.Value = vbChecked
        ChkPorcentaje.Value = vbChecked: ChkValorFijo.Value = vbChecked
        
        mTotalIngresado = CDec(0)
        
        txtRestante = FormatoDecimalesDinamicos(mVarMontoInicial - mTotalIngresado, 0, 2)
        txtMontoAplicar = txtRestante
        
        SeleccionarTexto txtMontoAplicar
        
        SafeFocus txtMontoAplicar
        
    End If
    
End Sub

Private Sub ResetearMontos()
    
    If ChkValorFijo.BackColor = mColorChecked Then
        txtRestante = FormatoDecimalesDinamicos(mVarMontoInicial, 0, 2)
    ElseIf ChkPorcentaje.BackColor = mColorChecked Then
        txtRestante = FormatoDecimalesDinamicos(100, 2, 2)
    End If
    
    txtMontoAplicar = txtRestante
    SeleccionarTexto txtMontoAplicar
    
    mTotalIngresado = CDec(0)
    
End Sub

Private Sub ChkUnidades_Click()
    CambiandoModoEjecucion = IIf(Not CambiandoModoEjecucion, _
    ChkUnidades.Value = vbChecked, CambiandoModoEjecucion)
    If CambiandoModoEjecucion Then
        If ChkUnidades.Value = vbChecked Then
            CambiandoModoEjecucion = False
            ChkUnidades.Value = vbUnchecked
            ChkLocalidades.Value = vbUnchecked
            ChkAmbos.Value = vbUnchecked
            ChkUnidades.BackColor = mColorChecked
            ChkLocalidades.BackColor = mColorUnchecked
            ChkAmbos.BackColor = mColorUnchecked
            '
            
            Limpiar
            txtUnidad.Enabled = True
            CmdUnidad.Enabled = True
            txtLocalidad.Enabled = False
            CmdLocalidad.Enabled = False
            PrepararGrid
            ResetearMontos
            
            SeleccionarTexto txtMontoAplicar
            
            '
            
        End If
    End If
    SafeFocus GridEvitarFoco
End Sub

Private Sub ChkLocalidades_Click()
    CambiandoModoEjecucion = IIf(Not CambiandoModoEjecucion, _
    ChkLocalidades.Value = vbChecked, CambiandoModoEjecucion)
    If CambiandoModoEjecucion Then
        If ChkLocalidades.Value = vbChecked Then
            CambiandoModoEjecucion = False
            ChkLocalidades.Value = vbUnchecked
            ChkUnidades.Value = vbUnchecked
            ChkAmbos.Value = vbUnchecked
            ChkLocalidades.BackColor = mColorChecked
            ChkUnidades.BackColor = mColorUnchecked
            ChkAmbos.BackColor = mColorUnchecked
            
            '
            
            Limpiar
            txtUnidad.Enabled = False
            CmdUnidad.Enabled = False
            txtLocalidad.Enabled = True
            CmdLocalidad.Enabled = True
            PrepararGrid
            ResetearMontos
            
            '
            
        End If
    End If
    SafeFocus GridEvitarFoco
End Sub

Private Sub ChkAmbos_Click()
    CambiandoModoEjecucion = IIf(Not CambiandoModoEjecucion, _
    ChkAmbos.Value = vbChecked, CambiandoModoEjecucion)
    If CambiandoModoEjecucion Then
        If ChkAmbos.Value = vbChecked Then
            CambiandoModoEjecucion = False
            ChkAmbos.Value = vbUnchecked
            ChkUnidades.Value = vbUnchecked
            ChkLocalidades.Value = vbUnchecked
            ChkAmbos.BackColor = mColorChecked
            ChkUnidades.BackColor = mColorUnchecked
            ChkLocalidades.BackColor = mColorUnchecked
            
            '
            
            Limpiar
            txtUnidad.Enabled = True
            CmdUnidad.Enabled = True
            txtLocalidad.Enabled = True
            CmdLocalidad.Enabled = True
            PrepararGrid
            ResetearMontos
            
            '
            
        End If
    End If
    SafeFocus GridEvitarFoco
End Sub

Private Sub Limpiar()
    
    Grid.Clear
    Grid.Rows = 1
    'Grid.Rows = 2
    
    txtUnidad_KeyDown vbKeyDelete, 0
    txtLocalidad_KeyDown vbKeyDelete, 0
    
End Sub

Private Sub PrepararGrid()
    
    With Grid
        
        .SelectionMode = flexSelectionFree
        .WordWrap = True
        .ScrollTrack = True
        .AllowUserResizing = flexResizeColumns
        
        .Rows = 2
        
        .FixedCols = 0
        .FixedRows = 1
        
        .Rows = 1
        
        .Cols = GridDM.ColCount
        ReDim NomCol(GridDM.ColCount)
        
        .RowHeight(0) = 425
        .RowHeightMin = 600
        
        .Row = 0
        
        .Col = ColNull
        NomCol(.Col) = Empty
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColRowID
        NomCol(.Col) = "Ln"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(.Col) = 1000
        
        .Col = ColMontoAplicar
        NomCol(.Col) = IIf(ChkValorFijo.BackColor = mColorChecked, _
        ChkValorFijo.Caption, ChkPorcentaje.Caption)
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(.Col) = 3000
        
        .Col = ColCodUnidad
        NomCol(.Col) = "Unidad"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColUnidad
        NomCol(.Col) = "Unidad"
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(.Col) = IIf(ChkAmbos.BackColor = mColorChecked, 5000, _
        IIf(ChkUnidades.BackColor = mColorChecked, 10000, 0))
        
        .Col = ColCodLocalidad
        NomCol(.Col) = StellarMensaje(10068)
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        .ColWidth(.Col) = 0
        
        .Col = ColLocalidad
        NomCol(.Col) = StellarMensaje(4126) ' Localidad
        .Text = NomCol(.Col)
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        .ColWidth(.Col) = IIf(ChkAmbos.BackColor = mColorChecked, 5000, _
        IIf(ChkLocalidades.BackColor = mColorChecked, 10000, 0))
        
        'VarColWidth = .ColWidth(.Col)
        
        .Width = 15000
        
        '.Row = 1
        
        .Col = ColMontoAplicar
        
    End With
    
End Sub

Private Sub ChkValorFijo_Click()
    CambiandoModoEjecucion = IIf(Not CambiandoModoEjecucion, _
    ChkValorFijo.Value = vbChecked, CambiandoModoEjecucion)
    If CambiandoModoEjecucion Then
        If ChkValorFijo.Value = vbChecked Then
            CambiandoModoEjecucion = False
            ChkValorFijo.Value = vbUnchecked
            ChkPorcentaje.Value = vbUnchecked
            ChkValorFijo.BackColor = mColorChecked
            ChkPorcentaje.BackColor = mColorUnchecked
            
            '
            
            Limpiar
            PrepararGrid
            lblCaptionValor.Caption = StellarMensaje(4125)
            ResetearMontos
            
            '
            
        End If
    End If
    SafeFocus GridEvitarFoco
End Sub

Private Sub ChkPorcentaje_Click()
    CambiandoModoEjecucion = IIf(Not CambiandoModoEjecucion, _
    ChkPorcentaje.Value = vbChecked, CambiandoModoEjecucion)
    If CambiandoModoEjecucion Then
        If ChkPorcentaje.Value = vbChecked Then
            CambiandoModoEjecucion = False
            ChkValorFijo.Value = vbUnchecked
            ChkPorcentaje.Value = vbUnchecked
            ChkPorcentaje.BackColor = mColorChecked
            ChkValorFijo.BackColor = mColorUnchecked
            '
            
            Limpiar
            PrepararGrid
            
            lblCaptionValor.Caption = StellarMensaje(4129)
            
            txtRestante = FormatoDecimalesDinamicos(100, 2, 2)
            txtMontoAplicar = txtRestante
            ResetearMontos
            '
            
        End If
    End If
    SafeFocus GridEvitarFoco
End Sub

Private Sub CmdAgregar_Click()
    
    Dim mValor
    
    If ChkValorFijo.BackColor = mColorChecked Then
        
        mValor = txtMontoAplicar
        
        If SDec(txtMontoAplicar) <= 0 _
        Or SDec(txtMontoAplicar) > SDec(txtRestante) Then
            
            Mensaje True, StellarMensaje(4128)
            SafeFocus txtMontoAplicar
            Exit Sub
            
        End If
        
    Else
        
        mValor = Replace(txtMontoAplicar.Text, "%", Empty, , , vbTextCompare)
        
        If SDec(mValor) <= 0 _
        Or SDec(mValor) > SDec(txtRestante) Then
            
            Mensaje True, StellarMensaje(4128)
            SafeFocus txtMontoAplicar
            Exit Sub
            
        End If
        
    End If
    
    If ChkUnidades.BackColor = mColorChecked _
    Or ChkAmbos.BackColor = mColorChecked Then
        
        If Trim(txtUnidad) = Empty Then
            
            Mensaje True, StellarMensaje(2821)
            SafeFocus txtUnidad
            Exit Sub
            
        End If
        
    End If
    
    If ChkLocalidades.BackColor = mColorChecked _
    Or ChkAmbos.BackColor = mColorChecked Then
        
        If Trim(txtLocalidad) = Empty Then
            
            Mensaje True, StellarMensaje(2821)
            SafeFocus txtLocalidad
            Exit Sub
            
        End If
        
    End If
    
    Dim mFilaNew As Long
    
    Grid.Rows = Grid.Rows + 1
    mFilaNew = Grid.Rows - 1
    
    ScrollGrid.Max = Grid.Rows
    
    Grid.TextMatrix(mFilaNew, ColRowID) = FormatoDecimalesDinamicos(mFilaNew)
    
    Grid.TextMatrix(mFilaNew, ColMontoAplicar) = FormatoDecimalesDinamicos(txtMontoAplicar)
    
    If ChkUnidades.BackColor = mColorChecked _
    Or ChkAmbos.BackColor = mColorChecked Then
        
        Grid.TextMatrix(mFilaNew, ColCodUnidad) = txtUnidad.Tag
        Grid.TextMatrix(mFilaNew, ColUnidad) = txtUnidad.Text
        
        txtUnidad_KeyDown vbKeyDelete, 0
        
    End If
    
    If ChkLocalidades.BackColor = mColorChecked _
    Or ChkAmbos.BackColor = mColorChecked Then
        
        Grid.TextMatrix(mFilaNew, ColCodLocalidad) = txtLocalidad.Tag
        Grid.TextMatrix(mFilaNew, ColLocalidad) = txtLocalidad.Text
        
        txtLocalidad_KeyDown vbKeyDelete, 0
        
    End If
    
    mTotalIngresado = mTotalIngresado + SDec(mValor)
    
    If ChkValorFijo.BackColor = mColorChecked Then
        txtRestante = FormatoDecimalesDinamicos(mVarMontoInicial - mTotalIngresado, 0, 2)
    Else
        txtRestante = FormatoDecimalesDinamicos(CDec(100) - mTotalIngresado, 0, 2)
    End If
    txtMontoAplicar = txtRestante
    
    SeleccionarTexto txtMontoAplicar
    SafeFocus txtMontoAplicar
    
End Sub

Private Sub CmdDelete_Click()
    
    If Grid.Row < 1 Then
        Exit Sub
    End If
    
    mTotalIngresado = mTotalIngresado - SDec(Grid.TextMatrix(Grid.Row, ColMontoAplicar))
    
    txtRestante = FormatoDecimalesDinamicos(mVarMontoInicial - mTotalIngresado, 0, 2)
    txtMontoAplicar = txtRestante
    
    SeleccionarTexto txtMontoAplicar
    
    If Grid.Row = 1 Then
        Grid.Rows = 1
    Else
        Grid.RemoveItem Grid.Row
    End If
    
    For I = 1 To Grid.Rows - 1
        Grid.TextMatrix(I, ColRowID) = FormatoDecimalesDinamicos(I)
    Next
    
    ScrollGrid.Max = Grid.Rows
    
End Sub

Private Sub CmdFinalizar_Click()
    
    If Grid.Rows < 1 Or SDec(txtRestante) > 0 Then
        Mensaje True, StellarMensaje(3291)
        Exit Sub
    End If
    
    If Not Mensaje(False, StellarMensaje(4130)) Then
        Exit Sub
    End If
    
    Set mDatosDevolver = New Dictionary
    
    mDatosDevolver("Documento") = mVarDocumento
    mDatosDevolver("Tipo") = mVarConcepto
    mDatosDevolver("Localidad") = mVarLocalidad
    mDatosDevolver("Concepto") = mVarCodConcepto
    mDatosDevolver("MontoInicial") = mVarMontoInicial
    
    Select Case True
        
        Case ChkUnidades.BackColor = mColorChecked
            
            mDatosDevolver("ModoDesglose") = "PorUnidad"
            
        Case ChkLocalidades.BackColor = mColorChecked
            
            mDatosDevolver("ModoDesglose") = "PorLocalidad"
            
        Case ChkAmbos.BackColor = mColorChecked
            
            mDatosDevolver("ModoDesglose") = "Ambos"
            
    End Select
    
    Dim mKey As String, mSumaTmp As Variant, mInicial As Variant
    
    Select Case True
        
        Case ChkValorFijo.BackColor = mColorChecked
            
            mDatosDevolver("TipoValorDesglose") = "Monto"
            
            mInicial = mVarMontoInicial
            
        Case ChkPorcentaje.BackColor = mColorChecked
            
            mDatosDevolver("TipoValorDesglose") = "Porcentaje"
            
            mInicial = CDec(100)
            
    End Select
    
    Set mDatosDevolver("Detalles") = New Dictionary
    
    mSumaTmp = CDec(0)
    
    For I = 1 To Grid.Rows - 1
        
        mKey = I
        
        Set mDatosDevolver("Detalles")(mKey) = New Dictionary
        
        mSumaTmp = mSumaTmp + SDec(Grid.TextMatrix(I, ColMontoAplicar))
        
        mDatosDevolver("Detalles")(mKey)("ColRowID") = SDec(Grid.TextMatrix(I, ColRowID))
        mDatosDevolver("Detalles")(mKey)("ColMontoAplicar") = SDec(Grid.TextMatrix(I, ColMontoAplicar))
        mDatosDevolver("Detalles")(mKey)("ColCodUnidad") = Grid.TextMatrix(I, ColCodUnidad)
        mDatosDevolver("Detalles")(mKey)("ColUnidad") = Grid.TextMatrix(I, ColUnidad)
        mDatosDevolver("Detalles")(mKey)("ColCodLocalidad") = Grid.TextMatrix(I, ColCodLocalidad)
        mDatosDevolver("Detalles")(mKey)("ColLocalidad") = Grid.TextMatrix(I, ColLocalidad)
        
        mDatosDevolver("Detalles")(mKey)("SumaTmpLinea") = mSumaTmp
        mDatosDevolver("Detalles")(mKey)("RestanteLinea") = mInicial - mSumaTmp
        
    Next
    
    Cancelar = False
    
    Unload Me
    
    Exit Sub
    
End Sub

Private Sub Exit_Click()
    Unload Me
    Cancelar = True
    Exit Sub
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If ScrollGrid.Value <> Grid.Row Then
        Grid.TopRow = ScrollGrid.Value
        Grid.Row = ScrollGrid.Value
        If PuedeObtenerFoco(Grid) Then Grid.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    ScrollGrid_Change
End Sub

Private Sub txtLocalidad_Click()
    
    Dim mResult As Variant
    Dim mLimpiar As Boolean
    
    mResult = FrmAppLink.Buscar_Localidad
    mLimpiar = True
    
    If Not IsEmpty(mResult) Then
        If Trim(mResult(0)) <> Empty Then
            mLimpiar = False
            txtLocalidad.Tag = mResult(0)
            txtLocalidad.Text = mResult(1)
            VerificarRepetidos
        End If
    End If
    
    If mLimpiar Then
        txtLocalidad.Tag = Empty
        txtLocalidad.Text = Empty
    End If
    
End Sub

Private Sub txtLocalidad_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Or KeyCode = vbKeyF2 Then
        txtLocalidad_Click
    ElseIf KeyCode = vbKeyDelete Or KeyCode = vbKeyBack Then
        txtLocalidad.Text = Empty
        txtLocalidad.Tag = Empty
    End If
End Sub

Private Sub VerificarRepetidos()
    
    Dim mRes As Variant
    
    If ChkUnidades.BackColor = mColorChecked Then
        
        mRes = BuscarValorGrid(Grid, txtUnidad.Tag, ColCodUnidad, , True)
        
        If Not IsEmpty(mRes) Then
            
            Mensaje True, StellarMensaje(4132)
            txtUnidad_KeyDown vbKeyDelete, 0
            
        End If
        
    ElseIf ChkLocalidades.BackColor = mColorChecked Then
        
        mRes = BuscarValorGrid(Grid, txtLocalidad.Tag, ColCodLocalidad, , True)
        
        If Not IsEmpty(mRes) Then
            
            Mensaje True, StellarMensaje(4132)
            txtLocalidad_KeyDown vbKeyDelete, 0
            
        End If
        
    ElseIf ChkAmbos.BackColor = mColorChecked Then
        
        mRes = EncontrarValoresGrid(Grid, Array(ColCodUnidad, ColCodLocalidad), _
        Array(txtUnidad.Tag, txtLocalidad.Tag), Array(True, True))
        
        If mRes > 0 Then
            
            Mensaje True, StellarMensaje(4132)
            
            txtUnidad_KeyDown vbKeyDelete, 0
            txtLocalidad_KeyDown vbKeyDelete, 0
            
        End If
        
    End If
    
End Sub

Private Sub txtMontoAplicar_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn
            txtMontoAplicar_LostFocus
    End Select
End Sub

Private Sub txtMontoAplicar_LostFocus()
    txtMontoAplicar = FormatoDecimalesDinamicos(SDec(Replace(txtMontoAplicar, _
    "%", Empty, , , vbTextCompare)))
End Sub

Private Sub txtRestante_Click()
    txtMontoAplicar.Text = txtRestante
    SeleccionarTexto txtMontoAplicar
    SafeFocus txtMontoAplicar
End Sub

Private Sub txtUnidad_Click()
    
    Dim mResult As Variant
    Dim mLimpiar As Boolean
    
    mResult = FrmAppLink.Buscar_UnidadDeNegocio
    mLimpiar = True
    
    If Not IsEmpty(mResult) Then
        If Trim(mResult(0)) <> Empty Then
            mLimpiar = False
            txtUnidad.Tag = mResult(0)
            txtUnidad.Text = mResult(1)
            VerificarRepetidos
        End If
    End If
    
    If mLimpiar Then
        txtUnidad.Tag = Empty
        txtUnidad.Text = Empty
    End If
    
End Sub

Private Sub txtUnidad_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Or KeyCode = vbKeyF2 Then
        txtUnidad_Click
    ElseIf KeyCode = vbKeyDelete Or KeyCode = vbKeyBack Then
        txtUnidad.Text = Empty
        txtUnidad.Tag = Empty
    End If
End Sub
