VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form Compras_DatosLibroLicores 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6180
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   8655
   ControlBox      =   0   'False
   Icon            =   "Compras_DatosLibroLicores.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6180
   ScaleWidth      =   8655
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame11 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   8760
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   5835
         TabIndex        =   11
         Top             =   75
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Confirme los Datos Logisticos de Compras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   75
         Width           =   5295
      End
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Creditos"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   5430
      Index           =   1
      Left            =   480
      TabIndex        =   6
      Top             =   600
      Width           =   7740
      Begin VB.CommandButton Cmd_Aceptar 
         Appearance      =   0  'Flat
         Caption         =   "Confirmar"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   2040
         Picture         =   "Compras_DatosLibroLicores.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   4080
         Width           =   1455
      End
      Begin VB.CommandButton CmdSalir 
         Appearance      =   0  'Flat
         Caption         =   "Salir"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1095
         Left            =   3720
         Picture         =   "Compras_DatosLibroLicores.frx":800C
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   4080
         Width           =   1455
      End
      Begin VB.TextBox txtNoControl 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   240
         MaxLength       =   50
         TabIndex        =   3
         Top             =   3390
         Width           =   6855
      End
      Begin VB.TextBox txtPlacaV 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   240
         MaxLength       =   50
         TabIndex        =   2
         Top             =   2550
         Width           =   6855
      End
      Begin VB.TextBox txtTransportistaID 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   240
         MaxLength       =   50
         TabIndex        =   1
         Top             =   1710
         Width           =   6855
      End
      Begin VB.TextBox txtTransportistaNombre 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   240
         MaxLength       =   100
         TabIndex        =   0
         Top             =   870
         Width           =   6855
      End
      Begin VB.Label lblNoControl 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "No. Control"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   270
         TabIndex        =   14
         Top             =   3120
         Width           =   960
      End
      Begin VB.Label lblPlacaV 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Placa del Veh�culo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   270
         TabIndex        =   13
         Top             =   2280
         Width           =   1545
      End
      Begin VB.Label lblTransportistaID 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "C�dula / ID del Transportista"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   270
         TabIndex        =   12
         Top             =   1440
         Width           =   2475
      End
      Begin VB.Label lblTransportistaNombre 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Nombre del Transportista"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   270
         TabIndex        =   8
         Top             =   600
         Width           =   2190
      End
      Begin VB.Line Line4 
         BorderColor     =   &H00AE5B00&
         X1              =   3255
         X2              =   7480
         Y1              =   240
         Y2              =   240
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Informaci�n Log�stica de Compras"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Index           =   3
         Left            =   120
         TabIndex        =   7
         Top             =   120
         Width           =   2910
      End
   End
   Begin MSComctlLib.ImageList Iconos_Encendidos 
      Left            =   3720
      Top             =   6120
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   7
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Compras_DatosLibroLicores.frx":9D8E
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Compras_DatosLibroLicores.frx":BB20
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Compras_DatosLibroLicores.frx":C7FA
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Compras_DatosLibroLicores.frx":E58C
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Compras_DatosLibroLicores.frx":1031E
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Compras_DatosLibroLicores.frx":120B0
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Compras_DatosLibroLicores.frx":13E42
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "Compras_DatosLibroLicores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private FormaCargada As Boolean

Public TransportistaNombre As String
Public TransportistaID As String
Public PlacaVehiculo As String
Public NoControl As String

Public Confirmar As Boolean

Private Sub Cmd_Aceptar_Click()
    
    If Trim(txtTransportistaNombre) = Empty Then
        Mensaje True, "El nombre del transportista es obligatorio."
        SafeFocus txtTransportistaNombre
        Exit Sub
    End If
    
    If Trim(txtTransportistaID) = Empty Then
        Mensaje True, "El ID del transportista es obligatorio."
        SafeFocus txtTransportistaID
        Exit Sub
    End If
    
    If Trim(txtPlacaV) = Empty Then
        Mensaje True, "La Placa del Veh�culo es obligatoria."
        SafeFocus txtPlacaV
        Exit Sub
    End If
    
    If FrmAppLink.CxP_Requiere_No_Control Then
        If Trim(txtNoControl) = Empty Then
            Mensaje True, "El N�mero de Control es obligatorio."
            SafeFocus txtNoControl
            Exit Sub
        End If
    End If
    
    TransportistaNombre = Trim(txtTransportistaNombre) ' Solo aqui Permitimos Comillas
    TransportistaID = Trim(QuitarComillasSimples(txtTransportistaID))
    PlacaVehiculo = Trim(QuitarComillasSimples(txtPlacaV))
    NoControl = Trim(QuitarComillasSimples(txtNoControl))
    
    Confirmar = True
    
    Unload Me
    
    Exit Sub
    
End Sub

Private Sub CmdSalir_Click()
    Confirmar = False
    Unload Me
    Exit Sub
End Sub

Private Sub Form_Load()
    
    FormaCargada = False
    
    FrmAppLink.SetFormaDLL = Me
    
    AjustarPantalla Me
    
End Sub

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        
        FormaCargada = True
        
        txtTransportistaNombre.Text = TransportistaNombre
        txtTransportistaID.Text = TransportistaID
        txtPlacaV.Text = PlacaVehiculo
        txtNoControl.Text = NoControl
        
        Select Case True
            Case Trim(txtTransportistaNombre) = Empty
                SafeFocus txtTransportistaNombre
            Case Trim(txtTransportistaID) = Empty
                SafeFocus txtTransportistaID
            Case Trim(txtPlacaV) = Empty
                SafeFocus txtPlacaV
            Case Trim(txtNoControl) = Empty
                SafeFocus txtNoControl
            Case Else
                SafeFocus Cmd_Aceptar
        End Select
        
        'If FrmAppLink.CxP_Requiere_No_Control Then
            
            'txtNoControl.Locked = True
            
            '' Se supone que ya viene asignado y validado desde Frm_Totalizar_Fac.
            '' No permitimos editar por que podria colocar un n�mero repetido.
            
            ' Actualizacion: Mejor lo pedimos antes de llamar a Frm_Totalizar_Fac _
            y se lo pasamos para que ya este rellenado. Lo permitimos cambiar aqui.
            
        'End If
        
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case Is = vbKeyReturn
            oTeclado.Key_Tab
        Case Is = vbKeyF12
            CmdSalir_Click
    End Select
End Sub
