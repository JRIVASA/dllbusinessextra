VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSComCt2.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "ComDlg32.ocx"
Begin VB.Form Rep_OfertasyPromociones 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   7050
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   9315
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7050
   ScaleWidth      =   9315
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton CmdOpenXML 
      Height          =   456
      Left            =   4770
      Picture         =   "Rep_OfertasyPromociones.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   27
      Top             =   6000
      Visible         =   0   'False
      Width           =   456
   End
   Begin VB.CommandButton CmdXml 
      Appearance      =   0  'Flat
      Caption         =   "&XML"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   972
      Index           =   2
      Left            =   4440
      Picture         =   "Rep_OfertasyPromociones.frx":3738
      Style           =   1  'Graphical
      TabIndex        =   26
      Top             =   5880
      Width           =   1095
   End
   Begin VB.CommandButton Aceptar 
      Appearance      =   0  'Flat
      Caption         =   "&Pantalla"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   972
      Index           =   0
      Left            =   5640
      Picture         =   "Rep_OfertasyPromociones.frx":54BA
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "Vista Preliminar (F2)"
      Top             =   5880
      Width           =   1095
   End
   Begin VB.CommandButton Aceptar 
      Appearance      =   0  'Flat
      Caption         =   "&Impresora"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Index           =   1
      Left            =   6855
      Picture         =   "Rep_OfertasyPromociones.frx":723C
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Imprimir Reporte (F8)"
      Top             =   5880
      Width           =   1095
   End
   Begin VB.CommandButton cmd_salir 
      Appearance      =   0  'Flat
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   8070
      Picture         =   "Rep_OfertasyPromociones.frx":8FBE
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Salir del Reporte (F3)"
      Top             =   5880
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   4812
      Left            =   120
      TabIndex        =   3
      Top             =   720
      Width           =   9135
      Begin VB.CommandButton CmdCampana 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   315
         Left            =   6720
         Picture         =   "Rep_OfertasyPromociones.frx":AD40
         Style           =   1  'Graphical
         TabIndex        =   29
         Top             =   3480
         Width           =   315
      End
      Begin VB.TextBox cmbCampanna 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1200
         MaxLength       =   250
         TabIndex        =   28
         ToolTipText     =   "Ingrese el C�digo a Buscar."
         Top             =   3480
         Width           =   5460
      End
      Begin VB.TextBox TxtPromocion 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1200
         MaxLength       =   250
         TabIndex        =   24
         ToolTipText     =   "Ingrese el C�digo a Buscar."
         Top             =   2880
         Width           =   5460
      End
      Begin VB.CommandButton Command1 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   315
         Left            =   6720
         Picture         =   "Rep_OfertasyPromociones.frx":B542
         Style           =   1  'Graphical
         TabIndex        =   23
         Top             =   2880
         Width           =   315
      End
      Begin VB.ComboBox cmbItemBusqueda 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   324
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   21
         ToolTipText     =   "Seleccione el Tipo de Impresi�n."
         Top             =   2280
         Width           =   2748
      End
      Begin VB.CommandButton CmdTipoPromocion 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   315
         Left            =   6720
         Picture         =   "Rep_OfertasyPromociones.frx":BD44
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   1560
         Width           =   315
      End
      Begin VB.TextBox txtTipoPromocion 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1200
         MaxLength       =   250
         TabIndex        =   18
         ToolTipText     =   "Ingrese el C�digo a Buscar."
         Top             =   1560
         Width           =   5460
      End
      Begin VB.ComboBox modo 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   324
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   13
         ToolTipText     =   "Seleccione el Tipo de Impresi�n."
         Top             =   4080
         Width           =   2748
      End
      Begin VB.TextBox txt_Sucursal 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1200
         MaxLength       =   250
         TabIndex        =   12
         ToolTipText     =   "Ingrese el C�digo a Buscar."
         Top             =   960
         Width           =   5460
      End
      Begin VB.CommandButton cmd_sucursal 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         Height          =   315
         Left            =   6720
         Picture         =   "Rep_OfertasyPromociones.frx":C546
         Style           =   1  'Graphical
         TabIndex        =   11
         Top             =   960
         Width           =   315
      End
      Begin MSComCtl2.DTPicker Fechalow 
         CausesValidation=   0   'False
         Height          =   336
         Left            =   1200
         TabIndex        =   15
         ToolTipText     =   "Fecha de la transferencia"
         Top             =   360
         Width           =   2136
         _ExtentX        =   3757
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   118030337
         CurrentDate     =   36415
      End
      Begin MSComCtl2.DTPicker Fechahigh 
         CausesValidation=   0   'False
         Height          =   336
         Left            =   4896
         TabIndex        =   16
         ToolTipText     =   "Fecha de la transferencia"
         Top             =   360
         Width           =   2136
         _ExtentX        =   3757
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   118030337
         CurrentDate     =   36415
      End
      Begin VB.Label Label6 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Campa�a:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   228
         Left            =   120
         TabIndex        =   25
         Top             =   3516
         Width           =   864
      End
      Begin VB.Label Label5 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Promoci�n:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   228
         Left            =   120
         TabIndex        =   22
         Top             =   2880
         Width           =   984
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Estatus"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   228
         Left            =   120
         TabIndex        =   20
         Top             =   2280
         Width           =   600
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Tipo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   228
         Left            =   120
         TabIndex        =   17
         Top             =   1570
         Width           =   384
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Modo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   120
         TabIndex        =   14
         Top             =   4080
         Width           =   708
      End
      Begin VB.Label Label4 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Sucursal"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   10
         Top             =   1035
         Width           =   1092
      End
      Begin VB.Label lbl_fecha2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3720
         TabIndex        =   6
         Top             =   435
         Width           =   1200
      End
      Begin VB.Label lbl_fecha1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   5
         Top             =   420
         Width           =   525
      End
      Begin VB.Line Line5 
         BorderColor     =   &H00AE5B00&
         X1              =   2030
         X2              =   8900
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   " Criterios de B�squeda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Index           =   1
         Left            =   0
         TabIndex        =   4
         Top             =   0
         Width           =   1935
      End
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9315
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   6315
         TabIndex        =   2
         Top             =   105
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Listado de Ofertas y Promociones"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   105
         Width           =   3615
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   8640
         Picture         =   "Rep_OfertasyPromociones.frx":CD48
         Top             =   0
         Width           =   480
      End
   End
   Begin MSComDlg.CommonDialog rutaXml 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "Rep_OfertasyPromociones"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Arr_TipoPromo As Object
Private EventoProgramado As Boolean
Private Arr_Campanas As Object
Private Arr_Promociones As Object
Private Arr_Localidades As Object

Private Sub Aceptar_Click(Index As Integer)
    CCriterio (Index)
End Sub

Private Sub cmbCampanna_Click()
    
    If Arr_Campanas Is Nothing Then
        
    Else
        
        Dim ListaStr As String
        
        For Each Item In Arr_Campanas
            ListaStr = ListaStr & vbNewLine & Item(0)
        Next
        
        If Len(ListaStr) > 0 Then
            
            FrmAppLink.ShowTooltip "" & "" & StellarMensaje(4154) & ": " & GetLines(2) & ListaStr & "", _
            3500, 3000, cmbCampanna, 0, _
            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True), "**[RefreshForm]**", , 0
            
            RefreshForm Me
            
        End If
        
    End If
    
End Sub

Private Sub cmd_salir_Click()
    Unload Me
End Sub

Private Sub CmdXml_Click(Index As Integer)
    CCriterio (Index)
End Sub

Private Sub CmdXml_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbLeftButton Then ' Abrir con Excel
            On Error Resume Next
            ShellEx Me.hWnd, "Open", "EXCEL", """" & rutaXml.FileName & """", vbNullString, 1
        ElseIf Button = vbMiddleButton Then ' Abrir con la App XML Handler / por defecto.
            On Error Resume Next
            ShellEx Me.hWnd, "Open", """" & rutaXml.FileName & """", vbNullString, vbNullString, 1
        ElseIf Button = vbRightButton Then ' Ocultar
            CmdOpenXML.Visible = False
    End If
End Sub

Private Sub Command1_Click()
     Call TxtPromocion_KeyDown(vbKeyF2, 0)
End Sub

Private Sub cmd_sucursal_Click()
    Call txt_sucursal_KeyDown(vbKeyF2, 0)
End Sub

Private Sub CmdTipoPromocion_Click()
    Call txtTipoPromocion_KeyDown(vbKeyF2, 0)
End Sub

Private Sub Exit_Click()
    cmd_salir_Click
End Sub

Private Sub Form_Activate()
    'RefreshForm Me
End Sub

Private Sub Form_Load()
    
    FrmAppLink.SetFormaDLL = Me
    
    lbl_fecha1.Caption = StellarMensaje(3045)
    lbl_fecha2.Caption = StellarMensaje(3046)
    lbl_fecha1.ToolTipText = lbl_fecha1.Caption
    lbl_fecha2.ToolTipText = lbl_fecha2.Caption
    
    Dim RsSucursal As New ADODB.Recordset
    
    RsSucursal.Open _
    "Select * from MA_SUCURSALES ", _
    Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
    
    RsSucursal.Close
    
    ' cbx_Sucursal.ListIndex = 0
    
    Me.Fechalow.Value = Date
    Me.Fechahigh.Value = EndOfDay(Now)
    
    modo.Visible = True
    
    modo.AddItem StellarMensaje(10039) '"Resumido"
    modo.AddItem StellarMensaje(10214) '"Detallado"
    
    modo.ListIndex = 0
    
    cmbItemBusqueda.AddItem StellarMensaje(4155) '"Activadas" '"Activadas"
    cmbItemBusqueda.AddItem StellarMensaje(4156) '"Completadas"
    cmbItemBusqueda.AddItem StellarMensaje(4157) '"Anuladas"
    cmbItemBusqueda.AddItem StellarMensaje(444) 'En Espera"
    'cmbItemBusqueda.AddItem "Activa - No iniciada aun"
    'cmbItemBusqueda.AddItem "Activa - En Curso"
    cmbItemBusqueda.AddItem StellarMensaje(16406) '"Todas"
    
    cmbItemBusqueda.ListIndex = 0
      
    Set ClsGrupos = FrmAppLink.GetClassGrupo
    
    EventoProgramado = True
    
    'ClsGrupos.cTipoGrupo = "CAMP"
    'ClsGrupos.CargarComboGrupos FrmAppLink.CnAdm, cmbCampanna
    'FrmAppLink.ListRemoveItems cmbCampanna, "Ninguno", vbNullString
    
    EventoProgramado = False
    
    'CargarTemporal
    
End Sub

Private Sub CargarTemporal()
    
    On Error GoTo Error
    
    Dim RsPromociones As ADODB.Recordset
    Set RsPromociones = New ADODB.Recordset
    
    Set RsPromociones = FrmAppLink.CnAdm.Execute( _
    "SELECT TOP 1 * FROM TMP_PROMOCION " & _
    "WHERE CodUsuario = '" & QuitarComillasSimples(FrmAppLink.GetCodUsuario) & "' " & _
    "AND Cod_Promocion = '" & QuitarComillasSimples(mCodPromo_Actual) & "' ")
    
    With RsPromociones
        If Not .EOF Then
            TipoPromo = !Tipo_Promocion
            EventoProgramado = True
                ListSafeItemSelection cmbCampanna, !Campa�a
            EventoProgramado = False
        End If
    End With
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ")."
    
End Sub

Private Sub cbx_Sucursal_Click()
    Call cbx_Sucursal_LostFocus
End Sub

Private Sub cbx_Sucursal_LostFocus()
    
    Dim Sucursal As New ADODB.Recordset
    
    If cbx_Sucursal.ListIndex <> 0 Then
    
        Sucursal.Open _
        "Select * from MA_SUCURSALES " & _
        "where c_codigo = '" & FixTSQL(cbx_Sucursal.Text) & "' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not Sucursal.EOF Then
            
            txt_sucursaldesc.Caption = Sucursal!c_Descripcion
            
        End If
        
    Else
        
        txt_sucursaldesc.Caption = Empty
        
    End If
    
    Sucursal.Close
    
End Sub

Private Sub CCriterio(Index As Integer)
    
    Dim mSQL As String, mSQLDetallado As String
    Dim mWhere As String
    Dim RsReporte As New ADODB.Recordset
    
    Dim HeadCriterio As String
    Dim ListaPromociones As Variant, ListaPromocionestipo As Variant, ListaPromocionesIn As String
    Dim ListaLocalidades As Variant, ListaLocalidadesIn As String
    Dim ListaCampana As Variant, ListaCampanaIn As String
    
    HeadCriterio = "" & StellarMensaje(16002) & " " & SDate(Fechalow.Value) & " " & _
    StellarMensaje(3046) & ": " & SDate(Fechahigh.Value) & " "
    
    If modo.ListIndex = 0 Then 'Text = "Resumido" Then
        
        mSQL = "SELECT p.* " & _
        "FROM MA_PROMOCION P " & _
        "WHERE p.Fecha_Inicio between '" & FechaBD(Fechalow.Value) & "' " & _
        "AND '" & FechaBD(EndOfDay(Fechahigh.Value), FBD_FULL) & "' "
        
    Else
        
        mSQL = "SELECT p.* " & _
        "FROM MA_PROMOCION P " & _
        "WHERE p.Fecha_Inicio between '" & FechaBD(Fechalow.Value) & "' " & _
        "AND '" & FechaBD(EndOfDay(Fechahigh.Value), FBD_FULL) & "' "
        
        mSQLDetallado = "SELECT P.*, pp.c_Descri, pp.c_Marca, " & _
        "md.c_Descripcio AS Departamento, pv.Precio_Oferta, pv.Monto_Descuento, " & _
        "pv.Porcentaje_Descuento1, pv.Porcentaje_Descuento2, " & _
        "isNULL(pxpar.c_descripcio, '') as DescripcionProveedor, " & _
        "CASE WHEN pc.Tipo_Condicion = 1 OR pc.Tipo_Condicion = 9 Then 'Condicion' " & _
        "WHEN pc.Tipo_Condicion = 2 OR pc.Tipo_Condicion = 10 Then 'Exclusion' " & _
        "WHEN pc.Tipo_Condicion = 3 Then 'Premio' " & _
        "WHEN pc.Tipo_Condicion = 4 Then 'Exclusion de Premio' " & _
        "WHEN pc.Tipo_Condicion = 11 THEN 'Permitido en Consumo' " & _
        "WHEN pc.Tipo_Condicion = 12 THEN 'Restringido en Consumo' END as TipoCondicion " & _
        "FROM MA_PROMOCION P " & _
        "INNER JOIN TR_PROMOCION_CONDICION pc " & _
        "ON pc.Cod_Promocion = p.Cod_Promocion " & _
        "INNER JOIN MA_PRODUCTOS pp " & _
        "ON pp.c_Codigo = pc.Cod_Producto " & _
        "LEFT JOIN MA_PROVEEDORES pxpar " & _
        "ON pxpar.c_codproveed = PC.Cod_Proveedor " & _
        "INNER JOIN TR_PROMOCION_VALORES pv " & _
        "ON pv.Cod_Promocion = p.Cod_Promocion " & _
        "AND pv.Linea_Valor = pc.Linea_Valor " & _
        "LEFT JOIN MA_DEPARTAMENTOS MD " & _
        "ON MD.C_CODIGO = Pc.Cod_Dpto " & _
        "WHERE p.Fecha_Inicio between '" & FechaBD(Fechalow.Value) & "' " & _
        "AND '" & FechaBD(EndOfDay(Fechahigh.Value), FBD_FULL) & "' " & _
        "AND pc.Tipo_Condicion IN (1, 2, 3, 4, 9, 10, 11, 12)  "
        
    End If
    
    If Trim(txt_Sucursal.Text) <> Empty Then
        
        Dim ListaStr As String
        
        txtLocalidades = QuitarComillasSimples(txt_Sucursal.Text)
        
        ListaLocalidadesIn = Empty
        ListaLocalidades = Split(txt_Sucursal.Text, ",")
        
        For Each Item In AsEnumerable(ListaLocalidades)
            ListaLocalidadesIn = ListaLocalidadesIn & "," & "'" & Item & "'"
        Next
        
        If Len(ListaLocalidadesIn) > 0 Then
            mWhere = mWhere & " AND p.Cod_Promocion IN ( " & _
            "SELECT DISTINCT ps.Cod_Promocion " & _
            "FROM MA_PROMOCION_SUCURSAL ps " & _
            "WHERE ps.Cod_Localidad IN (" & Mid(ListaLocalidadesIn, 2) & ")) " & vbNewLine
        End If
        
        For Each Item In Arr_Localidades
            ListaStr = ListaStr & ", " & Item(1)
        Next
        
        ListaStr = Mid(ListaStr, 3)
        
        HeadCriterio = HeadCriterio & ", " & StellarMensaje(212) & ": " & ListaStr & " "
        
    End If
    
    If Trim(txtTipoPromocion.Text) <> Empty Then
        
        Dim ListaTipoPromo As String
        
        txtTipoPromocion = QuitarComillasSimples(txtTipoPromocion.Text)
        
        ListaPromocionesIn = Empty
        ListaPromocionestipo = Split(txtTipoPromocion.Text, ",")
        
        For Each Item In AsEnumerable(ListaPromocionestipo)
            ListaPromocionesIn = ListaPromocionesIn & "," & "'" & Item & "'"
        Next
        
        If Len(ListaPromocionesIn) > 0 Then
           mWhere = mWhere & "AND p.Tipo_Promocion IN (" & Mid(ListaPromocionesIn, 2) & ") " & vbNewLine
        End If
        
        For Each Item In Arr_TipoPromo
           ListaTipoPromo = ListaTipoPromo & vbNewLine & Item(1)
        Next
        
        ListaTipoPromo = Mid(ListaTipoPromo, 3)
        
        HeadCriterio = HeadCriterio & ", " & StellarMensaje(4161) & ": " & ListaTipoPromo & " "
        
    End If
    
    If Trim(TxtPromocion.Text) <> Empty Then
        
        Dim ListaPromo As String
        
        TxtPromocion = QuitarComillasSimples(TxtPromocion.Text)
        
        ListaPromocionesIn = Empty
        ListaPromociones = Split(TxtPromocion.Text, ",")
        
        For Each Item In AsEnumerable(ListaPromociones)
            ListaPromocionesIn = ListaPromocionesIn & "," & "'" & Item & "'"
        Next
        
        If Len(ListaPromocionesIn) > 0 Then
            mWhere = mWhere & "AND p.Cod_Promocion IN (" & Mid(ListaPromocionesIn, 2) & ") " & vbNewLine
        End If
        
        For Each Item In Arr_Promociones
            ListaPromo = ListaPromo & ", " & Item(1)
        Next
        
        ListaPromo = Mid(ListaPromo, 3)
        
        HeadCriterio = HeadCriterio & ", " & StellarMensaje(4162) & ": " & ListaPromo & " "
        
    End If
    
    If cmbItemBusqueda.Text <> Empty Then
        
        Select Case Me.cmbItemBusqueda.ListIndex
            Case 0 ' Mostrar Activas
                mWhere = mWhere & "AND (p.Estatus = 'DPE')"
            Case 1 ' Mostrar Completadas
                mWhere = mWhere & "AND (p.Estatus = 'DCO')"
            Case 2 ' Mostrar Anuladas
                mWhere = mWhere & "AND (p.Estatus = 'ANU')"
            Case 3
                mWhere = mWhere & "AND (p.Estatus = 'DWT')"
            Case cmbItemBusqueda.ListCount - 1 ' Mostrar Todos
                mWhere = mWhere & "AND  1 = 1"
        End Select
        
        HeadCriterio = HeadCriterio & ", " & StellarMensaje(6086) & ": " & cmbItemBusqueda.Text & " "
        
    End If
    
    If Trim(cmbCampanna.Text) <> Empty Then
        
        'Dim ListaCampana As String
        Dim ListaCampanass As String
        
        TxtCampana = QuitarComillasSimples(cmbCampanna.Text)
        
        ListaCampanaIn = Empty
        ListaCampana = Split(cmbCampanna.Text, ",")
        
        For Each Item In AsEnumerable(ListaCampana)
            ListaCampanaIn = ListaCampanaIn & "," & "'" & Item & "'"
        Next
        
        If Len(ListaCampanaIn) > 0 Then
           mWhere = mWhere & "AND p.Campa�a IN (" & Mid(ListaCampanaIn, 2) & ") " & vbNewLine
        End If
        
        For Each Item In Arr_Campanas
            ListaCampanass = ListaCampanass & ", " & Item(0)
        Next
        
        ListaCampanass = Mid(ListaCampanass, 3)
        
        HeadCriterio = HeadCriterio & ", " & StellarMensaje(4150) & ": " & cmbCampanna.Text & " "
        
    End If
    
    If modo.ListIndex = 0 Then
        
        SQL = mSQL & mWhere
        
        RsReporte.Open SQL, Ent.BDD, adOpenStatic, adLockReadOnly
        
        If Not RsReporte.EOF Then
            
            Call REPO_CABE(DR_ListadOfertasyPromoRes, _
            StellarMensaje(4152) & " - " & modo.Text) '"Listado de Ofertas y Promociones")
            
            DR_ListadOfertasyPromoRes.Sections("enc_pag").Controls("lbl_criterio").Caption = HeadCriterio
            DR_ListadOfertasyPromoRes.Sections("enc_pag").Controls("lbl_criterio").CanGrow = True
            
            Set DR_ListadOfertasyPromoRes.DataSource = RsReporte
            
            Select Case Index
                Case 0
                   DR_ListadOfertasyPromoRes.Show vbModal
                Case 1
                   DR_ListadOfertasyPromoRes.PrintReport True
                Case 2
            'RsEureka
                Call GenerarXML(RsReporte)
            End Select
            
        Else
            
            Mensaje True, StellarMensaje(16441) '"No se encontraron resultados con los criterios ingresados."
            
        End If
        
        RefreshForm Me
        
    Else
        
        mSQL = mSQL & mWhere
        mSQLDetallado = mSQLDetallado & mWhere
        
'       Debug.Print SQL
        
        Criterio = ShapeStrAdvGC(mSQL, "MA_PROMOCION", _
        mSQLDetallado, "TRDETALLE", "Cod_Promocion", "Cod_Promocion", "TRDETALLE")
        
'       Debug.Print Criterio
        
        If Ent.SHAPE_ADM.State = adStateOpen Then
            Ent.SHAPE_ADM.Close
        End If
        
        Ent.SHAPE_ADM.Open
        
        RsReporte.Open Criterio, Ent.SHAPE_ADM, adOpenStatic, adLockReadOnly
   
        If Not RsReporte.EOF Then
            
            Call REPO_CABE(DR_ListadOfertasyPromoDet, StellarMensaje(4152) & " - " & modo.Text) '"Listado de Ofertas y Promociones Detallado")
            
            DR_ListadOfertasyPromoDet.Sections("enc_pag").Controls("lbl_criterio").Caption = HeadCriterio
            DR_ListadOfertasyPromoDet.Sections("enc_pag").Controls("lbl_criterio").CanGrow = True
            
            Set DR_ListadOfertasyPromoDet.DataSource = RsReporte
            
            Select Case Index
                Case 0
                   DR_ListadOfertasyPromoDet.Show vbModal
                Case 1
                   DR_ListadOfertasyPromoDet.PrintReport True
                Case 2
            'RsEureka
                Call GenerarXML(RsReporte)
            End Select
            
            RefreshForm Me
            
        Else
            
            Mensaje True, StellarMensaje(16441) '"No se encontraron resultados con los criterios ingresados."
            
        End If
        
        RefreshForm Me
        
   End If
        
End Sub

Public Sub REPO_CABE(ByRef Reporte, Titulo, Optional pMayuscula As Boolean = True)
    
    On Error Resume Next
    
    Dim Rs As New ADODB.Recordset, RsSucursal As New ADODB.Recordset
    Dim pSucursal As String
    
    Call Apertura_Recordset(Rs)
    
    Rs.Open "select * from " & FrmAppLink.Srv_Remote_BD_ADM & ".dbo.ESTRUC_SIS", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Call Apertura_Recordset(RsSucursal)
    
    'Willians
    pSucursal = sGetIni(FrmAppLink.GetSetup, "Branch", "branch", "?")
    
    RsSucursal.Open _
    "SELECT * FROM MA_SUCURSALES " & _
    "WHERE c_Codigo = '" & pSucursal & "' ", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    'Far.strEmpresa = rs!nom_org
    
    Reporte.Caption = Titulo
    
    If Not Rs.EOF Then
        Reporte.Sections("enc_pag").Controls("lbl_empresa").Caption = Rs!nom_org
        Reporte.Sections("enc_pag").Controls("lbl_tit_rep").Caption = IIf(pMayuscula, UCase(Titulo), Titulo)
        'Reporte.Sections("enc_pag").Controls("paginas").Caption = "Pg %p de %P"
        Reporte.Sections("enc_pag").Controls("paginas").Caption = "Pg %p " & FrmAppLink.StellarMensaje(15519) & " %P"
        Reporte.Sections("enc_pag").Controls("lblFechaEmisionRep").Caption = FrmAppLink.StellarMensaje(2523) & ":"
    End If
    
    If Not RsSucursal.EOF Then
        'Reporte.Sections("enc_pag").Controls("lbl_localidad").Caption = "Localidad:" & RsSucursal!c_Descripcion
        Reporte.Sections("enc_pag").Controls("lbl_localidad").Caption = _
        FrmAppLink.StellarMensaje(8024) & ":" & RsSucursal!c_Descripcion
    End If
    
    Call Cerrar_Recordset(Rs)
    Call Cerrar_Recordset(RsSucursal)
    
    Err.Clear
    
End Sub

Private Sub txt_Sucursal_Click()
   If Arr_Localidades Is Nothing Then
        
   Else
        
        Dim ListaStr As String
        
        For Each Item In Arr_Localidades
            ListaStr = ListaStr & vbNewLine & Item(1)
        Next
        
        If Len(ListaStr) > 0 Then
            
            FrmAppLink.ShowTooltip "" & "" & StellarMensaje(2077) & ": " & GetLines(2) & ListaStr & "", _
            3500, 3000, txt_Sucursal, 0, _
            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True), "**[RefreshForm]**", , 0
            
            RefreshForm Me
            
        End If
        
    End If
End Sub

Private Sub txt_sucursal_KeyDown(KeyCode As Integer, Shift As Integer)
  
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            BaseSelect = "SELECT c_Codigo, c_Descripcion FROM MA_SUCURSALES "
            
            Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
            
            With Frm_Super_Consultas
                
                .Inicializar BaseSelect, StellarMensaje(1251), FrmAppLink.CnAdm, , True
                
                .Add_ItemLabels StellarMensaje(142), "c_Codigo", 2000, 0
                .Add_ItemLabels StellarMensaje(143), "c_Descripcion", 9000, 0
                
                .Add_ItemSearching StellarMensaje(143), "c_Descripcion"
                .Add_ItemSearching StellarMensaje(142), "c_Codigo"
                
                .StrOrderBy = "c_Descripcion"
                
                .BusquedaInstantanea = True
                .DespliegueInstantaneo_RetornarCualquierValor = False
                .ModoDespliegueInstantaneo = True
                
                .CustomFontSize = 16
                
                .Show vbModal
                
                Set Arr_Localidades = .ArrResultado
                
                If Arr_Localidades Is Nothing Then
                    txt_Sucursal.Text = Empty
'                    ChkPorLocalidad.Value = vbUnchecked
                    RefreshForm Me
                Else
                    
                    Dim ListaStr As String
                    
                    For Each Item In Arr_Localidades
                        ListaStr = ListaStr & "," & Item(0)
                    Next
                    
                    ListaStr = Mid(ListaStr, 2)
                    
                    txt_Sucursal.Text = ListaStr
                    
                    txt_Sucursal_Click
                    
'                    ChkPorLocalidad.Value = vbChecked
                    
                End If
            
            End With
            
            Set Frm_Super_Consultas = Nothing
            
    End Select
    
    
End Sub

Private Sub TxtPromocion_Click()
    
    If Arr_Promociones Is Nothing Then
        
    Else
        
        Dim ListaStr As String
        
        For Each Item In Arr_Promociones
            ListaStr = ListaStr & vbNewLine & Item(1)
        Next
        
        If Len(ListaStr) > 0 Then
            
            FrmAppLink.ShowTooltip "" & "" & StellarMensaje(4163) & ": " & GetLines(2) & ListaStr & "", _
            3500, 3000, TxtPromocion, 0, _
            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True), "**[RefreshForm]**", , 0
            
            RefreshForm Me
            
        End If
        
    End If
    
End Sub

Private Sub TxtPromocion_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            BaseSelect = _
            "SELECT Cod_Promocion, Descripcion, Campa�a, Fecha_Inicio, Fecha_Fin " & _
            "FROM MA_PROMOCION " & _
            "WHERE 1 = 1 " '& _
            'IIf(txtFechaIni.Text <> Empty, _
            "AND Fecha_Inicio >= '" & FechaBD(txtFechaIni.Tag, FBD_FULL) & "' ", Empty) & _
            'IIf(txtFechaFin.Text <> Empty, _
            "AND Fecha_Fin <= '" & FechaBD(txtFechaFin.Tag, FBD_FULL) & "' ", Empty)
            
            Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
            
            With Frm_Super_Consultas
                
                .Inicializar BaseSelect, "" & StellarMensaje(1009) & "", FrmAppLink.CnAdm, , True
                
                .Add_ItemLabels "" & "" & StellarMensaje(142) & "" & "", "Cod_Promocion", 2200, 0
                .Add_ItemLabels "" & "" & StellarMensaje(143) & "" & "", "Descripcion", 2800, 0
                .Add_ItemLabels "" & "" & StellarMensaje(4150) & "" & "", "Campa�a", 2450, 0
                .Add_ItemLabels "" & "" & StellarMensaje(2024) & "" & "", "Fecha_Inicio", 1830, 0
                .Add_ItemLabels "" & "" & StellarMensaje(2025) & "" & "", "Fecha_Fin", 1820, 0
                
                .Add_ItemSearching "" & StellarMensaje(143) & "", "Descripcion"
                .Add_ItemSearching "" & StellarMensaje(4150) & "", "Campa�a"
                .Add_ItemSearching "" & StellarMensaje(142) & "", "Cod_Promocion"
                .Add_ItemSearching "" & StellarMensaje(2024) & " YYYY-MM-DD", "CONVERT(NVARCHAR(MAX), Fecha_Inicio, 23)"
                
                .ModoDespliegueInstantaneo = True
                .DespliegueInstantaneo_RetornarCualquierValor = False
                .BusquedaInstantanea = True
                
                .StrOrderBy = "Fecha_Inicio DESC"
                
                .CustomFontSize = 14
                
                .Show vbModal
                
                Set Arr_Promociones = .ArrResultado
                
                If Arr_Promociones Is Nothing Then
                    TxtPromocion.Text = Empty
                    RefreshForm Me
                Else
                    
                    Dim ListaStr As String
                    
                    For Each Item In Arr_Promociones
                        ListaStr = ListaStr & "," & Item(0)
                    Next
                    
                    ListaStr = Mid(ListaStr, 2)
                    
                    TxtPromocion.Text = ListaStr
                    
                    TxtPromocion_Click
                    
                    
                End If
                
            End With
            
            Set Frm_Super_Consultas = Nothing
            
    End Select
    
End Sub

Private Sub cmbCampanna_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            BaseSelect = _
            "SELECT cs_Grupo " & _
            "FROM MA_AUX_GRUPO " & _
            "WHERE cs_Tipo = 'CAMP' "
            
            Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
            
            With Frm_Super_Consultas
                
                .Inicializar BaseSelect, "" & StellarMensaje(4150) & "", FrmAppLink.CnAdm, , True
                .Add_ItemLabels "" & "" & StellarMensaje(143) & "" & "", "cs_Grupo", 11000, 0
'
                .Add_ItemSearching "" & StellarMensaje(4150) & "", "cs_Grupo"
                
                .ModoDespliegueInstantaneo = True
                .DespliegueInstantaneo_RetornarCualquierValor = False
                .BusquedaInstantanea = True
                
                .StrOrderBy = "cs_Grupo DESC"
                
                .CustomFontSize = 14
                
                .Show vbModal
                
                Set Arr_Campanas = .ArrResultado
                
                If Arr_Campanas Is Nothing Then
                    cmbCampanna.Text = Empty
                    RefreshForm Me
                Else
                    
                    Dim ListaStr As String
                    
                    For Each Item In Arr_Campanas
                        ListaStr = ListaStr & "," & Item(0)
                    Next
                    
                    ListaStr = Mid(ListaStr, 2)
                    
                    cmbCampanna.Text = ListaStr
                    
                    cmbCampanna_Click
                    
                    
                End If
                
            End With
            
            Set Frm_Super_Consultas = Nothing
            
    End Select
    
End Sub

Private Sub txtTipoPromocion_Click()
    
    If Arr_TipoPromo Is Nothing Then
        
    Else
        
        Dim ListaStr As String
        
        For Each Item In Arr_TipoPromo
            ListaStr = ListaStr & vbNewLine & Item(1)
        Next
        
        If Len(ListaStr) > 0 Then
            
            FrmAppLink.ShowTooltip "" & "" & StellarMensaje(4161) & ": " & GetLines(2) & ListaStr & "", _
            3500, 3000, txtTipoPromocion, 0, _
            &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True), "**[RefreshForm]**", , 0
            
            RefreshForm Me
            
        End If
        
    End If
    
End Sub

Private Sub GenerarXML(Rs As ADODB.Recordset)
    
    On Error GoTo Error
    
    Dim RutaGenerarXML As String
    Dim Campos As Dictionary: Set Campos = New Dictionary
    Dim TmpCount As Long: TmpCount = -1
    
    rutaXml.Filter = "XML (*.xml)"
    rutaXml.DefaultExt = "xml"
    
    rutaXml.CancelError = True
    
    rutaXml.ShowSave
    
    RutaGenerarXML = rutaXml.FileName
      
    Rs.MoveFirst
    
    If modo.ListIndex = 0 Then '.Text = "Resumido" Then
    
        TmpCount = TmpCount + 1
        Campos(TmpCount) = Array("Cod_Promocion", XMLHeader(StellarMensaje(142), False))
        
        TmpCount = TmpCount + 1
        Campos(TmpCount) = Array("Descripcion", XMLHeader(StellarMensaje(143), False))
        
        TmpCount = TmpCount + 1
        Campos(TmpCount) = Array("Fecha_Inicio", XMLHeader(StellarMensaje(2024), False))
        
        TmpCount = TmpCount + 1
        Campos(TmpCount) = Array("Fecha_Fin", XMLHeader(StellarMensaje(2025), False))
        
        TmpCount = TmpCount + 1
        Campos(TmpCount) = Array("Campa�a", XMLHeader(StellarMensaje(4150), False))
        
        TmpCount = TmpCount + 1
        Campos(TmpCount) = Array("Monto_Minimo_Venta", XMLHeader(StellarMensaje(4164), False))
        
        TmpCount = TmpCount + 1
        Campos(TmpCount) = Array("Monto_Maximo_Venta", XMLHeader(StellarMensaje(4165), False))
        
    Else
    
        TmpCount = TmpCount + 1
        Campos(TmpCount) = Array("Cod_Promocion", XMLHeader(StellarMensaje(4162), False))
        
        TmpCount = TmpCount + 1
        Campos(TmpCount) = Array("Descripcion", XMLHeader(StellarMensaje(143), False))
        
        TmpCount = TmpCount + 1
        Campos(TmpCount) = Array("Campa�a", XMLHeader(StellarMensaje(4150), False))
        
        TmpCount = TmpCount + 1
        Campos(TmpCount) = Array("c_Descri_TRDETALLE", XMLHeader(StellarMensaje(5508), False))
        
        TmpCount = TmpCount + 1
        Campos(TmpCount) = Array("c_Marca_TRDETALLE", XMLHeader(StellarMensaje(3022), False))
        
        TmpCount = TmpCount + 1
        Campos(TmpCount) = Array("DescripcionProveedor_TRDETALLE", XMLHeader(StellarMensaje(16446), False))
        
        TmpCount = TmpCount + 1
        Campos(TmpCount) = Array("Precio_Oferta_TRDETALLE", XMLHeader(StellarMensaje(16576), False))
        
        TmpCount = TmpCount + 1
        Campos(TmpCount) = Array("Monto_Descuento_TRDETALLE", XMLHeader(StellarMensaje(4166), False))
        
        TmpCount = TmpCount + 1
        Campos(TmpCount) = Array("Porcentaje_Descuento1_TRDETALLE", XMLHeader(StellarMensaje(5568) & "_1", False))
        
        TmpCount = TmpCount + 1
        Campos(TmpCount) = Array("Porcentaje_Descuento2_TRDETALLE", XMLHeader(StellarMensaje(5568) & "_2", False))
        
        TmpCount = TmpCount + 1
        Campos(TmpCount) = Array("TipoCondicion_TRDETALLE", XMLHeader(StellarMensaje(4167), False))
        
    End If
    
    Call FrmAppLink.Crear_Xml_Definido(Rs, RutaGenerarXML, Campos, True)
    
    CmdOpenXML.Visible = True
    
    'ShowTooltip "Click Izq. -> Abrir con Excel" & GetLines & _
    "Click Med. -> Abrir con aplicaci�n predeterminada para archivos XML" & GetLines & _
    "Click Der. -> Ocultar el bot�n." _
    & GetLines & Space(21) & "|" & GetLines & Space(21) & "V" & _
    GetLines, CmdOpenXML.Width * 5, 7500, CmdOpenXML, 1
    ShowTooltip Replace(Replace(StellarMensaje(475), _
    "$(Line)", vbNewLine), "$(Spaces)", Space(21)), _
    CmdOpenXML.Width * 5, 7500, CmdOpenXML, 1, , , , , "**[RefreshForm]**"
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    If mErrorNumber <> 32755 Then
        MsjErrorRapido mErrorDesc
    End If
    
    RefreshForm Me
    
End Sub

Private Sub txtTipoPromocion_KeyDown(KeyCode As Integer, Shift As Integer)
   
   Select Case KeyCode
        
        Case Is = vbKeyF2
            
            BaseSelect = _
            "SELECT 1 AS TipoPromo, '" & "" & StellarMensaje(16576) & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 2 AS TipoPromo, '" & "" & StellarMensaje(5513) & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 3 AS TipoPromo, '" & "" & StellarMensaje(2587) & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 4 AS TipoPromo, '" & "" & "M x N" & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 5 AS TipoPromo, '" & "" & "" & StellarMensaje(4168) & "" & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 6 AS TipoPromo, '" & "" & "" & StellarMensaje(4169) & "" & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 7 AS TipoPromo, '" & "" & "" & StellarMensaje(4170) & "" & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 8 AS TipoPromo, '" & "" & "" & StellarMensaje(4171) & "" & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 9 AS TipoPromo, '" & "" & "" & StellarMensaje(4172) & "" & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 10 AS TipoPromo, '" & "" & "" & StellarMensaje(4173) & "" & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 11 AS TipoPromo, '" & "" & "" & StellarMensaje(4174) & "" & "" & "' AS Descripcion " & vbNewLine
            
            BaseSelect = BaseSelect & _
            "UNION ALL " & vbNewLine & _
            "SELECT 16 AS TipoPromo, '" & "" & "" & StellarMensaje(4175) & "" & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 17 AS TipoPromo, '" & "" & "" & StellarMensaje(4176) & "" & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 18 AS TipoPromo, '" & "" & "" & StellarMensaje(4177) & "" & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 19 AS TipoPromo, '" & "" & "" & StellarMensaje(4178) & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 20 AS TipoPromo, '" & "" & "" & StellarMensaje(4179) & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 21 AS TipoPromo, '" & "" & "" & StellarMensaje(4180) & "" & "' AS Descripcion " & vbNewLine
            
            BaseSelect = BaseSelect & _
            "UNION ALL " & vbNewLine & _
            "SELECT 30 AS TipoPromo, '" & "" & "" & StellarMensaje(4181) & "" & "" & "' AS Descripcion " & vbNewLine & _
            "UNION ALL " & vbNewLine & _
            "SELECT 31 AS TipoPromo, '" & "" & "" & StellarMensaje(4182) & "" & "" & "' AS Descripcion " & vbNewLine
            
            BaseSelect = BaseSelect & _
            "SELECT * FROM ( " & vbNewLine & _
            BaseSelect & _
            ") TB " & vbNewLine
            
            Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
            
            With Frm_Super_Consultas
                
                .Inicializar BaseSelect, "" & "Tipos de Promoci�n" & "", FrmAppLink.CnAdm, , True
                
                .Add_ItemLabels "" & "" & StellarMensaje(4161) & "", "TipoPromo", 2500, 0
                .Add_ItemLabels "" & "" & StellarMensaje(143) & "" & "", "Descripcion", 7500, 0
                
                .Add_ItemSearching "" & StellarMensaje(143) & "", "Descripcion"
                
                .ModoDespliegueInstantaneo = True
                .DespliegueInstantaneo_RetornarCualquierValor = False
                .BusquedaInstantanea = True
                
                .StrOrderBy = "TipoPromo ASC"
                
                .CustomFontSize = 14
                
                .Show vbModal
                
                Set Arr_TipoPromo = .ArrResultado
                
                If Arr_TipoPromo Is Nothing Then
                    txtTipoPromocion.Text = Empty
                    'ChkPorTipoPromocion.Value = vbUnchecked
                    RefreshForm Me
                Else
                    
                    Dim ListaStr As String
                    
                    For Each Item In Arr_TipoPromo
                        ListaStr = ListaStr & "," & Item(0)
                    Next
                    
                    ListaStr = Mid(ListaStr, 2)
                    
                    txtTipoPromocion.Text = ListaStr
                    
                    txtTipoPromocion_Click
                    
                    'ChkPorTipoPromocion.Value = vbChecked
                    
                End If
                
            End With
            
            Set Frm_Super_Consultas = Nothing
            
    End Select
    
End Sub

Private Sub CmdCampana_Click()
    Call cmbCampanna_KeyDown(vbKeyF2, 0)
End Sub
