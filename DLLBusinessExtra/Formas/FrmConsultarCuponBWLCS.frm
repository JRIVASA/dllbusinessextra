VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RichTx32.ocx"
Begin VB.Form FrmConsultarCuponBWLCS 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   10275
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   9330
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10275
   ScaleWidth      =   9330
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin VB.CommandButton CmdCambiarFechas 
      Caption         =   "Cambiar Fechas"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   4200
      MouseIcon       =   "FrmConsultarCuponBWLCS.frx":0000
      MousePointer    =   99  'Custom
      Picture         =   "FrmConsultarCuponBWLCS.frx":030A
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   9000
      Width           =   1305
   End
   Begin VB.CommandButton CmdAnular 
      Caption         =   "Anular"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   2760
      MouseIcon       =   "FrmConsultarCuponBWLCS.frx":0FD4
      MousePointer    =   99  'Custom
      Picture         =   "FrmConsultarCuponBWLCS.frx":12DE
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   9000
      Width           =   1305
   End
   Begin VB.CommandButton CmdBuscarPerfilCupon 
      Appearance      =   0  'Flat
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   765
      Left            =   7320
      MouseIcon       =   "FrmConsultarCuponBWLCS.frx":3060
      MousePointer    =   99  'Custom
      Picture         =   "FrmConsultarCuponBWLCS.frx":336A
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   960
      Width           =   750
   End
   Begin VB.TextBox txtPerfilCupon 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   555
      IMEMode         =   3  'DISABLE
      Left            =   3000
      Locked          =   -1  'True
      TabIndex        =   0
      Top             =   1080
      Width           =   4080
   End
   Begin VB.CommandButton Cancelar 
      Caption         =   "Limpiar"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   1320
      MouseIcon       =   "FrmConsultarCuponBWLCS.frx":47B4
      MousePointer    =   99  'Custom
      Picture         =   "FrmConsultarCuponBWLCS.frx":4ABE
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   9000
      Width           =   1305
   End
   Begin VB.CommandButton Salir 
      Appearance      =   0  'Flat
      Caption         =   "#s"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1005
      Left            =   6960
      MousePointer    =   99  'Custom
      Picture         =   "FrmConsultarCuponBWLCS.frx":6840
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   9000
      Width           =   1230
   End
   Begin VB.CommandButton CmdCambiarMonto 
      Appearance      =   0  'Flat
      Caption         =   "Cambio de Valor"
      Height          =   1005
      Left            =   5640
      Picture         =   "FrmConsultarCuponBWLCS.frx":7C8A
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   9000
      Width           =   1230
   End
   Begin VB.CommandButton CmdBuscarCupon 
      Appearance      =   0  'Flat
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   765
      Left            =   7320
      MouseIcon       =   "FrmConsultarCuponBWLCS.frx":9A0C
      MousePointer    =   99  'Custom
      Picture         =   "FrmConsultarCuponBWLCS.frx":9D16
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   2040
      Width           =   750
   End
   Begin VB.TextBox txtSerial 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   555
      IMEMode         =   3  'DISABLE
      Left            =   3000
      TabIndex        =   2
      Top             =   2160
      Width           =   4080
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   9315
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   8640
         Picture         =   "FrmConsultarCuponBWLCS.frx":B160
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "BWLCS - Administrar Cup�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   105
         Width           =   3615
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   6315
         TabIndex        =   11
         Top             =   105
         Width           =   1815
      End
   End
   Begin RichTextLib.RichTextBox RichText 
      Height          =   5175
      Left            =   840
      TabIndex        =   4
      Top             =   3480
      Width           =   7800
      _ExtentX        =   13758
      _ExtentY        =   9128
      _Version        =   393217
      Enabled         =   -1  'True
      ReadOnly        =   -1  'True
      ScrollBars      =   3
      AutoVerbMenu    =   -1  'True
      TextRTF         =   $"FrmConsultarCuponBWLCS.frx":CEE2
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   15
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblPerfilCupon 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Tipo de Cup�n:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   285
      Left            =   1005
      TabIndex        =   14
      Top             =   1200
      Width           =   1665
   End
   Begin VB.Image CmdTeclado 
      Height          =   720
      Left            =   4440
      MouseIcon       =   "FrmConsultarCuponBWLCS.frx":CF62
      MousePointer    =   99  'Custom
      Picture         =   "FrmConsultarCuponBWLCS.frx":D26C
      Stretch         =   -1  'True
      Top             =   2760
      Width           =   840
   End
   Begin VB.Label lblSerial 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Serial del Cup�n:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   285
      Left            =   885
      TabIndex        =   13
      Top             =   2280
      Width           =   1830
   End
End
Attribute VB_Name = "FrmConsultarCuponBWLCS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private ErrorCargando As Boolean
Private ErrorNoDB As Boolean

Private mCnBWLCS As ADODB.Connection

Private mCnStr As String

Private mRs As ADODB.Recordset

Private FormaCargada As Boolean

Private TmpDBCode   As String

Private TmpIDSistema As String

Private TmpFechaIni As Date
Private TmpFechaFin As Date

Private TmpMontoBase As Variant
Private TmpMontoUsado As Variant
Private TmpMontoRestante As Variant

Private TmpCodStatus As String

Private Sub CmdBuscarCupon_Click()
    
    On Error GoTo Error
    
    If Trim(txtPerfilCupon.Tag) <> Empty And Trim(txtSerial.Text) <> Empty Then
        
        txtSerial.Text = QuitarComillasSimples(txtSerial.Text)
        
        ValidarConexion mCnBWLCS, mCnStr, mCnBWLCS.ConnectionTimeout
        
        Set mRs = New ADODB.Recordset
        
        Set mRs = mCnBWLCS.Execute("SELECT *, GetDate() AS FechaSrv, " & _
        "CAST(CreationDate AS DateTime) AS CreateDate1, " & _
        "CAST(ValidityDateStart AS DateTime) As ValidDate1, " & _
        "CAST(ValidityDateEnd AS DateTime) AS ValidDate2 " & _
        "FROM Coupon_Ma " & _
        "WHERE 1 = 1 " & _
        "AND CouponCode = '" & txtPerfilCupon.Tag & "' " & _
        "AND CouponNumber = '" & txtSerial.Text & "' ")
        
        If Not mRs.EOF Then
            
            Dim mText As String
            
            mText = Empty
            
            mText = mText & "" & StellarMensajeLocal(101) & "" & vbNewLine & vbNewLine
            
            TmpIDSistema = CDec(mRs!SystemNumber)
            
            mText = mText & "" & StellarMensajeLocal(102) & ": " & CDec(TmpIDSistema) & vbNewLine
            
            mLocalidad = FrmAppLink.BuscarNombreSucursal(mRs!LocationCodeOrigin)
            
            mText = mText & "" & StellarMensajeLocal(103) & ": " & mRs!LocationCodeOrigin & _
            IIf(mLocalidad <> Empty, " - " & mLocalidad, Empty) & vbNewLine
            
            mText = mText & "" & StellarMensaje(10047) & ": " & mRs!POSCodeOrigin & vbNewLine
            
            mText = mText & "" & StellarMensaje(3048) & ": " & GDate(mRs!CreateDate1) & vbNewLine
            
            TmpFechaIni = mRs!ValidDate1
            TmpFechaFin = mRs!ValidDate2
            
            mText = mText & "" & StellarMensajeLocal(104) & ": " & GDate(TmpFechaIni) & vbNewLine
            mText = mText & "" & StellarMensajeLocal(105) & ": " & GDate(TmpFechaFin) & vbNewLine
            
            If Trim(mRs!CurrencyCodeISO) <> Empty Then
                mText = mText & "" & StellarMensajeLocal(152) & ": " & mRs!CurrencyCodeISO & vbNewLine
            End If
            
            Set MonedaTmp = FrmAppLink.ClaseMonedaTemp
            
            If Trim(mRs!CurrencyCodeSystem) <> Empty Then
                
                If MonedaTmp.BuscarMonedas(, mRs!CurrencyCodeSystem) Then
                    mText = mText & "" & StellarMensajeLocal(106) & ": " & MonedaTmp.DesMoneda & _
                    " [" & MonedaTmp.CodMoneda & "]" & vbNewLine
                End If
                
            Else
                
                MonedaTmp.BuscarMonedas , FrmAppLink.CodMonedaPref
                
            End If
            
            TmpMontoBase = CDec(mRs!CurrencyBaseAmount)
            TmpMontoUsado = CDec(mRs!CurrencyUsedAmount)
            TmpMontoRestante = CDec(TmpMontoBase - TmpMontoUsado)
            
            mText = mText & "" & StellarMensajeLocal(107) & ": " & _
            FormatoDecimalesDinamicos(TmpMontoBase) & " " & MonedaTmp.SimMoneda & vbNewLine
            mText = mText & "" & StellarMensajeLocal(108) & ": " & _
            FormatoDecimalesDinamicos(TmpMontoUsado) & " " & MonedaTmp.SimMoneda & vbNewLine
            mText = mText & "" & StellarMensajeLocal(109) & ": " & _
            FormatoDecimalesDinamicos(TmpMontoRestante) & " " & MonedaTmp.SimMoneda & vbNewLine
            
            Dim mStatus
            
            mStatus = Empty
            
            Select Case UCase(mRs!CouponStatus)
                Case UCase("Pending")
DefaultStatus:
                    mStatus = "" & StellarMensajeLocal(110) & ""
                    TmpCodStatus = "DPE"
                    If EndOfDay(TmpFechaFin) < mRs!FechaSrv Then
                        mStatus = "" & StellarMensajeLocal(111) & ""
                        TmpCodStatus = "EXP"
                    ElseIf mRs!FechaSrv < TmpFechaIni Then
                        mStatus = mStatus & " - " & StellarMensajeLocal(112) & ""
                    End If
                Case UCase("Completed")
                    mStatus = "" & StellarMensajeLocal(113) & ""
                    TmpCodStatus = "DCO"
                Case UCase("Voided")
                    mStatus = "" & StellarMensajeLocal(114) & ""
                    TmpCodStatus = "ANU"
                Case Else
                    GoTo DefaultStatus
            End Select
            
            mText = mText & "" & StellarMensajeLocal(115) & ": " & mStatus & vbNewLine
            
            RichText.Text = mText
            
            txtSerial.Locked = True
            CmdBuscarCupon.Enabled = True
            Cancelar.Enabled = True
            
        Else
            Mensaje True, "" & StellarMensajeLocal(116) & "."
        End If
        
    Else
        Mensaje True, "" & StellarMensajeLocal(117) & ""
        If Trim(txtPerfilCupon) = Empty Then
            SafeFocus txtPerfilCupon
        Else
            SafeFocus txtSerial
        End If
    End If
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(CmdBuscarCupon)"
    
    Cancelar_Click
    
End Sub

Private Sub Cancelar_Click()
    
    RichText.Text = Empty
    txtSerial.Text = Empty
    txtSerial.Locked = False
    
    If Trim(txtPerfilCupon) = Empty Then
        SafeFocus txtPerfilCupon
    Else
        SafeFocus txtSerial
    End If
    
    Cancelar.Enabled = False
    
End Sub

Private Sub CmdAnular_Click()
    
    If Trim(RichText.Text) <> Empty Then
        
        If TmpCodStatus = "DPE" Then
            
            If Mensaje(False, "" & StellarMensajeLocal(118) & "") Then
                
                Dim mNivelReq, mNivelUser
                
                mNivelUser = FrmAppLink.GetNivelUsuario
                mNivelReq = Val(BuscarReglaNegocioStr("BWLCS_NivelAnularCupon", 0))
                
                If mNivelReq <= 0 Then
                    
                    Mensaje True, "" & StellarMensajeLocal(119) & "."
                    
                Else
                    
                    Dim Aceptado As Boolean
                    
                    If mNivelUser >= mNivelReq Then
                        Aceptado = True
                    Else
                        
                        Dim mAutorizadorCod As String
                        Dim mAutorizadorNom As String
                        
                        Set FrmAutorizacion = FrmAppLink.GetFrmAutorizacion
                        
                        FrmAutorizacion.mNivel = mNivelReq
                        FrmAutorizacion.Titulo = StellarMensaje(3138) ' "Introduzca las credenciales " & _
                        "de un usuario que ..."
                        
                        Set FrmAutorizacion.mConexion = FrmAppLink.CnAdm
                        
                        FrmAutorizacion.Show vbModal
                        
                        If FrmAutorizacion.mAceptada Then
                            
                            mAutorizadorCod = FrmAutorizacion.mCodigoUsuario
                            mAutorizadorNom = FrmAutorizacion.mUsuario
                            
                            Set FrmAutorizacion = Nothing
                            ' Continuar
                            
                            Aceptado = True
                            
                        Else
                            
                            Set FrmAutorizacion = Nothing
                            Mensaje True, StellarMensaje(16050)
                            Exit Sub
                            
                        End If
                        
                    End If
                    
                    If Aceptado Then
                        
                        Dim ActiveTrans As Boolean
                        
                        On Error GoTo ErrorTrans
                        
                        mCnBWLCS.BeginTrans
                        ActiveTrans = True
                        
                        mCnBWLCS.Execute _
                        "INSERT INTO Coupon_Tr " & _
                        "(TransactionNumber, CouponNumber, CouponCode, AccountCode, ChannelID, " & _
                        "LocationCodeUsed, POSCodeUsed, TransactionTypeUsed, DocumentNumberUsed, " & _
                        "ApplicationDate, CurrencyAmount, ExchangeRate, Voided) " & _
                        "SELECT " & _
                        "'N/A', '" & txtSerial.Text & "', '" & txtPerfilCupon.Tag & "', '" & TmpDBCode & "', " & _
                        "'BIZ', '" & FrmAppLink.GetCodLocalidadSistema & "', 'N/A', 'Void', 'N/A', " & _
                        "GetDate(), (" & CDec(TmpMontoRestante * CDec(-1)) & "), -1, 1 ", mNivelReq
                        
                        mCnBWLCS.Execute _
                        "UPDATE Coupon_Ma SET " & _
                        "CouponStatus = 'Voided', " & _
                        "CurrencyBaseAmount = CurrencyUsedAmount " & _
                        "WHERE SystemNumber = " & TmpIDSistema & " " & _
                        "AND CouponCode = '" & txtPerfilCupon.Tag & "' " & _
                        "AND CouponNumber = '" & txtSerial.Text & "' ", mNivelReq
                        
                        mCnBWLCS.CommitTrans
                        ActiveTrans = False
                        
                        Dim DatosLog, DL1, DL2 As String, ArrDatosLog
                        
                        DatosLog = BuscarValorBD("LogInfo", _
                        "SELECT Client_Net_Address + '|' + HOST_NAME() + '|' + APP_NAME() AS LogInfo " & _
                        "FROM Sys.DM_Exec_Connections WHERE Session_id = @@SPID", , FrmAppLink.CnAdm)
                        
                        ArrDatosLog = Split(DatosLog, "|", , vbTextCompare)
                        
                        If UBound(ArrDatosLog) >= 2 Then
                            DL2 = ArrDatosLog(1) & "|" & ArrDatosLog(2)
                        End If
                        
                        Call InsertarAuditoria(111, "" & StellarMensajeLocal(120) & "", _
                        "" & StellarMensajeLocal(121) & " [" & txtSerial & "] " & StellarMensajeLocal(122) & " " & txtPerfilCupon.Text & ", " & StellarMensajeLocal(123) & " [" & txtPerfilCupon.Tag & "]. " & _
                        "" & StellarMensajeLocal(166) & " -> " & FormatoDecimalesDinamicos(TmpMontoBase) & " " & MonedaTmp.SimMoneda & ". " & _
                        IIf(mAutorizadorCod <> Empty, "" & StellarMensajeLocal(124) & " [" & mAutorizadorNom & "][" & mAutorizadorCod & "]. ", Empty) & _
                        IIf(DatosLog <> Empty, "LogInfo => [" & DatosLog & "]", Empty), _
                        Me.Name, "CuponBWLCS", TmpIDSistema, FrmAppLink.CnAdm)
                        
                        FrmAppLink.ShowTooltip "" & StellarMensajeLocal(125) & ".", _
                        2500, 2000, CmdAnular, 1, _
                        &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True), "**[RefreshForm]**", , 0
                        
ErrorTrans:
                        
                        If Err.Number <> 0 Then
                            
                            mErrorNumber = Err.Number
                            mErrorDesc = Err.Description
                            mErrorSource = Err.Source
                            
                            Resume SafeErrHandler
                            
SafeErrHandler:
                            
                            On Error Resume Next
                            
                            MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(CmdAnular)"
                            
                            mCnBWLCS.RollbackTrans
                            ActiveTrans = False
                            
                            Exit Sub
                            
                        End If
                        
                        Cancelar_Click
                        
                        Exit Sub
                        
                    End If
                    
                End If
                
            End If
            
        Else
            Mensaje True, "" & StellarMensajeLocal(126) & ""
        End If
        
    Else
        Mensaje True, "" & StellarMensajeLocal(127) & ""
    End If
    
End Sub

Private Sub CmdBuscarPerfilCupon_Click()
    
    BaseSelect = "SELECT CouponCode, Description " & vbNewLine & _
    "FROM Coupon_Profile " & vbNewLine
    
    Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
    
    With Frm_Super_Consultas
        
        .Inicializar BaseSelect, "" & StellarMensajeLocal(128) & "", mCnBWLCS
        
        .Add_ItemLabels "" & StellarMensaje(142) & "", "CouponCode", 2500, 0
        .Add_ItemLabels "" & StellarMensaje(143) & "", "Description", 8500, 0
        
        .Add_ItemSearching "" & StellarMensaje(143) & "", "Description"
        .Add_ItemSearching "" & StellarMensaje(142) & "", "CouponCode"
        
        .ModoDespliegueInstantaneo = True
        .DespliegueInstantaneo_RetornarCualquierValor = False
        .BusquedaInstantanea = True
        
        .StrOrderBy = "Description ASC"
        
        .CustomFontSize = 16
        
        .Show vbModal
        
        mResp = .ArrResultado
        
    End With
    
    If Not IsEmpty(mResp) Then
        
        Cancelar_Click
        
        txtPerfilCupon.Tag = mResp(0)
        txtPerfilCupon.Text = mResp(1)
        
        FrmAppLink.CnAdm.Execute _
        "IF NOT EXISTS(SELECT * FROM MA_REGLASDENEGOCIO " & _
        "WHERE Campo = 'BWLCS_UltimoPerfilCupon') " & vbNewLine & _
        "INSERT INTO MA_REGLASDENEGOCIO (IDModulo, IDProceso, Descripcion, Tipo, ValorDefault, ValoresPermitidos, Campo, Valor, Oculto) " & _
        "SELECT 'BWLCS', 'BWLCS_ADMIN', 'C�digo del mas reciente perfil de cupones utilizado en el m�dulo Administrar Cup�n', " & _
        "'Alfanumerico', '', '', 'BWLCS_UltimoPerfilCupon', '" & txtPerfilCupon.Tag & "', 0 " & vbNewLine & _
        "ELSE" & vbNewLine & _
        "UPDATE MA_REGLASDENEGOCIO SET " & _
        "Valor = '" & txtPerfilCupon.Tag & "' " & _
        "WHERE Campo = 'BWLCS_UltimoPerfilCupon' "
        
    End If
    
End Sub

Private Sub CmdCambiarFechas_Click()
    
    If Trim(RichText.Text) <> Empty Then
        
        If TmpCodStatus = "DPE" Then
            
            'If Mensaje(False, "�Est� seguro de anular el cup�n consultado?") Then
                
                Dim mNivelReq, mNivelUser
                
                mNivelUser = FrmAppLink.GetNivelUsuario
                mNivelReq = Val(BuscarReglaNegocioStr("BWLCS_NivelCambiarFechaValidez", 0))
                
                If mNivelReq <= 0 Then
                    
                    Mensaje True, "" & StellarMensajeLocal(119) & "."
                    
                Else
                    
                    Dim Aceptado As Boolean
                    
                    If mNivelUser >= mNivelReq Then
                        Aceptado = True
                    Else
                        
                        Dim mAutorizadorCod As String
                        Dim mAutorizadorNom As String
                        
                        Set FrmAutorizacion = FrmAppLink.GetFrmAutorizacion
                        
                        FrmAutorizacion.mNivel = mNivelReq
                        FrmAutorizacion.Titulo = StellarMensaje(3138) ' "Introduzca las credenciales " & _
                        "de un usuario que ..."
                        
                        Set FrmAutorizacion.mConexion = FrmAppLink.CnAdm
                        
                        FrmAutorizacion.Show vbModal
                        
                        If FrmAutorizacion.mAceptada Then
                            
                            mAutorizadorCod = FrmAutorizacion.mCodigoUsuario
                            mAutorizadorNom = FrmAutorizacion.mUsuario
                            
                            Set FrmAutorizacion = Nothing
                            ' Continuar
                            
                            Aceptado = True
                            
                        Else
                            
                            Set FrmAutorizacion = Nothing
                            Mensaje True, StellarMensaje(16050)
                            Exit Sub
                            
                        End If
                        
                    End If
                    
                    If Aceptado Then
                        
                        Dim mNuevaFechaInicio, mNuevaFechaFin, mRawFechaIni, mRawFechaFin
                        
                        Dim mCls As Object 'clsFechaSeleccion
                        
                        Set mCls = FrmAppLink.GetClassFechaSeleccion
                        
                        mCls.MostrarInterfazFechaHora 1, "" & StellarMensajeLocal(129) & "" 'DateTypes.JustDate
                        
                        If mCls.Selecciono Then
                            mNuevaFechaInicio = mCls.Fecha
                            mRawFechaIni = CDec(mNuevaFechaInicio)
                        Else
                            Mensaje True, "" & StellarMensajeLocal(132) & "."
                            Exit Sub
                        End If
                        
                        Set mCls = FrmAppLink.GetClassFechaSeleccion
                        
                        mCls.MostrarInterfazFechaHora 1, "" & StellarMensajeLocal(130) & "" 'DateTypes.JustDate
                        
                        If mCls.Selecciono Then
                            mNuevaFechaFin = mCls.Fecha
                            mRawFechaFin = CDec(mNuevaFechaFin)
                        Else
                            Mensaje True, "" & StellarMensajeLocal(132) & "."
                            Exit Sub
                        End If
                        
                        Aceptado = False
                        
                        ActivarMensajeGrande 50
                        
                        Aceptado = Mensaje(False, _
                        "" & StellarMensajeLocal(133) & " " & _
                        vbNewLine & vbNewLine & _
                        "" & StellarMensajeLocal(134) & ": " & SDate(mNuevaFechaInicio) & vbNewLine & _
                        "" & StellarMensajeLocal(135) & ": " & SDate(mNuevaFechaFin) & vbNewLine & _
                        vbNewLine & _
                        "" & StellarMensajeLocal(136) & "")
                        
                        If Not Aceptado Then
                            Exit Sub
                        End If
                        
                        If mNuevaFechaFin < mNuevaFechaInicio Then
                            Mensaje True, "" & StellarMensajeLocal(137) & ""
                            Exit Sub
                        End If
                        
                        If mNuevaFechaFin < Date Then
                            Mensaje True, "" & StellarMensajeLocal(160) & ""
                            Exit Sub
                        End If
                        
                        Dim ActiveTrans As Boolean
                        
                        On Error GoTo ErrorTrans
                        
                        'mCnBWLCS.BeginTrans
                        'ActiveTrans = True
                        
                        mCnBWLCS.Execute _
                        "UPDATE Coupon_Ma SET " & _
                        "ValidityDateStart = '" & FechaBD(mNuevaFechaInicio) & "', " & _
                        "ValidityDateEnd = '" & FechaBD(EndOfDay(mNuevaFechaFin)) & "' " & _
                        "WHERE SystemNumber = " & TmpIDSistema & " " & _
                        "AND CouponCode = '" & txtPerfilCupon.Tag & "' " & _
                        "AND CouponNumber = '" & txtSerial.Text & "' "
                        
                        'mCnBWLCS.CommitTrans
                        'ActiveTrans = False
                       
                        Dim DatosLog, DL1, DL2 As String, ArrDatosLog
                        
                        DatosLog = BuscarValorBD("LogInfo", _
                        "SELECT Client_Net_Address + '|' + HOST_NAME() + '|' + APP_NAME() AS LogInfo " & _
                        "FROM Sys.DM_Exec_Connections WHERE Session_id = @@SPID", , FrmAppLink.CnAdm)
                        
                        ArrDatosLog = Split(DatosLog, "|", , vbTextCompare)
                        
                        If UBound(ArrDatosLog) >= 2 Then
                            DL2 = ArrDatosLog(1) & "|" & ArrDatosLog(2)
                        End If
                        
                        Call InsertarAuditoria(112, "" & StellarMensajeLocal(138) & "", _
                        "" & StellarMensajeLocal(139) & " [" & txtSerial & "] " & StellarMensajeLocal(122) & " " & txtPerfilCupon.Text & ", " & StellarMensajeLocal(123) & " [" & txtPerfilCupon.Tag & "]. " & _
                        "" & StellarMensajeLocal(153) & " [" & mNuevaFechaInicio & "<->" & mNuevaFechaFin & "], " & _
                        "" & StellarMensajeLocal(154) & " [" & TmpFechaIni & "<->" & TmpFechaFin & "]. " & _
                        IIf(mAutorizadorCod <> Empty, "" & StellarMensajeLocal(124) & " [" & mAutorizadorNom & "][" & mAutorizadorCod & "]. ", Empty) & _
                        IIf(DatosLog <> Empty, "LogInfo => [" & DatosLog & "]", Empty), _
                        Me.Name, "CuponBWLCS", TmpIDSistema, FrmAppLink.CnAdm)
                        
                        FrmAppLink.ShowTooltip "" & StellarMensajeLocal(140) & "", _
                        2500, 2000, CmdCambiarFechas, 1, _
                        &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True), "**[RefreshForm]**", , 0
                        
ErrorTrans:
                        
                        If Err.Number <> 0 Then
                            
                            mErrorNumber = Err.Number
                            mErrorDesc = Err.Description
                            mErrorSource = Err.Source
                            
                            Resume SafeErrHandler
                            
SafeErrHandler:
                            
                            On Error Resume Next
                            
                            MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(CmdCambiarFechas)"
                            
                            'mCnBWLCS.RollbackTrans
                            'ActiveTrans = False
                            
                            Exit Sub
                            
                        End If
                        
                        Cancelar_Click
                        
                        Exit Sub
                        
                    End If
                    
                End If
                
            'End If
            
        Else
            Mensaje True, "" & StellarMensajeLocal(126) & ""
        End If
        
    Else
        Mensaje True, "" & StellarMensajeLocal(127) & ""
    End If
    
End Sub

Private Sub CmdCambiarMonto_Click()
    
    If Trim(RichText.Text) <> Empty Then
        
        If TmpCodStatus = "DPE" Then
            
            'If Mensaje(False, "�Est� seguro de anular el cup�n consultado?") Then
                
                Dim mNivelReq, mNivelUser
                
                mNivelUser = FrmAppLink.GetNivelUsuario
                mNivelReq = Val(BuscarReglaNegocioStr("BWLCS_NivelCambiarSaldo", 0))
                
                If mNivelReq <= 0 Then
                    
                    Mensaje True, "" & StellarMensajeLocal(119) & "."
                    
                Else
                    
                    Dim Aceptado As Boolean
                    
                    If mNivelUser >= mNivelReq Then
                        Aceptado = True
                    Else
                        
                        Dim mAutorizadorCod As String
                        Dim mAutorizadorNom As String
                        
                        Set FrmAutorizacion = FrmAppLink.GetFrmAutorizacion
                        
                        FrmAutorizacion.mNivel = mNivelReq
                        FrmAutorizacion.Titulo = StellarMensaje(3138) ' "Introduzca las credenciales " & _
                        "de un usuario que ..."
                        
                        Set FrmAutorizacion.mConexion = FrmAppLink.CnAdm
                        
                        FrmAutorizacion.Show vbModal
                        
                        If FrmAutorizacion.mAceptada Then
                            
                            mAutorizadorCod = FrmAutorizacion.mCodigoUsuario
                            mAutorizadorNom = FrmAutorizacion.mUsuario
                            
                            Set FrmAutorizacion = Nothing
                            ' Continuar
                            
                            Aceptado = True
                            
                        Else
                            
                            Set FrmAutorizacion = Nothing
                            Mensaje True, StellarMensaje(16050)
                            Exit Sub
                            
                        End If
                        
                    End If
                    
                    If Aceptado Then
                        
                        Dim mNuevoMonto
                        
                        mNuevoMonto = SDec(QuickInputRequest("" & StellarMensajeLocal(141) & ": " & _
                        FormatoDecimalesDinamicos(TmpMontoBase) & vbNewLine & vbNewLine & _
                        "" & StellarMensajeLocal(142) & ": ", , , , "" & StellarMensajeLocal(143) & "", , , , , , , True))
                        
                        If mNuevoMonto <= 0 Then
                            Mensaje True, "" & StellarMensajeLocal(155) & ""
                            Exit Sub
                        Else
                            If mNuevoMonto <= TmpMontoUsado Then
                                Mensaje True, "" & StellarMensajeLocal(156) & ""
                                Exit Sub
                            End If
                        End If
                        
                        Aceptado = False
                        
                        ActivarMensajeGrande 75
                        
                        Aceptado = Mensaje(False, _
                        "" & StellarMensajeLocal(133) & " " & _
                        vbNewLine & vbNewLine & _
                        "" & StellarMensajeLocal(157) & ": " & FormatoDecimalesDinamicos(mNuevoMonto) & vbNewLine & _
                        "" & StellarMensajeLocal(108) & ": " & FormatoDecimalesDinamicos(TmpMontoUsado) & vbNewLine & _
                        "" & StellarMensajeLocal(144) & ": " & FormatoDecimalesDinamicos(CDec(mNuevoMonto - TmpMontoUsado)) & vbNewLine & _
                        vbNewLine & _
                        "" & StellarMensajeLocal(136) & "")
                        
                        If Not Aceptado Then
                            Exit Sub
                        End If
                        
                        Dim ActiveTrans As Boolean
                        
                        On Error GoTo ErrorTrans
                        
                        'mCnBWLCS.BeginTrans
                        'ActiveTrans = True
                        
                        mCnBWLCS.Execute _
                        "UPDATE Coupon_Ma SET " & _
                        "CurrencyBaseAmount = (" & mNuevoMonto & ") " & _
                        "WHERE SystemNumber = " & TmpIDSistema & " " & _
                        "AND CouponCode = '" & txtPerfilCupon.Tag & "' " & _
                        "AND CouponNumber = '" & txtSerial.Text & "' "
                        
                        'mCnBWLCS.CommitTrans
                        'ActiveTrans = False
                       
                        Dim DatosLog, DL1, DL2 As String, ArrDatosLog
                        
                        DatosLog = BuscarValorBD("LogInfo", _
                        "SELECT Client_Net_Address + '|' + HOST_NAME() + '|' + APP_NAME() AS LogInfo " & _
                        "FROM Sys.DM_Exec_Connections WHERE Session_id = @@SPID", , FrmAppLink.CnAdm)
                        
                        ArrDatosLog = Split(DatosLog, "|", , vbTextCompare)
                        
                        If UBound(ArrDatosLog) >= 2 Then
                            DL2 = ArrDatosLog(1) & "|" & ArrDatosLog(2)
                        End If
                        
                        Call InsertarAuditoria(113, "" & StellarMensajeLocal(158) & "", _
                        "" & StellarMensajeLocal(139) & " [" & txtSerial & "] " & StellarMensajeLocal(122) & " " & txtPerfilCupon.Text & ", " & StellarMensajeLocal(123) & " [" & txtPerfilCupon.Tag & "]. " & _
                        "" & StellarMensajeLocal(157) & " -> " & FormatoDecimalesDinamicos(mNuevoMonto) & " " & MonedaTmp.SimMoneda & ", " & _
                        "" & StellarMensajeLocal(145) & " -> " & FormatoDecimalesDinamicos(CDec(mNuevoMonto - TmpMontoUsado)) & " " & MonedaTmp.SimMoneda & ", " & _
                        "" & StellarMensajeLocal(146) & " -> " & FormatoDecimalesDinamicos(TmpMontoBase) & " " & MonedaTmp.SimMoneda & ", " & _
                        "" & StellarMensajeLocal(147) & " -> " & FormatoDecimalesDinamicos(TmpMontoRestante) & " " & MonedaTmp.SimMoneda & ". " & _
                        IIf(mAutorizadorCod <> Empty, "" & StellarMensajeLocal(124) & " [" & mAutorizadorNom & "][" & mAutorizadorCod & "]. ", Empty) & _
                        IIf(DatosLog <> Empty, "LogInfo => [" & DatosLog & "]", Empty), _
                        Me.Name, "CuponBWLCS", TmpIDSistema, FrmAppLink.CnAdm)
                        
                        FrmAppLink.ShowTooltip "" & StellarMensajeLocal(140) & "", _
                        2500, 2000, CmdCambiarMonto, 1, _
                        &H9E5300, vbWhite, &H9E5300, GetFont("Tahoma", "9", True), "**[RefreshForm]**", , 0
                        
ErrorTrans:
                        
                        If Err.Number <> 0 Then
                            
                            mErrorNumber = Err.Number
                            mErrorDesc = Err.Description
                            mErrorSource = Err.Source
                            
                            Resume SafeErrHandler
                            
SafeErrHandler:
                            
                            On Error Resume Next
                            
                            MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(CmdCambiarMonto)"
                            
                            'mCnBWLCS.RollbackTrans
                            'ActiveTrans = False
                            
                            Exit Sub
                            
                        End If
                        
                        Cancelar_Click
                        
                        Exit Sub
                        
                    End If
                    
                End If
                
            'End If
            
        Else
            Mensaje True, "" & StellarMensajeLocal(126) & ""
        End If
        
    Else
        Mensaje True, "" & StellarMensajeLocal(127) & ""
    End If
    
End Sub

Private Sub CmdTeclado_Click()
    
    On Error Resume Next
    
    If Screen.ActiveControl Is txtSerial Then
        
        If Not txtSerial.Locked Then
            
            Set CampoT = txtSerial
            
            CampoT.SelStart = Len(CampoT.Text)
            
            Set grutinas = CreateObject("DLLKeyboard.DLLTeclado")
            
            If CampoT.Text = Empty Then
                CampoT.Text = " "
                DoEvents
            End If
            
            grutinas.ShowKeyboardPOS txtSerial
            
            If CampoT.Text = " " Then
                CampoT.Text = Empty
            End If
            
            SafeFocus CampoT
            
        End If
        
    Else
        
        SafeFocus txtSerial
        
    End If
    
End Sub

Private Sub Exit_Click()
    Salir_Click
End Sub

Private Sub Form_Load()
    
    ErrorCargando = False
    FormaCargada = False
    ErrorNoDB = False
    
    Dim DBCheck, mEtapa As String, Records
    
    lbl_Organizacion.Caption = StellarMensajeLocal(159)
    Cancelar.Caption = StellarMensajeLocal(161)
    CmdAnular.Caption = StellarMensajeLocal(162)
    CmdCambiarFechas.Caption = StellarMensajeLocal(163)
    CmdCambiarMonto.Caption = StellarMensajeLocal(164)
    Salir.Caption = StellarMensajeLocal(165)
    
    On Error GoTo Error
    
    mEtapa = "" & StellarMensajeLocal(148) & ". "
    
    Set DBCheck = ExecuteSafeSQL("SELECT DB_ID('BWL_CouponServices') AS DB", FrmAppLink.CnAdm, , True, True)
    
    If DBCheck Is Nothing Then
NoDB:
        ErrorNoDB = True
        ErrorCargando = True
        Exit Sub
    End If
    
    If DBCheck.EOF Then GoTo NoDB
    
    If isDBNull(DBCheck!DB) Then GoTo NoDB
    
    Dim LastServerName As String, LastDBSelected As String, LastDBOnline As Boolean
    
    'LastServerName = QuitarComillasSimples(BuscarReglaNegocioStr("BWLCS_UltimoServer", Empty))
    LastDBSelected = QuitarComillasSimples(BuscarReglaNegocioStr("BWLCS_UltimaBD", Empty))
    
    mEtapa = "" & StellarMensajeLocal(149) & ". [" & LastDBSelected & "]. "
    
    LastDBOnline = True
    
    If LastDBSelected <> Empty Then
        
        Set DBCheck = ExecuteSafeSQL( _
        "SELECT DB_ID('" & LastDBSelected & "') AS DB", _
        FrmAppLink.CnAdm, , True, True)
        
        If DBCheck Is Nothing Then
            LastDBSelected = Empty
        End If
        
        If DBCheck.EOF Then
            LastDBSelected = Empty
        End If
        
        If isDBNull(DBCheck!DB) Then
            LastDBSelected = Empty
        End If
        
    End If
    
    If LastDBSelected = Empty Then
        
        BaseSelect = "SELECT DBHost, DBName " & vbNewLine & _
        "FROM BWL_CouponServices.DBO.Accounts_Database " & vbNewLine
        
        Set CheckOnlyOne = ExecuteSafeSQL( _
        BaseSelect, FrmAppLink.CnAdm, Records, True, True)
        
        If Records = 1 Then
            mResp = Array(CStr(CheckOnlyOne!DBHost), CStr(CheckOnlyOne!DBName))
        Else
            
            Set CheckOnlyOne = Nothing
            
            Set Frm_Super_Consultas = FrmAppLink.GetFrmSuperConsultas
            
            With Frm_Super_Consultas
                
                .Inicializar BaseSelect, "" & StellarMensajeLocal(150) & "", FrmAppLink.CnAdm
                
                .Add_ItemLabels "DBHost", "DBHost", 5000, 0
                .Add_ItemLabels "DBName", "DBName", 6000, 0
                
                .ModoDespliegueInstantaneo = True
                .DespliegueInstantaneo_RetornarCualquierValor = False
                .BusquedaInstantanea = True
                
                .StrOrderBy = "DBName ASC"
                
                .CustomFontSize = 16
                
                .txtDato.Text = "%"
                
                .Show vbModal
                
                mResp = .ArrResultado
                
            End With
            
        End If
        
        If Not IsEmpty(mResp) Then
            LastServerName = mResp(0)
            LastDBSelected = mResp(1)
            If Trim(LastDBSelected) = Empty Then
                GoTo NoDB
            End If
        Else
            GoTo NoDB
        End If
        
    End If
    
Retry:
    
    Set mCnBWLCS = New ADODB.Connection
    
    gPK = Chr(83) & Chr(81) & Chr(76) & "_" _
    & Chr(51) & Chr(55) & Chr(73) & Chr(51) _
    & Chr(88) & Chr(50) & Chr(49) & Chr(83) _
    & Chr(78) & Chr(68) & Chr(65) & Chr(74) _
    & Chr(68) & Chr(75) & Chr(87) & "-" _
    & Chr(57) & Chr(48) & Chr(72) & Chr(71) _
    & Chr(52) & Chr(50) & Chr(48) & "_" & Chr(90)
    
    Provider_Local = "SQLOLEDB.1"
    Srv_Remote = LastServerName 'FrmAppLink.Srv_Remote
    Srv_Remote_BD_ADM = LastDBSelected
    
    Srv_Remote_Login = QuitarComillasSimples(BuscarReglaNegocioStr("BWLCS_UltimoLogin", "SA"))
    Srv_Remote_Password = QuitarComillasSimples(BuscarReglaNegocioStr("BWLCS_UltimoPwd", Empty))
    
    If Not (UCase(Srv_Remote_Login) = "SA" And Len(Srv_Remote_Password) = 0) Then
        
        Dim mClsTmp As Object
        
        Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
        
        If Not mClsTmp Is Nothing Then
            Srv_Remote_Login = mClsTmp.Decode(FrmAppLink.GetCodProductoStellar, _
            FrmAppLink.GetNomProductoStellar, gPK, Srv_Remote_Login)
            Srv_Remote_Password = mClsTmp.Decode(FrmAppLink.GetCodProductoStellar, _
            FrmAppLink.GetNomProductoStellar, gPK, Srv_Remote_Password)
        End If
        
    End If
    
    mCnBWLCS.ConnectionTimeout = FrmAppLink.CnAdm.ConnectionTimeout
    
    mCnStr = "Provider=MSDataShape.1;" & _
    "Data Provider=" & Provider_Local & ";" & _
    "Data Source=" & Srv_Remote & ";" & _
    "Initial Catalog=" & Srv_Remote_BD_ADM & ";" & _
    IIf(Srv_Remote_Login = Empty Or Srv_Remote_Password = Empty, _
    "Persist Security Info=False;User ID=" & Srv_Remote_Login & ";", _
    "Persist Security Info=True;User ID=" & Srv_Remote_Login & ";Password=" & Srv_Remote_Password & ";")
    
    mCnBWLCS.Open mCnStr
    
    'Call ExecuteSafeSQL( _
    "IF NOT EXISTS(SELECT * FROM MA_REGLASDENEGOCIO " & _
    "WHERE Campo = 'BWLCS_UltimoServer') " & vbNewLine & _
    "INSERT INTO MA_REGLASDENEGOCIO (Campo, Valor, Oculto) " & _
    "SELECT 'BWLCS_UltimoServer', '" & Srv_Remote & "', 0 " & vbNewLine & _
    "ELSE" & vbNewLine & _
    "UPDATE MA_REGLASDENEGOCIO SET " & _
    "Valor = '" & Srv_Remote & "' " & _
    "WHERE Campo = 'BWLCS_UltimoServer' ", FrmAppLink.CnAdm)
    
    Call ExecuteSafeSQL( _
    "IF NOT EXISTS(SELECT * FROM MA_REGLASDENEGOCIO " & _
    "WHERE Campo = 'BWLCS_UltimaBD') " & vbNewLine & _
    "INSERT INTO MA_REGLASDENEGOCIO (IDModulo, IDProceso, Descripcion, Tipo, ValorDefault, ValoresPermitidos, Campo, Valor, Oculto) " & _
    "SELECT 'BWLCS', 'BWLCS_ADMIN', 'Nombre de la �ltima Base de Datos accedida para Administrar Cup�n', " & _
    "'Alfanumerico', '', '', 'BWLCS_UltimaBD', '" & Srv_Remote_BD_ADM & "', 0 " & vbNewLine & _
    "ELSE" & vbNewLine & _
    "UPDATE MA_REGLASDENEGOCIO SET " & _
    "Valor = '" & Srv_Remote_BD_ADM & "' " & _
    "WHERE Campo = 'BWLCS_UltimaBD' ", FrmAppLink.CnAdm)
    
    ErrorCargando = False
    
    TmpDBCode = Split(LastDBSelected, "_", 2, vbTextCompare)(1)
    
    AjustarPantalla Me
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    If mErrorNumber = -2147217843 Then
        
        Set mClsTmp = SafeCreateObject("SQLSafeGuard.Service")
        
        If mClsTmp Is Nothing Then GoTo UnhandledErr
        
        TmpVar = mClsTmp.RequestAccess(FrmAppLink.GetCodProductoStellar, FrmAppLink.GetNomProductoStellar, gPK)
        
        If Not IsEmpty(TmpVar) Then
            
            pUser = TmpVar(0): pPassword = TmpVar(1)
            
            NewUser = QuitarComillasSimples(TmpVar(2)): NewPassword = QuitarComillasSimples(TmpVar(3))
            
            FrmAppLink.CnAdm.Execute _
            "IF NOT EXISTS(SELECT * FROM MA_REGLASDENEGOCIO " & _
            "WHERE Campo = 'BWLCS_UltimoLogin') " & vbNewLine & _
            "INSERT INTO MA_REGLASDENEGOCIO (Campo, Valor, Oculto) " & _
            "SELECT 'BWLCS_UltimoLogin', '" & NewUser & "', 1 " & vbNewLine & _
            "ELSE" & vbNewLine & _
            "UPDATE MA_REGLASDENEGOCIO SET " & _
            "Valor = '" & NewUser & "' " & _
            "WHERE Campo = 'BWLCS_UltimoLogin' "
            
            FrmAppLink.CnAdm.Execute _
            "IF NOT EXISTS(SELECT * FROM MA_REGLASDENEGOCIO " & _
            "WHERE Campo = 'BWLCS_UltimoPwd') " & vbNewLine & _
            "INSERT INTO MA_REGLASDENEGOCIO (Campo, Valor, Oculto) " & _
            "SELECT 'BWLCS_UltimoPwd', '" & NewPassword & "', 1 " & vbNewLine & _
            "ELSE" & vbNewLine & _
            "UPDATE MA_REGLASDENEGOCIO SET " & _
            "Valor = '" & NewPassword & "' " & _
            "WHERE Campo = 'BWLCS_UltimoPwd' "
            
            Resume Retry
            
        End If
        
        Set mClsTmp = Nothing
        
    End If
    
    Resume UnhandledErr
    
UnhandledErr:
    
    On Error Resume Next
    
    MsjErrorRapido mEtapa & mErrorDesc & " " & "(" & mErrorNumber & ")"
    
    ErrorCargando = True
    
End Sub

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        
        If ErrorCargando Then
            
            FormaCargada = True
            
            If ErrorNoDB Then
                Mensaje True, "" & StellarMensajeLocal(151) & ""
            End If
            
            FormaCargada = False
            Unload Me
            Exit Sub
            
        End If
        
        FrmAppLink.SetFormaDLL = Me
        
        Dim mUltimoPerfilCupon, Records
        
        mUltimoPerfilCupon = BuscarReglaNegocioStr("BWLCS_UltimoPerfilCupon", Empty)
        
        If Trim(mUltimoPerfilCupon) <> Empty Then
            
            Set mRs = ExecuteSafeSQL( _
            "SELECT * FROM Coupon_Profile " & _
            "WHERE CouponCode = '" & mUltimoPerfilCupon & "' ", mCnBWLCS, Records, True, True)
            
            If Records > 0 Then
                
                txtPerfilCupon.Tag = mRs!CouponCode
                txtPerfilCupon.Text = mRs!Description
                
            End If
            
        End If
        
        FormaCargada = True
        
        Cancelar_Click
        
        Cancelar.Enabled = False
        
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyEscape, vbKeyF12
            Salir_Click
        Case vbKeyF7
            Cancelar_Click
        Case vbKeyF6
            CmdAnular_Click
        Case vbKeyF3
            CmdCambiarFechas_Click
        Case vbKeyF4
            CmdCambiarMonto_Click
        Case vbKeyF2
            If Screen.ActiveControl Is txtPerfilCupon Then
                CmdBuscarPerfilCupon_Click
            ElseIf Screen.ActiveControl Is txtSerial _
            Or Screen.ActiveControl = CmdBuscarCupon Then
                CmdBuscarCupon_Click
            End If
    End Select
End Sub

Private Sub Salir_Click()
    Unload Me
    Exit Sub
End Sub

Private Sub txtSerial_GotFocus()
    Set CampoT = txtSerial
End Sub

Private Sub txtSerial_KeyDown(KeyCode As Integer, Shift As Integer)
    'If txtSerial.Locked Then Exit Sub
    If KeyCode = vbKeyReturn Then
        CmdBuscarCupon_Click
    End If
End Sub
