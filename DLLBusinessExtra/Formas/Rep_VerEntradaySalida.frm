VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSComCt2.ocx"
Begin VB.Form Rep_VerEntradaySalida 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   4995
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   9315
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4995
   ScaleWidth      =   9315
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Aceptar 
      Appearance      =   0  'Flat
      Caption         =   "&Pantalla"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Index           =   0
      Left            =   5640
      Picture         =   "Rep_VerEntradaySalida.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "Vista Preliminar (F2)"
      Top             =   3840
      Width           =   1095
   End
   Begin VB.CommandButton Aceptar 
      Appearance      =   0  'Flat
      Caption         =   "&Impresora"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Index           =   1
      Left            =   6855
      Picture         =   "Rep_VerEntradaySalida.frx":1D82
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Imprimir Reporte (F8)"
      Top             =   3840
      Width           =   1095
   End
   Begin VB.CommandButton cmd_salir 
      Appearance      =   0  'Flat
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   8070
      Picture         =   "Rep_VerEntradaySalida.frx":3B04
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Salir del Reporte (F3)"
      Top             =   3840
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   2892
      Left            =   120
      TabIndex        =   3
      Top             =   840
      Width           =   9135
      Begin VB.TextBox txt_cajero 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   348
         Left            =   1200
         MaxLength       =   20
         TabIndex        =   13
         ToolTipText     =   "Ingrese el C�digo a Buscar."
         Top             =   1080
         Width           =   2100
      End
      Begin VB.CommandButton cmd_cajero 
         CausesValidation=   0   'False
         Height          =   312
         Left            =   3380
         Picture         =   "Rep_VerEntradaySalida.frx":5886
         Style           =   1  'Graphical
         TabIndex        =   12
         Top             =   1104
         Width           =   315
      End
      Begin MSComCtl2.DTPicker Fechahigh 
         CausesValidation=   0   'False
         Height          =   336
         Left            =   4560
         TabIndex        =   16
         ToolTipText     =   "Fecha de la transferencia"
         Top             =   360
         Width           =   2136
         _ExtentX        =   3757
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   115015681
         CurrentDate     =   36415
      End
      Begin MSComCtl2.DTPicker Fechalow 
         CausesValidation=   0   'False
         Height          =   336
         Left            =   1200
         TabIndex        =   14
         ToolTipText     =   "Fecha de la transferencia"
         Top             =   360
         Width           =   2136
         _ExtentX        =   3757
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   115015681
         CurrentDate     =   36415
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Usuario"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   228
         Left            =   120
         TabIndex        =   11
         Top             =   1080
         Width           =   648
      End
      Begin VB.Label lbl_cajero 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3756
         TabIndex        =   10
         Top             =   1080
         Width           =   5256
      End
      Begin VB.Label lbl_fecha2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3720
         TabIndex        =   6
         Top             =   435
         Width           =   1200
      End
      Begin VB.Label lbl_fecha1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   5
         Top             =   420
         Width           =   525
      End
      Begin VB.Line Line5 
         BorderColor     =   &H00AE5B00&
         X1              =   2030
         X2              =   8900
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   " Criterios de B�squeda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Index           =   1
         Left            =   0
         TabIndex        =   4
         Top             =   0
         Width           =   1935
      End
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9315
      Begin MSComCtl2.DTPicker Fechalow1 
         CausesValidation=   0   'False
         Height          =   336
         Left            =   840
         TabIndex        =   15
         ToolTipText     =   "Fecha de la transferencia"
         Top             =   1440
         Width           =   2136
         _ExtentX        =   3757
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   115015681
         CurrentDate     =   36415
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   6315
         TabIndex        =   2
         Top             =   105
         Width           =   1815
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Listado de Marcaje de Entradas y Salidas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   105
         Width           =   3615
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   8640
         Picture         =   "Rep_VerEntradaySalida.frx":6088
         Top             =   0
         Width           =   480
      End
   End
End
Attribute VB_Name = "Rep_VerEntradaySalida"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Aceptar_Click(Index As Integer)
    CCriterio (Index)
End Sub

Private Sub cbx_Cajero_Click()
    Call cbx_Cajero_LostFocus
End Sub

Private Sub cbx_Cajero_LostFocus()
    
    Dim RsUsuario As New ADODB.Recordset
    
    If cbx_Cajero.ListIndex <> 0 Then
        
        RsUsuario.Open _
        "SELECT * FROM MA_USUARIOS " & _
        "WHERE CodUsuario = '" & FixTSQL(cbx_Cajero.Text) & "' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not RsUsuario.EOF Then
            lbl_cajero.Caption = RsUsuario!Descripcion
        Else
            lbl_cajero.Caption = Empty
        End If
        
    Else
        lbl_cajero.Caption = Empty
    End If
    
    RsUsuario.Close
    
End Sub

Private Sub cmd_salir_Click()
    Unload Me
End Sub

Private Sub Exit_Click()
    cmd_salir_Click
End Sub

Private Sub Form_Load()
    
    lbl_fecha1.Caption = StellarMensaje(3045)
    lbl_fecha2.Caption = StellarMensaje(3046)
    
    Dim Cajero As New ADODB.Recordset
    
    Me.Fechalow.Value = Date
    Me.Fechahigh.Value = EndOfDay(Now)
    
    FrmAppLink.SetFormaDLL = Me
    
End Sub

Private Sub cbx_Cajero_Change()
    
    Dim RsUsuario As New ADODB.Recordset
    
    If cbx_Cajero.ListIndex <> 0 Then
        
        RsUsuario.Open _
        "SELECT * FROM MA_USUARIOS " & _
        "WHERE CodUsuario = '" & FixTSQL(Trim(cbx_Cajero.Text)) & "' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not RsUsuario.EOF Then
            lbl_cajero.Caption = RsUsuario!Descripcion
        Else
            lbl_cajero.Caption = Empty
        End If
        
    Else
        lbl_cajero.Caption = Empty
    End If
    
    RsUsuario.Close
    
End Sub

Private Sub cbx_Sucursal_Click()
    Call cbx_Sucursal_LostFocus
End Sub

Private Sub cbx_Sucursal_LostFocus()
    
    Dim Sucursal As New ADODB.Recordset
    
    If cbx_Sucursal.ListIndex <> 0 Then
        
        Sucursal.Open _
        "Select * from MA_SUCURSALES " & _
        "where c_codigo = '" & FixTSQL(cbx_Sucursal.Text) & "' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not Sucursal.EOF Then
            
            txt_sucursaldesc.Caption = Sucursal!c_Descripcion
            
        End If
        
    Else
        
        txt_sucursaldesc.Caption = Empty
        
    End If
    
    Sucursal.Close
    
End Sub

Private Sub CCriterio(Index As Integer)
    
    Dim mSQL As String
    Dim mWhere As String
    Dim RsReporte As New ADODB.Recordset
    
    Dim HeadCriterio As String
    
    HeadCriterio = "Fecha desde: " & SDate(Fechalow.Value) & " Hasta: " & SDate(Fechahigh.Value) & " "
    
    mSQL = "SELECT * " & _
    "FROM TR_EMPLEADOS_MARCAJES TM " & _
    "INNER JOIN MA_EMPLEADOS E ON E.c_Codigo = TM.CodigoEmpleado " & _
    "WHERE TM.ClockInDate between '" & FechaBD(Fechalow.Value) & "' " & _
    "AND '" & FechaBD(EndOfDay(Fechahigh.Value), FBD_FULL) & "'  "
    
'    mWhere = "AND p.f_Fecha between '" & FechaBD(Fechalow.Value) & "' " & _
'    "AND '" & FechaBD(EndOfDay(Fechahigh.Value), FBD_FULL) & "'  "
    
'    If Trim(txt_Sucursal.Text) <> Empty Then
'        mWhere = mWhere & "AND p.c_Sucursal = '" & FixTSQL(Trim(txt_Sucursal.Text)) & "' "
'        HeadCriterio = HeadCriterio & ", Sucursal: " & lbl_sucursal & " "
'    End If
    
    If Trim(txt_cajero.Text) <> Empty Then
        mWhere = mWhere & "AND E.c_relacion = '" & FixTSQL(Trim(txt_cajero.Text)) & "' "
        HeadCriterio = HeadCriterio & ", Usuario: " & lbl_cajero & " "
    End If
    
'    If cbx_caja_desde.ListIndex >= 0 Then
'        mWhere = mWhere & "AND p.c_Caja >= '" & FixTSQL(Trim(cbx_caja_desde.Text)) & "' "
'        HeadCriterio = HeadCriterio & ", Caja desde: " & Trim(cbx_caja_desde.Text) & " "
'    End If
'
'    If cbx_caja_hasta.ListIndex >= 0 Then
'        mWhere = mWhere & "AND p.c_Caja <= '" & FixTSQL(Trim(cbx_caja_hasta.Text)) & "'"
'        HeadCriterio = HeadCriterio & " hasta: " & Trim(cbx_caja_hasta.Text) & " "
'    End If
    
    SQL = mSQL & mWhere
    
    Debug.Print SQL
    
    RsReporte.Open SQL, Ent.BDD, adOpenStatic, adLockReadOnly
    
    If Not RsReporte.EOF Then
        
        Call REPO_CABE(DR_ListadoEntradaySalida, "Listado de Entrada y Salida")
        
        DR_ListadoEntradaySalida.Sections("enc_pag").Controls("lbl_criterio").Caption = HeadCriterio
        DR_ListadoEntradaySalida.Sections("enc_pag").Controls("lbl_criterio").CanGrow = True
        
        Set DR_ListadoEntradaySalida.DataSource = RsReporte
        
        Select Case Index
            Case 0
                DR_ListadoEntradaySalida.Show vbModal
            Case 1
                DR_ListadoEntradaySalida.PrintReport True
        End Select
        
        RefreshForm Me
        
    Else
        
        Mensaje True, "No se encontraron resultados con los criterios ingresados."
        
    End If
    
End Sub

Public Sub REPO_CABE(ByRef Reporte, Titulo, Optional pMayuscula As Boolean = True)
    
    On Error Resume Next
    
    Dim Rs As New ADODB.Recordset, RsSucursal As New ADODB.Recordset
    Dim pSucursal As String
    
    Call Apertura_Recordset(Rs)
    
    Rs.Open "select * from " & FrmAppLink.Srv_Remote_BD_ADM & ".dbo.ESTRUC_SIS", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Call Apertura_Recordset(RsSucursal)
    
    'Willians
    pSucursal = sGetIni(FrmAppLink.GetSetup, "Branch", "branch", "?")
    
    RsSucursal.Open _
    "SELECT * FROM MA_SUCURSALES " & _
    "WHERE c_Codigo = '" & pSucursal & "' ", _
    Ent.BDD, adOpenForwardOnly, adLockReadOnly, adCmdText
    'Far.strEmpresa = rs!nom_org
    
    Reporte.Caption = Titulo
    
    If Not Rs.EOF Then
        Reporte.Sections("enc_pag").Controls("lbl_empresa").Caption = Rs!nom_org
        Reporte.Sections("enc_pag").Controls("lbl_tit_rep").Caption = IIf(pMayuscula, UCase(Titulo), Titulo)
        'Reporte.Sections("enc_pag").Controls("paginas").Caption = "Pg %p de %P"
        Reporte.Sections("enc_pag").Controls("paginas").Caption = "Pg %p " & FrmAppLink.StellarMensaje(15519) & " %P"
        Reporte.Sections("enc_pag").Controls("lblFechaEmisionRep").Caption = FrmAppLink.StellarMensaje(2523) & ":"
    End If
    
    If Not RsSucursal.EOF Then
        'Reporte.Sections("enc_pag").Controls("lbl_localidad").Caption = "Localidad:" & RsSucursal!c_Descripcion
        Reporte.Sections("enc_pag").Controls("lbl_localidad").Caption = _
        FrmAppLink.StellarMensaje(8024) & ":" & RsSucursal!c_Descripcion
    End If
    
    Call Cerrar_Recordset(Rs)
    Call Cerrar_Recordset(RsSucursal)
    
    Err.Clear
    
End Sub

Private Sub cmd_cajero_Click()
    Call txt_cajero_KeyDown(vbKeyF2, 0)
End Sub

Private Sub txt_cajero_Change()
    If txt_cajero = Empty Then
        lbl_cajero.Caption = Empty
    End If
End Sub

Private Sub txt_cajero_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            'Tecla_Pulsada = True
            
            Dim mDatos As Variant
            
            'Set Campo_Txt = txt_cajero
            'Set Campo_Lbl = lbl_cajero
            
            mDatos = ModAppLink.BuscarInfo_Usuario(, , True)
            
            If Not IsEmpty(mDatos) Then
                If Trim(mDatos(0)) <> Empty Then
                    txt_cajero.Text = mDatos(0)
                    lbl_cajero.Caption = mDatos(1)
                End If
            Else
                txt_cajero.Text = Empty
                lbl_cajero.Caption = Empty
            End If
            
            'Call MAKE_VIEW("ma_usuarios", "codusuario", "descripcion", "" & StellarMensaje(1252) & "", Me, "GENERICO")
            
    End Select
    
End Sub

Private Sub txt_cajero_LostFocus()
    
    Dim RsUsuario As New ADODB.Recordset
    
    If Trim(txt_cajero.Text) <> Empty Then
        
        RsUsuario.Open _
        "SELECT * FROM MA_USUARIOS " & _
        "WHERE CodUsuario = '" & FixTSQL(Trim(txt_cajero.Text)) & "' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not RsUsuario.EOF Then
            
            lbl_cajero.Caption = RsUsuario!Descripcion
            
        Else
            
            lbl_cajero.Caption = Empty
            txt_cajero.Text = Empty
            
            Mensaje True, StellarMensaje(16354) '"No existe el usuario en el Sistema.")
            
        End If
        
    Else
        
        lbl_cajero.Caption = Empty
        txt_cajero.Text = Empty
        
    End If
    
End Sub

Private Sub cmd_sucursal_Click()
    Call txt_sucursal_KeyDown(vbKeyF2, 0)
End Sub

Private Sub txt_sucursal_KeyDown(KeyCode As Integer, Shift As Integer)
   
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
'            Tecla_Pulsada = True
'            Set Forma = Me
'            Set Campo_Txt = txt_Sucursal
'            Set Campo_Lbl = lbl_descripcion
'            Call MAKE_VIEW("ma_sucursales", "c_Codigo", "c_Descripcion", UCase(StellarMensaje(1251)) _
'            , Me, "GENERICO") '"S U C U R S A L E S"
            
            Dim mDatos As Variant
            
            mDatos = ModAppLink.Buscar_Localidad
            
            If Not IsEmpty(mDatos) Then
                If Trim(mDatos(0)) <> Empty Then
                    txt_Sucursal.Text = mDatos(0)
                    lbl_sucursal.Caption = mDatos(1)
                End If
            Else
                txt_Sucursal.Text = Empty
                lbl_sucursal.Caption = Empty
            End If
            
    End Select
    
End Sub

Private Sub txt_sucursal_LostFocus()
    
    Dim RsSucursal As New ADODB.Recordset
    
    If Trim(txt_Sucursal.Text) <> Empty Then
        
        Call Apertura_Recordset(RsSucursal)
        
        RsSucursal.Open _
        "Select * from ma_sucursales " & _
        "where c_Codigo = '" & FixTSQL(Trim(txt_Sucursal.Text)) & "' ", _
        Ent.BDD, adOpenDynamic, adLockBatchOptimistic, adCmdText
        
        If Not RsSucursal.EOF Then
            lbl_descripcion.Caption = RsSucursal!c_Descripcion
        Else
            Call Mensaje(True, StellarMensaje(16357))
            lbl_sucursal.Caption = Empty
            txt_Sucursal.Text = Empty
        End If
        
        RsSucursal.Close
        
    Else
        
        lbl_sucursal.Caption = Empty
        txt_Sucursal.Text = Empty
        
    End If
    
End Sub
