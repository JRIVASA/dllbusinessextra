VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form lista_monedas 
   BackColor       =   &H00606060&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   8505
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7665
   ForeColor       =   &H00585A58&
   LinkTopic       =   "Form1"
   ScaleHeight     =   8505
   ScaleWidth      =   7665
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmd_FormaDePagoQR 
      Height          =   1140
      Left            =   5315
      Picture         =   "lista_monedas.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   7335
      Visible         =   0   'False
      Width           =   1155
   End
   Begin VB.Frame FrameVentana 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   810
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   7635
      Begin VB.PictureBox PicStellar 
         Appearance      =   0  'Flat
         BackColor       =   &H00404040&
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         ForeColor       =   &H80000008&
         Height          =   795
         Left            =   4995
         Picture         =   "lista_monedas.frx":2374
         ScaleHeight     =   795
         ScaleWidth      =   2415
         TabIndex        =   5
         TabStop         =   0   'False
         Top             =   0
         Width           =   2415
      End
   End
   Begin VB.VScrollBar ScrollGrid 
      Height          =   6495
      LargeChange     =   2
      Left            =   6480
      TabIndex        =   3
      Top             =   840
      Width           =   1155
   End
   Begin VB.CommandButton Salir 
      Appearance      =   0  'Flat
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1200
      Left            =   6480
      MousePointer    =   99  'Custom
      Picture         =   "lista_monedas.frx":3603
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   7275
      Width           =   1155
   End
   Begin VB.CommandButton cmdClick 
      Caption         =   "Seleccionar"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1140
      Left            =   0
      TabIndex        =   1
      Top             =   7335
      Width           =   6540
   End
   Begin MSFlexGridLib.MSFlexGrid grid_monedas 
      Height          =   6435
      Left            =   0
      TabIndex        =   0
      Top             =   840
      Width           =   7560
      _ExtentX        =   13335
      _ExtentY        =   11351
      _Version        =   393216
      Rows            =   1
      Cols            =   3
      FixedRows       =   0
      FixedCols       =   0
      RowHeightMin    =   1100
      BackColor       =   14737632
      BackColorSel    =   12632256
      BackColorBkg    =   4210752
      WordWrap        =   -1  'True
      AllowBigSelection=   0   'False
      ScrollTrack     =   -1  'True
      ScrollBars      =   2
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "lista_monedas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public fVieneFormaPago As Boolean
Public fSalir As Boolean

Private FormaCargada As Boolean
Private Band As Boolean
Private mRowCell As Long, mColCell As Long, Fila As Long
Private EventoProgramado As Boolean

Private Sub cmd_FormaDePagoQR_Click()
    
    Dim DatosQR As String
    
    If Not (StellarWallet_DenominacionPorMoneda Is Nothing) Then
        
        Dim TiposWallet
        Dim MonDenWallet
        
        Dim FormaDePago
        Dim MonedaQR As String
        'Dim DenominacionQR As String
        
        Dim DatoQR_Cliente As String
        Dim DatoQR_Wallet As String
        
        '"Escanee el c�digo QR de la Tarjeta ")
        DatosQR = QuickInputRequest(StellarMensaje(816), , , , , , , , , , , , Not DebugMode)
        
        If DatosQR <> Empty Then
            
            DatosQR_Split = Split(DatosQR, ";")
            
            DatoQR_Cliente = DatosQR_Split(0)
            DatoQR_Wallet = DatosQR_Split(1)
            
            TiposWallet = StellarWallet_DenominacionPorMoneda.Items
            MonDenWallet = StellarWallet_DenominacionPorMoneda.Keys
            
            For I = 0 To StellarWallet_DenominacionPorMoneda.Count - 1
                
                If UCase(DatoQR_Wallet) = UCase(TiposWallet(I)) Then
                    
                    FormaDePago = Split(MonDenWallet(I), ";")
                    
                    MonedaQR = FormaDePago(0)
                    DenominacionQR = FormaDePago(1)
                    
                    Exit For
                    
                End If
                
            Next
            
            If MonedaQR <> Empty And DenominacionQR <> Empty Then
                
                Dim RsMonedaQR As New ADODB.Recordset
                
                Call Open_Rec(True, RsMonedaQR)
                
                RsMonedaQR.Open _
                "SELECT * FROM MA_MONEDAS " & _
                "WHERE c_codmoneda = '" & MonedaQR & "' " & _
                "AND b_Activa = 1 " & _
                IIf(MONCampoUsoPOS, "AND bUsoEnPOS = 1 ", Empty) & _
                "ORDER BY b_Preferencia DESC", _
                Conexion, adOpenStatic, adLockReadOnly, adCmdText
                
                If Not RsMonedaQR.EOF Then
                    
                    FormaCargada = False
                    
                    Moneda_Sel = RsMonedaQR!C_CODMONEDA
                    
                    Moneda_Sel_Des = Mid(RsMonedaQR!c_Descripcion, 1, 50)
                    
                    Moneda_Fac = RsMonedaQR!N_FACTOR
                    
                    FormaDePagoQR = True
                    
                    fSalir = False
                    
                    Unload Me
                    
                End If
                
            End If
            
        'Else
            
            'Mensaje True, "No existen datos para ejecutar esta funcionalidad"
            
        End If
        
    Else
        
        Mensaje True, "No existen datos para ejecutar esta funcionalidad."
        
    End If
    
End Sub

Private Sub FrameVentana_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    MoverVentana Me.hWnd
End Sub

Private Sub cmdClick_Click()
    grid_monedas_KeyPress vbKeyReturn
End Sub

Private Sub Form_Activate()
    
    If Not FormaCargada Then
        
        If grid_monedas.Rows <= 0 Then
            fSalir = True
            Unload Me
            Exit Sub
        End If
        
        FormaCargada = True
        
        FormaDePagoQR = False
        
        Fila = -1
        grid_monedas_EnterCell
        
        AjustarPantalla Me
        
        'Me.Left = Me.Left * POSRatioLeftVentanaPagos '11820 ' Para no tapar los totales y poder ver los totales y el descuento
        'Me.Top = Me.Top * POSRatioTopVentanaPagos  '3330
        
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF12 Then
        Salir_Click
    End If
End Sub

Private Sub Form_Load()
    
    On Error GoTo Falla_Lista
    
    cmd_FormaDePagoQR.Visible = StellarWallet_FormaDePagoPorQR
    
    FormaCargada = False
    
    FormaDePagoQR = False
    
    Set Conexion = Ent.BDD
    
    cmdClick.Caption = Stellar_Mensaje(5, True)
    
Reintentar:
    
    lista_monedas.Top = 3270
    lista_monedas.Left = 2000
    
    If fVieneFormaPago Then lista_monedas.Move _
    f_Total.Left + f_Total.GRID.Left + f_Total.GRID.CellLeft, _
    f_Total.GRID.Top + f_Total.GRID.CellTop + _
    f_Total.GRID.CellHeight + f_Total.Toolbar1.Height, Me.Width, Me.Height
    
    Call Open_Rec(True, RsMoneda)
    
    RsMoneda.Open _
    "SELECT * FROM MA_MONEDAS WHERE 1 = 1 " & _
    "AND b_Activa = 1 " & _
    IIf(MONCampoUsoPOS, "AND bUsoEnPOS = 1 ", Empty) & _
    IIf(ManejaIGTF And IGTF_RestringirEnEstaCaja, _
    "AND n_Porc_IGTF = 0 ", Empty) & _
    "ORDER BY b_Preferencia DESC ", _
    Conexion, adOpenStatic, adLockReadOnly, adCmdText
    
    With grid_monedas
        .RowHeightMin = 1200
        .ColWidth(0) = 0
        .ColAlignment(1) = flexAlignLeftCenter
        '.ColWidth(1) = .Width - 250 '2595 '2000
        .ColWidth(2) = 0
        .ScrollTrack = True
    End With
    
    I = 0
    
    ScrollGrid.Min = 0
    ScrollGrid.Max = RsMoneda.RecordCount
    ScrollGrid.Value = 0
    
    If Not RsMoneda.EOF Then
        
        Do Until RsMoneda.EOF
            
            grid_monedas.Row = I
            
            grid_monedas.Col = 0
            grid_monedas.Text = RsMoneda!C_CODMONEDA
            
            grid_monedas.Col = 1
            grid_monedas.Text = Mid(RsMoneda!c_Descripcion, 1, 50)
            
            grid_monedas.Col = 2
            grid_monedas.Text = RsMoneda!N_FACTOR
            
            RsMoneda.MoveNext
            
            I = I + 1
            
            grid_monedas.Rows = grid_monedas.Rows + 1
            
        Loop
        
        grid_monedas.Rows = grid_monedas.Rows - 1
        grid_monedas.Col = 0
        grid_monedas.Row = 0
        
        If grid_monedas.Rows > 5 Then
            ScrollGrid.Visible = True
            grid_monedas.ScrollBars = flexScrollBarVertical
            ReduccionScrollBar = ScrollGrid.Width - 100
            grid_monedas.ColWidth(1) = (grid_monedas.Width - ReduccionScrollBar)
        Else
            grid_monedas.ScrollBars = flexScrollBarNone
            ScrollGrid.Visible = False
            ReduccionScrollBar = 0
            grid_monedas.ColWidth(1) = grid_monedas.Width
        End If
        
        'SendKeys "{right}"
        oTeclado.Key_Right
        
        If Not DebugVBApp Then
            Call WheelHook(Me.hWnd)
        End If
        
        RsMoneda.Close
        
    Else
        RsMoneda.Close
        Call Stellar_Mensaje(28, False)
        Unload Me
    End If
    
    On Error GoTo 0
    
    fVieneFormaPago = False
    
    Exit Sub
    
Falla_Lista:
    
    Set Conexion = Ent.BDD
    'Call SetNet(False)
    GoTo Reintentar
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not DebugVBApp Then
        Call WheelUnHook(Me.hWnd)
    End If
End Sub

Private Sub grid_monedas_DblClick()
    grid_monedas_KeyPress vbKeyReturn
End Sub

Private Sub grid_monedas_EnterCell()
    
    On Error Resume Next
    
    If grid_monedas.Rows = 1 Or Not FormaCargada Then
        Exit Sub
    End If
    
    'If Not Band Then
        
        If Fila <> grid_monedas.Row Then
            
            If Fila > 0 Then
                grid_monedas.RowHeight(Fila) = grid_monedas.RowHeightMin
            End If
            
            grid_monedas.RowHeight(grid_monedas.Row) = 1600
            Fila = grid_monedas.Row
            
            EventoProgramado = True
            ScrollGrid.Value = Fila + 1
            EventoProgramado = False
            
        End If
        
        grid_monedas.RowSel = grid_monedas.Row
        
        If grid_monedas.Col <> 1 Then Band = True
        
        grid_monedas.Col = grid_monedas.Cols - 1
        
        grid_monedas.ColSel = 0
        
        If PuedeObtenerFoco(grid_monedas) Then grid_monedas.SetFocus
        
    'Else
        'Band = False
    'End If
    
End Sub

Private Sub grid_monedas_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
    grid_monedas_EnterCell
End Sub

Private Sub grid_monedas_SelChange()
    grid_monedas_EnterCell
End Sub

Private Sub grid_monedas_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = vbKeyReturn Then
        
        FormaCargada = False
        
        With grid_monedas
            
            .Row = .RowSel
            
            .Col = 1
            Moneda_Sel_Des = Trim(.Text)
            
            .Col = 0
            Moneda_Sel = Trim(.Text)
            
            .Col = 2
            Moneda_Fac = .Text
            
            fSalir = False
            
            Unload Me
            
        End With
        
    End If
    
End Sub

Private Sub grid_monedas_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case vbKeyA To vbKeyZ
            
            mFilaActual = grid_monedas.Row
            
            For mFilaProx = 0 To grid_monedas.Rows - 1
                
                If UCase(Mid(grid_monedas.TextMatrix(mFilaProx, 1), 1, 1)) = UCase(Chr(KeyCode)) Then
                    
                    mFilaAnterior = mFilaActual
                    mFilaActual = mFilaProx
                    
                    grid_monedas.Row = mFilaAnterior
                    grid_monedas.Col = 1
                    
                    'grid_monedas.CellBackColor = grid_monedas.BackColor
                    
                    grid_monedas.Row = mFilaActual
                    grid_monedas.Col = 1
                    
                    'grid_monedas.CellBackColor = grid_monedas.BackColorSel
                    
                    grid_monedas.TopRow = grid_monedas.Row
                    grid_monedas.SetFocus
                    
                    Exit Sub
                    
                End If
                
            Next mFilaProx
            
            grid_monedas.Row = mFilaActual
            grid_monedas.SetFocus
            
        Case vbKeyF12
            
            Salir_Click
            
        Case Else
            
            Call grid_monedas_KeyPress(KeyCode)
                     
    End Select
    
End Sub

Private Sub ScrollGrid_Change()
    On Error GoTo ErrScroll
    If EventoProgramado Then Exit Sub
    If ScrollGrid.Value <> grid_monedas.Row Then
        grid_monedas.TopRow = ScrollGrid.Value
        grid_monedas.Row = ScrollGrid.Value
        If PuedeObtenerFoco(grid_monedas) Then grid_monedas.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGrid_Scroll()
    'Debug.Print ScrollGrid.value
    ScrollGrid_Change
End Sub

Private Sub Salir_Click()
    fSalir = True
    Unload Me
End Sub

Public Sub MouseWheel(ByVal MouseKeys As Long, ByVal Rotation As Long, ByVal Xpos As Long, ByVal Ypos As Long)
  
    Dim Ctl As Control
    Dim bHandled As Boolean
    Dim bOver As Boolean
    
    For Each Ctl In Controls
      ' Is the mouse over the control
      On Error Resume Next
      bOver = (Ctl.Visible And IsOver(Ctl.hWnd, Xpos, Ypos))
      On Error GoTo 0
      
      If bOver Then
        ' If so, respond accordingly
        bHandled = True
        Select Case True
        
          Case TypeOf Ctl Is MSFlexGrid
            FlexGridScroll Ctl, MouseKeys, Rotation, Xpos, Ypos
            
          Case TypeOf Ctl Is PictureBox
            PictureBoxZoom Ctl, MouseKeys, Rotation, Xpos, Ypos
            
          Case TypeOf Ctl Is ListBox, TypeOf Ctl Is TextBox, TypeOf Ctl Is ComboBox
            ' These controls already handle the mousewheel themselves, so allow them to:
            If Ctl.Enabled Then Ctl.SetFocus
            
          Case Else
            bHandled = False
    
        End Select
        If bHandled Then Exit Sub
      End If
      bOver = False
    Next Ctl
    
    ' Scroll was not handled by any controls, so treat as a general message send to the form
    'Me.Caption = "Form Scroll " & IIf(Rotation < 0, "Down", "Up")
    
End Sub
