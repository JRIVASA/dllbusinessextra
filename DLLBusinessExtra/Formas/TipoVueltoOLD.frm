VERSION 5.00
Begin VB.Form f_TipoVuelto_OLD 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3375
   ClientLeft      =   15
   ClientTop       =   -30
   ClientWidth     =   10965
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   3375
   ScaleWidth      =   10965
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmd_StellarWallet 
      Caption         =   "Stellar Wallet"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2000
      Left            =   4440
      Picture         =   "TipoVueltoOLD.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   960
      Width           =   2000
   End
   Begin VB.CommandButton cmd_Merchant 
      Caption         =   "Merchant"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2000
      Left            =   2280
      Picture         =   "TipoVueltoOLD.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   960
      Width           =   2000
   End
   Begin VB.CommandButton cmd_Otros 
      Caption         =   "Otros"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2000
      Left            =   8760
      Picture         =   "TipoVueltoOLD.frx":74AC
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   960
      Width           =   2000
   End
   Begin VB.CommandButton cmd_Cupon 
      Caption         =   "Cupon"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2000
      Left            =   6600
      Picture         =   "TipoVueltoOLD.frx":9176
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   960
      Width           =   2000
   End
   Begin VB.CommandButton cmd_Efectivo 
      Caption         =   "Efectivo"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2000
      Left            =   120
      Picture         =   "TipoVueltoOLD.frx":9E40
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   960
      Width           =   2000
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H009E5300&
      BorderStyle     =   0  'None
      Height          =   615
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   11292
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   795
         Left            =   10200
         Picture         =   "TipoVueltoOLD.frx":10622
         Top             =   60
         Width           =   2415
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Tipo Vuelto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   375
         Left            =   360
         TabIndex        =   5
         Top             =   90
         Width           =   4695
      End
   End
End
Attribute VB_Name = "f_TipoVuelto_OLD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Salir As Boolean

'Public InfoMoneda       As Object
'Public InfoDenominacion As Object
'Public InfoRutinas      As Object

Public mClaseMoneda As New cls_Monedas
Private TotalVuelto As Double

Public CodMonedaUlt As String
Private CodDenominacionUlt As String

'Public Vuelt As Double
Private CodMoneda As String

Public TipoMonederoSW As String
Public DocumentoIDSW As String

Private Sub Image1_Click()
    
    Salir = True
    
    If Me.Visible Then Me.Hide
    
End Sub

Private Sub cmd_Cupon_Click()
    
    If Not VerificacionElectronica_Maneja Then
        Mensaje True, "Esta forma de vuelto necesita de Verificación Electrónica activa."  'StellarMensaje(1000)
        Exit Sub
    End If
    
'    If Not cmd_Cupon.Enabled Then Exit Sub
'
'    f_Vuelto.TipoVuelto = "Cupon_BWLCS"
'
'    f_Vuelto.txt_DenominacionCod.Visible = True
'    f_Vuelto.txt_DenominacionDesc.Visible = True
'    f_Vuelto.CmdBuscarDenominacion.Visible = True
'
'    Call f_Total.Cargar_Moneda
'
'    CodMoneda = Moneda_Sel
'
'    CodMonedaUlt = f_Total.GRID.TextMatrix(f_Total.GRID.Rows - 2, 7)
'
'    If CodMonedaUlt <> Empty Then
'        CodMoneda = CodMonedaUlt
'    Else
'        CodMoneda = Moneda_Sel
'    End If
'
'    InfoMoneda.BuscarMonedas , CodMoneda
'
'    InfoDenominacion.BuscarDenominacion , CodMoneda, DenominacionCuponCompra, , False
'
'    f_Vuelto.lbl_Vuelto.Caption = Stellar_Mensaje(217, True) & Space(1) & _
'    BuscarSimboloMoneda(Entorno.VAD10Local, _
'    BuscarCodigoMonedaPredeterminada(Entorno.VAD10Local)) '& InfoMoneda.SimMoneda
'
'    f_Vuelto.lbl_ValorVuelto.Caption = FormatNumber(Vuelt, gDecMonedaPred)
'
'    f_Vuelto.txt_MonedaCodigo.Text = InfoMoneda.CodMoneda
'    f_Vuelto.txt_MonedaDesc.Text = InfoMoneda.DesMoneda
'    f_Vuelto.txt_MonedaFactor.Text = FormatNumber(InfoMoneda.FacMoneda, InfoMoneda.DecMoneda)
'
'    f_Vuelto.txt_Cantidad.Text = FormatNumber(Vuelt, InfoMoneda.DecMoneda)
'
'    f_Vuelto.txt_Monto.Text = FormatNumber(Vuelt, gDecMonedaPred)
'
'    f_Vuelto.txt_DenominacionCod.Text = InfoDenominacion.CodDenomina
'    f_Vuelto.txt_DenominacionDesc.Text = InfoDenominacion.DesDenomina
'
'    'If UCase(InfoMoneda.CodMoneda) <> UCase(f_Total.mCodigoMonedaPredeterminada) Then
'
'        InfoMoneda.BuscarMonedas , CodMonedaUlt
'
'        If Trim(pMonedaAdicional) <> Empty _
'        And UCase(pMonedaAdicional) = UCase(CodMoneda) Then
'            f_Vuelto.lbl_ValorVuelto2.Tag = f_Total.Vuelto_MonedaAlterna.Tag
'        Else
'            f_Vuelto.lbl_ValorVuelto2.Tag = FormatNumber( _
'            CDbl(f_Vuelto.lbl_ValorVuelto.Caption) / InfoMoneda.FacMoneda, InfoMoneda.DecMoneda) ' Aqui guardamos el valor de vuelto en la _
'            moneda del pago al cliente con sus respectiva cantidad de decimales.
'        End If
'
'        f_Vuelto.lbl_ValorVuelto2.Caption = InfoMoneda.SimMoneda & " " & _
'        FormatNumber(f_Vuelto.lbl_ValorVuelto2.Tag, InfoMoneda.DecMoneda) ' Esto es para mostrar, a nivel de presentación.
'
'        f_Vuelto.txt_Cantidad.Text = _
'        FormatNumber(f_Vuelto.lbl_ValorVuelto2.Tag, InfoMoneda.DecMoneda)
'
'        If UCase(InfoMoneda.CodMoneda) <> UCase(f_Total.mCodigoMonedaPredeterminada) Then
'            f_Vuelto.lbl_ValorVuelto2.Visible = True
'        Else
'            f_Vuelto.lbl_ValorVuelto2.Visible = False
'        End If
'
'    'Else
'        'f_Vuelto.lbl_ValorVuelto2.Visible = False
'    'End If
'
'    f_Vuelto.Show vbModal
'
'    If f_Vuelto.Salir Then
'        Unload f_Vuelto
'        Set f_Vuelto = Nothing
'        Salir = True
'        Exit Sub
'    Else
'        Salir = False
'        Me.Hide
'    End If
    
End Sub

Private Sub cmd_Efectivo_Click()
    
    If Not cmd_Efectivo.Enabled Then Exit Sub
    
    f_Vuelto.TipoVuelto = "Efectivo"
    
    f_Vuelto.txt_DenominacionCod.Visible = False
    f_Vuelto.txt_DenominacionDesc.Visible = False
    f_Vuelto.CmdBuscarDenominacion.Visible = False
    
    'Call f_Total.Cargar_Moneda
    'InfoMoneda.BuscarMonedas , , 1
    
    'CodMonedaUlt = f_Total.GRID.TextMatrix(f_Total.GRID.Rows - 2, 7)
    
    If CodMonedaUlt <> Empty Then
        CodMoneda = CodMonedaUlt
    Else
        CodMoneda = gCodMonedaPred
    End If
    
    InfoMoneda.BuscarMonedas , CodMoneda
    
    InfoDenominacion.BuscarDenominacion , CodMoneda, "Efectivo", , False
    
    f_Vuelto.lbl_Vuelto.Caption = FrmAppLink.StellarMensaje(2517) & " " & FrmAppLink.StellarMensaje(7030) & " " & _
    gSimMonedaPred & ": "
    
    f_Vuelto.lbl_ValorVuelto.Caption = FormatNumber(CDec(Vuelt), gDecMonedaPred)
    
    If Trim(pMonedaAdicional) <> Empty _
    And UCase(pMonedaAdicional) = UCase(CodMoneda) Then
        f_Vuelto.lbl_ValorVuelto2.Tag = FormatNumber(CDec(Vuelt) / CDec(InfoMoneda.FacMoneda), InfoMoneda.DecMoneda) 'f_Total.Vuelto_MonedaAlterna.Tag
    Else
        f_Vuelto.lbl_ValorVuelto2.Tag = FormatNumber( _
        CDec(f_Vuelto.lbl_ValorVuelto.Caption) / InfoMoneda.FacMoneda, InfoMoneda.DecMoneda) ' Aqui guardamos el valor de vuelto en la _
        moneda del pago al cliente con sus respectiva cantidad de decimales.
    End If
    
    If CDec(f_Vuelto.lbl_ValorVuelto2.Tag) = 0 Then
        f_Vuelto.lbl_ValorVuelto2.Tag = (1# / (10 ^ InfoMoneda.DecMoneda))
    End If
    
    f_Vuelto.lbl_ValorVuelto2.Caption = InfoMoneda.SimMoneda & " " & _
    FormatNumber(CDec(f_Vuelto.lbl_ValorVuelto2.Tag), InfoMoneda.DecMoneda) ' Esto es para mostrar, a nivel de presentación.
    
    f_Vuelto.txt_MonedaCodigo.Text = InfoMoneda.CodMoneda
    f_Vuelto.txt_MonedaDesc.Text = InfoMoneda.DesMoneda
    f_Vuelto.txt_MonedaFactor.Text = FormatNumber(InfoMoneda.FacMoneda, InfoMoneda.DecMoneda)
    
    f_Vuelto.txt_Cantidad.Text = FormatNumber(CDec(f_Vuelto.lbl_ValorVuelto2.Tag), InfoMoneda.DecMoneda)
    
    f_Vuelto.txt_Monto.Text = FormatNumber(Vuelt, gDecMonedaPred)
    
    f_Vuelto.txt_DenominacionCod.Text = InfoDenominacion.CodDenomina
    f_Vuelto.txt_DenominacionDesc.Text = InfoDenominacion.DesDenomina
    
    If UCase(InfoMoneda.CodMoneda) <> UCase(gCodMonedaPred) Then
        f_Vuelto.lbl_ValorVuelto2.Visible = True
    Else
        f_Vuelto.lbl_ValorVuelto2.Visible = False
    End If
    
    f_Vuelto.Show vbModal
    
    If f_Vuelto.Salir Then
        Unload f_Vuelto
        Set f_Vuelto = Nothing
        Salir = True
        Exit Sub
    Else
        Salir = False
        Me.Hide
    End If
    
End Sub

Private Sub cmd_Merchant_Click()
    
    If Not cmd_Merchant.Enabled Then Exit Sub
    
    f_Vuelto.TipoVuelto = "Merchant"
    
    f_Vuelto.txt_DenominacionCod.Visible = True
    f_Vuelto.txt_DenominacionDesc.Visible = True
    f_Vuelto.CmdBuscarDenominacion.Visible = True
    
    'Call f_Total.Cargar_Moneda 'buscar la moneda predeterminada
    
    CodMoneda = gCodMonedaPred
    
    'CodMonedaUlt = f_Total.GRID.TextMatrix(f_Total.GRID.Rows - 2, 7)
    
'    If CodMonedaUlt <> Empty Then
'
'        CodMoneda = CodMonedaUlt
'
'    Else
'
'        CodMoneda = Moneda_Sel
'
'    End If
    
    InfoMoneda.BuscarMonedas , CodMoneda
    
    InfoDenominacion.BuscarDenominacion , CodMoneda, DenominacionVueltoMerchant, , False
    
    f_Vuelto.lbl_Vuelto.Caption = FrmAppLink.StellarMensaje(2517) & " " & FrmAppLink.StellarMensaje(7030) & " " & _
    gSimMonedaPred & ": "
    
    f_Vuelto.lbl_ValorVuelto.Caption = FormatNumber(CDec(Vuelt), gDecMonedaPred)
    
    f_Vuelto.txt_MonedaCodigo.Text = InfoMoneda.CodMoneda
    f_Vuelto.txt_MonedaDesc.Text = InfoMoneda.DesMoneda
    f_Vuelto.txt_MonedaFactor.Text = FormatNumber(InfoMoneda.FacMoneda, InfoMoneda.DecMoneda)
    
    f_Vuelto.txt_Cantidad.Text = FormatNumber(CDec(Vuelt), InfoMoneda.DecMoneda)
    
    f_Vuelto.txt_Monto.Text = FormatNumber(CDec(Vuelt), gDecMonedaPred)
    
    f_Vuelto.txt_DenominacionCod.Text = InfoDenominacion.CodDenomina
    f_Vuelto.txt_DenominacionDesc.Text = InfoDenominacion.DesDenomina
    
    'If UCase(InfoMoneda.CodMoneda) <> UCase(f_Total.mCodigoMonedaPredeterminada) Then
        
        InfoMoneda.BuscarMonedas , CodMoneda 'f_Total.GRID.TextMatrix(f_Total.GRID.Rows - 2, 7)
        
        If Trim(pMonedaAdicional) <> Empty _
        And UCase(pMonedaAdicional) = UCase(CodMoneda) Then
            f_Vuelto.lbl_ValorVuelto2.Tag = FormatNumber(CDec(Vuelt) / InfoMoneda.FacMoneda, InfoMoneda.DecMoneda)  'f_Total.Vuelto_MonedaAlterna.Tag
        Else
            f_Vuelto.lbl_ValorVuelto2.Tag = FormatNumber( _
            CDec(f_Vuelto.lbl_ValorVuelto.Caption) / InfoMoneda.FacMoneda, InfoMoneda.DecMoneda) ' Aqui guardamos el valor de vuelto en la _
            moneda del pago al cliente con sus respectiva cantidad de decimales.
        End If
        
        If CDec(f_Vuelto.lbl_ValorVuelto2.Tag) = 0 Then
            f_Vuelto.lbl_ValorVuelto2.Tag = (1# / (10 ^ InfoMoneda.DecMoneda))
        End If
        
        f_Vuelto.lbl_ValorVuelto2.Caption = InfoMoneda.SimMoneda & " " & _
        FormatNumber(CDec(f_Vuelto.lbl_ValorVuelto2.Tag), InfoMoneda.DecMoneda) ' Esto es para mostrar, a nivel de presentación.
        
        f_Vuelto.txt_Cantidad.Text = FormatNumber(CDec(f_Vuelto.lbl_ValorVuelto2.Tag), InfoMoneda.DecMoneda)
        
        If UCase(InfoMoneda.CodMoneda) <> UCase(gCodMonedaPred) Then
            f_Vuelto.lbl_ValorVuelto2.Visible = True
        Else
            f_Vuelto.lbl_ValorVuelto2.Visible = False
        End If
        
    'Else
        'f_Vuelto.lbl_ValorVuelto2.Visible = False
    'End If
    
    f_Vuelto.Show vbModal
    
    If f_Vuelto.Salir Then
        
        Unload f_Vuelto
        
        Set f_Vuelto = Nothing
        
        Salir = True
        
        Exit Sub
        
    Else
        Salir = False
        Me.Hide
    End If
    
End Sub

Private Sub cmd_Otros_Click()
    
    If Not cmd_Otros.Enabled Then Exit Sub
    
    f_Vuelto.TipoVuelto = "Otros"
    
    f_Vuelto.txt_DenominacionCod.Visible = True
    f_Vuelto.txt_DenominacionDesc.Visible = True
    f_Vuelto.CmdBuscarDenominacion.Visible = True
    
    f_Vuelto.CmdBuscarMoneda.Enabled = True
    f_Vuelto.CmdBuscarDenominacion.Enabled = True
    
    'Call f_Total.Cargar_Moneda
    'InfoMoneda.BuscarMonedas , , 1 ya se tienen los datos en las globales no es necesario llamar para buscar los datos predeterinados
    
    'aca hay que buscar el grid pero aun nose como para indicar la ultima moneda igual para los demas metodos
    'CodMonedaUlt = f_Total.GRID.TextMatrix(f_Total.GRID.Rows - 2, 7)
    'frm_cxpcancelacion
   
    If CodMonedaUlt <> Empty Then
        CodMoneda = CodMonedaUlt
    Else
        CodMoneda = InfoMoneda.CodMoneda
    End If
    
    InfoMoneda.BuscarMonedas , CodMoneda
    
    InfoDenominacion.BuscarDenominacion , CodMoneda, "Efectivo", , False
    
    f_Vuelto.lbl_Vuelto.Caption = FrmAppLink.StellarMensaje(2517) & " " & FrmAppLink.StellarMensaje(7030) & " " & _
    gSimMonedaPred & ": "
    
    f_Vuelto.lbl_ValorVuelto.Caption = FormatNumber(CDec(Vuelt), gDecMonedaPred)
    
'    If Trim(pMonedaAdicional) <> Empty _
'    And UCase(pMonedaAdicional) = UCase(CodMoneda) Then
'        f_Vuelto.lbl_ValorVuelto2.Tag = FormatNumber(Vuelt / InfoMoneda.FacMoneda, InfoMoneda.DecMoneda)
'    Else
        f_Vuelto.lbl_ValorVuelto2.Tag = FormatNumber( _
        CDec(CDec(f_Vuelto.lbl_ValorVuelto.Caption)) / CDec(InfoMoneda.FacMoneda), InfoMoneda.DecMoneda)
        ' Aqui guardamos el valor de vuelto en la _
        moneda del pago al cliente con sus respectiva cantidad de decimales.
    'End If
    
    If CDec(f_Vuelto.lbl_ValorVuelto2.Tag) = 0 Then
        f_Vuelto.lbl_ValorVuelto2.Tag = (1# / (10 ^ InfoMoneda.DecMoneda))
    End If
    
    f_Vuelto.lbl_ValorVuelto2.Caption = InfoMoneda.SimMoneda & " " & _
    FormatNumber(CDec(f_Vuelto.lbl_ValorVuelto2.Tag), InfoMoneda.DecMoneda) ' Esto es para mostrar, a nivel de presentación.
    
    f_Vuelto.txt_MonedaCodigo.Text = InfoMoneda.CodMoneda
    f_Vuelto.txt_MonedaDesc.Text = InfoMoneda.DesMoneda
    f_Vuelto.txt_MonedaFactor.Text = FormatNumber(InfoMoneda.FacMoneda, InfoMoneda.DecMoneda)
    
    f_Vuelto.txt_Cantidad.Text = FormatNumber(CDec(f_Vuelto.lbl_ValorVuelto2.Tag), InfoMoneda.DecMoneda)
    
    f_Vuelto.txt_Monto.Text = FormatNumber(CDec(Vuelt), gDecMonedaPred)
    
    f_Vuelto.txt_DenominacionCod.Text = InfoDenominacion.CodDenomina
    f_Vuelto.txt_DenominacionDesc.Text = InfoDenominacion.DesDenomina
    
    If UCase(InfoMoneda.CodMoneda) <> UCase(gCodMonedaPred) Then
        f_Vuelto.lbl_ValorVuelto2.Visible = True
    Else
        f_Vuelto.lbl_ValorVuelto2.Visible = False
    End If
    
    f_Vuelto.Show vbModal
    
    If f_Vuelto.Salir Then
        Unload f_Vuelto
        Set f_Vuelto = Nothing
        Salir = True
        Exit Sub
    Else
        Salir = False
        Me.Hide
    End If
    
End Sub

Private Sub cmd_StellarWallet_Click()
    
    If Not cmd_StellarWallet.Enabled Then Exit Sub
    
    If Not VerificacionElectronica_Maneja Then
        Mensaje True, "Esta forma de vuelto necesita de Verificación Electrónica activa."  'StellarMensaje(1000)
        Exit Sub
    End If
    
    Lc_Cambio = Vuelt 'CDec(Me.lbl_MontoVuelto.Tag) - CDec(Me.lbl_MontoPagado.Tag)
    
    If Round(Lc_Cambio, gDecMonedaPred) <= 0 Then
        Exit Sub
    End If
    
    DocumentoIDSW = Empty
    TipoMonederoSW = Empty
    
    'If Not cmd_Merchant.Enabled Then Exit Sub
    
    f_Vuelto.TipoVuelto = "Stellar_Wallet"
    
    f_Vuelto.txt_DenominacionCod.Visible = True
    f_Vuelto.txt_DenominacionDesc.Visible = True
    f_Vuelto.CmdBuscarDenominacion.Visible = True
    
    'Dim ClsMoneda_VE As New cls_Monedas
    Dim ClsMoneda_VE As Object
    Set ClsMoneda_VE = InfoMoneda
    Dim ClsDenominacion_VE As Object
    Set ClsDenominacion_VE = InfoDenominacion
    'ClsMoneda_VE.InicializarConexiones Entorno.VAD10Local, Entorno.VAD20Local
    ClsMoneda_VE.InicializarConexiones Ent.BDD, Ent.POS
    
    'ClsDenominacion_VE.InicializarConexiones Entorno.VAD10Local, Entorno.VAD20Local
    ClsDenominacion_VE.InicializarConexiones Ent.BDD, Ent.POS
    'ClsDenominacion_VE.DesDenomina
    
    'ClsDenominacion_VE.BuscarDenominacion , CodMoneda, codDenominacion, , False
    
    SafePropAssign gVerificacionElectro, "ClsMoneda_VE_Prop", ClsMoneda_VE
    SafePropAssign gVerificacionElectro, "ClsDenominacion_VE_Prop", ClsDenominacion_VE
    
'    If StellarWallet_IDClienteAuto Then
'        SafePropAssign gVerificacionElectro, "IDCliente_Entrada_Prop", _
'        IIf(Trim(TmpRifCliente) <> Empty, TmpRifCliente, Empty)
'    End If
    
    Dim DatosMonedero As String
    
    DatosMonedero = gVerificacionElectro.StellarWallet_InfoMonedero
    
    If Trim(DatosMonedero) = Empty Then Exit Sub
    
    Datos = Split(DatosMonedero, "|")
    
    Dim CodMonedaSW As String
    Dim CodDenominacionSW As String
    'Dim TipoMonederoSW As String
    'Dim DocumentoIDSW As String
    
    FormaPago = Split(Datos(0), ";")
    
    CodMonedaSW = FormaPago(0)
    CodDenominacionSW = FormaPago(1)
    
    DataRecarga = Split(Datos(1), ";")
    
    DocumentoIDSW = DataRecarga(0)
    TipoMonederoSW = DataRecarga(1)
    
    'Call f_Total.Cargar_Moneda
    
    CodMoneda = CodMonedaSW
    
    InfoMoneda.BuscarMonedas , CodMoneda
    'InfoMoneda.BuscarMonedas , FP_Moneda
    
    InfoDenominacion.BuscarDenominacion , CodMoneda, CodDenominacionSW, , False
    
'    Set f_Vuelto.InfoMoneda = InfoMoneda
'    Set f_Vuelto.InfoDenominacion = InfoDenominacion
    
    f_Vuelto.lbl_Vuelto.Caption = Stellar_Mensaje(217, True) & Space(1) & _
    BuscarSimboloMoneda(Ent.BDD, BuscarCodigoMonedaPredeterminada(Ent.BDD))
    
    Lc_Cambio = CDec(Vuelt)
    'Lc_Cambio = CDec(Me.lbl_MontoVuelto.Tag) - CDec(Me.lbl_MontoPagado.Tag) 'CDec(Me.lbl_MontoVuelto.Caption) - CDec(Me.lbl_MontoPagado.Caption)
    Dim Lc_Cambio_Alterna As Variant
    
    Lc_Cambio_Alterna = CDec(Lc_Cambio) / CDec(InfoMoneda.FacMoneda)
    f_Vuelto.lbl_ValorVuelto.Caption = FormatNumber(Lc_Cambio, gDecMonedaPred)
    
    If Trim(pMonedaAdicional) <> Empty _
    And UCase(pMonedaAdicional) = UCase(CodMoneda) _
    And UCase(CodMoneda) <> UCase(gCodMonedaPred) Then
        
        'f_Vuelto.lbl_ValorVuelto2.Tag = f_Total.Vuelto_MonedaAlterna.Tag---- f_Total.Vuelto_MonedaAlterna.Tag
        'TasaImplicita = Lc_Cambio / (CDec(f_Total.Vuelto_MonedaAlterna.Tag) - CDec(MontoTotalVueltoPagado / InfoMoneda.FacMoneda))
        
        If CDec(Lc_Cambio_Alterna) <> 0 Then
            TasaImplicita = CDec(Lc_Cambio) / CDec(Lc_Cambio_Alterna)
            If TasaImplicita <> 0 Then
                lc_CambioAlt = CDec(Lc_Cambio / TasaImplicita)
                f_Vuelto.lbl_ValorVuelto2.Tag = lc_CambioAlt
            Else
                f_Vuelto.lbl_ValorVuelto.Caption = "0"
            End If
        Else
            f_Vuelto.lbl_ValorVuelto.Caption = "0"
        End If
        
    Else
        f_Vuelto.lbl_ValorVuelto2.Tag = FormatNumber( _
        CDec(f_Vuelto.lbl_ValorVuelto.Caption) / InfoMoneda.FacMoneda, InfoMoneda.DecMoneda) ' Aqui guardamos el valor de vuelto en la _
        moneda del pago al cliente con sus respectiva cantidad de decimales.
    End If
    
    If CDec(f_Vuelto.lbl_ValorVuelto2.Tag) = 0 Then
        f_Vuelto.lbl_ValorVuelto2.Tag = (1# / (10 ^ InfoMoneda.DecMoneda))
    End If
    
    f_Vuelto.lbl_ValorVuelto2.Caption = InfoMoneda.SimMoneda & " " & _
    FormatNumber(CDec(f_Vuelto.lbl_ValorVuelto2.Tag), InfoMoneda.DecMoneda) ' Esto es para mostrar, a nivel de presentación.
    
    f_Vuelto.txt_MonedaCodigo.Text = InfoMoneda.CodMoneda
    f_Vuelto.txt_MonedaDesc.Text = InfoMoneda.DesMoneda
    f_Vuelto.txt_MonedaFactor.Text = FormatNumber(InfoMoneda.FacMoneda, InfoMoneda.DecMoneda)
    
    f_Vuelto.txt_Cantidad.Text = FormatNumber(CDec(f_Vuelto.lbl_ValorVuelto2.Tag), InfoMoneda.DecMoneda)
    'f_Vuelto.txt_montoapagar.Text = f_Vuelto.txt_Cantidad.Text
    
    f_Vuelto.txt_Monto.Text = FormatNumber(Lc_Cambio, gDecMonedaPred)
    
    f_Vuelto.txt_DenominacionCod.Text = InfoDenominacion.CodDenomina
    f_Vuelto.txt_DenominacionDesc.Text = InfoDenominacion.DesDenomina
    
    'If UCase(InfoMoneda.CodMoneda) <> UCase(f_Total.mCodigoMonedaPredeterminada) Then
        
        'If UCase(InfoMoneda.CodMoneda) <> UCase(f_Total.mCodigoMonedaPredeterminada) Then
        If UCase(InfoMoneda.CodMoneda) <> UCase(gCodMonedaPred) Then
            f_Vuelto.lbl_ValorVuelto2.Visible = True
        Else
            f_Vuelto.lbl_ValorVuelto2.Visible = False
        End If
        
    'Else
        'f_Vuelto.lbl_ValorVuelto2.Visible = False
    'End If
    
    f_Vuelto.Show vbModal
    
    If f_Vuelto.Salir Then
        
        Unload f_Vuelto
        
        Set f_Vuelto = Nothing
        
        Salir = True
        
        Exit Sub
        
    Else
        
        'Salir = False
        'Me.Hide
        
'        Me.lbl_MontoPagado.Caption = FormatNumber(MontoTotalVueltoPagado, 2)
'        Me.lbl_MontoPagado.Tag = CDec(MontoTotalVueltoPagado)
'        Me.lbl_montoRestante.Caption = FormatNumber(CDec(Me.lbl_MontoVuelto.Caption) - CDec(Me.lbl_MontoPagado.Caption), gDecMonedaPred)
'
'        If ngFactorMonedaAlterna1 <> 0 Then
'            Me.lbl_MontoPagadoAlt.Caption = FormatNumber(CDec(MontoTotalVueltoPagado) / CDec(ngFactorMonedaAlterna1), InfoMoneda.DecMoneda)
'            Me.lbl_MontoRestanteAlt.Caption = FormatNumber(CDec(Me.lbl_MontoVueltoAlt.Caption) - CDec(Me.lbl_MontoPagadoAlt.Caption), InfoMoneda.DecMoneda)
'        End If
        
       ' If CDec(Me.lbl_MontoVuelto.Caption) = CDec(Me.lbl_MontoPagado.Caption) Then
            Salir = False
            Me.Hide
        'End If
        
    End If
    
End Sub

Private Sub Exit_Click()
    Salir = True
    If Me.Visible Then Me.Hide
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyEscape, vbKeyF12
            Exit_Click
    End Select
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKey1
            cmd_Efectivo_Click
        Case vbKey2
            cmd_Merchant_Click
        Case vbKey3
            cmd_Otros_Click
        Case vbKey4
            cmd_Cupon_Click
    End Select
End Sub

Private Sub Form_Load()
    
    InfoMoneda.InicializarConexiones Ent.BDD, Ent.POS
    InfoDenominacion.InicializarConexiones Ent.BDD, Ent.POS
    
    InfoMoneda.BuscarMonedas , , 1
    
    gCodMonedaPred = InfoMoneda.CodMoneda
    gDesMonedaPred = InfoMoneda.DesMoneda
    gSimMonedaPred = InfoMoneda.SimMoneda
    gDecMonedaPred = InfoMoneda.DecMoneda
    gFacMonedaPred = InfoMoneda.FacMoneda
    
End Sub
