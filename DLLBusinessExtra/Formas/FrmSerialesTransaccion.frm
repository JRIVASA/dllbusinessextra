VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form FrmSerialesTransaccion 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   11850
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   15330
   ControlBox      =   0   'False
   Icon            =   "FrmSerialesTransaccion.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   11850
   ScaleWidth      =   15330
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   600
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   15360
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   600
         Left            =   14640
         Picture         =   "FrmSerialesTransaccion.frx":628A
         Stretch         =   -1  'True
         Top             =   0
         Width           =   600
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Seriales de Transacci�n POS"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   240
         TabIndex        =   9
         Top             =   105
         Width           =   9255
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   495
         Left            =   11835
         TabIndex        =   8
         Top             =   105
         Width           =   2535
      End
   End
   Begin VB.Frame Frame_Detalle 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Formato de Comisiones"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   8775
      Left            =   240
      TabIndex        =   6
      Top             =   2760
      Width           =   14775
      Begin VB.VScrollBar ScrollGridPrd 
         Height          =   7395
         LargeChange     =   10
         Left            =   13890
         TabIndex        =   4
         Top             =   600
         Width           =   675
      End
      Begin VB.Frame FrameEdit 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   735
         Left            =   12960
         TabIndex        =   16
         Top             =   7920
         Visible         =   0   'False
         Width           =   720
         Begin VB.Image CmdEdit 
            Height          =   645
            Left            =   0
            Picture         =   "FrmSerialesTransaccion.frx":800C
            Stretch         =   -1  'True
            Top             =   0
            Width           =   645
         End
      End
      Begin VB.Frame FrameView 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   735
         Left            =   13800
         TabIndex        =   15
         Top             =   7920
         Visible         =   0   'False
         Width           =   720
         Begin VB.Image CmdView 
            Height          =   645
            Left            =   0
            Picture         =   "FrmSerialesTransaccion.frx":84E3
            Stretch         =   -1  'True
            Top             =   0
            Width           =   645
         End
      End
      Begin MSFlexGridLib.MSFlexGrid GrdProductos 
         Height          =   7440
         Left            =   240
         TabIndex        =   3
         Top             =   600
         Width           =   14340
         _ExtentX        =   25294
         _ExtentY        =   13123
         _Version        =   393216
         BackColor       =   16448250
         ForeColor       =   3355443
         BackColorFixed  =   5000268
         ForeColorFixed  =   16777215
         BackColorSel    =   15658734
         ForeColorSel    =   0
         BackColorBkg    =   16448250
         GridColor       =   13421772
         WordWrap        =   -1  'True
         ScrollTrack     =   -1  'True
         FocusRect       =   0
         FillStyle       =   1
         GridLinesFixed  =   0
         SelectionMode   =   1
         BorderStyle     =   0
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00AE5B00&
         X1              =   3720
         X2              =   14500
         Y1              =   240
         Y2              =   240
      End
      Begin VB.Label lblSerialesDocumento 
         BackColor       =   &H00E7E8E8&
         Caption         =   "Seriales de Documento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   375
         Left            =   240
         TabIndex        =   10
         Top             =   120
         Width           =   3375
      End
   End
   Begin VB.Frame frame_Datos 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "Datos de Descripcion"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00585A58&
      Height          =   1695
      Left            =   240
      TabIndex        =   5
      Top             =   840
      Width           =   14775
      Begin VB.CommandButton CmdBuscar 
         Caption         =   "&Buscar"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   6600
         Picture         =   "FrmSerialesTransaccion.frx":8A30
         Style           =   1  'Graphical
         TabIndex        =   1
         Top             =   480
         Width           =   1215
      End
      Begin VB.TextBox txtDocumento 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   480
         Left            =   3240
         TabIndex        =   0
         Top             =   960
         Width           =   2505
      End
      Begin VB.CommandButton Cancelar 
         Caption         =   "&Cancelar"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   975
         Left            =   8040
         Picture         =   "FrmSerialesTransaccion.frx":A7B2
         Style           =   1  'Graphical
         TabIndex        =   2
         Top             =   480
         Visible         =   0   'False
         Width           =   1095
      End
      Begin VB.OptionButton OptConceptoDEV 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Devoluci�n"
         CausesValidation=   0   'False
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   4425
         TabIndex        =   13
         Top             =   480
         Width           =   1665
      End
      Begin VB.OptionButton OptConceptoVEN 
         Appearance      =   0  'Flat
         BackColor       =   &H00E7E8E8&
         Caption         =   "Venta"
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   2670
         TabIndex        =   12
         Top             =   480
         Value           =   -1  'True
         Width           =   1350
      End
      Begin VB.Label lblTipoDoc 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Tipo de Documento: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   240
         TabIndex        =   17
         Top             =   480
         Width           =   2265
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "N�mero de Documento: "
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   285
         Left            =   240
         TabIndex        =   14
         Top             =   1080
         Width           =   2880
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00AE5B00&
         X1              =   1200
         X2              =   14500
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label lblPerfil 
         BackColor       =   &H00E7E8E8&
         Caption         =   "General"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   375
         Left            =   240
         TabIndex        =   11
         Top             =   0
         Width           =   1095
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   12960
      Top             =   1440
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   8
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmSerialesTransaccion.frx":C534
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmSerialesTransaccion.frx":D20E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmSerialesTransaccion.frx":EFA0
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmSerialesTransaccion.frx":10D32
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmSerialesTransaccion.frx":12AC4
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmSerialesTransaccion.frx":14856
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmSerialesTransaccion.frx":165E8
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "FrmSerialesTransaccion.frx":1837A
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "FrmSerialesTransaccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Const ColorODC As Long = &HC0C0C0
Private Const ColorDefault As Long = &HFAFAFA
Private Const ColorModif As Long = &HFFC0C0
Private Const ColorAlertaComprobantes As Long = &HC0C0FF

Private mRowCellPrd As Long, mColCellPrd As Long, FilaPrd As Long
Private BandPrd As Boolean

Dim blnModificar As Boolean
Dim FormaCargada As Boolean
Dim CargandoGrid As Boolean

Private mNivelEditarSeriales As Integer

Private Enum GrdProd
    
    ColLinea
    
    ColCodPro
    ColDescPro
    ColNumeroSerie
    
    ColDBRowID ' ID en la BD. Nos permitir� saber si la linea es nueva o ya ha sido grabada.
    
    [ColCount]
    
End Enum

Private Type DatosDocumento
    Documento       As String
    Concepto        As String
    CodLocalidad    As String
    CodCaja         As String
    CodUsuario      As String
    Turno           As Double
    Fecha           As Date
    ID              As Double
    SyncSxS         As String
End Type

Private DatosDoc As DatosDocumento

Const mMaxLenSerialSistemaExterno = 36 ' Lo maximo que aguanta SAP para el campo de serial.

Private Sub CmdBuscar_Click()
    InicializarGrid
    CargarDatos
End Sub

Private Sub Exit_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    If Not FormaCargada Then
        FormaCargada = True
        InicializarGrid
        OptConceptoVEN.Value = True
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyF12 Then
        Exit_Click
    ElseIf KeyCode = vbKeyF2 Then
        CmdBuscar_Click
    End If
End Sub

Private Sub Form_Load()
    
    FormaCargada = False
    txtDocumento.Enabled = True
    txtDocumento.Locked = False
    OptConceptoVEN.Enabled = True
    OptConceptoDEV.Enabled = True
    
    mNivelEditarSeriales = Val(BuscarReglaNegocioStr("POS_NivelEditarSerialesEnTransaccion", 10))
    
End Sub

Private Sub InicializarGrid()
    
    With GrdProductos
        
        CargandoGrid = True
        
        .Cols = GrdProd.ColCount + 1
        .Rows = 2
        .Row = 0
        .RowHeightMin = 600
        .RowHeight(0) = 600
        
        .FixedCols = 0
        .FixedRows = 1
        
        .Rows = 1
        
        .AllowUserResizing = flexResizeColumns
        .SelectionMode = flexSelectionFree 'flexSelectionByRow
        
        .TextMatrix(0, GrdProd.ColLinea) = "Ln."
        .TextMatrix(0, GrdProd.ColCodPro) = "C�digo"
        .TextMatrix(0, GrdProd.ColDescPro) = "Desc. Item"
        .TextMatrix(0, GrdProd.ColNumeroSerie) = "N�mero de Serie"
        .TextMatrix(0, GrdProd.ColDBRowID) = "ID"
        .TextMatrix(0, GrdProd.ColCount) = Empty
        
        .Col = GrdProd.ColLinea
        .ColWidth(.Col) = 800
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = GrdProd.ColCodPro
        .ColWidth(.Col) = 2000
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = GrdProd.ColDescPro
        '.ColWidth(.Col) = Tama�o Dinamico definido en ValidarMostrarScrollGridPrd
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = GrdProd.ColNumeroSerie
        .ColWidth(.Col) = 5000
        .ColAlignment(.Col) = flexAlignCenterCenter
        .CellAlignment = flexAlignCenterCenter
        
        .Col = GrdProd.ColDBRowID
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        .Col = GrdProd.ColCount
        .ColWidth(.Col) = 0
        .ColAlignment(.Col) = flexAlignRightCenter
        .CellAlignment = flexAlignRightCenter
        
        CargandoGrid = False
        
        .Col = GrdProd.ColCount
        .ColSel = GrdProd.ColCount
        
        .ScrollTrack = True
        .WordWrap = True
        
    End With
    
    ScrollGridPrd.Visible = False
    
End Sub

Private Sub CargarDatos()
    
    With GrdProductos
        
        Dim mSQL As String, mRs As ADODB.Recordset
        Dim mRsSerial As ADODB.Recordset
        Dim UltSerialIDxProducto As Dictionary
        
        mSQL = "WITH DET_TR AS ( " & vbNewLine & _
        "SELECT TR.c_Localidad, TR.c_Concepto, TR.c_Numero, CAST(TR.f_Fecha AS DATE) AS Fecha, " & vbNewLine & _
        "TR.Cod_Principal, PRO.c_Descri, '' AS CodProductoCompuesto, " & vbNewLine & _
        "0 AS TmpCantPartes, MIN(n_Linea) AS Linea, ABS(SUM(TR.Cantidad)) AS CantSeriales " & vbNewLine & _
        "FROM VAD20.DBO.MA_TRANSACCION TR " & vbNewLine & _
        "INNER JOIN VAD20.DBO.MA_PRODUCTOS PRO " & vbNewLine & _
        "ON TR.Cod_Principal = PRO.c_Codigo " & vbNewLine & _
        "WHERE PRO.c_Seriales = '2' " & vbNewLine & _
        "AND c_Localidad = '" & FrmAppLink.GetCodLocalidadSistema & "' " & vbNewLine & _
        "AND c_Concepto = '" & IIf(OptConceptoVEN.Value, "VEN", "DEV") & "' " & vbNewLine & _
        "AND c_Numero IN ('" & QuitarComillasSimples(txtDocumento) & "') " & vbNewLine & _
        "GROUP BY TR.c_Localidad, TR.c_Concepto, TR.c_Numero, " & vbNewLine & _
        "CAST(TR.f_Fecha AS DATE), TR.Cod_Principal, PRO.c_Descri " & vbNewLine & _
        "), "
        
        mSQL = mSQL & "DET_COMP AS ( " & vbNewLine & _
        "SELECT TR.c_Localidad, TR.c_Concepto, TR.c_Numero, CAST(TR.f_Fecha AS DATE) AS Fecha, " & vbNewLine & _
        "HIJO.c_Codigo AS Cod_Principal, HIJO.c_Descri, PADRE.c_Codigo AS CodProductoCompuesto, " & _
        "MAX(ITM.n_Cantidad) AS TmpCantPartes, MIN(n_Linea) AS Linea, " & vbNewLine & _
        "ABS(SUM(TR.Cantidad * ITM.n_Cantidad)) AS CantSeriales " & vbNewLine & _
        "FROM VAD20.DBO.MA_TRANSACCION TR " & vbNewLine & _
        "INNER JOIN VAD20.DBO.MA_PRODUCTOS PADRE " & vbNewLine & _
        "ON PADRE.c_Codigo = TR.Cod_Principal " & vbNewLine & _
        "INNER JOIN VAD10.DBO.MA_PARTES ITM " & vbNewLine & _
        "ON TR.Cod_Principal = ITM.c_Codigo " & vbNewLine & _
        "INNER JOIN VAD20.DBO.MA_PRODUCTOS HIJO " & vbNewLine & _
        "ON HIJO.c_Codigo = ITM.c_Parte " & vbNewLine & _
        "WHERE HIJO.c_Seriales = '2' " & vbNewLine & _
        "AND PADRE.n_TipoPeso = 5 " & vbNewLine & _
        "AND c_Localidad = '" & FrmAppLink.GetCodLocalidadSistema & "' " & vbNewLine & _
        "AND c_Concepto = '" & IIf(OptConceptoVEN.Value, "VEN", "DEV") & "' " & vbNewLine & _
        "AND c_Numero IN ('" & QuitarComillasSimples(txtDocumento) & "') " & vbNewLine & _
        "GROUP BY TR.c_Localidad, TR.c_Concepto, TR.c_Numero, " & vbNewLine & _
        "CAST(TR.f_Fecha AS DATE), PADRE.c_Codigo, HIJO.c_Codigo, HIJO.c_Descri " & vbNewLine & _
        "), "
        
        mSQL = mSQL & _
        "PTR AS ( " & vbNewLine & _
        "    SELECT PTR.CodLocalidad, PTR.Concepto, PTR.Numero, PTR.CodProducto, " & vbNewLine & _
        "    isNULL(SUM(PTR.CantPendiente), 0) AS CantPend " & vbNewLine & _
        "    FROM VAD20.DBO.MA_TRANSACCION_PENDIENTE_X_ENTREGA PTR " & vbNewLine & _
        "    INNER JOIN DET_TR " & vbNewLine & _
        "    ON PTR.Concepto = DET_TR.c_Concepto " & vbNewLine & _
        "    AND PTR.Numero = DET_TR.c_Numero " & vbNewLine & _
        "    AND PTR.CodLocalidad = DET_TR.c_Localidad " & vbNewLine & _
        "    AND PTR.CodProducto = DET_TR.Cod_Principal " & vbNewLine & _
        "    GROUP BY PTR.CodLocalidad, PTR.Concepto, PTR.Numero, PTR.CodProducto " & vbNewLine & _
        "), PTR_COMP AS ( " & vbNewLine & _
        "    SELECT PTR.CodLocalidad, PTR.Concepto, PTR.Numero, DET_COMP.Cod_Principal AS CodProducto, " & vbNewLine & _
        "    isNULL(SUM(PTR.CantPendiente * DET_COMP.TmpCantPartes), 0) AS CantPend " & vbNewLine & _
        "    FROM VAD20.DBO.MA_TRANSACCION_PENDIENTE_X_ENTREGA PTR " & vbNewLine & _
        "    INNER JOIN DET_COMP " & vbNewLine & _
        "    ON PTR.Concepto = DET_COMP.c_Concepto " & vbNewLine & _
        "    AND PTR.Numero = DET_COMP.c_Numero " & vbNewLine & _
        "    AND PTR.CodLocalidad = DET_COMP.c_Localidad " & vbNewLine & _
        "    AND PTR.CodProducto = DET_COMP.CodProductoCompuesto " & vbNewLine & _
        "    GROUP BY PTR.CodLocalidad, PTR.Concepto, PTR.Numero, DET_COMP.Cod_Principal " & vbNewLine & _
        "), "
        
        mSQL = mSQL & "DET_2 AS ( " & vbNewLine & _
        "SELECT DET_TR.*, isNULL(PTR.CantPend, 0) AS PxE " & vbNewLine & _
        "FROM DET_TR LEFT JOIN PTR " & vbNewLine & _
        "ON DET_TR.c_Concepto = PTR.Concepto " & vbNewLine & _
        "AND DET_TR.c_Numero = PTR.Numero " & vbNewLine & _
        "AND DET_TR.c_Localidad = PTR.CodLocalidad " & vbNewLine & _
        "AND DET_TR.Cod_Principal = PTR.CodProducto " & vbNewLine & _
        "UNION ALL " & vbNewLine & _
        "SELECT DET_COMP.*, isNULL(PTR_COMP.CantPend, 0) AS PxE " & vbNewLine & _
        "FROM DET_COMP LEFT JOIN PTR_COMP " & vbNewLine & _
        "ON DET_COMP.c_Concepto = PTR_COMP.Concepto " & vbNewLine & _
        "AND DET_COMP.c_Numero = PTR_COMP.Numero " & vbNewLine & _
        "AND DET_COMP.c_Localidad = PTR_COMP.CodLocalidad " & vbNewLine & _
        "AND DET_COMP.Cod_Principal = PTR_COMP.CodProducto " & vbNewLine
        
        mSQL = mSQL & "), DET_3 AS ( " & vbNewLine & _
        "SELECT *, ROUND(CantSeriales - PxE, 4, 0) AS SerialesRestantes " & vbNewLine & _
        "FROM DET_2 WHERE ROUND(CantSeriales - PxE, 4, 0) > 0 " & vbNewLine & _
        "), SER_TR AS ( " & vbNewLine & _
        "    SELECT PTR.c_Localidad, PTR.c_Concepto, PTR.c_Numero, PTR.Cod_Principal, '' AS CodProductoCompuesto, " & _
        "    CASE WHEN isNULL(COUNT(PTR.c_Serial), 0) < isNULL(MAX(DET_TR.CantSeriales), 0) " & _
        "THEN isNULL(COUNT(PTR.c_Serial), 0) ELSE isNULL(MAX(DET_TR.CantSeriales), 0) END AS SerialesAsignados " & vbNewLine & _
        "    FROM VAD20.DBO.MA_TRANSACCION_SERIALES PTR " & vbNewLine & _
        "    INNER JOIN DET_TR " & vbNewLine & _
        "    ON PTR.c_Concepto = DET_TR.c_Concepto " & vbNewLine & _
        "    AND PTR.c_Numero = DET_TR.c_Numero " & vbNewLine & _
        "    AND PTR.c_Localidad = DET_TR.c_Localidad " & vbNewLine & _
        "    AND PTR.Cod_Principal = DET_TR.Cod_Principal " & vbNewLine & _
        "    GROUP BY PTR.c_Localidad, PTR.c_Concepto, PTR.c_Numero, PTR.Cod_Principal " & vbNewLine & _
        "    UNION ALL " & vbNewLine & _
        "    SELECT PTR.c_Localidad, PTR.c_Concepto, PTR.c_Numero, PTR.Cod_Principal, DET_COMP.CodProductoCompuesto, " & _
        "    CASE WHEN isNULL(COUNT(PTR.c_Serial), 0) < isNULL(MAX(DET_COMP.CantSeriales), 0) " & _
        "THEN isNULL(COUNT(PTR.c_Serial), 0) ELSE isNULL(MAX(DET_COMP.CantSeriales), 0) END AS SerialesAsignados " & vbNewLine & _
        "    FROM VAD20.DBO.MA_TRANSACCION_SERIALES PTR " & vbNewLine & _
        "    INNER JOIN DET_COMP " & vbNewLine & _
        "    ON PTR.c_Concepto = DET_COMP.c_Concepto " & vbNewLine & _
        "    AND PTR.c_Numero = DET_COMP.c_Numero " & vbNewLine & _
        "    AND PTR.c_Localidad = DET_COMP.c_Localidad " & vbNewLine & _
        "    AND PTR.Cod_Principal = DET_COMP.Cod_Principal " & vbNewLine & _
        "    GROUP BY PTR.c_Localidad, PTR.c_Concepto, PTR.c_Numero, PTR.Cod_Principal, DET_COMP.CodProductoCompuesto " & vbNewLine
        
        mSQL = mSQL & "), DET_4 AS ( " & vbNewLine & _
        "SELECT T1.*, isNULL(T2.SerialesAsignados, 0) AS Asignados " & vbNewLine & _
        "FROM DET_3 T1 " & vbNewLine & _
        "LEFT JOIN SER_TR T2 " & vbNewLine & _
        "ON T1.c_Concepto = T2.c_Concepto " & vbNewLine & _
        "AND T1.c_Numero = T2.c_Numero " & vbNewLine & _
        "AND T1.c_Localidad = T2.c_Localidad " & vbNewLine & _
        "AND T1.Cod_Principal = T2.Cod_Principal " & vbNewLine & _
        "AND T1.CodProductoCompuesto = T2.CodProductoCompuesto " & vbNewLine & _
        ") " & vbNewLine & _
        "SELECT * FROM DET_4 " & vbNewLine & _
        "ORDER BY Fecha, c_Numero, Linea, c_Descri " & vbNewLine & _
        "; " & vbNewLine
        
        Set mRs = Ent.POS.Execute(mSQL)
        
        .Visible = False
        
        If Not mRs.EOF Then
            
            I = 0
            
            Set UltSerialIDxProducto = New Dictionary
            
            Do While Not mRs.EOF
                
                Dim CodProd As String, DescProd As String, CantSeriales As Double, CantAsignados As Double, CantFaltante As Double
                Dim UltID As Double
                
                CodProd = mRs!Cod_Principal
                DescProd = mRs!c_Descri
                CantSeriales = mRs!SerialesRestantes
                CantAsignados = 0
                CantFaltante = CantSeriales
                
                If Not UltSerialIDxProducto.Exists(CodProd) Then
                    UltSerialIDxProducto(CodProd) = 0
                End If
                
                While CantFaltante > 0
                    
                    I = I + 1
                    .Rows = .Rows + 1
                    .Row = .Rows - 1
                    
                    .TextMatrix(.Row, GrdProd.ColLinea) = I
                    .TextMatrix(.Row, GrdProd.ColCodPro) = CodProd
                    .TextMatrix(.Row, GrdProd.ColDescPro) = DescProd
                    
                    UltID = UltSerialIDxProducto(CodProd)
                    
                    Set mRsSerial = Ent.POS.Execute( _
                    "SELECT TOP 1 * FROM VAD20.DBO.MA_TRANSACCION_SERIALES " & _
                    "WHERE Cod_Principal = '" & CodProd & "' " & _
                    "AND c_Concepto = '" & mRs!c_Concepto & "' " & _
                    "AND c_Numero = '" & mRs!c_Numero & "' " & _
                    "AND c_Localidad = '" & mRs!c_Localidad & "' " & _
                    "AND ID > " & UltID & " ORDER BY ID")
                    
                    If Not mRsSerial.EOF Then
                        .TextMatrix(.Row, GrdProd.ColNumeroSerie) = mRsSerial!c_Serial
                        UltID = mRsSerial!ID
                        UltSerialIDxProducto(CodProd) = UltID
                        .TextMatrix(.Row, GrdProd.ColDBRowID) = UltID
                    Else
                        .TextMatrix(.Row, GrdProd.ColNumeroSerie) = Empty
                        .TextMatrix(.Row, GrdProd.ColDBRowID) = -1
                    End If
                    
                    CantFaltante = Round(CantFaltante - 1, 0)
                    mRsSerial.Close
                    
                Wend
                
                mRs.MoveNext
                
            Loop
            
            If .Rows > 1 Then
                
                .Row = 1
                
                Set mRs = Ent.POS.Execute( _
                "SELECT * FROM MA_PAGOS " & _
                "WHERE c_Sucursal = '" & FrmAppLink.GetCodLocalidadSistema & "' " & _
                "AND c_Concepto = '" & IIf(OptConceptoVEN.Value, "VEN", "DEV") & "' " & _
                "AND c_Numero = '" & QuitarComillasSimples(txtDocumento) & "' ")
                
                If Not mRs.EOF Then
                    
                    DatosDoc.Documento = mRs!c_Numero
                    DatosDoc.Concepto = mRs!c_Concepto
                    DatosDoc.CodLocalidad = mRs!c_Sucursal
                    DatosDoc.CodCaja = mRs!c_Caja
                    DatosDoc.CodUsuario = mRs!c_Usuario
                    DatosDoc.Fecha = mRs!f_Fecha
                    DatosDoc.Turno = mRs!Turno
                    DatosDoc.ID = mRs!ID
                    
                    If ExisteCampoTabla("cs_Sync_SxS", mRs) Then
                        DatosDoc.SyncSxS = mRs!cs_Sync_Sxs
                    End If
                    
                End If
                
            End If
            
        End If
        
        '.Rows = .Rows + 1
        
        .Visible = True
        
        mRs.Close
        
        ValidarMostrarScrollGridPrd
        GrdProductos_EnterCell
        
        '.Row = .Rows - 1
        
    End With
    
End Sub

Private Sub ValidarMostrarScrollGridPrd()
    
    Dim ColWidthDesc As Long
    
    ColWidthDesc = 5675
    
    If GrdProductos.Rows > 1 Then
        
        If Not ScrollGridPrd.Visible Then
            ScrollGridPrd.Min = 0
            ScrollGridPrd.Max = GrdProductos.Rows - 1
            ScrollGridPrd.Value = 1
            ScrollGridPrd.Visible = True
            GrdProductos.ColWidth(GrdProd.ColDescPro) = (ColWidthDesc - ScrollGridPrd.Width)
        End If
        
        ScrollGridPrd.Max = GrdProductos.Rows - 1
        
    Else
        
        If ScrollGridPrd.Visible Then
            ScrollGridPrd.Visible = False
            ScrollGridPrd.Min = 1
            ScrollGridPrd.Max = 1
            ScrollGridPrd.Value = 1
        End If
        
        GrdProductos.ColWidth(GrdProd.ColDescPro) = (ColWidthDesc)
        
    End If
    
End Sub

Private Sub GrdProductos_DblClick()
    
    On Error GoTo Error
    
    Dim ActiveTrans As Boolean
    
    Dim mCol, mRow, mRecordsAffected
    
    If GrdProductos.Col = GrdProd.ColNumeroSerie Then
        
        If DatosDoc.SyncSxS = Empty Or _
        DatosDoc.SyncSxS = "!!!!!!!!!!" Or _
        DatosDoc.SyncSxS = ".........." Or _
        DatosDoc.SyncSxS = "-_-_-_-_-_" Then
            
            If FrmAppLink.GetNivelUsuario < mNivelEditarSeriales Then
                Mensaje True, "No posee el nivel para realizar esta acci�n."
                Exit Sub
            End If
            
            mCol = GrdProductos.Col
            mRow = GrdProductos.Row
            
            Dim mNewSerial As String
            Dim mCodProducto As String
            Dim mSerialAct As String, mIDAct As Double
            
            mCodProducto = GrdProductos.TextMatrix(mRow, GrdProd.ColCodPro)
            mSerialAct = GrdProductos.TextMatrix(mRow, mCol)
            mIDAct = GrdProductos.TextMatrix(mRow, GrdProd.ColDBRowID)
            
            If Mensaje(False, "El documento a�n no ha sido sincronizado. Desea cambiar el serial " & _
            "de este producto en esta transacci�n? Si est� seguro presione Aceptar.") Then
                
                mNewSerial = QuitarComillasSimples(QuickInputRequest("Introduzca el n�mero de serie para esta transacci�n " & _
                "(Max " & mMaxLenSerialSistemaExterno & " caracteres), producto " & _
                mCodProducto & ". A sustituir: " & mSerialAct, , , mSerialAct, "Escriba el n�mero de serie"))
                
                mNewSerial = Left(Replace(Replace(Trim(mNewSerial), vbCr, Empty), vbLf, Empty), mMaxLenSerialSistemaExterno) ' mMaxLenSerialSistemaExterno Es el largo m�ximo del campo en SAP
                
                If Trim(mNewSerial) = Empty Then
                    Exit Sub
                ElseIf UCase(mNewSerial) = UCase(mSerialAct) Then
                    Exit Sub
                Else
                    
                    If UCase(DatosDoc.Concepto) = "VEN" Then
                        
                        If Val(BuscarValorBD("Disponible", _
                        "SELECT (isNULL(SUM(CASE WHEN c_Concepto = 'VEN' THEN -1 WHEN c_Concepto = 'DEV' THEN 1 END), 0) + 1) AS Disponible " & _
                        "FROM MA_TRANSACCION_SERIALES " & _
                        "WHERE Cod_Principal = '" & mCodProducto & "' " & _
                        "AND c_Serial = '" & mNewSerial & "'", _
                         1, Ent.POS)) <= 0 Then
                            ' Revisando repetido en tabla (entrada previa de seriales facturado en otra compra).
                            Mensaje True, "El serial [" & mNewSerial & "] para este producto no est� disponible. " & _
                            "Ya ha sido utilizado en una transacci�n y su estatus tampoco es devuelto."
                            Exit Sub
                        End If
                        
                    ElseIf UCase(DatosDoc.Concepto) = "DEV" Then
                        
                        ' Las validaciones ser�an muy avanzadas. Mejor que sepa lo que este haciendo...
                        
                    End If
                    
                    ActivarMensajeGrande 30
                    
                    If Mensaje(False, "Atenci�n, est� totalmente seguro de realizar el cambio de serial de [" & _
                    mSerialAct & "] por [" & mNewSerial & "] para el producto [" & mCodProducto & "][" & _
                    GrdProductos.TextMatrix(mRow, GrdProd.ColDescPro) & "] en esta transacci�n? En caso contrario presione Cancelar. ") Then
                        
                        Ent.BDD.BeginTrans: ActiveTrans = True
                        
                        If mIDAct <> -1 Then
                            Ent.BDD.Execute "UPDATE VAD20.DBO.MA_TRANSACCION_SERIALES SET " & _
                            "c_Serial = '" & mNewSerial & "' WHERE ID = " & mIDAct & " " & _
                            "AND c_Numero = '" & DatosDoc.Documento & "' " & _
                            "AND c_Concepto = '" & DatosDoc.Concepto & "' " & _
                            "AND c_Localidad = '" & DatosDoc.CodLocalidad & "' ", mRecordsAffected
                        Else
                            Ent.BDD.Execute "INSERT INTO VAD20.DBO.MA_TRANSACCION_SERIALES " & _
                            "(c_Numero, c_Concepto, c_Localidad, c_Caja, c_Usuario, Turno, Cod_Principal, c_Serial) " & _
                            "SELECT '" & DatosDoc.Documento & "', '" & DatosDoc.Concepto & "', " & _
                            "'" & DatosDoc.CodLocalidad & "', '" & DatosDoc.CodCaja & "', " & _
                            "'" & DatosDoc.CodUsuario & "', (" & DatosDoc.Turno & "), " & _
                            "'" & mCodProducto & "', '" & mNewSerial & "' ", RecordsAffected
                        End If
                        
                        mPK = DatosDoc.CodLocalidad & DatosDoc.Concepto & DatosDoc.Documento
                        
                        If Not InsertarAuditoria(10077, "Cambiar Serial en Transaccion POS", _
                        "Cambi� de serial [" & mSerialAct & "] por [" & mNewSerial & "], producto " & _
                        "[" & mCodProducto & "][" & GrdProductos.TextMatrix(mRow, GrdProd.ColDescPro) & "] " & _
                        "en documento " & mPK & " realizado por " & FrmAppLink.GetNomUsuario & " (" & _
                        FrmAppLink.GetCodUsuario & ")", "FrmSerialesTransaccion", "Documento", mPK, Ent.BDD) Then
                            Err.Raise 999, , "Error al insertar auditor�a. Cambio no realizado."
                        End If
                        
                        Ent.BDD.CommitTrans: ActiveTrans = False
                        
                        CmdBuscar_Click
                        
                    End If
                    
                End If
                
            End If
            
        Else ' No permitimos hacer nada ya que el documento ya est� sincronizado.
            Mensaje True, "No es posible cambiar los n�meros de serie en documentos ya sincronizados."
            Exit Sub
        End If
        
    End If
    
    Exit Sub
    
Error:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    Resume SafeErrHandler
    
SafeErrHandler:
    
    On Error Resume Next
    
    If ActiveTrans Then
        Ent.BDD.RollbackTrans
    End If
    
    ActivarMensajeGrande 50
    MsjErrorRapido mErrorDesc & " " & "(" & mErrorNumber & ").(Cambiando Seriales)"
    
End Sub

Private Sub GrdProductos_SelChange()
    GrdProductos_EnterCell
End Sub

Private Sub GrdProductos_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    'GrdProductos_EnterCell
End Sub

Private Sub GrdProductos_EnterCell()
    
    If Not FormaCargada Then Exit Sub
    If CargandoGrid Then Exit Sub
    
    On Error Resume Next
    
    If GrdProductos.Rows = 1 Then
        'FrameAddPend.Visible = False
        GrdProductos.Col = GrdProd.ColCount
        GrdProductos.ColSel = GrdProd.ColCount
        Exit Sub
    End If
    
    If Not BandPrd Then
        
        If FilaPrd <> GrdProductos.Row Then
            
            If FilaPrd > 0 Then
                GrdProductos.RowHeight(FilaPrd) = 600
            End If
            
            GrdProductos.RowHeight(GrdProductos.Row) = 720
            FilaPrd = GrdProductos.Row
            
        End If
        
        MostrarIconosPrd Me, GrdProductos, Nothing, mRowCellPrd, mColCellPrd
        
        GrdProductos.ColSel = 0
        
        SafeFocus GrdProductos
        
    Else
        BandPrd = False
    End If
    
End Sub

Public Sub MostrarIconosPrd(pFrm As Form, pGrd As Object, _
ByRef txteditor As Object, ByRef cellRow As Long, ByRef cellCol As Long, _
Optional pResp As Boolean = False)
    
    On Error Resume Next
    
    With pGrd
        
        .RowSel = .Row
        
        cellRow = .Row
        cellCol = .Col
        
    End With
    
End Sub

Private Sub OptConceptoDEV_Click()
    If OptConceptoDEV.Value Then
        InicializarGrid
    End If
End Sub

Private Sub OptConceptoVEN_Click()
    If OptConceptoVEN.Value Then
        InicializarGrid
    End If
End Sub

Private Sub ScrollGridPrd_Change()
    On Error GoTo ErrScroll
    If ScrollGridPrd.Value <> GrdProductos.Row Then
        GrdProductos.TopRow = ScrollGridPrd.Value
        GrdProductos.Row = ScrollGridPrd.Value
        GrdProductos.SetFocus
    End If
ErrScroll:
    Err.Clear
End Sub

Private Sub ScrollGridPrd_Scroll()
    'Debug.Print ScrollGridPrd.value
    ScrollGridPrd_Change
End Sub

Private Sub txtDocumento_Change()
    InicializarGrid
    SafeFocus txtDocumento
End Sub

Private Sub txtDocumento_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        CmdBuscar_Click
    End If
End Sub
