VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSComCt2.ocx"
Begin VB.Form Rep_PedidosVsFactura 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   5580
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   9315
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5580
   ScaleWidth      =   9315
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmd_salir 
      Appearance      =   0  'Flat
      Caption         =   "&Salir"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Left            =   8070
      Picture         =   "Rep_PedidosVsFactura.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   9
      ToolTipText     =   "Salir del Reporte (F3)"
      Top             =   4440
      Width           =   1095
   End
   Begin VB.CommandButton Aceptar 
      Appearance      =   0  'Flat
      Caption         =   "&Impresora"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Index           =   1
      Left            =   6855
      Picture         =   "Rep_PedidosVsFactura.frx":1D82
      Style           =   1  'Graphical
      TabIndex        =   8
      ToolTipText     =   "Imprimir Reporte (F8)"
      Top             =   4440
      Width           =   1095
   End
   Begin VB.CommandButton Aceptar 
      Appearance      =   0  'Flat
      Caption         =   "&Pantalla"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   975
      Index           =   0
      Left            =   5640
      Picture         =   "Rep_PedidosVsFactura.frx":3B04
      Style           =   1  'Graphical
      TabIndex        =   7
      ToolTipText     =   "Vista Preliminar (F2)"
      Top             =   4440
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Appearance      =   0  'Flat
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   3495
      Left            =   120
      TabIndex        =   14
      Top             =   720
      Width           =   9135
      Begin VB.ComboBox documento 
         Appearance      =   0  'Flat
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   1215
         Style           =   2  'Dropdown List
         TabIndex        =   6
         ToolTipText     =   "Seleccione el Tipo de Impresi�n."
         Top             =   2040
         Width           =   2145
      End
      Begin VB.CommandButton Cmd_Sucursal 
         CausesValidation=   0   'False
         Height          =   312
         Left            =   3375
         Picture         =   "Rep_PedidosVsFactura.frx":5886
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   1590
         Width           =   315
      End
      Begin VB.TextBox txt_Sucursal 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   348
         Left            =   1200
         MaxLength       =   20
         TabIndex        =   4
         ToolTipText     =   "Ingrese el C�digo a Buscar."
         Top             =   1560
         Width           =   2100
      End
      Begin VB.CommandButton cmd_cliente 
         CausesValidation=   0   'False
         Height          =   312
         Left            =   3380
         Picture         =   "Rep_PedidosVsFactura.frx":6088
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   1104
         Width           =   315
      End
      Begin VB.TextBox txt_cliente 
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         CausesValidation=   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   348
         Left            =   1200
         MaxLength       =   20
         TabIndex        =   2
         ToolTipText     =   "Ingrese el C�digo a Buscar."
         Top             =   1080
         Width           =   2100
      End
      Begin MSComCtl2.DTPicker Fechahigh 
         CausesValidation=   0   'False
         Height          =   336
         Left            =   4560
         TabIndex        =   1
         ToolTipText     =   "Fecha de la transferencia"
         Top             =   360
         Width           =   2136
         _ExtentX        =   3757
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   115671041
         CurrentDate     =   36415
      End
      Begin MSComCtl2.DTPicker Fechalow 
         CausesValidation=   0   'False
         Height          =   336
         Left            =   1200
         TabIndex        =   0
         ToolTipText     =   "Fecha de la transferencia"
         Top             =   360
         Width           =   2136
         _ExtentX        =   3757
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   115671041
         CurrentDate     =   36415
      End
      Begin VB.Label Label3 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Documento"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   22
         Top             =   2040
         Width           =   1320
      End
      Begin VB.Label Lbl_Sucursal 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3750
         TabIndex        =   21
         Top             =   1560
         Width           =   5250
      End
      Begin VB.Label Label2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Sucursal"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   20
         Top             =   1560
         Width           =   735
      End
      Begin VB.Label C�digo 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   " Criterios de B�squeda"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00AE5B00&
         Height          =   240
         Index           =   1
         Left            =   0
         TabIndex        =   19
         Top             =   0
         Width           =   1935
      End
      Begin VB.Line Line5 
         BorderColor     =   &H00AE5B00&
         X1              =   2030
         X2              =   8900
         Y1              =   120
         Y2              =   120
      End
      Begin VB.Label lbl_fecha1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Desde"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   18
         Top             =   420
         Width           =   525
      End
      Begin VB.Label lbl_fecha2 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Hasta"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   3720
         TabIndex        =   17
         Top             =   435
         Width           =   1200
      End
      Begin VB.Label lbl_cliente 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   360
         Left            =   3756
         TabIndex        =   16
         Top             =   1080
         Width           =   5256
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         AutoSize        =   -1  'True
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Cliente"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   240
         Left            =   120
         TabIndex        =   15
         Top             =   1080
         Width           =   585
      End
   End
   Begin VB.Frame Frame5 
      Appearance      =   0  'Flat
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   480
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   9315
      Begin MSComCtl2.DTPicker Fechalow1 
         CausesValidation=   0   'False
         Height          =   336
         Left            =   840
         TabIndex        =   11
         ToolTipText     =   "Fecha de la transferencia"
         Top             =   1440
         Width           =   2136
         _ExtentX        =   3757
         _ExtentY        =   582
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Format          =   115671041
         CurrentDate     =   36415
      End
      Begin VB.Image Exit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   8640
         Picture         =   "Rep_PedidosVsFactura.frx":688A
         Top             =   0
         Width           =   480
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Listado de Pedidos contra Facturas"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   105
         Width           =   3615
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   6315
         TabIndex        =   12
         Top             =   105
         Width           =   1815
      End
   End
End
Attribute VB_Name = "Rep_PedidosVsFactura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Criterio As String

Private Sub CCriterio(opcion)
    
    Dim HeadCriterio As String
    Dim RsReporte As New ADODB.Recordset
    
    HeadCriterio = "Fecha desde: " & SDate(Fechalow.Value) & " Hasta: " & SDate(Fechahigh.Value) & " "
    
    Criterio = "SELECT PED.c_DOCUMENTO AS Pedido, PED.d_FECHA as FechaPed, PED.c_DESCRIPCION AS DesCliente, " & _
    "PED.c_CODCLIENTE as CodCliente, PED.c_rif, PED.c_direccion, " & _
    "PED.N_SUBTOTAL, PED.N_IMPUESTO, PED.N_TOTAL, PED.c_CODLOCALIDAD, SUC.c_descripcion as Des_Suc " & _
    ", case when PED.c_status = 'DCO' then 'Completado' when  PED.c_status = 'DPE' then 'Pendiente' " & _
    "when PED.c_status = 'ANU' then 'Anulado' else 'En Espera' end  as c_status " & _
    ",TVEN.Factura, TVEN.Fec_Fact, TVEN.Total_Fact " & _
    "FROM MA_VENTAS PED " & _
    "INNER JOIN MA_SUCURSALES SUC ON SUC.C_codigo = PED.c_CODLOCALIDAD " & _
    "Left Join " & _
    "(SELECT isnull(VEN.c_DOCUMENTO,'') as Factura, isnull(VEN.d_FECHA,'') as Fec_Fact, " & _
    "isnull(VEN.N_TOTAL,0) as Total_Fact, isnull(DETVEN.c_Documento_Origen,'') as pedido " & _
    "FROM MA_VENTAS VEN INNER JOIN TR_INVENTARIO DETVEN " & _
    "ON VEN.c_DOCUMENTO = DETVEN.c_Documento " & _
    "AND VEN.c_CONCEPTO = DETVEN.c_Concepto " & _
    "AND DETVEN.c_TipoDoc_Origen = 'PED' " & _
    "Where DETVEN.c_Documento_Origen " & _
    "in (Select c_DOCUMENTO from MA_VENTAS where c_CONCEPTO = 'PED' " & _
    "and  MA_VENTAS.d_FECHA between '" & FechaBD(Fechalow.Value) & "' " & _
    "and '" & FechaBD(EndOfDay(Fechahigh.Value), FBD_FULL) & "' ) " & _
    "Group by VEN.c_DOCUMENTO, DETVEN.c_Documento_Origen, VEN.d_FECHA, VEN.N_TOTAL " & _
    ") TVEN ON PED.c_DOCUMENTO = TVEN.pedido " & _
    "WHERE PED.c_CONCEPTO = 'PED' " & _
    "AND PED.d_FECHA between '" & FechaBD(Fechalow.Value) & "' " & _
    "AND '" & FechaBD(EndOfDay(Fechahigh.Value), FBD_FULL) & "' "
    
    If documento.ListIndex = 0 Then
        'completados
        Criterio = Criterio & " AND PED.c_status = 'DCO' "
        HeadCriterio = HeadCriterio & "Estatus: Completados "
    ElseIf documento.ListIndex = 1 Then
        'pendientes
        Criterio = Criterio & " AND PED.c_status = 'DPE' "
        HeadCriterio = HeadCriterio & "Estatus: Pendientes "
    ElseIf documento.ListIndex = 2 Then
        'anulados
        Criterio = Criterio & " AND PED.c_status = 'ANU' "
        HeadCriterio = HeadCriterio & "Estatus: Anulados "
    Else
        'todos
        Criterio = Criterio & " AND PED.c_status NOT IN ('DWT') "
        HeadCriterio = HeadCriterio & "Estatus: Todos "
    End If
    
    If Trim(txt_Sucursal.Text) <> Empty Then
        mWhere = mWhere & "AND PED.c_CODLOCALIDAD = '" & FixTSQL(Trim(txt_Sucursal.Text)) & "' "
        HeadCriterio = HeadCriterio & ", Sucursal: " & Lbl_Sucursal & " "
    End If
    
    If Trim(txt_cliente.Text) <> Empty Then
        mWhere = mWhere & "AND PED.c_CodCliente = '" & FixTSQL(Trim(txt_cliente.Text)) & "' "
        HeadCriterio = HeadCriterio & ", Cliente: " & lbl_cliente & " "
    End If
    
    RsReporte.Open Criterio, Ent.BDD, , adOpenStatic, adLockReadOnly
    
    If Not RsReporte.EOF Then
        
        Call REPO_CABE(DR_ListadoPedidovsFactura, "Listado de Pedidos vs Facturas")
        
        DR_ListadoPedidovsFactura.Sections("enc_pag").Controls("lbl_criterio").Caption = HeadCriterio
        DR_ListadoPedidovsFactura.Sections("enc_pag").Controls("lbl_criterio").CanGrow = True
        
        Set DR_ListadoPedidovsFactura.DataSource = RsReporte
        
        Select Case Index
            Case 0
                DR_ListadoPedidovsFactura.Show vbModal
            Case 1
                DR_ListadoPedidovsFactura.PrintReport True
        End Select
        
        RefreshForm Me
        
    Else
        
        Mensaje True, "No se encontraron resultados con los criterios ingresados."
        
    End If
    
End Sub

Private Sub cmd_salir_Click()
    Unload Me
End Sub

Private Sub Aceptar_Click(Index As Integer)
    CCriterio (Index)
End Sub

Private Sub Cmd_Sucursal_Click()
    Call txt_sucursal_KeyDown(vbKeyF2, 0)
End Sub

Private Sub Cmd_cliente_Click()
    Call txt_cliente_KeyDown(vbKeyF2, 0)
End Sub

Private Sub Exit_Click()
    cmd_salir_Click
End Sub

Private Sub txt_Sucursal_Change()
    If txt_Sucursal = Empty Then
        Lbl_Sucursal.Caption = Empty
    End If
End Sub

Private Sub txt_cliente_Change()
    If txt_cliente = Empty Then
        lbl_cliente.Caption = Empty
    End If
End Sub

Private Sub txt_cliente_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            Dim mDatos As Variant
            
            mDatos = ModAppLink.Buscar_Cliente
            
            If Not IsEmpty(mDatos) Then
                If Trim(mDatos(0)) <> Empty Then
                    txt_cliente.Text = mDatos(0)
                    lbl_cliente.Caption = mDatos(1)
                End If
            Else
                txt_cliente.Text = Empty
                lbl_cliente.Caption = Empty
            End If
            
    End Select
    
End Sub

Private Sub txt_sucursal_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Select Case KeyCode
        
        Case Is = vbKeyF2
            
            Dim mDatos As Variant
            
            mDatos = ModAppLink.Buscar_Localidad
            
            If Not IsEmpty(mDatos) Then
                If Trim(mDatos(0)) <> Empty Then
                    txt_Sucursal.Text = mDatos(0)
                    Lbl_Sucursal.Caption = mDatos(1)
                End If
            Else
                txt_Sucursal.Text = Empty
                Lbl_Sucursal.Caption = Empty
            End If
            
    End Select
    
End Sub

Private Sub Form_Load()
    
    Me.Fechalow.Value = Date
    Me.Fechahigh.Value = EndOfDay(Now)
    
    documento.Clear
    
    documento.AddItem StellarMensaje(2758) '"Completados"
    documento.AddItem StellarMensaje(10) '"Pendientes"
    documento.AddItem StellarMensaje(486) '"Anulados"
    documento.AddItem StellarMensaje(2545) ' Todos
    
    documento.ListIndex = 0
    
End Sub
