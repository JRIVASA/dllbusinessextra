VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFlxGrd.ocx"
Begin VB.Form f_TipoVuelto 
   Appearance      =   0  'Flat
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   9495
   ClientLeft      =   15
   ClientTop       =   -30
   ClientWidth     =   12465
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   9495
   ScaleWidth      =   12465
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame3 
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      Height          =   2295
      Left            =   6600
      TabIndex        =   18
      Top             =   1440
      Width           =   5655
      Begin VB.Label lbl_VueltoAlt 
         BackStyle       =   0  'Transparent
         Caption         =   "Vuelto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   375
         Left            =   360
         TabIndex        =   27
         Top             =   120
         Width           =   1815
      End
      Begin VB.Label Lbl_PagadoAlt 
         BackStyle       =   0  'Transparent
         Caption         =   "Pagado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   375
         Left            =   360
         TabIndex        =   26
         Top             =   840
         Width           =   1815
      End
      Begin VB.Label lbl_MontoVueltoAlt 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   615
         Left            =   1080
         TabIndex        =   25
         Top             =   120
         Width           =   3615
      End
      Begin VB.Label lbl_MontoPagadoAlt 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000C000&
         Height          =   615
         Left            =   1080
         TabIndex        =   24
         Top             =   840
         Width           =   3615
      End
      Begin VB.Label lbl_simbVuelAlt 
         BackStyle       =   0  'Transparent
         Caption         =   "Bs."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   615
         Left            =   4750
         TabIndex        =   23
         Top             =   120
         Width           =   720
      End
      Begin VB.Label lbl_simbpagAlt 
         BackStyle       =   0  'Transparent
         Caption         =   "Bs."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000C000&
         Height          =   615
         Left            =   4750
         TabIndex        =   22
         Top             =   840
         Width           =   720
      End
      Begin VB.Label lbl_RetanteAlt 
         BackStyle       =   0  'Transparent
         Caption         =   "Restante"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   375
         Left            =   360
         TabIndex        =   21
         Top             =   1560
         Width           =   1815
      End
      Begin VB.Label lbl_MontoRestanteAlt 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   615
         Left            =   1080
         TabIndex        =   20
         Top             =   1560
         Width           =   3615
      End
      Begin VB.Label lbl_SimboloMontoRestanteAlt 
         BackStyle       =   0  'Transparent
         Caption         =   "Bs."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   615
         Left            =   4750
         TabIndex        =   19
         Top             =   1560
         Width           =   720
      End
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00404040&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   2295
      Left            =   240
      TabIndex        =   8
      Top             =   1440
      Width           =   5655
      Begin VB.Label lbl_Vuelto 
         BackStyle       =   0  'Transparent
         Caption         =   "Vuelto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   375
         Left            =   240
         TabIndex        =   17
         Top             =   120
         Width           =   1815
      End
      Begin VB.Label Lbl_Pagado 
         BackStyle       =   0  'Transparent
         Caption         =   "Pagado"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   375
         Left            =   240
         TabIndex        =   16
         Top             =   840
         Width           =   1815
      End
      Begin VB.Label lbl_MontoVuelto 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   615
         Left            =   1080
         TabIndex        =   15
         Top             =   120
         Width           =   3615
      End
      Begin VB.Label lbl_MontoPagado 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000C000&
         Height          =   495
         Left            =   1080
         TabIndex        =   14
         Top             =   840
         Width           =   3615
      End
      Begin VB.Label lbl_simbVuelto 
         BackStyle       =   0  'Transparent
         Caption         =   "Bs."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   615
         Left            =   4750
         TabIndex        =   13
         Top             =   120
         Width           =   720
      End
      Begin VB.Label lbl_simbPagado 
         BackStyle       =   0  'Transparent
         Caption         =   "Bs."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0000C000&
         Height          =   615
         Left            =   4750
         TabIndex        =   12
         Top             =   840
         Width           =   720
      End
      Begin VB.Label lbl_Restante 
         BackStyle       =   0  'Transparent
         Caption         =   "Restante"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   14.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   375
         Left            =   240
         TabIndex        =   11
         Top             =   1560
         Width           =   1815
      End
      Begin VB.Label lbl_montoRestante 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "0.00"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   495
         Left            =   1080
         TabIndex        =   10
         Top             =   1560
         Width           =   3615
      End
      Begin VB.Label lbl_SimboloMontoRestante 
         BackStyle       =   0  'Transparent
         Caption         =   "Bs."
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   24
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   615
         Left            =   4750
         TabIndex        =   9
         Top             =   1560
         Width           =   720
      End
   End
   Begin VB.CommandButton cmd_StellarWallet 
      Caption         =   "Stellar Wallet"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1200
      Left            =   4200
      Picture         =   "TipoVuelto.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   8040
      Width           =   2000
   End
   Begin VB.CommandButton cmd_Merchant 
      Caption         =   "Merchant"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1200
      Left            =   2160
      Picture         =   "TipoVuelto.frx":0CCA
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   8040
      Width           =   2000
   End
   Begin VB.CommandButton cmd_Otros 
      Caption         =   "Otros"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1200
      Left            =   8280
      Picture         =   "TipoVuelto.frx":74AC
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   8040
      Width           =   2000
   End
   Begin VB.CommandButton cmd_Cupon 
      Caption         =   "Cupon"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1200
      Left            =   6240
      Picture         =   "TipoVuelto.frx":9176
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   8040
      Width           =   2000
   End
   Begin VB.CommandButton cmd_Efectivo 
      Caption         =   "Efectivo"
      CausesValidation=   0   'False
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1200
      Left            =   120
      Picture         =   "TipoVuelto.frx":9E40
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   8040
      Width           =   2000
   End
   Begin VB.Frame Frame1 
      BackColor       =   &H009E5300&
      BorderStyle     =   0  'None
      Height          =   1290
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   12495
      Begin VB.Image cmdExit 
         Appearance      =   0  'Flat
         Height          =   480
         Left            =   11760
         Picture         =   "TipoVuelto.frx":10622
         Stretch         =   -1  'True
         Top             =   390
         Width           =   480
      End
      Begin VB.Image PicLogoStellar 
         Height          =   900
         Left            =   8760
         Picture         =   "TipoVuelto.frx":123A4
         Top             =   240
         Width           =   2700
      End
      Begin VB.Label Label1 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Tipo Vuelto"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   495
         Left            =   360
         TabIndex        =   5
         Top             =   330
         Width           =   4815
      End
   End
   Begin MSFlexGridLib.MSFlexGrid GridVuelto 
      Height          =   4095
      Left            =   240
      TabIndex        =   28
      Top             =   3840
      Width           =   12015
      _ExtentX        =   21193
      _ExtentY        =   7223
      _Version        =   393216
      Cols            =   9
      FixedCols       =   0
      RowHeightMin    =   900
      BackColor       =   16448250
      ForeColor       =   4210752
      BackColorFixed  =   10375936
      ForeColorFixed  =   16777215
      BackColorSel    =   16761024
      ForeColorSel    =   16777215
      BackColorBkg    =   16448250
      GridColor       =   -2147483644
      AllowBigSelection=   0   'False
      ScrollBars      =   2
      SelectionMode   =   1
      BorderStyle     =   0
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Image ImgSC_Delete 
      Height          =   720
      Left            =   5880
      Picture         =   "TipoVuelto.frx":15216
      Stretch         =   -1  'True
      Top             =   3000
      Visible         =   0   'False
      Width           =   720
   End
   Begin VB.Label lbl_finalizar 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H0000C000&
      Caption         =   "                     Finalizar"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FAFAFA&
      Height          =   1200
      Left            =   10440
      TabIndex        =   7
      Top             =   8040
      Width           =   1905
   End
End
Attribute VB_Name = "f_TipoVuelto"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Salir As Boolean

Private InfoMoneda       As New cls_Monedas
Private InfoDenominacion As New cls_Denominaciones

Private TotalVuelto As Double

Public CodMonedaUlt As String
Public CodDenominacionUlt As String

Private CodMoneda As String

Public TipoMonederoSW As String
Public DocumentoIDSW As String

Private Sub Image1_Click()
    
    Salir = True
    
    If Me.Visible Then
        Me.Hide
    End If
    
End Sub

Private Sub cmd_Cupon_Click()
    
    If Not cmd_Cupon.Enabled Then
        Exit Sub
    End If
    
    If Not VerificacionElectronica_Maneja Then
        Mensaje True, StellarMensaje(4135) '"Esta forma de vuelto necesita de Verificación Electrónica activa."
        Exit Sub
    End If

    Dim DenominacionCuponCompra As String ' Esto debe venir global, borrarse cuando se vaya a implementar.
    
    If Trim(IDCuenta_BWL_CS) = Empty _
    Or Trim(DenominacionCuponCompra) = Empty Then
        Mensaje True, StellarMensaje(4144) '"La configuración de cupones o sus denominaciones no están definidas."
        Exit Sub
    End If
    
    Lc_Cambio = RoundDownFive(CDec(Me.lbl_MontoVuelto.Tag) - CDec(Me.lbl_MontoPagado.Tag), 8)
    
    If RoundUp(Lc_Cambio, gDecMonedaPred) <= 0 Then
        Exit Sub
    End If
    
    f_Vuelto.TipoVuelto = "Cupon_BWLCS"
    
    f_Vuelto.txt_DenominacionCod.Visible = True
    f_Vuelto.txt_DenominacionDesc.Visible = True
    f_Vuelto.CmdBuscarDenominacion.Visible = True
    
    If CodMonedaUlt <> Empty Then
        CodMoneda = CodMonedaUlt
    Else
        CodMoneda = Moneda_Sel
    End If
    
    InfoMoneda.BuscarMonedas , CodMoneda
    
    InfoDenominacion.BuscarDenominacion , CodMoneda, DenominacionCuponCompra, , False
    
    Set f_Vuelto.InfoMoneda = InfoMoneda
    Set f_Vuelto.InfoDenominacion = InfoDenominacion
    
    f_Vuelto.lbl_Vuelto.Caption = _
    StellarMensaje(2735) & Space(1) & gSimMonedaPred 'BuscarSimboloMoneda(Entorno.VAD10Local, BuscarCodigoMonedaPredeterminada(Entorno.VAD10Local)) '& InfoMoneda.SimMoneda
    
    f_Vuelto.txt_MonedaCodigo.Text = InfoMoneda.CodMoneda
    f_Vuelto.txt_MonedaDesc.Text = InfoMoneda.DesMoneda
    f_Vuelto.txt_MonedaFactor.Text = FormatNumber(InfoMoneda.FacMoneda, InfoMoneda.DecMoneda)
    
    Lc_Cambio = RoundDownFive(CDec(Me.lbl_MontoVuelto.Tag) - CDec(Me.lbl_MontoPagado.Tag), 8)
    
    'f_Vuelto.lbl_ValorVuelto.Caption = FormatNumber(Vuelt, gDecMonedaPred)
    'f_Vuelto.lbl_ValorVuelto.Caption = FormatNumber(Lc_Cambio, gDecMonedaPred)
    f_Vuelto.lbl_ValorVuelto.Caption = Me.lbl_montoRestante.Caption
    
    If gFactorMonedaAlterna1 > 0 _
    And UCase(gCodMonedaAlterna1) = UCase(CodMoneda) Then
        'f_Vuelto.lbl_ValorVuelto2.Tag = FrmPagos.lblVueltoDivisas.Tag
        If (CDec(Me.lbl_MontoVueltoAlt.Tag) - CDec(MontoTotalVueltoPagado / InfoMoneda.FacMoneda)) <> 0 Then
            TasaImplicita = Lc_Cambio / (CDec(Me.lbl_MontoVueltoAlt.Tag) - CDec(MontoTotalVueltoPagado / InfoMoneda.FacMoneda))
            lc_CambioAlt = CDec(Lc_Cambio / TasaImplicita)
            f_Vuelto.lbl_ValorVuelto2.Tag = lc_CambioAlt
        Else
            f_Vuelto.lbl_ValorVuelto.Caption = "0"
        End If
    Else
        f_Vuelto.lbl_ValorVuelto2.Tag = FormatNumber( _
        CDec(f_Vuelto.lbl_ValorVuelto.Caption) / InfoMoneda.FacMoneda, InfoMoneda.DecMoneda) ' Aqui guardamos el valor de vuelto en la _
        moneda del pago al cliente con sus respectiva cantidad de decimales.
    End If
    
    InfoMoneda.BuscarMonedas , CodMonedaUlt
    
    If CDec(f_Vuelto.lbl_ValorVuelto2.Tag) = 0 Then
        f_Vuelto.lbl_ValorVuelto2.Tag = (1# / (10 ^ InfoMoneda.DecMoneda))
    End If
    
    f_Vuelto.lbl_ValorVuelto2.Caption = InfoMoneda.SimMoneda & " " & _
    FormatNumber(f_Vuelto.lbl_ValorVuelto2.Tag, InfoMoneda.DecMoneda) ' Esto es para mostrar, a nivel de presentación.
    
    f_Vuelto.txt_MonedaCodigo.Text = InfoMoneda.CodMoneda
    f_Vuelto.txt_MonedaDesc.Text = InfoMoneda.DesMoneda
    f_Vuelto.txt_MonedaFactor.Text = FormatNumber(InfoMoneda.FacMoneda, InfoMoneda.DecMoneda)
    
    'f_Vuelto.txt_Cantidad.Text = FormatNumber(Vuelt, InfoMoneda.DecMoneda)
    f_Vuelto.txt_Cantidad.Text = FormatNumber(f_Vuelto.lbl_ValorVuelto2.Tag, InfoMoneda.DecMoneda)
    f_Vuelto.txt_montoapagar.Text = f_Vuelto.txt_Cantidad.Text
    
    'f_Vuelto.txt_Monto.Text = FormatNumber(Vuelt, gDecMonedaPred)
    f_Vuelto.txt_Monto.Text = Lc_Cambio 'FormatNumber(Vuelt, gDecMonedaPred)
    
    f_Vuelto.txt_DenominacionCod.Text = InfoDenominacion.CodDenomina
    f_Vuelto.txt_DenominacionDesc.Text = InfoDenominacion.DesDenomina
    
    'f_Vuelto.txt_Cantidad.Text = _
    'FormatNumber(f_Vuelto.lbl_ValorVuelto2.Tag, InfoMoneda.DecMoneda)
    
    If UCase(InfoMoneda.CodMoneda) <> UCase(gCodMonedaPred) Then
        f_Vuelto.lbl_ValorVuelto2.Visible = True
    Else
        f_Vuelto.lbl_ValorVuelto2.Visible = False
    End If
        
    'Else
        'f_Vuelto.lbl_ValorVuelto2.Visible = False
    'End If
    
    f_Vuelto.Show vbModal
    
    If f_Vuelto.Salir Then
        
        Unload f_Vuelto
        
        Set f_Vuelto = Nothing
        
        Salir = True
        
        Exit Sub
        
    Else
        
        'Salir = False
        'Me.Hide
        
        Unload f_Vuelto
        
        Set f_Vuelto = Nothing
        
        Me.lbl_MontoPagado.Tag = CDec(MontoTotalVueltoPagado)
        Me.lbl_MontoPagado.Caption = FormatNumber(MontoTotalVueltoPagado, gDecMonedaPred)
        Me.lbl_montoRestante.Tag = CDec(Me.lbl_MontoVuelto.Caption) - CDec(Me.lbl_MontoPagado.Caption)
        Me.lbl_montoRestante.Caption = FormatNumber(CDec(Me.lbl_montoRestante.Tag), gDecMonedaPred)
        
        If gFactorMonedaAlterna1 <> 0 Then
            
            Me.lbl_MontoPagadoAlt.Caption = FormatNumber( _
            CDec(MontoTotalVueltoPagado) / CDec(gFactorMonedaAlterna1), gDecimalesMonedaAlterna1)
            
            Me.lbl_MontoRestanteAlt.Caption = FormatNumber( _
            CDec(Me.lbl_MontoVueltoAlt.Caption) - CDec(Me.lbl_MontoPagadoAlt.Caption), gDecimalesMonedaAlterna1)
            
        End If
        
        If CDec(Me.lbl_MontoPagado.Caption) >= CDec(Me.lbl_MontoVuelto.Caption) Then
            Salir = False
            Me.Hide
        End If
        
    End If
    
End Sub

Private Sub cmd_Efectivo_Click()
    
    If Not cmd_Efectivo.Enabled Then
        Exit Sub
    End If
    
    Lc_Cambio = RoundDownFive(CDec(Me.lbl_MontoVuelto.Tag) - CDec(Me.lbl_MontoPagado.Tag), 8)
    
    If RoundUp(Lc_Cambio, gDecMonedaPred) <= 0 Then
        Exit Sub
    End If
    
    f_Vuelto.TipoVuelto = "Efectivo"
    
    f_Vuelto.txt_DenominacionCod.Visible = False
    f_Vuelto.txt_DenominacionDesc.Visible = False
    f_Vuelto.CmdBuscarDenominacion.Visible = False
    
    If CodMonedaUlt <> Empty Then
        CodMoneda = CodMonedaUlt
    Else
        CodMoneda = Moneda_Sel
    End If
    
    InfoMoneda.BuscarMonedas , CodMoneda
    
    InfoDenominacion.BuscarDenominacion , CodMoneda, "Efectivo", , False
    
    Set f_Vuelto.InfoMoneda = InfoMoneda
    Set f_Vuelto.InfoDenominacion = InfoDenominacion
    
    f_Vuelto.lbl_Vuelto.Caption = _
    StellarMensaje(2735) & Space(1) & gSimMonedaPred 'BuscarSimboloMoneda(Entorno.VAD10Local, BuscarCodigoMonedaPredeterminada(Entorno.VAD10Local))
    
    Lc_Cambio = RoundDownFive(CDec(Me.lbl_MontoVuelto.Tag) - CDec(Me.lbl_MontoPagado.Tag), 8)
    
    'f_Vuelto.lbl_ValorVuelto.Caption = FormatNumber(Vuelt, gDecMonedaPred)
    'f_Vuelto.lbl_ValorVuelto.Caption = FormatNumber(Lc_Cambio, gDecMonedaPred)
    f_Vuelto.lbl_ValorVuelto.Caption = Me.lbl_montoRestante.Caption
    
    If gFactorMonedaAlterna1 > 0 _
    And UCase(gCodMonedaAlterna1) = UCase(CodMoneda) Then
        'f_Vuelto.lbl_ValorVuelto2.Tag = FrmPagos.lblVueltoDivisas.Tag
        If (CDec(Me.lbl_MontoVueltoAlt.Tag) - CDec(MontoTotalVueltoPagado / InfoMoneda.FacMoneda)) <> 0 Then
            TasaImplicita = Lc_Cambio / (CDec(Me.lbl_MontoVueltoAlt.Tag) - CDec(MontoTotalVueltoPagado / InfoMoneda.FacMoneda))
            lc_CambioAlt = CDec(Lc_Cambio / TasaImplicita)
            f_Vuelto.lbl_ValorVuelto2.Tag = lc_CambioAlt
        Else
            f_Vuelto.lbl_ValorVuelto.Caption = "0"
        End If
    Else
        f_Vuelto.lbl_ValorVuelto2.Tag = FormatNumber( _
        CDec(f_Vuelto.lbl_ValorVuelto.Caption) / InfoMoneda.FacMoneda, InfoMoneda.DecMoneda) ' Aqui guardamos el valor de vuelto en la _
        moneda del pago al cliente con sus respectiva cantidad de decimales.
    End If
    
    If CDec(f_Vuelto.lbl_ValorVuelto2.Tag) = 0 Then
        f_Vuelto.lbl_ValorVuelto2.Tag = (1# / (10 ^ InfoMoneda.DecMoneda))
    End If
    
    f_Vuelto.lbl_ValorVuelto2.Caption = InfoMoneda.SimMoneda & " " & _
    FormatNumber(f_Vuelto.lbl_ValorVuelto2.Tag, InfoMoneda.DecMoneda) ' Esto es para mostrar, a nivel de presentación.
    
    f_Vuelto.txt_MonedaCodigo.Text = InfoMoneda.CodMoneda
    f_Vuelto.txt_MonedaDesc.Text = InfoMoneda.DesMoneda
    f_Vuelto.txt_MonedaFactor.Text = FormatNumber(InfoMoneda.FacMoneda, InfoMoneda.DecMoneda)
    
    f_Vuelto.txt_Cantidad.Text = FormatNumber(f_Vuelto.lbl_ValorVuelto2.Tag, InfoMoneda.DecMoneda)
    f_Vuelto.txt_montoapagar.Text = f_Vuelto.txt_Cantidad.Text
    
    f_Vuelto.txt_Monto.Text = Lc_Cambio 'FormatNumber(Vuelt, gDecMonedaPred)
    
    f_Vuelto.txt_DenominacionCod.Text = InfoDenominacion.CodDenomina
    f_Vuelto.txt_DenominacionDesc.Text = InfoDenominacion.DesDenomina
    
    If UCase(InfoMoneda.CodMoneda) <> UCase(gCodMonedaPred) Then 'UCase(f_Total.mCodigoMonedaPredeterminada) Then
        f_Vuelto.lbl_ValorVuelto2.Visible = True
    Else
        f_Vuelto.lbl_ValorVuelto2.Visible = False
    End If
    
    f_Vuelto.Show vbModal
    
    If f_Vuelto.Salir Then
        
        Unload f_Vuelto
        
        Set f_Vuelto = Nothing
        
        Salir = True
        
        Exit Sub
        
    Else
        
        'Salir = False
        'Me.Hide
        
        Unload f_Vuelto
        
        Set f_Vuelto = Nothing
        
        Me.lbl_MontoPagado.Tag = CDec(MontoTotalVueltoPagado)
        Me.lbl_MontoPagado.Caption = FormatNumber(MontoTotalVueltoPagado, gDecMonedaPred)
        Me.lbl_montoRestante.Tag = CDec(Me.lbl_MontoVuelto.Caption) - CDec(Me.lbl_MontoPagado.Caption)
        Me.lbl_montoRestante.Caption = FormatNumber(CDec(Me.lbl_montoRestante.Tag), gDecMonedaPred)
        
        If gFactorMonedaAlterna1 <> 0 Then
            
            Me.lbl_MontoPagadoAlt.Caption = FormatNumber( _
            CDec(MontoTotalVueltoPagado) / CDec(gFactorMonedaAlterna1), gDecimalesMonedaAlterna1)
            
            Me.lbl_MontoRestanteAlt.Caption = FormatNumber( _
            CDec(Me.lbl_MontoVueltoAlt.Caption) - CDec(Me.lbl_MontoPagadoAlt.Caption), gDecimalesMonedaAlterna1)
            
        End If
        
        If CDec(Me.lbl_MontoPagado.Caption) >= CDec(Me.lbl_MontoVuelto.Caption) Then
            Salir = False
            Me.Hide
        End If
        
    End If
    
End Sub

Private Sub cmd_Merchant_Click()
    
    If Not cmd_Merchant.Enabled Then
        Exit Sub
    End If
    
    Lc_Cambio = RoundDownFive(CDec(Me.lbl_MontoVuelto.Tag) - CDec(Me.lbl_MontoPagado.Tag), 8)
    
    If RoundUp(Lc_Cambio, gDecMonedaPred) <= 0 Then
        Exit Sub
    End If
    
    f_Vuelto.TipoVuelto = "Merchant"
    
    f_Vuelto.txt_DenominacionCod.Visible = True
    f_Vuelto.txt_DenominacionDesc.Visible = True
    f_Vuelto.CmdBuscarDenominacion.Visible = True
    
    CodMoneda = gCodMonedaPred
    
    InfoMoneda.BuscarMonedas , CodMoneda
    
    InfoDenominacion.BuscarDenominacion , CodMoneda, DenominacionVueltoMerchant, , False
    
    Set f_Vuelto.InfoMoneda = InfoMoneda
    Set f_Vuelto.InfoDenominacion = InfoDenominacion
    
    f_Vuelto.lbl_Vuelto.Caption = _
    StellarMensaje(2735) & Space(1) & gSimMonedaPred 'BuscarSimboloMoneda(Entorno.VAD10Local, BuscarCodigoMonedaPredeterminada(Entorno.VAD10Local))
    
    Lc_Cambio = RoundDownFive(CDec(Me.lbl_MontoVuelto.Tag) - CDec(Me.lbl_MontoPagado.Tag), 8)
    
    ' f_Vuelto.lbl_ValorVuelto.Caption = FormatNumber(Vuelt, gDecMonedaPred)
    'f_Vuelto.lbl_ValorVuelto.Caption = FormatNumber(Lc_Cambio, gDecMonedaPred)
    f_Vuelto.lbl_ValorVuelto.Caption = Me.lbl_montoRestante.Caption
    
    'If UCase(InfoMoneda.CodMoneda) <> UCase(f_Total.mCodigoMonedaPredeterminada) Then
        
        InfoMoneda.BuscarMonedas , CodMoneda 'f_Total.GRID.TextMatrix(f_Total.GRID.Rows - 2, 7)
        
        If gFactorMonedaAlterna1 > 0 _
        And UCase(gCodMonedaAlterna1) = UCase(CodMoneda) Then
            If (CDec(FrmPagos.lblVueltoDivisas.Tag) - CDec(MontoTotalVueltoPagado / InfoMoneda.FacMoneda)) <> 0 Then
                TasaImplicita = Lc_Cambio / (CDec(FrmPagos.lblVueltoDivisas.Tag) - CDec(MontoTotalVueltoPagado / InfoMoneda.FacMoneda))
                lc_CambioAlt = CDec(Lc_Cambio / TasaImplicita)
                f_Vuelto.lbl_ValorVuelto2.Tag = lc_CambioAlt
            Else
                f_Vuelto.lbl_ValorVuelto.Caption = "0"
            End If
        Else
            f_Vuelto.lbl_ValorVuelto2.Tag = FormatNumber( _
            CDec(f_Vuelto.lbl_ValorVuelto.Caption) / InfoMoneda.FacMoneda, InfoMoneda.DecMoneda) ' Aqui guardamos el valor de vuelto en la _
            moneda del pago al cliente con sus respectiva cantidad de decimales.
        End If
        
        If CDec(f_Vuelto.lbl_ValorVuelto2.Tag) = 0 Then
            f_Vuelto.lbl_ValorVuelto2.Tag = (1# / (10 ^ InfoMoneda.DecMoneda))
        End If
        
        f_Vuelto.lbl_ValorVuelto2.Caption = InfoMoneda.SimMoneda & " " & _
        FormatNumber(f_Vuelto.lbl_ValorVuelto2.Tag, InfoMoneda.DecMoneda) ' Esto es para mostrar, a nivel de presentación.
        
        f_Vuelto.txt_MonedaCodigo.Text = InfoMoneda.CodMoneda
        f_Vuelto.txt_MonedaDesc.Text = InfoMoneda.DesMoneda
        f_Vuelto.txt_MonedaFactor.Text = FormatNumber(InfoMoneda.FacMoneda, InfoMoneda.DecMoneda)
        
        f_Vuelto.txt_Cantidad.Text = FormatNumber(f_Vuelto.lbl_ValorVuelto2.Tag, InfoMoneda.DecMoneda)  'FormatNumber(Vuelt, InfoMoneda.DecMoneda)
        f_Vuelto.txt_montoapagar.Text = f_Vuelto.txt_Cantidad.Text
        
        f_Vuelto.txt_Monto.Text = Lc_Cambio 'FormatNumber(Vuelt, gDecMonedaPred)
        
        f_Vuelto.txt_DenominacionCod.Text = InfoDenominacion.CodDenomina
        f_Vuelto.txt_DenominacionDesc.Text = InfoDenominacion.DesDenomina
            
        'f_Vuelto.txt_Cantidad.Text = FormatNumber(f_Vuelto.lbl_ValorVuelto2.Tag, InfoMoneda.DecMoneda)
        
        If UCase(InfoMoneda.CodMoneda) <> UCase(gCodMonedaPred) Then
            f_Vuelto.lbl_ValorVuelto2.Visible = True
        Else
            f_Vuelto.lbl_ValorVuelto2.Visible = False
        End If
        
    'Else
        'f_Vuelto.lbl_ValorVuelto2.Visible = False
    'End If
    
    f_Vuelto.Show vbModal
    
    If f_Vuelto.Salir Then
        
        Unload f_Vuelto
        
        Set f_Vuelto = Nothing
        
        Salir = True
        
        Exit Sub
        
    Else
        
        'Salir = False
        'Me.Hide
        
        Unload f_Vuelto
        
        Set f_Vuelto = Nothing
        
        Me.lbl_MontoPagado.Tag = CDec(MontoTotalVueltoPagado)
        Me.lbl_MontoPagado.Caption = FormatNumber(MontoTotalVueltoPagado, gDecMonedaPred)
        Me.lbl_montoRestante.Tag = CDec(Me.lbl_MontoVuelto.Caption) - CDec(Me.lbl_MontoPagado.Caption)
        Me.lbl_montoRestante.Caption = FormatNumber(CDec(Me.lbl_montoRestante.Tag), gDecMonedaPred)
        
        If gFactorMonedaAlterna1 <> 0 Then
            
            Me.lbl_MontoPagadoAlt.Caption = FormatNumber( _
            CDec(MontoTotalVueltoPagado) / CDec(gFactorMonedaAlterna1), gDecimalesMonedaAlterna1)
            
            Me.lbl_MontoRestanteAlt.Caption = FormatNumber( _
            CDec(Me.lbl_MontoVueltoAlt.Caption) - CDec(Me.lbl_MontoPagadoAlt.Caption), gDecimalesMonedaAlterna1)
            
        End If
        
        If CDec(Me.lbl_MontoPagado.Caption) >= CDec(Me.lbl_MontoVuelto.Caption) Then
            
            Salir = False
            Me.Hide
            
        End If
        
    End If
    
End Sub

Private Sub cmd_Otros_Click()
    
    If Not cmd_Otros.Enabled Then
        Exit Sub
    End If
    
    Lc_Cambio = RoundDownFive(CDec(Me.lbl_MontoVuelto.Tag) - CDec(Me.lbl_MontoPagado.Tag), 8)
    
    If RoundUp(Lc_Cambio, gDecMonedaPred) <= 0 Then
        Exit Sub
    End If
    
    f_Vuelto.TipoVuelto = "Otros"
    
    f_Vuelto.txt_DenominacionCod.Visible = True
    f_Vuelto.txt_DenominacionDesc.Visible = True
    f_Vuelto.CmdBuscarDenominacion.Visible = True
    
    f_Vuelto.CmdBuscarMoneda.Enabled = True
    f_Vuelto.CmdBuscarDenominacion.Enabled = True
    
    If CodMonedaUlt <> Empty Then
        CodMoneda = CodMonedaUlt
    Else
        CodMoneda = Moneda_Sel
    End If
    
    InfoMoneda.BuscarMonedas , CodMoneda
    
    InfoDenominacion.BuscarDenominacion , CodMoneda, "Efectivo", , False
    
    Set f_Vuelto.InfoMoneda = InfoMoneda
    Set f_Vuelto.InfoDenominacion = InfoDenominacion
    
    f_Vuelto.lbl_Vuelto.Caption = _
    StellarMensaje(2735) & Space(1) & gSimMonedaPred 'BuscarSimboloMoneda(Entorno.VAD10Local, BuscarCodigoMonedaPredeterminada(Entorno.VAD10Local))
    
    Lc_Cambio = RoundDownFive(CDec(Me.lbl_MontoVuelto.Tag) - CDec(Me.lbl_MontoPagado.Tag), 8)
    
    'f_Vuelto.lbl_ValorVuelto.Caption = FormatNumber(Vuelt, gDecMonedaPred)
    'f_Vuelto.lbl_ValorVuelto.Caption = FormatNumber(Lc_Cambio, gDecMonedaPred)
    f_Vuelto.lbl_ValorVuelto.Caption = Me.lbl_montoRestante.Caption
    
    If gFactorMonedaAlterna1 > 0 _
    And UCase(gCodMonedaAlterna1) = UCase(CodMoneda) Then
        'f_Vuelto.lbl_ValorVuelto2.Tag = FrmPagos.lblVueltoDivisas.Tag
        If (CDec(Me.lbl_MontoVueltoAlt.Tag) - CDec(MontoTotalVueltoPagado / InfoMoneda.FacMoneda)) <> 0 Then
            TasaImplicita = Lc_Cambio / (CDec(Me.lbl_MontoVueltoAlt.Tag) - CDec(MontoTotalVueltoPagado / InfoMoneda.FacMoneda))
            lc_CambioAlt = CDec(Lc_Cambio / TasaImplicita)
            f_Vuelto.lbl_ValorVuelto2.Tag = lc_CambioAlt
        Else
            f_Vuelto.lbl_ValorVuelto.Caption = "0"
        End If
    Else
        f_Vuelto.lbl_ValorVuelto2.Tag = FormatNumber( _
        CDec(f_Vuelto.lbl_ValorVuelto.Caption) / InfoMoneda.FacMoneda, InfoMoneda.DecMoneda) ' Aqui guardamos el valor de vuelto en la _
        moneda del pago al cliente con sus respectiva cantidad de decimales.
    End If
    
    If CDec(f_Vuelto.lbl_ValorVuelto2.Tag) = 0 Then
        f_Vuelto.lbl_ValorVuelto2.Tag = (1# / (10 ^ InfoMoneda.DecMoneda))
    End If
    
    f_Vuelto.lbl_ValorVuelto2.Caption = InfoMoneda.SimMoneda & " " & _
    FormatNumber(f_Vuelto.lbl_ValorVuelto2.Tag, InfoMoneda.DecMoneda) ' Esto es para mostrar, a nivel de presentación.
    
    f_Vuelto.txt_MonedaCodigo.Text = InfoMoneda.CodMoneda
    f_Vuelto.txt_MonedaDesc.Text = InfoMoneda.DesMoneda
    f_Vuelto.txt_MonedaFactor.Text = FormatNumber(InfoMoneda.FacMoneda, InfoMoneda.DecMoneda)
    
    f_Vuelto.txt_Cantidad.Text = FormatNumber(f_Vuelto.lbl_ValorVuelto2.Tag, InfoMoneda.DecMoneda)
    f_Vuelto.txt_montoapagar.Text = f_Vuelto.txt_Cantidad.Text
    
    f_Vuelto.txt_Monto.Text = Lc_Cambio 'FormatNumber(Vuelt, gDecMonedaPred)
    
    f_Vuelto.txt_DenominacionCod.Text = InfoDenominacion.CodDenomina
    f_Vuelto.txt_DenominacionDesc.Text = InfoDenominacion.DesDenomina
    
    If UCase(InfoMoneda.CodMoneda) <> UCase(gCodMonedaPred) Then
        f_Vuelto.lbl_ValorVuelto2.Visible = True
    Else
        f_Vuelto.lbl_ValorVuelto2.Visible = False
    End If
    
    f_Vuelto.Show vbModal
    
    If f_Vuelto.Salir Then
        
        Unload f_Vuelto
        
        Set f_Vuelto = Nothing
        
        Salir = True
        
        Exit Sub
        
    Else
        
        'Salir = False
        'Me.Hide
        
        Unload f_Vuelto
        
        Set f_Vuelto = Nothing
        
        Me.lbl_MontoPagado.Tag = CDec(MontoTotalVueltoPagado)
        Me.lbl_MontoPagado.Caption = FormatNumber(MontoTotalVueltoPagado, gDecMonedaPred)
        Me.lbl_montoRestante.Tag = CDec(Me.lbl_MontoVuelto.Caption) - CDec(Me.lbl_MontoPagado.Caption)
        Me.lbl_montoRestante.Caption = FormatNumber(CDec(Me.lbl_montoRestante.Tag), gDecMonedaPred)
        
        If gFactorMonedaAlterna1 <> 0 Then
            
            Me.lbl_MontoPagadoAlt.Caption = FormatNumber( _
            CDec(MontoTotalVueltoPagado) / CDec(gFactorMonedaAlterna1), gDecimalesMonedaAlterna1)
            
            Me.lbl_MontoRestanteAlt.Caption = FormatNumber( _
            CDec(Me.lbl_MontoVueltoAlt.Caption) - CDec(Me.lbl_MontoPagadoAlt.Caption), gDecimalesMonedaAlterna1)
            
        End If
        
        If CDec(Me.lbl_MontoPagado.Caption) >= CDec(Me.lbl_MontoVuelto.Caption) Then
            
            Salir = False
            Me.Hide
            
        End If
        
    End If
    
End Sub

Private Sub cmd_StellarWallet_Click()
    
    If Not cmd_StellarWallet.Enabled Then
        Exit Sub
    End If
    
    If Not gVerificacion.Verificacion Then
        Mensaje True, StellarMensaje(4135) '"Esta forma de vuelto necesita de Verificación Electrónica activa."
        Exit Sub
    End If
    
    Lc_Cambio = RoundDownFive(CDec(Me.lbl_MontoVuelto.Tag) - CDec(Me.lbl_MontoPagado.Tag), 8)
    
    If RoundDecimalUp(Lc_Cambio, gDecMonedaPred) <= 0 Then
        Exit Sub
    End If
    
    Dim CodMonedaSW As String
    Dim CodDenominacionSW As String
    
    f_Vuelto.TipoVuelto = "Stellar_Wallet"
    
    f_Vuelto.txt_DenominacionCod.Visible = True
    f_Vuelto.txt_DenominacionDesc.Visible = True
    f_Vuelto.CmdBuscarDenominacion.Visible = True
    
    Dim ClsMoneda_VE As New cls_Monedas
    Dim ClsDenominacion_VE As New cls_Denominaciones
    
    ClsMoneda_VE.InicializarConexiones Ent.BDD, Ent.POS 'Entorno.VAD10Local, Entorno.VAD20Local
    ClsDenominacion_VE.InicializarConexiones Ent.BDD, Ent.POS 'Entorno.VAD10Local, Entorno.VAD20Local
    
    SafePropAssign gVerificacionElectro, "ClsMoneda_VE_Prop", ClsMoneda_VE
    SafePropAssign gVerificacionElectro, "ClsDenominacion_VE_Prop", ClsDenominacion_VE
    
'    If StellarWallet_IDClienteAuto Then
'        SafePropAssign gVerificacionElectro, "IDCliente_Entrada_Prop", _
'        IIf(Trim(FrmPagos.ClaseMeseros.ClienteRif) <> Empty, FrmPagos.ClaseMeseros.ClienteRif, Empty)
'    End If
    
    Dim DatosMonedero As String
    
    DatosMonedero = gVerificacionElectro.StellarWallet_InfoMonedero
    
    If Trim(DatosMonedero) = Empty Then
        Exit Sub
    End If
    
    Datos = Split(DatosMonedero, "|")
    
    FormaPago = Split(Datos(0), ";")
    
    CodMonedaSW = FormaPago(0)
    CodDenominacionSW = FormaPago(1)
    
    DataRecarga = Split(Datos(1), ";")
    
    DocumentoIDSW = DataRecarga(0)
    TipoMonederoSW = DataRecarga(1)
    
    ''TEST
    'FormaPago = "Dol_Tarj" '"Bs" '"Dol_Tarj"
    'CodMonedaSW = "0003" '"0000000002" '"0003"
    'CodDenominacionSW = "Dol_Tarj" '"DC_CARD" '"Dol_Tarj"
    'DocumentoIDSW = "23739969"
    'TipoMonederoSW = "1"
    
    CodMoneda = CodMonedaSW
    
    'CodMonedaUlt = f_Total.Grid.TextMatrix(f_Total.Grid.Rows - 2, 7)
    
    'If CodMonedaUlt <> Empty Then
    '  CodMoneda = CodMonedaUlt
    'Else
    '  CodMoneda = Moneda_Sel
    'End If
    
    InfoMoneda.BuscarMonedas , CodMoneda
    
    InfoDenominacion.BuscarDenominacion , CodMoneda, CodDenominacionSW, , False
    
    Set f_Vuelto.InfoMoneda = InfoMoneda
    Set f_Vuelto.InfoDenominacion = InfoDenominacion
    
    f_Vuelto.lbl_Vuelto.Caption = _
    StellarMensaje(2735) & Space(1) & gSimMonedaPred  'BuscarSimboloMoneda(Entorno.VAD10Local, BuscarCodigoMonedaPredeterminada(Entorno.VAD10Local)) '& InfoMoneda.SimMoneda
    
    f_Vuelto.txt_MonedaCodigo.Text = InfoMoneda.CodMoneda
    f_Vuelto.txt_MonedaDesc.Text = InfoMoneda.DesMoneda
    f_Vuelto.txt_MonedaFactor.Text = FormatNumber(InfoMoneda.FacMoneda, InfoMoneda.DecMoneda)
    
    Lc_Cambio = RoundDownFive(CDec(Me.lbl_MontoVuelto.Tag) - CDec(Me.lbl_MontoPagado.Tag), 8)
    
    'f_Vuelto.lbl_ValorVuelto.Caption = FormatNumber(Vuelt, gDecMonedaPred)
    'f_Vuelto.lbl_ValorVuelto.Caption = FormatNumber(Lc_Cambio, gDecMonedaPred)
    f_Vuelto.lbl_ValorVuelto.Caption = Me.lbl_montoRestante.Caption
    
    If gFactorMonedaAlterna1 > 0 _
    And UCase(gCodMonedaAlterna1) = UCase(CodMoneda) Then
        'f_Vuelto.lbl_ValorVuelto2.Tag = FrmPagos.lblVueltoDivisas.Tag
        If (CDec(Me.lbl_MontoVueltoAlt.Tag) - CDec(MontoTotalVueltoPagado / InfoMoneda.FacMoneda)) <> 0 Then
            TasaImplicita = Lc_Cambio / (CDec(Me.lbl_MontoVueltoAlt.Tag) - CDec(MontoTotalVueltoPagado / InfoMoneda.FacMoneda))
            lc_CambioAlt = CDec(Lc_Cambio / TasaImplicita)
            f_Vuelto.lbl_ValorVuelto2.Tag = lc_CambioAlt
        Else
            f_Vuelto.lbl_ValorVuelto.Caption = "0"
        End If
    Else
        f_Vuelto.lbl_ValorVuelto2.Tag = FormatNumber( _
        CDbl(f_Vuelto.lbl_ValorVuelto.Caption) / InfoMoneda.FacMoneda, InfoMoneda.DecMoneda) ' Aqui guardamos el valor de vuelto en la _
        moneda del pago al cliente con sus respectiva cantidad de decimales.
    End If
    
    If CDec(f_Vuelto.lbl_ValorVuelto2.Tag) = 0 Then
        f_Vuelto.lbl_ValorVuelto2.Tag = (1# / (10 ^ InfoMoneda.DecMoneda))
    End If
    
    f_Vuelto.lbl_ValorVuelto2.Caption = InfoMoneda.SimMoneda & " " & _
    FormatNumber(f_Vuelto.lbl_ValorVuelto2.Tag, InfoMoneda.DecMoneda) ' Esto es para mostrar, a nivel de presentación.
    
    f_Vuelto.txt_MonedaCodigo.Text = InfoMoneda.CodMoneda
    f_Vuelto.txt_MonedaDesc.Text = InfoMoneda.DesMoneda
    f_Vuelto.txt_MonedaFactor.Text = FormatNumber(InfoMoneda.FacMoneda, InfoMoneda.DecMoneda)
    
    'f_Vuelto.txt_Cantidad.Text = FormatNumber(Vuelt, InfoMoneda.DecMoneda)
    f_Vuelto.txt_Cantidad.Text = FormatNumber(f_Vuelto.lbl_ValorVuelto2.Tag, InfoMoneda.DecMoneda)
    f_Vuelto.txt_montoapagar.Text = f_Vuelto.txt_Cantidad.Text
    
    'f_Vuelto.txt_Monto.Text = FormatNumber(Vuelt, gDecMonedaPred)
    f_Vuelto.txt_Monto.Text = Lc_Cambio 'FormatNumber(Vuelt, gDecMonedaPred)
    
    f_Vuelto.txt_DenominacionCod.Text = InfoDenominacion.CodDenomina
    f_Vuelto.txt_DenominacionDesc.Text = InfoDenominacion.DesDenomina
    
    'f_Vuelto.txt_Cantidad.Text = _
    'FormatNumber(f_Vuelto.lbl_ValorVuelto2.Tag, InfoMoneda.DecMoneda)
    
    If UCase(InfoMoneda.CodMoneda) <> UCase(gCodMonedaPred) Then
        f_Vuelto.lbl_ValorVuelto2.Visible = True
    Else
        f_Vuelto.lbl_ValorVuelto2.Visible = False
    End If
        
    'Else
        'f_Vuelto.lbl_ValorVuelto2.Visible = False
    'End If
    
    f_Vuelto.Show vbModal
    
    If f_Vuelto.Salir Then
        
        Unload f_Vuelto
        
        Set f_Vuelto = Nothing
        
        Salir = True
        
        Exit Sub
        
    Else
        
        'Salir = False
        'Me.Hide
        
        Unload f_Vuelto
        
        Set f_Vuelto = Nothing
        
        Me.lbl_MontoPagado.Tag = CDec(MontoTotalVueltoPagado)
        Me.lbl_MontoPagado.Caption = FormatNumber(MontoTotalVueltoPagado, gDecMonedaPred)
        Me.lbl_montoRestante.Tag = CDec(Me.lbl_MontoVuelto.Caption) - CDec(Me.lbl_MontoPagado.Caption)
        Me.lbl_montoRestante.Caption = FormatNumber(CDec(Me.lbl_montoRestante.Tag), gDecMonedaPred)
        
        If gFactorMonedaAlterna1 <> 0 Then
            
            Me.lbl_MontoPagadoAlt.Caption = FormatNumber( _
            CDec(MontoTotalVueltoPagado) / CDec(gFactorMonedaAlterna1), gDecimalesMonedaAlterna1)
            
            Me.lbl_MontoRestanteAlt.Caption = FormatNumber( _
            CDec(Me.lbl_MontoVueltoAlt.Caption) - CDec(Me.lbl_MontoPagadoAlt.Caption), gDecimalesMonedaAlterna1)
            
        End If
        
        If CDec(Me.lbl_MontoPagado.Caption) >= CDec(Me.lbl_MontoVuelto.Caption) Then
            
            Salir = False
            Me.Hide
            
        End If
        
    End If
    
End Sub

Private Sub CmdExit_Click()
    
    Salir = True
    
    Call LimpiarVueltosNoElectronicos
    
    If Me.Visible Then
        Me.Hide
    End If
    
End Sub

Private Sub Form_Activate()
    
    Call LlenarDatosGridVueltos(Me.GridVuelto)
    
    Me.lbl_MontoPagado.Tag = CDec(MontoTotalVueltoPagado)
    Me.lbl_MontoPagado.Caption = FormatNumber(MontoTotalVueltoPagado, gDecMonedaPred)
    Me.lbl_montoRestante.Tag = CDec(Me.lbl_MontoVuelto.Caption) - CDec(Me.lbl_MontoPagado.Caption)
    Me.lbl_montoRestante.Caption = FormatNumber(CDec(Me.lbl_montoRestante.Tag), gDecMonedaPred)
    
    Me.lbl_MontoRestanteAlt.Caption = FormatNumber( _
    CDec(Me.lbl_MontoVueltoAlt.Caption) - CDec(Me.lbl_MontoPagadoAlt.Caption), gDecimalesMonedaAlterna1)
    
    Me.lbl_MontoRestanteAlt.Tag = CDec(Me.lbl_MontoVueltoAlt.Caption) - CDec(Me.lbl_MontoPagadoAlt.Caption)
    
    If Me.GridVuelto.Rows > 1 Then
        Me.ImgSC_Delete.Visible = True
        GridVuelto.SelectionMode = flexSelectionByRow
    Else
        GridVuelto.SelectionMode = flexSelectionFree
        GridVuelto.Col = 0
        GridVuelto.ColSel = 0
    End If
    
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyEscape, vbKeyF12
            CmdExit_Click
    End Select
End Sub

Private Sub Form_KeyPress(KeyAscii As Integer)
    Select Case KeyAscii
        Case vbKey1
            cmd_Efectivo_Click
        Case vbKey2
            cmd_Merchant_Click
        Case vbKey3
            cmd_Otros_Click
        Case vbKey4
            cmd_Cupon_Click
    End Select
End Sub

Private Sub Form_Load()
    
    InfoMoneda.InicializarConexiones Ent.BDD, Ent.POS 'Entorno.VAD10Local, Entorno.VAD20Local
    InfoDenominacion.InicializarConexiones Ent.BDD, Ent.POS 'Entorno.VAD10Local, Entorno.VAD20Local
    
    Me.Lbl_Pagado.Caption = StellarMensaje(2518) ' pagado
    Me.Lbl_PagadoAlt.Caption = StellarMensaje(2518) ' pagado
    
    Me.lbl_Vuelto.Caption = Stellar_Mensaje(7035, True) ' & Space(1) & _
    BuscarSimboloMoneda(Entorno.VAD10Local, _
    BuscarCodigoMonedaPredeterminada(Entorno.VAD10Local))
    
    Me.lbl_VueltoAlt = StellarMensaje(2735) ' & Space(1) & _
    BuscarSimboloMoneda(Entorno.VAD10Local, _
    UCase(pMonedaAdicional))
    
    Me.lbl_Restante.Caption = Stellar_Mensaje(4124, True) 'restante
    Me.lbl_RetanteAlt.Caption = Stellar_Mensaje(4124, True) 'restante
    
    Me.lbl_simbPagado.Caption = gSimMonedaPred 'BuscarSimboloMoneda(Entorno.VAD10Local, BuscarCodigoMonedaPredeterminada(Entorno.VAD10Local))
    Me.lbl_simbVuelto.Caption = Me.lbl_simbPagado.Caption
    Me.lbl_SimboloMontoRestante = Me.lbl_simbPagado.Caption
    
    Me.lbl_simbVuelAlt.Caption = gSimboloMonedaAlterna1 'BuscarSimboloMoneda(Entorno.VAD10Local, UCase(pMonedaAdicional))
    Me.lbl_simbpagAlt.Caption = Me.lbl_simbVuelAlt.Caption
    Me.lbl_SimboloMontoRestanteAlt.Caption = Me.lbl_simbVuelAlt.Caption
    
    ConstruirGrid
    
End Sub

Private Sub ConstruirGrid()
    
    With Me.GridVuelto
        
        .RowHeightMin = 900
        .Rows = 1
        .Row = 0
        
        .Cols = 10
        
        .Col = 0 ' codigo moneda
            .Text = "codigo moneda"
            .ColWidth(0) = 0
            
        .Col = 1
            
            .Text = UCase(Replace(StellarMensaje(134), ":", Empty)) ' Moneda
            .ColAlignment(.Col) = flexAlignLeftCenter
            .CellAlignment = 4
            .ColWidth(1) = 2400
            
        .Col = 2
            
            .Text = UCase(StellarMensaje(3333)) ' Tasa '"FAC" ' Factor
            .ColAlignment(.Col) = flexAlignCenterCenter
            .CellAlignment = flexAlignCenterCenter
            .ColWidth(2) = 2000
            
        .Col = 3
            
            .Text = "cod denomina"
            .ColWidth(3) = 0
            
        .Col = 4
            
            .Text = UCase(StellarMensaje(15518)) ' Tipo / Denominación
            .CellAlignment = 4
            .ColWidth(4) = 2500
            
        .Col = 5
            
            .Text = "EsVerifONo"
            .ColWidth(5) = 0
            
        .Col = 6
            
            .Text = UCase(StellarMensaje(3001)) 'Cantidad
            .ColAlignment(.Col) = flexAlignRightCenter ' flexAlignLeftCenter
            .CellAlignment = 4
            .ColWidth(6) = 2500
            
        .Col = 7
            
            .Text = UCase(StellarMensaje(2517)) ' Monto
            .CellAlignment = 4
            .ColWidth(7) = 2500
            
        .Col = 8
            
            .Text = UCase("Linea grid")
            .ColWidth(8) = 0
            
        .Col = 9
            
            .Text = "Referencia"
            .ColWidth(9) = 0
            
    End With
    
End Sub

Private Sub GridVuelto_DblClick()
    GridVuelto_KeyDown vbKeyDelete, 0
End Sub

Private Sub GridVuelto_KeyDown(KeyCode As Integer, Shift As Integer)
    
    Dim mAutorizadorCod As String, mAutorizadorNom As String, mAutorizadorNivel As Integer
    
    If KeyCode = vbKeyDelete And GridVuelto.Rows > 1 Then
        
        'If GridVuelto.TextMatrix(GridVuelto.Row, 5) = "0" Then
            
            ValidarConexion Ent.BDD  ' Entorno.VAD20Local
            
            mTmpEsVerifElec = GridVuelto.TextMatrix(GridVuelto.Row, 5)
            
            If SDec(mTmpEsVerifElec) <= 0 Then
                
                Call EliminarLineaVueltoTMP(CDec(GridVuelto.TextMatrix(GridVuelto.Row, 8)))
                
            Else
                
                If Not PermitirEliminarVueltoVerifElec Then
                    
                    Mensaje True, StellarMensaje(4143) '"No se puede eliminar el renglón, posee validación electrónica"
                    Exit Sub
                    
                Else
                    
                    '"Esta operación está conformada vía electrónica." & GetLines & _
                    "Para anularla, debe solicitar la autorización de un supervisor." & GetLines & _
                    "Presione Aceptar si desea continuar."
                    
                    Mensaje False, Replace(StellarMensaje(16173) & _
                    StellarMensaje(16174), "$(Line)", vbNewLine)
                    
                    If FrmAppLink.RetornoMensaje Then
                        
                        Aceptado = FrmAppLink.GetNivelUsuario >= PermitirEliminarVueltoVerifElec_Nivel
                        
                        If Not Aceptado Then
                            
                            Set FrmAutorizacion = FrmAppLink.GetFrmAutorizacion
                            
                            FrmAutorizacion.mNivel = PermitirEliminarVueltoVerifElec_Nivel
                            FrmAutorizacion.Titulo = StellarMensaje(3138) ' "Introduzca las credenciales " & _
                            "de un usuario que ..."
                            
                            Set FrmAutorizacion.mConexion = Ent.BDD
                            
                            FrmAutorizacion.Show vbModal
                            
                            If FrmAutorizacion.mAceptada Then
                                
                                mAutorizadorCod = FrmAutorizacion.mCodigoUsuario
                                mAutorizadorNom = FrmAutorizacion.mUsuario
                                mAutorizadorNivel = FrmAutorizacion.mNivelSupervisor
                                
                                Set FrmAutorizacion = Nothing
                                
                                ' Continuar
                                
                                Aceptado = True
                                
                            Else
                                
                                Set FrmAutorizacion = Nothing
                                Mensaje True, StellarMensaje(16050)
                                Exit Sub
                                
                            End If
                            
                            If Aceptado Then
                                
                                If mAutorizadorNivel >= PermitirEliminarVueltoVerifElec_Nivel Then
                                    
                                    MonedaTmpLocal.BuscarMonedas , GridVuelto.TextMatrix(GridVuelto.Row, 0)
                                    
                                    mTmpInfoAdicional = "Vuelto Eliminado con Verificación Electrónica " & _
                                    "[" & GridVuelto.TextMatrix(GridVuelto.Row, 1) & "]" & _
                                    "[" & GridVuelto.TextMatrix(GridVuelto.Row, 4) & "] " & _
                                    "por Monto: " & IIf(UCase(GridVuelto.TextMatrix(GridVuelto.Row, 0)) <> _
                                    UCase(gCodMonedaPred), _
                                    FormatoDecimalesDinamicos(GridVuelto.TextMatrix(GridVuelto.Row, 6)) & " " & _
                                    MonedaTmpLocal.SimMoneda & _
                                    " (" & FormatNumber(GridVuelto.TextMatrix(GridVuelto.Row, 7), gDecMonedaPred) & " " & _
                                    gSimMonedaPred & ") ", _
                                    FormatNumber(GridVuelto.TextMatrix(GridVuelto.Row, 7), gDecMonedaPred) & _
                                    " " & gSimMonedaPred)
                                    
                                    'Call GrabarAutorizacion("0000000050", mTmpInfoAdicional)
                                    
                                    InsertarAuditoria 480, "Vuelto Eliminado con Verificación Electrónica", _
                                    mTmpInfoAdicional, "f_TipoVuelto", "Vuelto", "N/A", Ent.BDD
                                    
                                Else
                                    
                                    Aceptado = False
                                    Mensaje True, StellarMensaje(11) '"No posee el Nivel Necesario para realizar esta acción."
                                    
                                End If
                                
                            Else
                                Aceptado = False
                            End If
                            
                        End If
                        
                        If Not Aceptado Then
                            Exit Sub
                        Else
                            Call EliminarLineaVueltoTMP_ConVerifElec( _
                            CDec(GridVuelto.TextMatrix(GridVuelto.Row, 8)))
                        End If
                        
                    Else
                        
                        Exit Sub
                        
                    End If
                    
                End If
                
            End If
            
            Call LlenarDatosGridVueltos(GridVuelto)
            
            Me.lbl_MontoPagado.Tag = CDec(MontoTotalVueltoPagado)
            Me.lbl_MontoPagado.Caption = FormatNumber(MontoTotalVueltoPagado, gDecMonedaPred)
            Me.lbl_montoRestante.Tag = CDec(Me.lbl_MontoVuelto.Caption) - CDec(Me.lbl_MontoPagado.Caption)
            Me.lbl_montoRestante.Caption = FormatNumber(CDec(Me.lbl_montoRestante.Tag), gDecMonedaPred)
            
            If gFactorMonedaAlterna1 <> 0 Then
                
                Me.lbl_MontoPagadoAlt.Caption = FormatNumber( _
                CDec(MontoTotalVueltoPagado) / CDec(gFactorMonedaAlterna1), gDecimalesMonedaAlterna1)
                
                Me.lbl_MontoRestanteAlt.Caption = FormatNumber( _
                CDec(Me.lbl_MontoVueltoAlt.Caption) - CDec(Me.lbl_MontoPagadoAlt.Caption), gDecimalesMonedaAlterna1)
                
            End If
            
        'Else
            'Mensaje True, "No se puede eliminar el reglon posee validacion electronica"
        'End If
        
        If GridVuelto.Rows = 1 Then
            Me.ImgSC_Delete.Visible = False
            GridVuelto.SelectionMode = flexSelectionFree
            GridVuelto.Col = 0
            GridVuelto.ColSel = 0
        Else
            GridVuelto.SelectionMode = flexSelectionByRow
        End If
        
    End If
    
End Sub

Private Sub GridVuelto_KeyPress(KeyAscii As Integer)
'    If KeyAscii = vbKeyDelete Then
'        If GridVuelto.TextMatrix(GridVuelto.Row, 5) = "0" Then
'            EliminarLineaVueltoTMP (CInt(GridVuelto.TextMatrix(GridVuelto.Row, 8)))
'            LlenarDatosGridVueltos (GridVuelto)
'        Else
'            Mensaje True, "No se puede eliminar el reglon posee validacion electronica"
'        End If
'
'    End If
End Sub

Private Sub GridVuelto_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    If Button = vbRightButton Then
        GridVuelto_KeyDown vbKeyDelete, 0
    End If
End Sub

Private Sub ImgSC_Delete_Click()
    GridVuelto_KeyDown vbKeyDelete, 0
End Sub

Private Sub lbl_finalizar_Click()
    
    'Salir = False
    'Me.Hide
    
    Me.lbl_MontoPagado.Tag = CDec(MontoTotalVueltoPagado)
    Me.lbl_MontoPagado.Caption = FormatNumber(MontoTotalVueltoPagado, gDecMonedaPred)
    Me.lbl_montoRestante.Tag = CDec(Me.lbl_MontoVuelto.Caption) - CDec(Me.lbl_MontoPagado.Caption)
    Me.lbl_montoRestante.Caption = FormatNumber(CDec(Me.lbl_montoRestante.Tag), gDecMonedaPred)
    
    If gFactorMonedaAlterna1 <> 0 Then
        
        Me.lbl_MontoPagadoAlt.Caption = FormatNumber( _
        CDec(MontoTotalVueltoPagado) / CDec(gFactorMonedaAlterna1), gDecimalesMonedaAlterna1)
        
        Me.lbl_MontoRestanteAlt.Caption = FormatNumber( _
        CDec(Me.lbl_MontoVueltoAlt.Caption) - CDec(Me.lbl_MontoPagadoAlt.Caption), gDecimalesMonedaAlterna1)
        
    End If
    
    If CDec(Me.lbl_MontoPagado.Caption) >= CDec(Me.lbl_MontoVuelto.Caption) Then
        Salir = False
        Me.Hide
    Else
        Mensaje True, StellarMensaje(4136) '"Falta un monto por devolver o el monto devuelto no coincide con el total de vuelto."
    End If
    
End Sub

Public Sub LimpiarVueltosNoElectronicos()
    
    mSQL = "DELETE FROM TMP_VENTAS_VUELTO  " & _
    "WHERE CodUsuario = '" & FixTSQL(LcCodUsuario) & "' " & _
    "AND LAN_DeviceName = '" & FixTSQL(PCName) & "' " & _
    "AND c_Factura = '[STELLAR_REPLACE]' " & _
    "AND c_Numero = '0' " & _
    "AND CodigoCliente = '" & TmpCodigoCliente & "' " & _
    "AND RifCliente = '" & TmpRifCliente & "' "
    
    Ent.BDD.Execute mSQL, Rows
    
End Sub

Public Sub EliminarLineaVueltoTMP(ByVal pID As Variant)
    
    mSQL = "DELETE FROM TMP_VENTAS_VUELTO  " & _
    "WHERE CodUsuario = '" & FixTSQL(LcCodUsuario) & "' " & _
    "AND LAN_DeviceName = '" & FixTSQL(PCName) & "' " & _
    "AND c_Factura = '[STELLAR_REPLACE]' " & _
    "AND c_Numero = '0' " & _
    "AND CodigoCliente = '" & TmpCodigoCliente & "' " & _
    "AND RifCliente = '" & TmpRifCliente & "' " & _
    "AND ID = " & CDec(pID) & " "
    
    Ent.BDD.Execute mSQL, Rows
    
End Sub

Public Sub EliminarLineaVueltoTMP_ConVerifElec(ByVal pID As Variant)
    
    mSQL = "DELETE FROM TMP_VENTAS_VUELTO  " & _
    "WHERE CodUsuario = '" & FixTSQL(LcCodUsuario) & "' " & _
    "AND LAN_DeviceName = '" & FixTSQL(PCName) & "' " & _
    "AND c_Factura = '[STELLAR_REPLACE]' " & _
    "AND c_Numero = '1' " & _
    "AND CodigoCliente = '" & TmpCodigoCliente & "' " & _
    "AND RifCliente = '" & TmpRifCliente & "' " & _
    "AND ID = " & CDec(pID) & " "
    
    Ent.BDD.Execute mSQL, Rows
    
End Sub

Public Sub LlenarDatosGridVueltos(pGrid As MSFlexGrid)
    
    mSQL = "SELECT DPV.*, " & _
    "MON.c_Descripcion AS DesMoneda, DEN.c_Denominacion AS DesDenominacion " & _
    "FROM TMP_VENTAS_VUELTO DPV " & _
    "INNER JOIN MA_MONEDAS MON " & _
    "ON DPV.c_CodMoneda = MON.c_CodMoneda " & _
    "INNER JOIN MA_DENOMINACIONES DEN " & _
    "ON DPV.c_CodMoneda = DEN.c_CodMoneda " & _
    "AND DPV.c_CodDenominacion = DEN.c_CodDenomina " & _
    "WHERE DPV.c_Factura = '[STELLAR_REPLACE]' " & _
    "AND DPV.CodigoCliente = '" & TmpCodigoCliente & "' " & _
    "AND DPV.RifCliente = '" & TmpRifCliente & "' "
    
    Dim mRsTmpVueltos As New ADODB.Recordset
    
    MontoTotalVueltoPagado = CDec(0)
    
    mRsTmpVueltos.Open mSQL, Ent.BDD, adOpenDynamic, adLockOptimistic
    
    pGrid.Rows = 1
    
    If Not mRsTmpVueltos.EOF Then
        
        Do While Not mRsTmpVueltos.EOF
            
            With pGrid
                
                .Rows = .Rows + 1
                
                .Row = .Rows - 1
                
                .Col = 0 ' cod moneda
                .Text = mRsTmpVueltos!c_CodMoneda
                
                .Col = 1 ' des moneda
                .Text = mRsTmpVueltos!DesMoneda
                
                .Col = 2 'factor
                .Text = FormatNumber(mRsTmpVueltos!n_Factor, 2) 'FormatoDecimalesDinamicos(CDec(mRsTmpVueltos!N_FACTOR))
                
                .Col = 3 'cod denominacion
                .Text = mRsTmpVueltos!c_CodDenominacion
                
                .Col = 4 'descripcion de la denominacion
                .Text = mRsTmpVueltos!DesDenominacion
                
                .Col = 5 'EsVerifElecONo
                .Text = mRsTmpVueltos!c_Numero
                
                .Col = 6 'monto en divisas
                .Text = FormatNumber(mRsTmpVueltos!n_Cantidad, 2)
                .CellAlignment = 7
                
                .Col = 7 'monto en moneda predeterminda
                .Text = FormatNumber(mRsTmpVueltos!n_Monto, 2)
                .CellAlignment = 7
                
                .Col = 8 ' idlinea
                .Text = CDec(mRsTmpVueltos!ID)
                
                .Col = 9 'Referencia
                .Text = mRsTmpVueltos!c_Numero_Grid
                
                MontoTotalVueltoPagado = CDec(MontoTotalVueltoPagado) + CDec(mRsTmpVueltos!n_Monto)
                
            End With
            
            mRsTmpVueltos.MoveNext
            
        Loop
        
        mRsTmpVueltos.Close
        
    End If
    
End Sub
