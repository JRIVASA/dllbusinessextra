VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form Frm_InterfazGruposXEntidad 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3255
   ClientLeft      =   15
   ClientTop       =   60
   ClientWidth     =   7200
   ControlBox      =   0   'False
   Icon            =   "Frm_InterfazGruposXEntidad.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3255
   ScaleWidth      =   7200
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame1 
      BackColor       =   &H00E7E8E8&
      BorderStyle     =   0  'None
      Caption         =   "&H00E7E8E8&"
      ForeColor       =   &H00E7E8E8&
      Height          =   1335
      Left            =   240
      TabIndex        =   5
      Top             =   1680
      Width           =   6735
      Begin VB.TextBox nombre 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   390
         Left            =   240
         TabIndex        =   6
         Top             =   600
         Width           =   6150
      End
      Begin VB.Image CmdTeclado 
         Height          =   600
         Left            =   5760
         MouseIcon       =   "Frm_InterfazGruposXEntidad.frx":628A
         MousePointer    =   99  'Custom
         Picture         =   "Frm_InterfazGruposXEntidad.frx":6594
         Stretch         =   -1  'True
         Top             =   0
         Width           =   600
      End
      Begin VB.Label lbl_tipoprestamos 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Descripci�n"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00585A58&
         Height          =   270
         Left            =   240
         TabIndex        =   7
         Top             =   120
         Width           =   1080
      End
   End
   Begin VB.Frame Frame5 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   12120
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Grupos"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   75
         Width           =   4935
      End
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   5115
         TabIndex        =   3
         Top             =   75
         Width           =   1935
      End
   End
   Begin MSComctlLib.ImageList Icono_Apagado 
      Left            =   6000
      Top             =   360
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   3
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_InterfazGruposXEntidad.frx":6640
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_InterfazGruposXEntidad.frx":83D2
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "Frm_InterfazGruposXEntidad.frx":A164
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.PictureBox CoolBar 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   1080
      Left            =   -120
      ScaleHeight     =   1050
      ScaleWidth      =   7410
      TabIndex        =   0
      Top             =   421
      Width           =   7440
      Begin MSComctlLib.Toolbar Toolbar1 
         Height          =   810
         Left            =   240
         TabIndex        =   1
         Top             =   120
         Width           =   6435
         _ExtentX        =   11351
         _ExtentY        =   1429
         ButtonWidth     =   1376
         ButtonHeight    =   1429
         AllowCustomize  =   0   'False
         Style           =   1
         ImageList       =   "Icono_Apagado"
         _Version        =   393216
         BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
            NumButtons      =   4
            BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Grabar"
               Key             =   "Grabar"
               Object.ToolTipText     =   "Grabar esta Ficha"
               ImageIndex      =   1
            EndProperty
            BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Style           =   3
            EndProperty
            BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Salir"
               Key             =   "Salir"
               Object.ToolTipText     =   "Salir del Fichero"
               ImageIndex      =   2
            EndProperty
            BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
               Caption         =   "&Ayuda"
               Key             =   "Ayuda"
               Object.ToolTipText     =   "Ayuda del Sistema V�rtigo"
               ImageIndex      =   3
            EndProperty
         EndProperty
      End
   End
End
Attribute VB_Name = "Frm_InterfazGruposXEntidad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CmdTeclado_Click()
    TecladoAvanzado CampoT
End Sub

Private Sub Form_Activate()
    If PuedeObtenerFoco(nombre) Then nombre.SetFocus
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF4
            GrabarCambio
        Case vbKeyF12, vbKeyEscape
            nombre.Tag = Empty
            Me.Hide
    End Select
End Sub

Private Sub Form_Load()
    
    Me.Toolbar1.Buttons(1).Caption = StellarMensaje(103) 'grabar
    Me.Toolbar1.Buttons(3).Caption = StellarMensaje(54) 'salir
    Me.Toolbar1.Buttons(4).Caption = StellarMensaje(7) 'ayuda
    Me.lbl_Organizacion.Caption = "Item" 'StellarMensaje(161) 'grupo
    Me.lbl_tipoprestamos.Caption = StellarMensaje(143) 'descripcion
    
    nombre.MaxLength = CampoLength("cs_Grupo", "MA_AUX_GRUPO", Ent.BDD, Alfanumerico, 50)
    
End Sub

Private Sub nombre_Click()
    If ModoTouch Then
        CmdTeclado_Click
    End If
End Sub

Private Sub nombre_GotFocus()
    Set CampoT = nombre
    SeleccionarTexto CampoT
End Sub

Private Sub nombre_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF4, vbKeyF12, vbKeyEscape
            Form_KeyDown KeyCode, Shift
        Case vbKeyReturn
            Form_KeyDown vbKeyF4, 0
    End Select
End Sub

Private Sub Toolbar1_ButtonClick(ByVal Button As MSComctlLib.Button)
    Select Case UCase(Button.Key)
        Case "GRABAR"
            Form_KeyDown vbKeyF4, 0
        Case "SALIR"
            Form_KeyDown vbKeyF12, 0
    End Select
End Sub

Private Sub GrabarCambio()
    If Trim(nombre.Text) <> Empty And VerificarNombreGrupo Then
        nombre.Tag = nombre.Text
        Me.Hide
    Else
        'Mensaje True, "El Nombre No es Valido o Ya Existe"
        Mensaje True, StellarMensaje(99)
    End If
End Sub

Private Function VerificarNombreGrupo() As Boolean
    
    Dim mFila As Long
    
    For mFila = 1 To frm_GruposSeleccion.lvGrupos.ListItems.Count
        If Trim(UCase(nombre.Text)) = UCase(Trim(frm_GruposSeleccion.lvGrupos.ListItems(mFila).Text)) Then
            Exit Function
        End If
    Next mFila
    
    VerificarNombreGrupo = True
    
End Function
