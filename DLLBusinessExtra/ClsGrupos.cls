VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsGrupos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Private mvarTipoGrupo                                                           As String

Property Let cTipoGrupo(pValor As String)
    mvarTipoGrupo = pValor
End Property

Property Get cTipoGrupo() As String
    cTipoGrupo = mvarTipoGrupo
End Property

Public Sub CargarComboGrupos(pCn, pCbo As Object, Optional pPermitirAgregar As Boolean = True)
    ClsGrupos.cTipoGrupo = Me.cTipoGrupo
    ClsGrupos.CargarComboGrupos pCn, pCbo, pPermitirAgregar
End Sub

Public Sub AgregarModificarGrupo(pCn, ByRef pCbo As Object)
    ClsGrupos.cTipoGrupo = Me.cTipoGrupo
    ClsGrupos.AgregarModificarGrupo pCn, pCbo
End Sub

Private Sub Class_Initialize()
    Set ClsGrupos = FrmAppLink.GetClassGrupo
End Sub
