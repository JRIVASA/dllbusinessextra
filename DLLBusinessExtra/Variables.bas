Attribute VB_Name = "Variables"
Public FrmAppLink                   As Object
Public ClsGrupos                    As Object

Global CodMoneda                    As String
Public MonedaDoc                    As Object
Public MonedaProd                   As Object
Public MonedaTmp                    As Object

Public InfoMoneda                   As Object
Public InfoDenominacion             As Object
Public InfoRutinas                  As Object

Public pClsGrupos                   As Object

Public Forma                        As Object
Public Titulo                       As String
Public Tabla                        As String
Public CampoT                       As Object
Global Campo_Txt                    As Object
Global Campo_Lbl                    As Object

Public CancelarPromo_Salir          As Boolean
Public PromocionCreada              As Boolean

Public Campanna                     As String
Public TipoPromo                    As Integer
Public EstatusPromo                 As String
Public mCodPromo_Origen             As String
Public mCodPromo_Actual             As String
Public mCodPromo_New                As String

Public RecordsAffected              As Long ' ByRef Output

Public CombinarProductos            As Boolean
Public CantLlevar                   As Double
Public CantPagar                    As Double

Public Find_Concept                 As String
Public Find_Status                  As String
Global LcConsecu                    As String
Global lcLocalidad                  As String

Public Type DTEnv
    BDD                             As Object
    POS                             As Object
    SHAPE_ADM                       As Object
    SHAPE_POS                       As Object
    RsReglasComerciales             As Object
End Type

Public Ent                          As DTEnv

Global gVerificacion                As VerificacionElectronica

Public Type VerificacionElectronica
    Verificacion            As Boolean
    Consorcio               As Integer ' 0 CreditCard, 1 Megasoft, 2 Sitef, 3 Megasoft VNetPos, 4 POSLink
    NivelAnulacion          As Integer
    Pregunta                As Boolean
End Type

Global Std_Decm                     As Integer
Global Std_DecC                     As Integer

Global Producto_Imp                 As String
Global RsEureka                     As New ADODB.Recordset

Global TmpValue                     As Variant

Global gCls                         As ClsBusinessExtra

' Globales para Vuelto Avanzado.

Global ManejaDetalleVuelto          As Boolean
Global PermitirVueltoEfectivo       As Boolean
Global PermitirVueltoMerchant       As Boolean
Global PermitirVueltoCupon          As Boolean
Global PermitirVueltoOtros          As Boolean
Global PermitirVueltoStellarWallet  As Boolean
Global PermitirVueltoVerifElecMontoParcial                  As Boolean
Global PermitirVueltoVerifElecMontoParcial_Nivel            As Integer
Global PermitirEliminarVueltoVerifElec                      As Boolean
Global PermitirEliminarVueltoVerifElec_Nivel                As Integer

Global MontoTotalVueltoPagado                               As Variant 'se agrega para llevar el total de vuelto dado

Public SalirVuelto                  As Boolean

Global gCodMonedaPred               As String
Global gDesMonedaPred               As String
Global gSimMonedaPred               As String
Global gFacMonedaPred               As Double
Global gDecMonedaPred               As Long

Global gCodMonedaAlterna1                                   As String
Global gDescripcionMonedaAlterna1                           As String
Global gFactorMonedaAlterna1                                As Double
Global gSimboloMonedaAlterna1                               As String
Global gDecimalesMonedaAlterna1                             As Integer

Global gCodMonedaAlterna2                                   As String
Global gDescripcionMonedaAlterna2                           As String
Global gFactorMonedaAlterna2                                As Double
Global gSimboloMonedaAlterna2                               As String
Global gDecimalesMonedaAlterna2                             As Integer

Public DenominacionVueltoMerchant   As String

Global Moneda_Sel                   As String
Global Moneda_Sel_Des               As String
Global Denominacion_Sel             As String
Global Denominacion_Sel_Des         As String
Global Denominacion_Real            As Boolean

Global RsDenomina                   As New ADODB.Recordset
Global RxMonedas                    As New ADODB.Recordset
Global RxDenomina                   As New ADODB.Recordset
Global RsMoneda                     As New ADODB.Recordset
Global RsBanco                      As New ADODB.Recordset

Public gVerificacionElectro         As Object

Public VerificacionElectronica_Maneja                               As Boolean
Global VerificacionElectronica_IDClienteAuto                        As Boolean
Global MerchantSUMA_IDClienteAlfaNumerico                           As Boolean
Public MerchantSUMA_TmpVar                                          As String
Public CodigoMonedaTipo             As Dictionary

Global Vuelt                        As Variant ' Decimal
Global VueltRaw                     As Variant ' Decimal

Global Resp                         As Boolean

Global TmpNumeroFactura             As String
Global TmpCodigoCliente             As String
Global TmpNombreCliente             As String
Global TmpRifCliente                As String
Global TmpTelefonoCliente           As String
Global TmpDireccionCliente          As String

Global LcCodUsuario                 As String
Global PCName                       As String
Global PCName_Raw                   As String
Global LocalIP                      As String
