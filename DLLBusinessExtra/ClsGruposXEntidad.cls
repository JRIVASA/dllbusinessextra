VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsGruposXEntidad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mVarGrupo                                                               As String
Private mvarTipoGrupo                                                           As String
Private mvarGrabo                                                               As Boolean

Property Let cGrupo(pValor As String)
    mVarGrupo = pValor
End Property

Property Get cGrupo() As String
    cGrupo = mVarGrupo
End Property

Property Let cTipoGrupo(pValor As String)
    mvarTipoGrupo = pValor
End Property

Property Get cTipoGrupo() As String
    cTipoGrupo = mvarTipoGrupo
End Property

Property Let GraboGrupo(pValor As Boolean)
    mvarGrabo = pValor
End Property

Property Get GraboGrupo() As Boolean
    GraboGrupo = mvarGrabo
End Property

Public Function CargarGrupos(pCn, ByRef pLv As ListView) As Boolean
    
    Dim mRs As New ADODB.Recordset
    Dim mSQl As String, mItm As ListItem
    
    On Error GoTo Errores
    
    pLv.ListItems.Clear
    
    mSQl = "SELECT * FROM MA_AUX_GRUPO WHERE cs_Tipo = '" & Me.cTipoGrupo & "' ORDER BY ID"
    
    mRs.Open mSQl, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Do While Not mRs.EOF
        Set mItm = pLv.ListItems.add(, , mRs!cs_grupo)
        mRs.MoveNext
    Loop
    
    mRs.Close
    
    CargarGrupos = True
    
    Exit Function
    
Errores:
    
    MsgBox Err.Description
    Err.Clear
    
End Function

Public Sub CargarComboGrupos(pCn, pCbo As ComboBox, Optional pPermitirAgregar As Boolean = True)
    
    Dim mRs As New ADODB.Recordset
    Dim mSQl As String
    
    pCbo.Clear
    pCbo.AddItem vbNullString
    
    mSQl = "SELECT DISTINCT cs_Grupo FROM " & _
    "(SELECT CASE cs_Grupo WHEN 'Ninguno' THEN ('a' + cs_Grupo) " & _
    "ELSE ('z' + cs_Grupo) END AS cs_Grupo FROM MA_AUX_GRUPO " & _
    "WHERE cs_Tipo IN ('SYS', '" & cTipoGrupo & "')) TB ORDER BY cs_Grupo"
    
    mRs.Open mSQl, pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    Do While Not mRs.EOF
        pCbo.AddItem Mid(mRs!cs_grupo, 2, Len(mRs!cs_grupo))
        mRs.MoveNext
    Loop
    
    pCbo.AddItem "Ninguno"
    
    If pPermitirAgregar Then pCbo.AddItem StellarMensaje(197) & " " & "Item" '"Nuevo Grupo"
    
    pCbo.ListIndex = 0
    
End Sub

Public Sub BuscarGruposCbo(ByRef pCbo As ComboBox, ByVal pTexto As String)
    
    Dim mItm As Long
    
    For mItm = 0 To pCbo.ListCount - 1
        If (pTexto = "" And Trim(UCase(pCbo.List(mItm))) = "NINGUNO") _
        Or Trim(UCase(pCbo.List(mItm))) = Trim(UCase(pTexto)) Then
            pCbo.ListIndex = mItm
            Exit Sub
        End If
    Next mItm
    
End Sub

Public Sub AgregarModificarGrupo(pCn, ByRef pCbo As ComboBox)
    
    Dim mPosAnterior As Long
    
    mPosAnterior = pCbo.ListIndex
    
    Set frm_GruposSeleccion.mClsGrupos = Me
    
    frm_GruposSeleccion.Show vbModal
    
    If GraboGrupo Then
        CargarComboGrupos pCn, pCbo
        pCbo.ListIndex = 0
    End If
    
End Sub

Public Function GrabarModificarGrupo(pCn, ByVal pTexto As String, _
Optional ByVal pAnterior As String = "") As Boolean
    
    Dim mSQl As String
    
    On Error GoTo Errores
    
    If pAnterior <> Empty Then
        pCn.Execute = "DELETE FROM MA_AUX_GRUPO WHERE cs_Grupo = '" & QuitarComillasSimples(pAnterior) & "' AND cs_Tipo = '" & Me.cTipoGrupo & "' "
    End If
    
    mSQl = "INSERT INTO MA_AUX_GRUPO (cs_Grupo, cs_Tipo) VALUES ('" & QuitarComillasSimples(pTexto) & "', '" & Me.cTipoGrupo & "') "
    
    pCn.Execute mSQl
    
    GrabarModificarGrupo = True
    
Errores:
    
    Err.Clear
    
End Function

Public Sub EliminarGrupos(pCn, pGrupo As String)
    
    Dim mTabla As String, mValor As String
    Dim mSujeto As String, mCampo As String
    mValor = "''"
    
    Select Case Me.cTipoGrupo
        Case "CLI"
            mTabla = "MA_CLIENTES"
            mSujeto = "Clientes"
            mCampo = "c_Grupo"
        Case "UNI"
            mTabla = "MA_UNIDADES"
            mSujeto = "Unidades"
            mCampo = "cs_Grupo"
        Case "PROV"
            mTabla = "MA_PROVEEDORES"
            mSujeto = "Proveedores"
            mCampo = "c_Grupo"
        Case "BAN"
            mTabla = "MA_BANCOS"
            mSujeto = "Bancos"
            mCampo = "c_Grupo"
        Case "IMP"
            mTabla = "MA_PRODUCTOS"
            mSujeto = "Productos"
            mCampo = "n_Impuesto1"
            mValor = "0"
        Case Else
    End Select
    
    If UCase(pGrupo) <> "NINGUNO" Or (mTabla = "MA_PRODUCTOS" And UCase(pGrupo) <> "0") Then
        
        If Not ExisteGrupoSel(pCn, pGrupo, mTabla, mCampo) Then
            'If mTabla <> "MA_PRODUCTOS" Then
                'mResp = MsgBox("Algunos (" & mSujeto & ") estan relacionados con el Grupo & " & pGrupo & ", Desea Eliminar esta Relacion? ", vbYesNo)
                'If mResp = vbNo Then Exit Sub
                'pCn.Execute "Update from " & mTabla & " set " & mcampo & "=" & mValor & "  where " & mcampo & "='" & pGrupo & "' "
            'Else
                'MsgBox "Se Eliminara El Impuesto del Grupo. Para Cambiar los Impuestos debera dirigirse al Modulo Tributario del Sistema ", vbInformation
            'End If
            pCn.Execute "DELETE FROM MA_AUX_GRUPO WHERE cs_Grupo = '" & QuitarComillasSimples(pGrupo) & "' AND cs_Tipo = '" & Me.cTipoGrupo & "' "
        Else
            Mensaje True, "Algunos [" & mSujeto & "] estan relacionados con el Grupo " & pGrupo
        End If
        
    End If
            
End Sub

Private Function ExisteGrupoSel(pCn, pGrupo As String, pTabla As String, pCampo As String) As Boolean
    
    Dim mRs As New ADODB.Recordset
    
    If pTabla = "" Then Exit Function 'pTabla = "MA_AUX_GRUPO"
    
    mRs.Open "SELECT " & pCampo & " FROM " & pTabla & " " & _
    "WHERE " & pCampo & " = '" & pGrupo & "' " & _
    "GROUP BY " & pCampo & " ", _
    pCn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
    ExisteGrupoSel = Not mRs.EOF
    mRs.Close
    
End Function
