VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.ocx"
Begin VB.Form frm_GruposSeleccion 
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   6765
   ClientLeft      =   15
   ClientTop       =   60
   ClientWidth     =   4950
   ControlBox      =   0   'False
   Icon            =   "frm_GruposSeleccion.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6765
   ScaleWidth      =   4950
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmd_modif 
      Caption         =   "&Modificar"
      Height          =   915
      Left            =   2540
      Picture         =   "frm_GruposSeleccion.frx":628A
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   5640
      Width           =   990
   End
   Begin VB.CommandButton cmd_delete 
      Caption         =   "&Eliminar"
      Height          =   915
      Left            =   1350
      Picture         =   "frm_GruposSeleccion.frx":800C
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   5640
      Width           =   990
   End
   Begin VB.CommandButton cmd_add 
      Caption         =   "&Agregar"
      Height          =   915
      Left            =   135
      Picture         =   "frm_GruposSeleccion.frx":9D8E
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   5640
      Width           =   990
   End
   Begin VB.Frame Frame5 
      BackColor       =   &H00AE5B00&
      BorderStyle     =   0  'None
      Height          =   420
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   12120
      Begin VB.Label lbl_website 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "www.mistellar.com"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   5115
         TabIndex        =   4
         Top             =   75
         Width           =   1935
      End
      Begin VB.Label lbl_Organizacion 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "Seleccione un Grupo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FAFAFA&
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   75
         Width           =   4935
      End
   End
   Begin VB.CommandButton cmd_salir 
      Caption         =   "&Salir"
      Height          =   915
      Left            =   3735
      Picture         =   "frm_GruposSeleccion.frx":AA58
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   5640
      Width           =   990
   End
   Begin MSComctlLib.ListView lvGrupos 
      Height          =   4815
      Left            =   135
      TabIndex        =   0
      Top             =   600
      Width           =   4620
      _ExtentX        =   8149
      _ExtentY        =   8493
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   5790296
      BackColor       =   16777215
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Grupo"
         Object.Width           =   7586
      EndProperty
   End
   Begin VB.Menu menu 
      Caption         =   "Opciones"
      Visible         =   0   'False
      Begin VB.Menu agregar 
         Caption         =   "Agregar"
      End
      Begin VB.Menu modificar 
         Caption         =   "Modificar"
      End
      Begin VB.Menu eliminar 
         Caption         =   "Eliminar"
      End
   End
End
Attribute VB_Name = "frm_GruposSeleccion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public mClsGrupos As New cls_grupos
Private Cargando As Boolean

Private Sub Agregar_Click()
    
    Dim mCambioGrupo As String
    
    If VerificarItem(True) Then
        mCambioGrupo = ModificarGrupos
        If mCambioGrupo <> Empty Then
            If mClsGrupos.GrabarModificarGrupo(Ent.BDD, mCambioGrupo) Then
                mClsGrupos.GraboGrupo = True
                Unload Me
            End If
        End If
    End If
    
End Sub

Private Sub cmd_add_Click()
    Agregar_Click
End Sub

Private Sub cmd_delete_Click()
    Eliminar_Click
End Sub

Private Sub cmd_modif_Click()
    Modificar_Click
End Sub

Private Sub cmd_salir_Click()
    Unload Me
End Sub

Private Sub Eliminar_Click()
    If VerificarItem(False) Then
        
        'Resp = MsgBox("�Est� seguro de eliminar el grupo?" & lvGrupos.SelectedItem, vbYesNo)
        'If Resp = vbYes Then
        
        Mensaje False, Replace(StellarMensaje(320), "$(Grupo)", lvGrupos.SelectedItem.Text)
        
        If Retorno Then
            Call mClsGrupos.EliminarGrupos(Ent.BDD, lvGrupos.SelectedItem)
            If Not mClsGrupos.CargarGrupos(Ent.BDD, lvGrupos) Then
                'Call Mensaje(True, "No se pudo cargar los grupos o no existe ninguno.")
                Call Mensaje(True, StellarMensaje(127))
                Unload Me
            End If
        End If
        
        If PuedeObtenerFoco(lvGrupos) Then lvGrupos.SetFocus
        
    End If
End Sub

Private Sub Form_Activate()
    If Cargando Then
        Cargando = False
        If Not mClsGrupos.CargarGrupos(Ent.BDD, lvGrupos) Then
            'Call Mensaje(True, "No se pudo cargar los grupos o no existe ninguno.")
            Call Mensaje(True, StellarMensaje(127))
            Unload Me
        End If
    End If
End Sub

Private Sub Form_Load()
    
    Cargando = True
    
    Me.lbl_Organizacion.Caption = StellarMensaje(321) ' selecciones un grupo
    Me.cmd_add.Caption = StellarMensaje(197) 'agregar
    Me.cmd_delete.Caption = StellarMensaje(208) 'eliminar
    Me.cmd_modif.Caption = StellarMensaje(207) 'modificar
    Me.cmd_salir.Caption = StellarMensaje(107) 'salir
    Me.lvGrupos.ColumnHeaders.Item(1).Text = StellarMensaje(143) 'descripcion
    
End Sub

Private Sub lvGrupos_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyF3
            cmd_add_Click
        Case vbKeyF5
            cmd_modif_Click
        Case vbKeyF6
            cmd_delete_Click
        Case vbKeyF12, vbKeyEscape
            cmd_salir_Click
    End Select
End Sub

Private Sub lvGrupos_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Select Case Button
        Case 2
            PopupMenu menu
    End Select
End Sub

Private Sub Modificar_Click()
    
    Dim Anterior As String
    Dim mCambioGrupo As String
    
    If VerificarItem() Then
        
        Anterior = UCase(lvGrupos.SelectedItem)
        
        mCambioGrupo = ModificarGrupos(lvGrupos.SelectedItem)
        
        If Trim(mCambioGrupo) <> "" And Trim(mCambioGrupo) <> Trim(Anterior) Then
            If mClsGrupos.GrabarModificarGrupo(Ent.BDD, mCambioGrupo, Anterior) Then
                mClsGrupos.GraboGrupo = True
                Unload Me
            End If
        End If
        
    End If
    
End Sub

Private Function VerificarItem(Optional pAgregar As Boolean = False) As Boolean
    
    On Error GoTo Errores
    
    If Not pAgregar Then
        mAux = lvGrupos.SelectedItem
        
        If UCase(mAux) <> "NINGUNO" Then
            VerificarItem = True
        End If
    Else
        VerificarItem = True
    End If
    
    Exit Function
    
Errores:
    
    Err.Clear
    
End Function

Private Function ModificarGrupos( _
Optional ByVal pItem As String = "") As String
    
    Frm_InterfazGrupos.nombre = pItem
    Call SeleccionarTexto(Frm_InterfazGrupos.nombre)
    Frm_InterfazGrupos.Show vbModal
    
    If Trim(Frm_InterfazGrupos.nombre.Tag) <> Empty Then
        ModificarGrupos = UCase(Frm_InterfazGrupos.nombre.Tag)
        Frm_InterfazGrupos.nombre.Tag = Empty
    End If
    
    Unload Frm_InterfazGrupos
    
End Function
