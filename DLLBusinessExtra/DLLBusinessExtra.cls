VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ClsBusinessExtra"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Property Let Entrada(ByVal pValor As Object)
    Set FrmAppLink = pValor
    Set Ent.BDD = FrmAppLink.CnAdm
    Set Ent.POS = FrmAppLink.CnPos
    Set Ent.SHAPE_ADM = FrmAppLink.CnADMShape
    Set Ent.SHAPE_POS = FrmAppLink.CnPOSShape
    Set Ent.RsReglasComerciales = FrmAppLink.RsReglasComerciales
    Set RsEureka = FrmAppLink.GetRsEureka
    Set gCls = Me
    LcCodUsuario = FrmAppLink.GetCodUsuario ' Usuario Logeado
End Property

Public Sub Main()
    
End Sub

Public Sub FichaGestionNCF_RD()
    Ficha_Gestion_NCF_RD.Show vbModal
End Sub

Public Sub SerialesDeTransaccionPOS()
    FrmSerialesTransaccion.Show vbModal
End Sub

Public Sub ConsultarAdministrarCuponStellar()
    FrmConsultarCuponBWLCS.Show vbModal
End Sub

Public Sub VerificarFactura()
    frm_VerificarFactura.Show vbModal
End Sub

Public Function TipoVuelto() As Boolean
    
    PermitirVueltoEfectivo = Val(sGetIni(FrmAppLink.GetSetup, "VUELTO", "PermitirEfectivo", "0")) = 1
    PermitirVueltoMerchant = Val(sGetIni(FrmAppLink.GetSetup, "VUELTO", "PermitirMerchant", "0")) = 1
    PermitirVueltoCupon = Val(sGetIni(FrmAppLink.GetSetup, "VUELTO", "PermitirCupon", "0")) = 1
    PermitirVueltoOtros = Val(sGetIni(FrmAppLink.GetSetup, "VUELTO", "PermitirOtros", "0")) = 1
    PermitirVueltoStellarWallet = Val(sGetIni(FrmAppLink.GetSetup, "VUELTO", "PermitirStellarWallet", "0")) = 1
    
    VerificacionElectronica_IDClienteAuto = CBool(Val(sGetIni(FrmAppLink.GetSetup, _
    "VUELTO", "VerificacionElectronica_IDClienteAuto", "1")))
    
    DenominacionVueltoMerchant = sGetIni(FrmAppLink.GetSetup, _
    "VUELTO", "DenominacionVueltoMerchant", Empty)
    
    MerchantSUMA_IDClienteAlfaNumerico = CBool(Val(sGetIni(FrmAppLink.GetSetup, "POS", _
    "MerchantSUMA_IDClienteAlfaNumerico", "0")))
    
    Set CodigoMonedaTipo = ConvertirCadenaDeAsociacionAvanzado( _
    sGetIni(FrmAppLink.GetSetup, "VUELTO", "CodigoMonedaTipo", Empty), pKeysCaseOption:=2)
    ' Estructura: "Moneda:ISO|Y asi sucesivamente."
    If CodigoMonedaTipo Is Nothing Then Set CodigoMonedaTipo = New Dictionary
    
    PermitirVueltoVerifElecMontoParcial = _
    Val(sGetIni(FrmAppLink.GetSetup, "VUELTO", "PermitirVueltoVerifElecMontoParcial", "0")) = 1
    
    PermitirVueltoVerifElecMontoParcial_Nivel = _
    Val(sGetIni(FrmAppLink.GetSetup, "VUELTO", "PermitirVueltoVerifElecMontoParcial_Nivel", "9"))
    
    PermitirEliminarVueltoVerifElec = _
    Val(sGetIni(FrmAppLink.GetSetup, "VUELTO", "PermitirEliminarVueltoVerifElec", "0")) = 1
    
    PermitirEliminarVueltoVerifElec_Nivel = _
    Val(sGetIni(FrmAppLink.GetSetup, "VUELTO", "PermitirEliminarVueltoVerifElec_Nivel", "9"))
    
    Set f_TipoVuelto = Nothing
    Set f_Vuelto = Nothing
    
    gVerificacion.Consorcio = FrmAppLink.PropVerElecConsorcio
    gVerificacion.NivelAnulacion = FrmAppLink.PropVerElecNivelAnulacion
    gVerificacion.Pregunta = FrmAppLink.PropVerElecPregunta
    gVerificacion.Verificacion = FrmAppLink.PropVerElecVerificacion
    
    Set gVerificacionElectro = FrmAppLink.DllVerificacionElectronica
    
    VerificacionElectronica_Maneja = gVerificacion.Verificacion
    
    TmpNumeroFactura = FrmAppLink.NumeroFactura
    TmpCodigoCliente = FrmAppLink.CodigoCliente
    TmpNombreCliente = FrmAppLink.NombreCliente
    TmpRifCliente = FrmAppLink.RifCliente
    TmpTelefonoCliente = FrmAppLink.TelefonoCliente
    TmpDireccionCliente = FrmAppLink.DireccionCliente
    
    f_TipoVuelto.CodMonedaUlt = FrmAppLink.UltimaMonedaGeneraVuelto
    
    gCodMonedaPred = FrmAppLink.CodMonedaPref
    gDesMonedaPred = FrmAppLink.DesMonedaPref
    gDecMonedaPred = FrmAppLink.DecMonedaPref
    gFacMonedaPred = FrmAppLink.FacMonedaPref
    gSimMonedaPred = FrmAppLink.SimMonedaPref
    
    gCodMonedaAlterna1 = FrmAppLink.CodMonedaAlterna1
    gDescripcionMonedaAlterna1 = FrmAppLink.DescripcionMonedaAlterna1
    gDecimalesMonedaAlterna1 = FrmAppLink.DecimalesMonedaAlterna1
    gFactorMonedaAlterna1 = FrmAppLink.FactorMonedaAlterna1
    gSimboloMonedaAlterna1 = FrmAppLink.SimboloMonedaAlterna1
    
    gCodMonedaAlterna2 = FrmAppLink.CodMonedaAlterna2
    gDescripcionMonedaAlterna2 = FrmAppLink.DescripcionMonedaAlterna2
    gDecimalesMonedaAlterna2 = FrmAppLink.DecimalesMonedaAlterna2
    gFactorMonedaAlterna2 = FrmAppLink.FactorMonedaAlterna2
    gSimboloMonedaAlterna2 = FrmAppLink.SimboloMonedaAlterna2
    
    Set InfoMoneda = FrmAppLink.MonedaTmp
    Set InfoRutinas = FrmAppLink.RutinasTmp
    Set InfoDenominacion = FrmAppLink.DenominacionTmp
    
    LcCodUsuario = FrmAppLink.GetCodUsuario
    
    PCName_Raw = FrmAppLink.GetPCName_Raw
    PCName = FrmAppLink.GetPCName
    LocalIP = FrmAppLink.GetLAN_IP
    Vuelt = FrmAppLink.Vuelt
    VueltRaw = FrmAppLink.VueltRaw
    
    ' A diferencia del POS / FOOD, a pesar de que est� disponible, no usamos VueltRaw en vez de Vuelt,
    ' ya que el business si permite pagar una parte de la factura y el resto dejarlo a credito.
    ' Por lo cual el manejo es un poco distinto, ya que lo que el usuario paga en todo
    ' momento es un monto redondeado a los decimales que maneje la moneda de la factura,
    ' Por lo tanto nunca esta pagando el monto exactamente crudo. Aparte aqui no hay cierre de caja
    ' cuadre en el cual una minima diferencia decimal pudiera afectar.
    ' En resumen aqui usamos para los calculos Vuelt en vez de VueltRaw.
    ' En los otros sistemas si se debe usar VueltRaw.
    
    f_TipoVuelto.cmd_Efectivo.Enabled = PermitirVueltoEfectivo
    f_TipoVuelto.cmd_Efectivo.Visible = PermitirVueltoEfectivo
    f_TipoVuelto.cmd_Merchant.Enabled = PermitirVueltoMerchant
    f_TipoVuelto.cmd_Merchant.Visible = PermitirVueltoMerchant
    f_TipoVuelto.cmd_Cupon.Enabled = PermitirVueltoCupon
    f_TipoVuelto.cmd_Cupon.Visible = PermitirVueltoCupon
    f_TipoVuelto.cmd_Otros.Enabled = PermitirVueltoOtros
    f_TipoVuelto.cmd_Otros.Visible = PermitirVueltoOtros
    f_TipoVuelto.cmd_StellarWallet.Enabled = PermitirVueltoStellarWallet
    f_TipoVuelto.cmd_StellarWallet.Visible = PermitirVueltoStellarWallet
    
    f_TipoVuelto.lbl_MontoVuelto.Caption = FormatNumber(Vuelt, gDecMonedaPred)
    f_TipoVuelto.lbl_MontoVuelto.Tag = CDec(VueltRaw)
    
    If gFactorMonedaAlterna1 > 0 Then
        
        f_TipoVuelto.lbl_MontoVueltoAlt.Tag = CDec(f_TipoVuelto.lbl_MontoVuelto.Caption) / gFactorMonedaAlterna1
        f_TipoVuelto.lbl_MontoVueltoAlt.Caption = FormatNumber(CDec(f_TipoVuelto.lbl_MontoVueltoAlt.Tag), gDecimalesMonedaAlterna1)
        
        f_TipoVuelto.lbl_MontoPagadoAlt.Visible = True
        f_TipoVuelto.lbl_simbpagAlt.Visible = True
        f_TipoVuelto.lbl_MontoVueltoAlt.Visible = True
        f_TipoVuelto.lbl_simbpagAlt.Visible = True
        f_TipoVuelto.lbl_MontoRestanteAlt.Visible = True
        f_TipoVuelto.Lbl_PagadoAlt.Visible = True
        f_TipoVuelto.lbl_VueltoAlt.Visible = True
        f_TipoVuelto.lbl_RetanteAlt.Visible = True
        
    Else
        
        f_TipoVuelto.lbl_MontoVueltoAlt = 0
        f_TipoVuelto.lbl_MontoVueltoAlt.Tag = 0
        
    End If
    
    f_TipoVuelto.Show vbModal
    
    TipoVuelto = Not f_TipoVuelto.Salir
    
End Function

Public Function DesglosarMontoFactura(ByVal pDoc, ByVal pTipo, _
ByVal pLoc, ByVal pCodConcepto, ByVal pMontoInicial As Variant, _
Optional ByVal pSimboloMonedaDoc As String) As Dictionary
    
    FrmDesglosarMonto.Documento = pDoc
    FrmDesglosarMonto.TipoDoc = pTipo
    FrmDesglosarMonto.Localidad = pLoc
    FrmDesglosarMonto.Concepto = pCodConcepto
    FrmDesglosarMonto.MontoInicial = pMontoInicial
    FrmDesglosarMonto.SimboloMonedaDoc = pSimboloMonedaDoc
    
    FrmDesglosarMonto.Show vbModal
    
    If Not FrmDesglosarMonto.Cancelar Then
        Set DesglosarMontoFactura = FrmDesglosarMonto.DatosDesglose
    Else
        Set DesglosarMontoFactura = Nothing
    End If
    
End Function

Public Function Compra_SolicitarDatosLogisticos( _
ByRef pNombreTransportista As String, ByRef pIDTransportista As String, _
ByRef pPlacaVehiculo As String, ByRef pNoControl As String) As Boolean
    
    Compras_DatosLibroLicores.TransportistaNombre = pNombreTransportista
    Compras_DatosLibroLicores.TransportistaID = pIDTransportista
    Compras_DatosLibroLicores.PlacaVehiculo = pPlacaVehiculo
    Compras_DatosLibroLicores.NoControl = pNoControl
    
    Compras_DatosLibroLicores.Show vbModal
    
    If Compras_DatosLibroLicores.Confirmar Then
        
        pNombreTransportista = Compras_DatosLibroLicores.TransportistaNombre
        pIDTransportista = Compras_DatosLibroLicores.TransportistaID
        pPlacaVehiculo = Compras_DatosLibroLicores.PlacaVehiculo
        pNoControl = Compras_DatosLibroLicores.NoControl
        
        Compra_SolicitarDatosLogisticos = True
        
    End If
    
End Function

Public Function SolicitarDatosGuiaLicores( _
ByRef pNombreTransportista As String, ByRef pIDTransportista As String, _
ByRef pTipoVehiculo As String, ByRef pPlacaVehiculo As String, _
ByRef pNoControl As String, ByRef pNoGuia As String, _
Optional ByVal pTipoGuia As Integer = 1) As Boolean
    
    FrmDatosGuiaLicores.TransportistaNombre = pNombreTransportista
    FrmDatosGuiaLicores.TransportistaID = pIDTransportista
    FrmDatosGuiaLicores.TipoVehiculo = pTipoVehiculo
    FrmDatosGuiaLicores.PlacaVehiculo = pPlacaVehiculo
    FrmDatosGuiaLicores.NoControl = pNoControl
    FrmDatosGuiaLicores.NoGuia = pNoGuia
    
    If pTipoGuia = 1 Then
        FrmDatosGuiaLicores.lbl_Organizacion.Caption = "Confirme los Datos de Guia de Licores"
        FrmDatosGuiaLicores.lblTituloFrame.Caption = "Informaci�n Gu�a de Licores"
    ElseIf pTipoGuia = 2 Then
        FrmDatosGuiaLicores.lbl_Organizacion.Caption = "Confirme los Datos de la Orden de Despacho"
        FrmDatosGuiaLicores.lblTituloFrame.Caption = "Informaci�n Orden de Despacho"
    End If
    
    FrmDatosGuiaLicores.Show vbModal
    
    If FrmDatosGuiaLicores.Confirmar Then
        
        pNombreTransportista = FrmDatosGuiaLicores.TransportistaNombre
        pIDTransportista = FrmDatosGuiaLicores.TransportistaID
        pTipoVehiculo = FrmDatosGuiaLicores.TipoVehiculo
        pPlacaVehiculo = FrmDatosGuiaLicores.PlacaVehiculo
        pNoControl = FrmDatosGuiaLicores.NoControl
        pNoGuia = FrmDatosGuiaLicores.NoGuia
        
        SolicitarDatosGuiaLicores = True
        
    End If
    
End Function

Public Sub LLamarListadoFacturaVerificadas()
    Rep_VerificarFacturas.Show vbModal
End Sub

Public Sub LLamarRep_ListadoEntradaySalida()
    Rep_VerEntradaySalida.Show vbModal
End Sub

Public Sub LLamarRep_OfertasyPromociones()
    Rep_OfertasyPromociones.Show vbModal
End Sub

Public Sub LlamarRep_PedidosVsFactura()
    Rep_PedidosVsFactura.Show vbModal
End Sub
